/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9214.3.10  Starts(19 May 2014)           */
/*                                                                                              */
/* ******************************************************************************************** */


--ZD 101347 START

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	MaxiControlRooms int NULL
GO
COMMIT

--Need to update License in Sys_Settings_d
--Added New Tag 'MaxiControlRooms' under Room Tag.
--<MaxiControlRooms>5</MaxiControlRooms>
--<myVRMSiteLicense><IR>1-QSS04</IR><Site><ExpirationDate>1/31/2016</ExpirationDate><MaxOrganizations>10</MaxOrganizations><Cloud>0</Cloud><PublicRoomService>0</PublicRoomService><IsLDAP>1</IsLDAP><ServerActivation><ProcessorIDs><ID>BFEBFBFF0001067A</ID><ID>BFEBFBFF0001067A</ID></ProcessorIDs><MACAddresses><ID>44:87:FC:F5:26:87</ID><ID>44:87:FC:F5:26:87</ID></MACAddresses></ServerActivation></Site><Organizations><Rooms><MaxNonVideoRooms>50</MaxNonVideoRooms><MaxVideoRooms>50</MaxVideoRooms><MaxVMRRooms>20</MaxVMRRooms><MaxiControlRooms>5</MaxiControlRooms><MaxGuestRooms>10</MaxGuestRooms><MaxROHotdesking>10</MaxROHotdesking><MaxVCHotdesking>10</MaxVCHotdesking></Rooms><Hardware><MaxStandardMCUs>20</MaxStandardMCUs><MaxEnhancedMCUs>20</MaxEnhancedMCUs><MaxEndpoints>50</MaxEndpoints><MaxCDRs></MaxCDRs></Hardware><Users><MaxTotalUsers>400</MaxTotalUsers><MaxGuestsPerUser>10</MaxGuestsPerUser><MaxExchangeUsers>100</MaxExchangeUsers><MaxDominoUsers>100</MaxDominoUsers><MaxMobileUsers>5</MaxMobileUsers><MaxPCUsers>5</MaxPCUsers><MaxWebexUsers>5</MaxWebexUsers></Users><Modules><MaxFacilitiesModules>10</MaxFacilitiesModules><MaxCateringModules>10</MaxCateringModules><MaxHousekeepingModules>10</MaxHousekeepingModules><MaxAPIModules>10</MaxAPIModules><MaxBlueJeans>3</MaxBlueJeans><MaxJabber>3</MaxJabber><MaxLync>5</MaxLync><MaxVidtel>5</MaxVidtel><MaxAdvancedReport>2</MaxAdvancedReport></Modules><Languages><MaxSpanish></MaxSpanish><MaxMandarin></MaxMandarin><MaxHindi></MaxHindi><MaxFrench></MaxFrench></Languages></Organizations></myVRMSiteLicense>

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Loc_Room_D ADD
	iControl int NULL
GO
COMMIT

update Loc_Room_D set iControl = 0


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.License_Defaults_D ADD
	MaxiControlRooms int NULL
GO
COMMIT

--Setting the RoomCategory for existing Normal room 
update Loc_Room_D Set RoomCategory = 1 where IsVMR=0 and Extroom =0 and RoomCategory != 4

--Setting the RoomCategory for existing VMR room
update Loc_Room_D Set RoomCategory = 2 where IsVMR=1

--Setting the RoomCategory for existing Guest room
update Loc_Room_D Set RoomCategory = 3 where Extroom=1

--Setting the RoomCategory for existing iControl room
update Loc_Room_D Set RoomCategory = 6 where iControl=1


--ZD 101347 END


--ZD 101357 START

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	isExpressConference smallint NULL
GO
COMMIT


Update Conf_Conference_D set isExpressConference = 0


--ZD 101357 END
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9214.3.10  Ends (20 May 2014)			*/
/*								Features & Bugs for V2.9214.3.11  Starts(19 May 2014)			*/
/*                                                                                              */
/* ******************************************************************************************** */

-- ZD 101228
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	AVWOAlertTime int NULL,
	CatWOAlertTime int NULL,
	FacilityWOAlertTime int NULL
GO
COMMIT

Update Org_Settings_D set AVWOAlertTime =0,CatWOAlertTime=0,FacilityWOAlertTime=0

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9214.3.11  Ends (29 May 2014)			*/
/*								Features & Bugs for V2.9214.3.12  Starts(30 May 2014)			*/
/*                                                                                              */
/* ******************************************************************************************** */
/*********** ZD 101271- Start  *****************/

update Org_Settings_D set EnableSecurityBadge = 0, SecurityBadgeType = 0, SecurityDeskEmailId = ''

/*********** ZD 101271- End*****************/


/*********** ZD 101387- Start  *****************/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	RoomRecordsView int NULL
GO
COMMIT


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	RoomRecordsView int NULL
GO
COMMIT

update Usr_List_D set RoomRecordsView = 20

update Usr_Inactive_D set RoomRecordsView = 20


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Loc_Room_D ADD
       [TopTier] nvarchar(250) NULL,
       [MiddleTier] nvarchar(250) NULL        
GO
COMMIT

update loc_room_d set toptier = t2.TopTierName, middletier= t2.MiddleTierName  from loc_room_d t1 join
      (select l3.Name as TopTierName, l2.Name as MiddleTierName, l3.id as TopId, l2.id as MiddleId from loc_room_d loc, Loc_Tier3_D l3, Loc_Tier2_D l2 where loc.L3Locationid = l3.id and loc.L2Locationid = l2.id
      ) t2
      on t1.L3Locationid = t2.TopId and t1.L2Locationid = t2.MiddleId


/*********** ZD 101387- End*****************/

/*********** ZD 101377- START*****************/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Tmp_List_D ADD
	confsecure smallint NULL
GO
COMMIT


update Tmp_List_D set confsecure=0
/*********** ZD 101377- END*****************/

/*********** ZD 101388 Starts **************/

update Usr_Roles_D
set roleMenuMask='8*240-4*11+7*126+4*15+4*15+5*31+5*31+5*31+2*0+2*3+4*11+8*0+5*0+2*0+8*0+2*0+2*0+2*0+1*0-7*92'
where roleName='General User'

update Usr_Roles_D
set roleMenuMask='8*248-4*15+7*127+4*15+4*15+5*31+5*31+5*31+2*3+2*3+4*11+8*255+5*31+2*3+8*255+2*3+2*3+2*3+1*0-7*93'
where roleName='Organization Administrator 1'

update Usr_Roles_D
set roleMenuMask='8*252-4*15+7*127+4*15+4*15+5*31+5*31+5*31+2*3+2*3+4*15+8*255+5*31+2*3+8*255+2*3+2*3+2*3+1*1-7*127'
where roleName='Site Administrator'

update Usr_Roles_D
set roleMenuMask='8*136-4*0+7*0+4*0+4*0+5*0+5*0+5*0+2*0+2*0+4*0+8*2+5*0+2*0+8*0+2*0+2*3+2*0+1*0-7*92'
where roleName='Catering Administrator'

update Usr_Roles_D
set roleMenuMask='8*136-4*0+7*0+4*0+4*0+5*0+5*0+5*0+2*0+2*0+4*0+8*4+5*0+2*0+8*0+2*3+2*0+2*0+1*0-7*92'
where roleName='Audiovisual Administrator'

update Usr_Roles_D
set roleMenuMask='8*136-4*0+7*0+4*0+4*0+5*0+5*0+5*0+2*0+2*0+4*0+8*1+5*0+2*0+8*0+2*0+2*0+2*3+1*0-7*92'
where roleName='Facility Administrator'

update Usr_Roles_D
set roleMenuMask='8*96-4*1+7*80+4*15+4*0+5*30+5*0+5*0+2*0+2*2+4*0+8*0+5*0+2*0+8*0+2*0+2*0+2*0+1*0-7*92'
where roleName='Express Profile'

update Usr_Roles_D
set roleMenuMask='8*96-4*9+7*80+4*15+4*0+5*30+5*0+5*0+2*0+2*2+4*0+8*0+5*0+2*0+8*0+2*0+2*0+2*0+1*0-7*92'
where roleName='Express Profile Manage'

update Usr_Roles_D
set roleMenuMask='8*112-4*9+7*126+4*15+4*15+5*30+5*30+5*30+2*0+2*2+4*4+8*0+5*0+2*0+8*0+2*0+2*0+2*0+1*0-7*92'
where roleName='Express Profile Advanced'

update Usr_Roles_D
set roleMenuMask='8*208-4*9+7*126+4*9+4*10+5*16+5*16+5*16+2*0+2*0+4*11+8*0+5*0+2*0+8*0+2*0+2*0+2*0+1*0-7*92'
where roleName='View-Only'

update Usr_Roles_D
set roleMenuMask='8*248-4*15+7*127+4*15+4*15+5*31+5*31+5*31+2*3+2*3+4*11+8*255+5*23+2*3+8*189+2*3+2*3+2*3+1*0-7*93'
where roleName='VNOC Operator'

update Usr_Roles_D
set roleMenuMask='8*248-4*15+7*127+4*15+4*15+5*31+5*31+5*31+2*3+2*3+4*15+8*255+5*31+2*3+8*255+2*3+2*3+2*3+1*0-7*93'
where roleName='Organization Administrator 2'

-- IMPORTANT: Change the existing custom role manually

update list
set list.MenuMask = roles.roleMenuMask
from Usr_List_D as list, Usr_Roles_D as roles
where list.roleID = roles.roleID

update Inactive
set Inactive.MenuMask = roles.roleMenuMask
from Usr_Inactive_D as Inactive, Usr_Roles_D as roles
where Inactive.roleID = roles.roleID

/*********** ZD 101388 Ends **************/


--ZD 101379 


delete from Gen_VideoEquipment_S where VEid = 58

update Gen_VideoEquipment_S set Familyid = 
(select MID from Gen_EquipmentManufacturer_s where ManufacturerName = 'Polycom')
where Familyid =(select MID from Gen_EquipmentManufacturer_s where ManufacturerName = 'ViewStation')

delete from Gen_EquipmentManufacturer_s where ManufacturerName = 'ViewStation'

update Gen_EquipmentManufacturer_s set ManufacturerName ='Huawei' where ManufacturerName = 'HUAWEI'

update Gen_VideoEquipment_S set VEname = 'Huawei Room Presence (RP100-46S/55S)', DisplayName = 'Room Presence (RP100-46S/55S)' where VEid = 60

update Gen_VideoEquipment_S set VEname = 'Huawei TP3200 Series Panovision Telepresence' where VEid = 61
update Gen_VideoEquipment_S set VEname = 'Huawei TP3100 Series Immersive Telepresence' where VEid = 62


update Gen_VideoEquipment_S set VEname = 'Polycom RealPresence G300 *', DisplayName = 'RealPresence G300 *' where VEid = 63
update Gen_VideoEquipment_S set VEname = 'Polycom RealPresence G500 *', DisplayName = 'RealPresence G500 *' where VEid = 64
update Gen_VideoEquipment_S set VEname = 'Polycom RealPresence G700 *', DisplayName = 'RealPresence G700 *' where VEid = 65

delete from Gen_VideoEquipment_S where VEid in (41, 42, 69, 27, 4, 39)

update Gen_VideoEquipment_S set VEname = 'Cisco TX1310', DisplayName = 'TX1310' where VEid = 57

Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid, DisplayName) values (76,'CTS 500',2,'CTS 500')
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid, DisplayName) values (77,'CTS 1100',2,'CTS 1100')
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid, DisplayName) values (78,'CTS 1300',2,'CTS 1300')

Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid, DisplayName) values (79, 'Polycom VSX 3000x*', 3, 'VSX 3000x *')
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid, DisplayName) values (80,'Huawei Room Presence (RP200-46S/55S)',8,'Room Presence (RP200-46S/55S)')

update Gen_VideoEquipment_S set VEname = 'Polycom VSX 5000x*', DisplayName = 'VSX 5000x *' where VEid = 3
update Gen_VideoEquipment_S set VEname = 'Polycom VSX 6000x*', DisplayName = 'VSX 6000x *' where VEid = 40

update Ept_List_D set videoequipmentid = 2 where videoequipmentid in (41,42)
update Ept_List_D set videoequipmentid = 25 where videoequipmentid in (69)
update Ept_List_D set videoequipmentid = 76 where videoequipmentid in (27)
update Ept_List_D set videoequipmentid = 79 where videoequipmentid in (4,39)
update Ept_List_D set videoequipmentid = 18 where videoequipmentid in (58)

--ZD 101379 Ends
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9214.3.12  Ends (09 June 2014)		*/
/*				Features & Bugs for V2.9214.3.12  Starts(10 June 2014)		*/
/*                                                                                              */
/* ******************************************************************************************** */

/* ****************** ZD 101217,ZD 101441 Start **************** */

--Insert into Mcu_Vendor_S (id,name,BridgeInterfaceId,videoparticipants,audioparticipants) values (16,'Pexip',10,50,50)


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Gen_SystemLocation_D](
	[UID] [int] IDENTITY(1,1) NOT NULL,
	[MCUId] [int] NULL,
	[SysLocId] [nvarchar](50) NULL,
	[max_audio_calls] [nvarchar](50) NULL,
	[max_hd_calls] [nvarchar](50) NULL,
	[max_sd_calls] [nvarchar](50) NULL,
	[resource_uri] [nvarchar](50) NULL,
 CONSTRAINT [PK_Gen_SystemLocation_D] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	ConfURI nvarchar(MAX) NULL
GO
COMMIT

update Conf_Conference_D set ConfURI = ''

/* ****************** ZD 101217,ZD 101441 End **************** */
/* ****************** ZD 100513 Starts **************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	isOBTP tinyint NULL
GO
COMMIT

Update Conf_Conference_D set isOBTP = 0


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnableWETConference tinyint NULL,
	WebExLaunch tinyint NULL
GO
COMMIT

Update Org_Settings_D set EnableWETConference=0,WebExLaunch=0
/* ****************** ZD 100513 End **************** */
/* ****************** ZD 101244 Starts **************** */
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Loc_Tier3_D ADD
	Secure smallint NULL,
	Securityemail nvarchar(1024) NULL
GO
COMMIT

Update Loc_Tier3_D set Secure =0 , Securityemail=''

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Loc_Room_D ADD
	Secure smallint NULL,
	Securityemail nvarchar(1024) NULL,
	UsersSecurityemail nvarchar(1024) NULL
GO
COMMIT

Update Loc_Room_D set Secure =0 , Securityemail='', UsersSecurityemail =''

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Loc_Tier2_D ADD
	Secure smallint NULL,
	Securityemail nvarchar(1024) NULL
GO
COMMIT

Update Loc_Tier2_D set Secure =0 , Securityemail=''

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Audit_Loc_Room_D ADD
	Secure smallint NULL,
	Securityemail nvarchar(1024) NULL,
	UsersSecurityemail nvarchar(1024) NULL
GO
COMMIT

Update Audit_Loc_Room_D set Secure =0 , Securityemail='' ,UsersSecurityemail=''
/* ****************** ZD 101244 End **************** */

/* ****************** ZD 101476 Starts **************** */

Update Org_Settings_D set EmailDateFormat = 0

/* ****************** ZD 101476 End **************** */

/* ********************** ZD 101445 Starts ********************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	RoomDenialCommt int NULL	
GO
COMMIT

update Org_Settings_D set RoomDenialCommt=0


/* ********************** ZD 101445 End ********************** */


/* ********************** ZD ZD 101602-LDAP License Part Starts ********************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Ldap_ServerConfig_D ADD
	EnableDomain int NULL,
	TenantOption int NULL,
	EnableSSOMode int NULL,
	OrgId int NULL
GO
COMMIT


Update Ldap_ServerConfig_D set EnableDomain = 0, TenantOption = -1 ,EnableSSOMode = 0, OrgId = 11


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Ldap_ServerConfig_D
	DROP CONSTRAINT DF_Ldap_ServerConfig_D_LoginKey
GO
ALTER TABLE dbo.Ldap_ServerConfig_D
	DROP CONSTRAINT DF_Ldap_ServerConfig_D_UID
GO
CREATE TABLE dbo.Tmp_Ldap_ServerConfig_D
	(
	serveraddress nvarchar(512) NULL,
	login nvarchar(256) NULL,
	password nvarchar(256) NULL,
	port int NULL,
	timeout int NULL,
	schedule int NULL,
	SyncTime datetime NULL,
	SearchFilter nvarchar(256) NULL,
	LoginKey nvarchar(50) NOT NULL,
	domainPrefix nvarchar(256) NULL,
	scheduleTime datetime NULL,
	scheduleDays nvarchar(50) NULL,
	UID int NOT NULL IDENTITY (1, 1),
	AuthType int NULL,
	EnableDomain int NULL,
	TenantOption int NULL,
	EnableSSOMode int NULL,
	OrgId int NULL
	)  ON [PRIMARY]
GO
DECLARE @v sql_variant 
SET @v = N'1'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Ldap_ServerConfig_D', N'COLUMN', N'UID'
GO
ALTER TABLE dbo.Tmp_Ldap_ServerConfig_D ADD CONSTRAINT
	DF_Ldap_ServerConfig_D_LoginKey DEFAULT ('on') FOR LoginKey
GO
SET IDENTITY_INSERT dbo.Tmp_Ldap_ServerConfig_D ON
GO
IF EXISTS(SELECT * FROM dbo.Ldap_ServerConfig_D)
	 EXEC('INSERT INTO dbo.Tmp_Ldap_ServerConfig_D (serveraddress, login, password, port, timeout, schedule, SyncTime, SearchFilter, LoginKey, domainPrefix, 

scheduleTime, scheduleDays, UID, AuthType, EnableDomain, TenantOption, EnableSSOMode, OrgId)
		SELECT serveraddress, login, password, port, timeout, schedule, SyncTime, SearchFilter, LoginKey, domainPrefix, scheduleTime, scheduleDays, UID, 

AuthType, EnableDomain, TenantOption, EnableSSOMode, OrgId FROM dbo.Ldap_ServerConfig_D WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Ldap_ServerConfig_D OFF
GO
DROP TABLE dbo.Ldap_ServerConfig_D
GO
EXECUTE sp_rename N'dbo.Tmp_Ldap_ServerConfig_D', N'Ldap_ServerConfig_D', 'OBJECT' 
GO
ALTER TABLE dbo.Ldap_ServerConfig_D ADD CONSTRAINT
	PK_Ldap_ServerConfig_D PRIMARY KEY CLUSTERED 
	(
	UID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
/* ********************** ZD ZD 101602-LDAP License Part End ********************** */




/* ZD 101562 ability to hide templates in booking forms START*/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnableTemplateBooking int NULL
GO
COMMIT

update Org_Settings_D set EnableTemplateBooking = 1

/* ZD 101562 END*/

/* ZD 101575 Enhancement - Add Polycom V700 as a selectable endpoint START*/

Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid, DisplayName) values (81,'Polycom V700',3,'V700')

/* ZD 101575 END*/

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9214.3.14  Ends (10 Junly 2014)			*/
/*								Features & Bugs for V2.9314.3.0  Starts(10 Junly 2014)			*/
/*                                                                                              */
/* ******************************************************************************************** */



/********************** ZD 101602 - LDAP PART ***************************/


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Ldap_Groups_D](
	[UID] [int] IDENTITY(1,1) NOT NULL,
	[OrgId] [int] NULL,
	[GroupName] [nvarchar](MAX) NULL,
	[RoleId] [int] NULL,
	[Deleted] [int] NULL,
	[ModifiedUserId] [int] NULL,
	[LastModifiedDate] [datetime] NULL
 CONSTRAINT [PK_Ldap_Groups_D] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

-- IMPORTANT: Please change the existing custom role's menu mask manually.

update Usr_Roles_D
set roleMenuMask='8*240-4*11+7*126+4*15+4*15+5*31+5*31+5*31+2*0+2*3+4*11+8*0+5*0+2*0+9*0+2*0+2*0+2*0+1*0-7*92'
where roleName='General User'

update Usr_Roles_D
set roleMenuMask='8*248-4*15+7*127+4*15+4*15+5*31+5*31+5*31+2*3+2*3+4*11+8*255+5*31+2*3+9*511+2*3+2*3+2*3+1*0-7*93'
where roleName='Organization Administrator 1'

update Usr_Roles_D
set roleMenuMask='8*252-4*15+7*127+4*15+4*15+5*31+5*31+5*31+2*3+2*3+4*15+8*255+5*31+2*3+9*511+2*3+2*3+2*3+1*1-7*127'
where roleName='Site Administrator'


update Usr_Roles_D
set roleMenuMask='8*136-4*0+7*0+4*0+4*0+5*0+5*0+5*0+2*0+2*0+4*0+8*2+5*0+2*0+9*0+2*0+2*3+2*0+1*0-7*92'
where roleName='Catering Administrator'

update Usr_Roles_D
set roleMenuMask='8*136-4*0+7*0+4*0+4*0+5*0+5*0+5*0+2*0+2*0+4*0+8*4+5*0+2*0+9*0+2*3+2*0+2*0+1*0-7*92'
where roleName='Audiovisual Administrator'

update Usr_Roles_D
set roleMenuMask='8*136-4*0+7*0+4*0+4*0+5*0+5*0+5*0+2*0+2*0+4*0+8*1+5*0+2*0+9*0+2*0+2*0+2*3+1*0-7*92'
where roleName='Facility Administrator'

update Usr_Roles_D
set roleMenuMask='8*96-4*1+7*80+4*15+4*0+5*30+5*0+5*0+2*0+2*2+4*0+8*0+5*0+2*0+9*0+2*0+2*0+2*0+1*0-7*92'
where roleName='Express Profile'

update Usr_Roles_D
set roleMenuMask='8*96-4*9+7*80+4*15+4*0+5*30+5*0+5*0+2*0+2*2+4*0+8*0+5*0+2*0+9*0+2*0+2*0+2*0+1*0-7*92'
where roleName='Express Profile Manage'

update Usr_Roles_D
set roleMenuMask='8*112-4*9+7*126+4*15+4*15+5*30+5*30+5*30+2*0+2*2+4*4+8*0+5*0+2*0+9*0+2*0+2*0+2*0+1*0-7*92'
where roleName='Express Profile Advanced'

update Usr_Roles_D
set roleMenuMask='8*208-4*9+7*126+4*9+4*10+5*16+5*16+5*16+2*0+2*0+4*11+8*0+5*0+2*0+9*0+2*0+2*0+2*0+1*0-7*92'
where roleName='View-Only'

update Usr_Roles_D
set roleMenuMask='8*248-4*15+7*127+4*15+4*15+5*31+5*31+5*31+2*3+2*3+4*11+8*255+5*23+2*3+9*381+2*3+2*3+2*3+1*0-7*93'
where roleName='VNOC Operator'

update Usr_Roles_D
set roleMenuMask='8*248-4*15+7*127+4*15+4*15+5*31+5*31+5*31+2*3+2*3+4*15+8*255+5*31+2*3+9*511+2*3+2*3+2*3+1*0-7*93'
where roleName='Organization Administrator 2'



update list
set list.MenuMask = roles.roleMenuMask
from Usr_List_D as list, Usr_Roles_D as roles
where list.roleID = roles.roleID

update Inactive
set Inactive.MenuMask = roles.roleMenuMask
from Usr_Inactive_D as Inactive, Usr_Roles_D as roles
where Inactive.roleID = roles.roleID



/********************** ZD 101602 - LDAP PART ***************************/


/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9314.3.0   Ends (16 Junly 2014)			*/
/*								Features & Bugs for V2.9314.3.1  Starts(17 Junly 2014)			*/
/*                                                                                              */
/* ******************************************************************************************** */


/********************** ZD 101522 - START ***************************/


INSERT [dbo].[MCU_Params_D] ( [BridgeName], [BridgeTypeid],[ConfLockUnLock], [confMessage], [confAudioTx], [confAudioRx],[confVideoTx], [confVideoRx], [confLayout], 
[confCamera], [confPacketloss], [confRecord], [partyBandwidth], [partySetfocus],
[partyMessage], [partyAudioTx], [partyAudioRx], [partyVideoTx], [partyVideoRx], [partyLayout], [partyCamera], [partyPacketloss], [partyImagestream], [partyLockUnLock], 
[partyRecord], [partyLecturemode],[confmuteallexcept],[confunmuteall],[partyLeader],[LoginURL],[PassPhrase],[activeSpeaker],[snapshotSupport] )
VALUES ( N'Pexip', 16, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0,0,0,'','',0,0)

ALTER TABLE Gen_SystemLocation_D DROP COLUMN max_audio_calls
ALTER TABLE Gen_SystemLocation_D DROP COLUMN max_hd_calls
ALTER TABLE Gen_SystemLocation_D DROP COLUMN max_sd_calls
ALTER TABLE Gen_SystemLocation_D DROP COLUMN resource_uri

ALTER TABLE Gen_SystemLocation_D ADD Name nvarchar(MAX)


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Mcu_List_D ADD
	SysLocationId int NULL
GO
COMMIT


Update Mcu_List_D set SysLocationId = 0


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Bridge_D ADD
	SystemLocation int NULL
GO
COMMIT


Update Conf_Bridge_D set SystemLocation = 0



/********************** ZD 101522 - END ***************************/

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9314.3.2   Ends (28 Junly 2014)			*/
/*								Features & Bugs for V2.9314.3.3  Starts(29 Junly 2014)			*/
/*                                                                                              */
/* ******************************************************************************************** */

/********************** ZD 101363 - START ***************************/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Ept_List_D ADD
	SSHSupport smallint NULL	
GO
COMMIT

Update Ept_List_D set SSHSupport =0



BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Audit_Ept_List_D ADD
	[SSHSupport] smallint NULL	
GO
COMMIT


Update Audit_Ept_List_D set [SSHSupport] =0


update Gen_VideoEquipment_S set VEname = 'CTS 500*' , DisplayName = 'CTS 500 *' where VEid = 76

/********************** ZD 101363 - END ***************************/

/********************** ZD 101764 - START ***************************/

Declare  @userid as int, @orgid as int
DECLARE getStrCursor CURSOR for

select userid from usr_list_d where admin=3 

OPEN getStrCursor

FETCH NEXT FROM getStrCursor INTO  @userid
WHILE @@FETCH_STATUS = 0
BEGIN

insert into Usr_Dept_D (departmentid,userid)
select departmentId,@userid from Dept_List_D where departmentId not in 
(select departmentId from Usr_Dept_D where userid = @userid)


FETCH NEXT FROM getStrCursor INTO @userid
END
CLOSE getStrCursor
DEALLOCATE getStrCursor

/********************** ZD 101764 - END ***************************/



/********************** ZD 101708 - START ***************************/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.BatchRpt_Settings_D ADD
	Operator nvarchar(5) NULL,
	ConfTitle nvarchar(250) NULL,
	HostID int NULL,
	RequestorID int NULL,
	VNOCID int NULL,
	CallURI nvarchar(50) NULL,
	Timezone int NULL,
	MCUID int NULL,
	EndpointID int NULL,
	RoomIDs nvarchar(250) NULL
GO
COMMIT


/********************** ZD 101708 - END ***************************/

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9314.3.3  Ends (4 Aug 2014)			*/
/*								Features & Bugs for V2.9314.3.4  Starts(5 Aug 2014)			*/
/*                                                                                              */
/* ******************************************************************************************** */
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9314.3.4  Ends (5 Aug 2014)			*/
/*								Features & Bugs for V2.9314.3.5  Starts(6 Aug 2014)			*/
/*                                                                                              */
/* ******************************************************************************************** */


/********************** ZD 101736 - START ***************************/


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Loc_Room_D ADD
	Domain nvarchar(100) NULL,
	ExchangeURL nvarchar(100) NULL
GO
COMMIT


Update Loc_Room_D set Domain = '',ExchangeURL=''


/********************** ZD 101736 - End ***************************/

/*								Features & Bugs for V2.9314.3.6  START(8th Aug 2014)			*/
/*                                                                                              */
/* ******************************************************************************************** */
/********************** ZD 101755,101753 Starts ***************************/

 
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnableSetupTimeDisplay tinyint NULL,
	EnableTeardownTimeDisplay tinyint NULL
GO
COMMIT

Update Org_Settings_D set EnableSetupTimeDisplay =0 ,EnableTeardownTimeDisplay =0

/********************** ZD 101755,101753 - End ***************************/

/********************** ZD 101837 Start ***************************/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Sys_Settings_D ADD
	ConfRecurLimit int NULL
GO
ALTER TABLE dbo.Sys_Settings_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

update Sys_Settings_D set ConfRecurLimit = 250 



/********************** ZD 101837 End ***************************/

/********************** ZD 101757 Starts ***************************/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnableActMsgDelivery int NULL
GO
COMMIT

Update Org_Settings_D set EnableActMsgDelivery =  0

/********************** ZD 101757 End ***************************/

/* ****************** ZD 101603 Start **************** */

Insert into Mcu_Vendor_S (id,name,BridgeInterfaceId,videoparticipants,audioparticipants) values (16,'Pexip',10,50,50)

/* ****************** ZD 101603 End **************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9314.3.6  Ends (18 Aug 2014)			*/
/*								Features & Bugs for V2.9314.3.7  Starts(19 Aug 2014)			*/
/*                                                                                              */
/* ******************************************************************************************** */
/* ****************** ZD 101808 Starts **************** */

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Loc_Sync_D](
	[RoomID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[endpointid] [int] NULL,
	[orgId] [int] NOT NULL,
	[endPointName] [nvarchar](256) NULL,
	[endPointCategory] [int] NULL,
	[identifierValue] [int] NULL
) ON [PRIMARY]

GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Ept_Sync_D](
	[uId] [int] IDENTITY(1,1) NOT NULL,
	[endpointId] [int] NOT NULL,
	[name] [nvarchar](256) NULL,
	[protocol] [smallint] NULL,
	[addresstype] [smallint] NULL,
	[address] [nvarchar](512) NULL,
	[profileId] [int] NOT NULL,
	[profileName] [nvarchar](256) NULL,
	[isDefault] [int] NOT NULL,
	[videoequipmentid] [int] NULL,
	[orgId] [int] NULL,
	[Lastmodifieddate] [datetime2](7) NOT NULL,
	[identifierValue] [int] NULL,
	[mcuID] [int] NULL,
	[ManufacturerID] [int] NULL,
 CONSTRAINT [pk_myConstraint] PRIMARY KEY CLUSTERED 
(
	[endpointId] ASC,
	[profileId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Ept_List_D ADD
	CreateCategory int NULL,
	identifierValue int NULL
GO
ALTER TABLE dbo.Ept_List_D ADD CONSTRAINT
	DF_Ept_List_D_CreateCategory DEFAULT 1 FOR CreateCategory
GO
ALTER TABLE dbo.Ept_List_D ADD CONSTRAINT
	DF_Ept_List_D_identifierValue DEFAULT 0 FOR identifierValue
GO
COMMIT

Update Ept_List_D set CreateCategory = 1, identifierValue =0

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Loc_Room_D ADD
	CreateType int NULL
GO
ALTER TABLE dbo.Loc_Room_D ADD CONSTRAINT
	DF_Loc_Room_D_CreateType DEFAULT 1 FOR CreateType
GO
COMMIT

Update Loc_Room_D set CreateType = 1

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnableRPRMRoomSync int NULL,
	RoomSyncAuto int NULL,
	RoomSyncPollTime int NULL,
	LastUpdatedRoomSync datetime2(7) NULL
GO
COMMIT

Update Org_Settings_D set EnableRPRMRoomSync =0,RoomSyncAuto=0,RoomSyncPollTime=0,LastUpdatedRoomSync = GETUTCDATE()


/* ****************** ZD 101808 End **************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9314.3.7  Ends (21 Aug 2014)			*/
/*								Features & Bugs for V2.9314.3.8  Starts(21 Aug 2014)			*/
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9314.3.8  Ends (22 Aug 2014)			*/
/*								Features & Bugs for V2.9314.3.9  Starts(22 Aug 2014)			*/
/*                                                                                              */
/* ******************************************************************************************** */


--ZD 101884
Update Mcu_List_D set SoftwareVer = '8.0.x' where BridgeTypeId = 13

--ZD 101865
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
       UserOrgin smallint NULL        
GO
COMMIT

update Usr_List_D set UserOrgin=1 

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
       UserOrgin smallint NULL        
GO
COMMIT

update Usr_Inactive_D set UserOrgin=1


/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9314.3.9  Ends (28 Aug 2014)		    	*/
/*								Features & Bugs for V2.9314.3.10  Starts(28 Aug 2014)			*/
/*                                                                                              */
/* ******************************************************************************************** */

--ZD 101846

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Sys_Settings_D ADD
	ReqUsrAcc int NULL
GO
ALTER TABLE dbo.Sys_Settings_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

update Sys_Settings_D set ReqUsrAcc = 0, viewpublicconf= 2

/* ****************** ZD 101446 Starts - 01 Sep 2014 **************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	ParticipantCode nvarchar(50) NULL
GO
COMMIT

Update Usr_List_D set ParticipantCode=''

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	ParticipantCode nvarchar(50) NULL
GO
COMMIT

Update Usr_Inactive_D set ParticipantCode =''



BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnablePartyCode smallint NULL
GO
COMMIT


Update Org_Settings_D set EnablePartyCode =0

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Audit_Usr_List_D ADD
	ParticipantCode nvarchar(50) NULL
GO
COMMIT

Update Audit_Usr_List_D set ParticipantCode =''


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_User_D ADD
	ParticipantCode nvarchar(50) NULL
GO
COMMIT

Update Conf_User_D set ParticipantCode =''


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Audit_Conf_User_D ADD
	ParticipantCode nvarchar(50) NULL
GO
COMMIT

Update Audit_Conf_User_D set ParticipantCode =''

/* ****************** ZD 101446 Ends - 01 Sep 2014**************** */


/* ****************** ZD 101869 starts - 04 Sep 2014**************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	DefCodianLO tinyint NULL
GO
COMMIT

Update Org_Settings_D set DefCodianLO = 1
Update Org_Settings_D set DefPolycomMGCLO = 1 where DefPolycomMGCLO = 0
Update Org_Settings_D set DefPolycomRMXLO = 1 where DefPolycomRMXLO = 0


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_AdvAVParams_D ADD
	Familylayout smallint NULL
GO
COMMIT


Update Conf_AdvAVParams_D set Familylayout = 0


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE Conf_Bridge_D ADD
	VideoLayoutId int NULL,
	FamilyLayout int NULL
GO
COMMIT

update a
set a.videoLayoutID = b.videoLayoutID, a.FamilyLayout = 0
from Conf_Bridge_D as a, Conf_AdvAVParams_D as b
where a.ConfID = b.ConfID and a.InstanceID = b.InstanceID


--Update query for ChainPosition in MCU table Starts

Declare  @bridgeId as int, @orgid as int, @chainPosition as int
set @chainPosition = 0
DECLARE getStrCursor CURSOR for
select bridgeId from mcu_list_D where deleted = 0

OPEN getStrCursor
FETCH NEXT FROM getStrCursor INTO  @bridgeId
WHILE @@FETCH_STATUS = 0
BEGIN

set @chainPosition += 1

update Mcu_List_D set chainposition = @chainPosition where bridgeID = @bridgeId and ChainPosition = 0

FETCH NEXT FROM getStrCursor INTO @bridgeId
END
CLOSE getStrCursor
DEALLOCATE getStrCursor

--Update query for ChainPosition in MCU table End

/* ****************** ZD 101869 Ends - 04 Sep 2014**************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9314.3.10  Ends (04 Sep 2014)		  	*/
/*								Features & Bugs for V2.9314.3.11  Starts(04 Sep 2014)			*/
/*                                                                                              */
/* ******************************************************************************************** */

--ZD 101978

Delete from Email_Queue_D where [To] = '' or [To] is null


update User_Lobby_D set iconid = 19 where iconid = 45  and label = 'Express Conference'

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9314.3.11  Ends (09 Sep 2014)		  	*/
/*								Features & Bugs for V2.9314.3.12  Starts(09 Sep 2014)			*/
/*                                                                                              */
/* ******************************************************************************************** */


--ZD 101970
Update Usr_List_D set PrivateVMR = '' where PrivateVMR is null
Update Usr_Inactive_D set PrivateVMR = '' where PrivateVMR is null

/* ****************** ZD 102004 Starts - 11 Sep 2014**************** */
update Org_Settings_D set EnableVidtel = 0
Delete from Usr_PCDetails_D where PCId = 4
update Conf_Conference_D set isPCconference =0 , pcVendorId = 0 where pcVendorId = 4
--Need to update License in Sys_Settings_d
--Removed Existing Tag 'MaxVidtel' under Modules Tag.
--<myVRMSiteLicense><IR>1-QSS04</IR><Site><ExpirationDate>1/31/2016</ExpirationDate><MaxOrganizations>10</MaxOrganizations><Cloud>0</Cloud><PublicRoomService>0</PublicRoomService><IsLDAP>1</IsLDAP><ServerActivation><ProcessorIDs><ID>BFEBFBFF0001067A</ID><ID>BFEBFBFF0001067A</ID></ProcessorIDs><MACAddresses><ID>44:87:FC:F5:26:87</ID><ID>44:87:FC:F5:26:87</ID></MACAddresses></ServerActivation></Site><Organizations><Rooms><MaxNonVideoRooms>50</MaxNonVideoRooms><MaxVideoRooms>50</MaxVideoRooms><MaxVMRRooms>20</MaxVMRRooms><MaxiControlRooms>5</MaxiControlRooms><MaxGuestRooms>10</MaxGuestRooms><MaxROHotdesking>10</MaxROHotdesking><MaxVCHotdesking>10</MaxVCHotdesking><MaxFleetMgt>20</MaxFleetMgt></Rooms><Hardware><MaxStandardMCUs>20</MaxStandardMCUs><MaxEnhancedMCUs>20</MaxEnhancedMCUs><MaxEndpoints>50</MaxEndpoints><MaxCDRs></MaxCDRs></Hardware><Users><MaxTotalUsers>400</MaxTotalUsers><MaxGuestsPerUser>10</MaxGuestsPerUser><MaxExchangeUsers>100</MaxExchangeUsers><MaxDominoUsers>100</MaxDominoUsers><MaxMobileUsers>5</MaxMobileUsers><MaxPCUsers>5</MaxPCUsers><MaxWebexUsers>5</MaxWebexUsers></Users><Modules><MaxFacilitiesModules>10</MaxFacilitiesModules><MaxCateringModules>10</MaxCateringModules><MaxHousekeepingModules>10</MaxHousekeepingModules><MaxAPIModules>10</MaxAPIModules><MaxBlueJeans>3</MaxBlueJeans><MaxJabber>3</MaxJabber><MaxLync>5</MaxLync><MaxAdvancedReport>2</MaxAdvancedReport></Modules><Languages><MaxSpanish></MaxSpanish><MaxMandarin></MaxMandarin><MaxHindi></MaxHindi><MaxFrench></MaxFrench></Languages></Organizations></myVRMSiteLicense>
/* ****************** ZD 102004 Ends - 11 Sep 2014**************** */
/* ****************** ZD 101931 start - 11 Sep 2014**************** */
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	ShowVideoLayout tinyint NULL
GO
COMMIT

update Org_Settings_D set ShowVideoLayout = 1
/* ****************** ZD 101931 start - 11 Sep 2014**************** */
--ZD 101931
update Org_Settings_D set ShowVideoLayout = 0


//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End //ZD 100886
using System;
using System.Data;
using System.Xml;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for Tier1
/// </summary>
/// 
namespace ns_DataImport
{
    public class Endpoint
    {
        int externalDatabaseType;
        DataTable masterDT;
        string configPath;
        ns_Logger.Logger log;
        myVRMNet.NETFunctions obj;
        System.Web.UI.WebControls.DropDownList lstVideoEquipment;
        System.Web.UI.WebControls.DropDownList lstConnType; //ZD 100456
        System.Web.UI.WebControls.DropDownList lstAddressType; //ZD 100456
        System.Web.UI.WebControls.DropDownList lstLineRate;
        System.Web.UI.WebControls.DropDownList lstManufacturer;//ZD 103055
        MyVRMNet.LoginManagement obj1; //ZD 100456
        Hashtable lstBridgeaddress = null;
        

        public Endpoint(int external, DataTable masterDataTable, string config)
        {
            externalDatabaseType = external;
            masterDT = masterDataTable;
            configPath = config;
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
            obj1 = new MyVRMNet.LoginManagement();//ZD 100456
        }

        public bool Process(ref int cnt, ref Boolean alluserimport, ref DataTable dterror)
        {
            DataRow dr;
            //DataRow drNext;//FB 2519
            String outXML = "";

            String inXML = "";
            string eptnme = "";//FB 2362
            try
            {
                //ZD 100456
                XmlDocument failedlist = new XmlDocument();
                XmlNodeList nodelist2;
                XmlNode newnode = null;
                DataRow errow = null;
               
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID>";
                inXML += "</login>";
                outXML = obj.CallMyVRMServer("GetBridgeList", inXML, configPath);
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//bridgeInfo/bridges/bridge");
                if (nodes.Count > 0)
                {
                    for (int j = 0; j < nodes.Count; j++)
                    {
                        if (lstBridgeaddress == null)
                            lstBridgeaddress = new Hashtable();

                        if (!lstBridgeaddress.Contains(nodes[j]["address"].InnerText.ToString().Trim()))
                            lstBridgeaddress.Add(nodes[j]["address"].InnerText.ToString().Trim(), nodes[j]["ID"].InnerText.ToString());
                    }

                }


                lstVideoEquipment = new DropDownList();
                lstVideoEquipment.ID = "DIVideoEquip"; //ZD 104091
                lstVideoEquipment.DataValueField = "VideoEquipmentID";
                lstVideoEquipment.DataTextField = "EquipmentDisplayName";//ZD 103055
                obj.BindVideoEquipment(lstVideoEquipment);

                //ZD 103055 - Start
                lstManufacturer = new DropDownList();
                lstManufacturer.ID = "lstManufacturer"; //ZD 104901
                lstManufacturer.DataValueField = "ManufacturerID";
                lstManufacturer.DataTextField = "ManufacturerName";
                obj.BindManufacturer(lstManufacturer);
                //ZD 103055 - End

                //ZD 100456
                lstLineRate = new DropDownList();
                lstLineRate.ID = "lstLineRate";
                lstLineRate.DataValueField = "LineRateID";
                lstLineRate.DataTextField = "LineRateName";
                obj.BindLineRate(lstLineRate);

                lstConnType = new DropDownList();
                lstConnType.ID = "lstConnType";
                lstConnType.DataValueField = "Name";
                lstConnType.DataTextField = "ID";
                obj.BindDialingOptions(lstConnType);

                lstAddressType = new DropDownList();
                lstAddressType.ID = "lstAddressType";
                lstAddressType.DataValueField = "Name";
                lstAddressType.DataTextField = "ID";
                obj.BindAddressType(lstAddressType);

                for (int j = 0; j <= masterDT.Rows.Count - 1; j++)//FB 2519
                {
                    dr = masterDT.Rows[j];
                    errow = dterror.NewRow(); //ZD 100456
                    int errorusernum = 0;
                    errorusernum = j + 2;
                    //drNext = masterDT.Rows[j + 1];//FB 2519
                    //ZD 103896
                    if (dr["id"].ToString().ToLower().IndexOf(obj.GetTranslatedText("note").ToLower()) >= 0)
                        break;

                    if (dr[obj.GetTranslatedText("name")].ToString().Trim() == "" || dr[obj.GetTranslatedText("Address")].ToString().Trim() == ""
                        || dr[obj.GetTranslatedText("Address Type")].ToString().Trim() == "")
                    {
                        if (dr[obj.GetTranslatedText("name")].ToString().Trim() == "")
                        {
                            errow["Row No"] = errorusernum.ToString();
                            errow["Reason"] = obj.GetTranslatedText("Endpoint doesn't have name");
                            alluserimport = false;
                            dterror.Rows.Add(errow);
                            continue;
                        }
                        if (dr[obj.GetTranslatedText("Address")].ToString().Trim() == "")
                        {
                            errow["Row No"] = errorusernum.ToString();
                            errow["Reason"] = obj.GetTranslatedText("Endpoint doesn't have address");
                            alluserimport = false;
                            dterror.Rows.Add(errow);
                            continue;
                        }
                        if (dr[obj.GetTranslatedText("Address Type")].ToString().Trim() == "")
                        {
                            errow["Row No"] = errorusernum.ToString();
                            errow["Reason"] = obj.GetTranslatedText("Endpoint doesn't have address type");
                            alluserimport = false;
                            dterror.Rows.Add(errow);
                            continue;
                        }
                    }

                    if (dr[obj.GetTranslatedText("Name")].ToString().Trim() != "")//FB 2362
                    {
                        String str = "";

                        if (dr[obj.GetTranslatedText("Name")].ToString().IndexOf('&') >= 0)
                            str = "";

                        //ZD 100456

                        if (dr[obj.GetTranslatedText("Name")].ToString().Length > 20)
                        {
                            errow["Row No"] = errorusernum.ToString();
                            errow["Reason"] = obj.GetTranslatedText("Endpoint name exceeds 20 character.");
                            alluserimport = false;
                            dterror.Rows.Add(errow);
                            continue;
                        }

                        String eptID = "";
                        String proID = "";
                        if (dr["id"].ToString() != "")
                        {
                            eptID = dr["id"].ToString();
                            obj1.simpleDecrypt(ref eptID);
                        }
                        if (dr["pid"].ToString() != "")
                        {
                            proID = dr["pid"].ToString();
                            obj1.simpleDecrypt(ref proID);
                        }

                        String patternIP = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}\:?(\d{4})?$";
                        Regex check = new Regex(patternIP);
                        String patternISDN = @"^[0-9]+$";
                        Regex checkISDN = new Regex(patternISDN);
                        if (dr[obj.GetTranslatedText("Address Type")].ToString().Trim().ToLower() == "isdn phone number")
                        {
                            if (!checkISDN.IsMatch(dr[obj.GetTranslatedText("Address")].ToString(), 0))
                            {
                                errow["Row No"] = errorusernum.ToString();
                                errow["Reason"] = obj.GetTranslatedText("Invalid ISDN Address") + " : " + dr["Address"].ToString();
                                alluserimport = false;
                                dterror.Rows.Add(errow);
                                continue;
                            }
                        }
                        else if (dr[obj.GetTranslatedText("Address Type")].ToString().Trim().ToLower() == "ip address")
                        {
                            if (!check.IsMatch(dr[obj.GetTranslatedText("Address")].ToString(), 0))
                            {
                                errow["Row No"] = errorusernum.ToString();
                                errow["Reason"] = obj.GetTranslatedText("Invalid IP Address") + " : " + dr["Address"].ToString();
                                alluserimport = false;
                                dterror.Rows.Add(errow);
                                continue;
                            }
                        }
                        //ZD 104091 Start
                        string equipmentID = "24";
                        string manufactureID = "3";//ZD 103055

                        try
                        {
                            manufactureID = lstManufacturer.Items.FindByText(dr[obj.GetTranslatedText("Manufacturer")].ToString()).Value;
                        }
                        catch (Exception e)
                        {
                            manufactureID = "3";
                        }

                        string strEquipmentID = "";
                        try
                        {
                            if (dr[obj.GetTranslatedText("Model")].ToString().Trim() != "")
                            {
                                strEquipmentID = lstVideoEquipment.Items.FindByText(dr[obj.GetTranslatedText("Model")].ToString().ToLower()).Value;

                                string[] strEquArr = strEquipmentID.Split('|');
                                equipmentID = strEquArr[0];

                                if (manufactureID != strEquArr[1])
                                {
                                    errow["Row No"] = errorusernum.ToString();
                                    errow["Reason"] = obj.GetTranslatedText("This Model is not under the selected Manufacturer.");
                                    alluserimport = false;
                                    dterror.Rows.Add(errow);
                                    continue;
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            equipmentID = "24";
                        }

                        string profype = "0";
                        if (dr[obj.GetTranslatedText("Profile Type")].ToString().Trim() != "")
                            profype = dr[obj.GetTranslatedText("Profile Type")].ToString().Trim();

                        if (obj.GetTranslatedText(profype).ToLower() == "both")
                            profype = "0";
                        else if (obj.GetTranslatedText(profype).ToLower() == "mcu")
                            profype = "1";
                        else if (obj.GetTranslatedText(profype).ToLower() == "point-to-point")
                            profype = "2";
                        
                        if ((profype == "0" || profype == "2") && dr[obj.GetTranslatedText("Address Type")].ToString().Trim().ToLower() != "ip address")
                        {
                            if (!check.IsMatch(dr[obj.GetTranslatedText("Address")].ToString(), 0))
                            {
                                errow["Row No"] = errorusernum.ToString();
                                errow["Reason"] = obj.GetTranslatedText("Only an IP address is permitted for this profile type") + " : " + dr["Address"].ToString();
                                alluserimport = false;
                                dterror.Rows.Add(errow);
                                continue;
                            }
                        }                        
                        //ZD 104091 End

                        String EndpointInXML = Create_EndpointInXML(dr, eptID, proID, manufactureID, equipmentID, profype); //ZD 104091
                        log.Trace("<br>" + EndpointInXML);
                        outXML = obj.CallMyVRMServer("SetEndpoint", EndpointInXML, configPath);
                        log.Trace(outXML);
                        if (outXML.IndexOf("<error>") >= 0)
                        {

                            log.Trace(obj.ShowErrorMessage(outXML));

                            //ZD 100456
                            alluserimport = false;
                            failedlist.LoadXml(outXML);
                            newnode = failedlist.SelectSingleNode("//error/message");
                            nodelist2 = failedlist.SelectNodes("//error/message");
                            if (nodelist2.Count > 0)
                            {
                                errow["Row No"] = errorusernum.ToString();
                                errow["Reason"] = newnode.InnerXml;
                                dterror.Rows.Add(errow);
                            }
                        }
                        else
                        {
                            log.Trace("Success");
                            cnt++;
                        }
                    }
                }

            }
            catch (Exception)
            {
                
                
            }
            
            return true;
        }
        //FB 2519 start
        protected string EndpointPassword(string PW)
        {
            string Encrypted = "";
            XmlDocument docs = null;
            try
            {
                string inxmls = "<System><Cipher>" + PW + "</Cipher></System>";

                string outXML = obj.CallMyVRMServer("GetEncrpytedText", inxmls, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    docs = new XmlDocument();
                    docs.LoadXml(outXML);
                    XmlNode nde = docs.SelectSingleNode("System/Cipher");
                    if (nde != null)
                        Encrypted = nde.InnerXml;
                }
                else
                {
                    obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
            }
            return Encrypted;
        }
        //FB 2519 End
        private String Create_EndpointInXML(DataRow dr, String eptID, String proID, string manufactureID, string equipmentID, string profype)//ZD 100456//ZD 104091
        {
            string bridgeID = "-1";
            
            try
            {
                //ZD 100456
                String editfrom = "D";
                if (eptID == "")
                {
                    eptID = "new";
                    editfrom = "ND";
                }
                if (proID == "") proID = "new";

                TextBox txtapiTemp = null;
                String inXML = "<SetEndpoint>";
                inXML += obj.OrgXMLElement();
                inXML += "      <EndpointID>" + eptID + "</EndpointID>"; //ZD 100456
                inXML += "      <EditFrom>" + editfrom + "</EditFrom>"; //ZD 100456
                inXML += "      <EndpointName>" + dr[obj.GetTranslatedText("Name")].ToString().Replace("&", " and ") + "</EndpointName>";
                inXML += "      <EntityType></EntityType>";
                inXML += "      <UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID>";
                inXML += "      <userID>new</userID>";
                inXML += "      <Profiles>";
                inXML += "      <Profile>";
                inXML += "      <ProfileID>" + proID + "</ProfileID>"; //ZD 100456
                if(eptID == "new")
                    inXML += "      <ProfileName>" + dr[obj.GetTranslatedText("Name")].ToString() + "</ProfileName>";
                else
                    inXML += "      <ProfileName></ProfileName>";
                inXML += "      <Deleted>0</Deleted>";//FB 2362
                inXML += " <ProfileType>" + profype + "</ProfileType>"; //ZD 104091
                if ((eptID != "new" && proID == "1") || (eptID == "new" && proID == "new")) //ZD 100456
                    inXML += "      <Default>1</Default>";
                else
                    inXML += "      <Default>0</Default>";

                inXML += "      <EncryptionPreferred>0</EncryptionPreferred>";
                //ZD 100456
                
                for (Int32 a = 0; a < lstAddressType.Items.Count; a++)
                {
                    if (lstAddressType.Items[a].Value.ToLower() == dr[obj.GetTranslatedText("Address Type")].ToString().ToLower().Trim())
                    {
                        lstAddressType.SelectedValue = lstAddressType.Items[a].Value;
                        break;
                    }
                }

                //if (dr["Address Type"].ToString().Trim() != "")
                //    lstAddressType.SelectedValue = dr["Address Type"].ToString().ToLower().Trim();
                inXML += "          <AddressType>" + lstAddressType.SelectedItem.Text + "</AddressType>";
                inXML += "          <Password>" + EndpointPassword(dr[obj.GetTranslatedText("Password")].ToString())+"</Password>";//FB 2519
                inXML += "          <Address>" + dr[obj.GetTranslatedText("Address")].ToString() + "</Address>";
                inXML += "          <URL></URL>";
                String isoutside = "0";
                if (dr[obj.GetTranslatedText("Located Outside The Network")].ToString().ToLower().Trim() == obj.GetTranslatedText("yes").ToLower())
                    isoutside = "1";
                inXML += "          <IsOutside>" + isoutside +"</IsOutside>";
                //ZD 100456
                //String dialOpt = "";
                //dialOpt = dr["Preferred Dialing Option"].ToString().Trim();
                //if (dialOpt == "")
                //    dialOpt = "2";

                if (dr[obj.GetTranslatedText("Preferred Dialing Option")].ToString().Trim() != "")
                {
                    for (Int32 a = 0; a < lstConnType.Items.Count; a++)
                    {
                        if (lstConnType.Items[a].Value.ToLower() == dr[obj.GetTranslatedText("Preferred Dialing Option")].ToString().ToLower().Trim())
                        {
                            lstConnType.SelectedValue = lstConnType.Items[a].Value;
                            break;
                        }
                    }
                }
                else
                    lstConnType.SelectedItem.Text = "2"; //ZD 104091


                if (lstAddressType.SelectedValue == "-1")
                    lstAddressType.SelectedValue = "2";

                inXML += "          <ConnectionType>" + lstConnType.SelectedItem.Text + "</ConnectionType>";//FB 2519 //ZD 100456

                //ZD 104091               
                inXML += "<Manufacturer>" + manufactureID + "</Manufacturer>";

               
                //for (int j = 0; j < lstVideoEquipment.Items.Count ; j++)
                //{
                //    if (lstVideoEquipment.Items[j].Text != "") //ZD 103055
                //    {
                //        if (lstVideoEquipment.Items[j].Text.ToUpper().Contains(dr[obj.GetTranslatedText("Model")].ToString().ToUpper())
                //            || dr[obj.GetTranslatedText("Model")].ToString().ToUpper().Contains(lstVideoEquipment.Items[j].Text.ToUpper()))
                //            equipmentID = lstVideoEquipment.Items[j].Value;
                //    }
                //}

                if (equipmentID == "")
                    equipmentID = "1";

                inXML += "          <VideoEquipment>"+ equipmentID +"</VideoEquipment>";

                //ZD 103055 - Start
                //for (int m = 0; m < lstManufacturer.Items.Count; m++)
                //{
                //    if (lstManufacturer.Items[m].Text.ToUpper().Contains(dr[obj.GetTranslatedText("Model")].ToString().ToUpper())
                //        || dr[obj.GetTranslatedText("Model")].ToString().ToUpper().Contains(lstManufacturer.Items[m].Text.ToUpper()))
                //    {
                //        manufactureID = lstManufacturer.Items[m].Value;
                //    }
                //}
               
                //ZD 103055 - End

                //String bandwith = dr["Preferred Bandwidth"].ToString().Trim().ToLower().Replace("mbps", "").Replace("kbps", "");
                lstLineRate.SelectedValue = "-1";
                for (int b = 0; b < lstLineRate.Items.Count; b++)
                {
                    if (lstLineRate.Items[b].Text.ToLower() == dr[obj.GetTranslatedText("Preferred Bandwidth")].ToString().ToLower().Trim())
                    {
                        lstLineRate.SelectedValue = lstLineRate.Items[b].Value;
                        break;
                    }
                }
                
                //lstLineRate.SelectedItem.Text = bandwith;
                if (lstLineRate.SelectedValue == "-1")
                    lstLineRate.SelectedValue = "768";

                inXML += "          <LineRate>" + lstLineRate.SelectedValue + "</LineRate>"; //ZD 100456
                inXML += "          <DefaultProtocol>1</DefaultProtocol>";

                if (lstBridgeaddress != null)
                    if (lstBridgeaddress.Contains(dr[obj.GetTranslatedText("MCU Assignment")].ToString().Trim()))
                        bridgeID = lstBridgeaddress[dr[obj.GetTranslatedText("MCU Assignment")].ToString().Trim()].ToString();

                inXML += "          <Bridge>" + bridgeID + "</Bridge>";
                inXML += "          <MCUAddress>" + dr[obj.GetTranslatedText("MCU Assignment")].ToString() + "</MCUAddress>";
                
                inXML += "          <MCUAddressType>1</MCUAddressType>";
                inXML += "          <TelnetAPI>0</TelnetAPI>";
                inXML += "          <SSHSupport>0</SSHSupport>";//ZD 101363
                inXML += "          <ExchangeID></ExchangeID>";
                inXML += "          <IsCalendarInvite>0</IsCalendarInvite>";
                inXML += "              <ApiPortno></ApiPortno>";
				//FB 2519 start
                inXML += "          <isTelePresence>0</isTelePresence>"; 
                inXML += "         <Secured>0</Secured>";
                inXML += "         <NetworkURL></NetworkURL>";
                inXML += "         <Securedport></Securedport>";
				//FB 2519 End
                inXML += "        </Profile>";
                inXML += "      </Profiles>";
                inXML += "<EM7EndpointStatus>2</EM7EndpointStatus>"; //FB 2519 ZD 100825
                inXML += "<Eptcurrentstatus>0</Eptcurrentstatus>"; //FB 2519
                inXML += "    </SetEndpoint>";
                return inXML;
            }

            catch (Exception ex)
            {
                return "";
            }
        }
    }
}
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ManageDataImport" EnableEventValidation="false"  Debug="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" /> 
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"     Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly ="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2.Export, Version=10.2.11.0, Culture=neutral,PublicKeyToken=b88d1754d700e49a" 
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%--Basic validation function--%>
<script type="text/javascript">

    var servertoday = new Date();

    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
        else
            document.getElementById("dataLoadingDIV").innerHTML = "";
    }

    function OpenDataImport() {
        DataLoading(1);
        window.location = 'DataImport.aspx?t=1';
    }

    function fnCancel() {
        DataLoading(1);
        window.location.replace('organisationsettings.aspx');
    }

    function setHiddenValue() {
        document.getElementById('hdnValue').value = '1'; 
        return true;
    }
     
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head> 
    <title>Report</title>    
    <script type="text/javascript" src="inc/functions.js"></script>
    <style type="text/css">   
    LABEL 
    {
        FONT-SIZE: 8pt;
        VERTICAL-ALIGN: top;
        COLOR: black;
        FONT-FAMILY: Arial, Helvetica;
        TEXT-DECORATION: none;
    }
   
    </style>
</head>
<body>
    <form id="frmReport" runat="server">   
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <input type="hidden" id="hdnValue" runat="server" />
    <table width="100%">
        <tr>
            <td colspan="2">
                <h3>
                      <asp:Label ID="lblHeading" runat="server"></asp:Label>
                </h3>
            </td>
        </tr>
        <tr> <td colspan="2"> <div id="dataLoadingDIV" align="center"></div></td></tr> 
        <tr>
            <td colspan="2" >
                <table width="100%">
                    <tr>
                        <td align="center">
                            <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError" ></asp:Label><br />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="100%" border="1" style="border-color:Gray;border-style:solid;" cellpadding="0" cellspacing="0">
        <tr>     
            <td valign="top">
                <table width="100%" border="0" cellpadding="0" cellspacing="5">
                    <tr>
                        <td valign="top" colspan="2"><%--ZD 102029--%>
                            <b class="blackblodtext"><asp:Literal ID="Literal4" Text='<%$ Resources:WebResources, Select%>' runat='server' /></b>
                            
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <dx:ASPxComboBox ID="lstDataImportType" runat="server" SelectedIndex="0" Width="85%"
                                CssClass="altSelectFormat" ClientInstanceName="lstDataImportType">
                                <Items>
                                    <dx:ListEditItem Text="<%$ Resources:WebResources, DefaultLicense_Users%>" Value="1" />
                                    <dx:ListEditItem Text="<%$ Resources:WebResources, ConferenceList_lblRooms%>" Value="2" />
                                    <dx:ListEditItem Text="<%$ Resources:WebResources, ConferenceSetup_Endpoint%>" Value="3" />
                                    <dx:ListEditItem Text="MCU" Value="4" />
                                 </Items>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="right" colspan="2">
                            <br />
                            <button id="btnView" runat="server" class="altMedium0BlueButtonFormat" onserverclick="btnOk_Click"
                            onclick="setHiddenValue();"><asp:Literal Text='<%$ Resources:WebResources, AddNewEndpoint_btnViewMCU%>' runat='server' /></button>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="right" colspan="2"><%--ZD 102029--%>
                            <br />
                            <hr />
                            <br />
                            <button id="btnImport" runat="server" class="altMedium0BlueButtonFormat"  onclick="javascript:OpenDataImport();return false;"><asp:Literal ID="Literal2" Text='<%$ Resources:WebResources, OrganisationSettings_btnCloudImport%>' runat='server' /></button>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="right" colspan="2">
                            <br />
                            <button id="btnCancel" runat="server" class="altMedium0BlueButtonFormat"  onclick="javascript:fnCancel();return false;"><asp:Literal ID="Literal3" Text='<%$ Resources:WebResources, Cancel%>' runat='server' /></button>
                        </td>
                    </tr>
                </table>
            </td>       
            <td valign="top" >             
                <table width="100%" align="center" border="0">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="btnExcel" ValidationGroup="group" src="image/excel.gif" runat="server" 
                            AlternateText="Export to Excel" Style="vertical-align: middle;" OnClick="ExportExcel" ToolTip="<%$ Resources:WebResources, ExporttoExcel%>" />
                            &nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr valign="top" runat="server" id="trDetails">
                        <td align="center">
                            <div id="MainDiv" style="overflow-y:auto;overflow-x:auto; word-break:break-all;HEIGHT: 440px;"  >
                            <dx:ASPxGridView ID="MainGrid" ClientInstanceName="MainGrid" runat="server" Width="100%" EnableCallBacks="false" AllowSort="true"
                             CssFilePath="../en/App_Themes/Plastic Blue/{0}/styles.css" KeyFieldName="RequestID"
                             OnDataBound="MainGrid_DataBound" >                                
                                <Styles CssFilePath="../en/App_Themes/Plastic Blue/{0}/styles.css"  CssPostfix="PlasticBlue">
                                    <Header ImageSpacing="9px" SortingImageSpacing="9px" ForeColor="White" Font-Size="9pt" HorizontalAlign="Center"></Header>
                                    <Cell Font-Size="9pt"></Cell>                                 
                                </Styles>
                                <Images ImageFolder="../en/App_Themes/Plastic Blue/{0}/">
                                    <CollapsedButton Height="10px" Url="../en/App_Themes/Plastic Blue/GridView/gvCollapsedButton.png" Width="9px"  AlternateText="Collapse" /> 
                                    <ExpandedButton Height="9px" Url="../en/App_Themes/Plastic Blue/GridView/gvExpandedButton.png" Width="9px"  AlternateText="Expand"/> 
                                    <HeaderFilter Height="11px" Url="../en/App_Themes/Plastic Blue/GridView/gvHeaderFilter.png" Width="11px" AlternateText="HeaderFilter" /> 
                                    <HeaderActiveFilter Height="11px" Url="../en/App_Themes/Plastic Blue/GridView/gvHeaderFilterActive.png" Width="11px"  AlternateText="HeaderActivefilter" /> 
                                    <HeaderSortDown Height="11px" Url="../en/App_Themes/Plastic Blue/GridView/gvHeaderSortDown.png" Width="11px"  AlternateText="HeaderSortDown"/>
                                    <HeaderSortUp Height="11px" Url="../en/App_Themes/Plastic Blue/GridView/gvHeaderSortUp.png" Width="11px"  AlternateText="HeaderSortUp"/>
                                    <FilterRowButton Height="13px" Width="13px" />
                                    <CustomizationWindowClose Height="14px" Width="14px" />
                                    <PopupEditFormWindowClose Height="14px" Width="14px" />
                                    <FilterBuilderClose Height="14px" Width="14px" />
                                </Images>  
                                <SettingsText EmptyDataRow="<%$ Resources:WebResources, NoData%>" GroupPanel="<%$ Resources:WebResources, GridViewGroupMsg%>"/>
                                <Settings ShowFilterRow="True"  ShowHeaderFilterButton="True" ShowHeaderFilterBlankItems="False" />
                                <SettingsPager ShowDefaultImages="False" Mode="ShowPager" AlwaysShowPager="true" Position="Top">
                                    <AllButton Text="<%$ Resources:WebResources, All%>"></AllButton>
                                    <NextPageButton Text="<%$ Resources:WebResources, Next%>"></NextPageButton>
                                    <PrevPageButton Text="<%$ Resources:WebResources, Prev%>"></PrevPageButton>
                                </SettingsPager>                              
                            </dx:ASPxGridView>
                            </div>
                            <dx:ASPxGridViewExporter OnRenderBrick="exporter_RenderBrick" ID="gridExport" runat="server" GridViewID="MainGrid"></dx:ASPxGridViewExporter>
                         </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
<%--code added for Soft Edge button--%>

<script type="text/javascript">
    var mainDiv = document.getElementById("MainDiv");
    if(mainDiv)
    {  //Difference 180
    
        if (window.screen.width <= 1024)
            mainDiv.style.width = "845px";
        else
            mainDiv.style.width = "1184px";        
    }


    document.onkeydown = function(evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };
         
</script>


<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->

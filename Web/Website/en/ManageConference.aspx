<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>

<%@ Page Language="C#" Inherits="ns_MyVRM.ManageConference" Buffer="true" EnableEventValidation="false"
    ValidateRequest="false" %>

<%--ZD 100170--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%--FB 2779--%>
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- FB 2050 -->
<style type="text/css">
    #Menu22 div
    {
        width: 123px; /* FB 2050 */
        height: 35px;
        z-index: 0;
    }
    #Menu22 td
    {
        vertical-align: top; /* FB 2050 */
    }
</style>
<script src="script/CallMonitorJquery/jquery.1.4.2.js" type="text/javascript"></script>
<script src="../i18n/i18Utils.js" type="text/javascript"></script>
<script runat="server">

    protected void Menu22_MenuItemClick(object sender, MenuEventArgs e) // FB 2050

    {
      
        int index = Int32.Parse(e.Item.Value);
        MultiView1.ActiveViewIndex = index;
        if (index == 1)
        {
            if (ImagesPath.Text.Equals(""))
                GetVideoLayouts();
            LoadEndpoints();
        }
        if (index == 2)
            CheckResourceAvailability();
            
    }
</script>
<script language="javascript">


    if (document.getElementById("Menu22") != null) // FB 2050
    {
        document.getElementById("Menu22").innerHTML = document.getElementById("Menu22").innerHTML.replace("javascript:", "javascript:if (DataLoading(1)) "); // FB 2050
    }
                    

</script>
<% 
    //Response.Write(Request.QueryString["t"]);
if (Request.QueryString["t"] != null)
if (!Request.QueryString["t"].ToString().Equals("hf"))
   { %>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--ZD 101388 Commented--%>
<%--<!-- ZD 100288 Starts -->
    <% if (Session["isExpressUser"].ToString() == "1"){%>
    <!-- #INCLUDE FILE="inc/maintopNETExp.aspx" -->
    <%}else{%>
    <!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
    <%}%>
    <!-- ZD 100288 Ends -->   --%>
<% }
   else
   {
      // Response.Write(Request.QueryString["t"]);
%>
<!-- #INCLUDE FILE="inc/maintop4.aspx" -->
<%} %>
<% 
    //Response.Write(Request.QueryString["t"]);
if (Request.QueryString["hf"] != null)
if (Request.QueryString["hf"].ToString().Equals("1"))
   { %>
<!-- #INCLUDE FILE="inc/maintop4.aspx" -->
<%} %>
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css">
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css">
<script type="text/javascript">    // FB 2790
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815

    document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
</script>
<link rel="alternate" type="application/atom+xml" title="your feed title here" href="http://www.buy.com/rss/feed.asp?loc=273&grp=4">
<script type="text/javascript" src="script/mytree.js"></script>
<script type="text/javascript" src="script/mousepos.js"></script>
<script type="text/javascript" src="script/DisplayDIV.js"></script>
<script type="text/javascript" src="../<%=Session["language"] %>/inc/functions.js"></script>
<script type="text/javascript" src="script/Workorder.js"></script>
<%--ZD 100221 starts--%>
<script language="javascript" type="text/javascript">

    function fnshow(obj) {
        var curleft = 0;
        var curtop = 0;
        if (obj.offsetParent) {
            do {
                curleft += obj.offsetLeft;
                curtop += obj.offsetTop;
            } while (obj = obj.offsetParent);
        }
        var webexdetails = document.getElementById("WebExDialog");
        webexdetails.style.display = 'block';
        webexdetails.style.position = "absolute";
        webexdetails.style.left = curleft - 720 + "px";
        webexdetails.style.top = curtop - 120 + "px";


    }
    function fnHide() {
        var webexdetails = document.getElementById("WebExDialog");
        webexdetails.style.display = 'none';
    }
</script>
<script language="javascript" type="text/javascript">
//ZD 100604 start
var img = new Image();
img.src = "../en/image/wait1.gif";
//ZD 100604 End
//ZD 100429
function DataLoading(val) {
    if (val == "1")
        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
    else
        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
}


function ViewBridgeDetails(bid)
{
    url = "BridgeDetailsViewOnly.aspx?hf=1&bid=" + bid;
    window.open(url, "BrdigeDetails", "width=900,height=800,resizable=yes,scrollbars=yes,status=no");
    return false;
}
    function ShowImage(obj)
    {
        //alert(obj.src);
        document.getElementById("myPic").src = obj.src;
        getMouseXY();
        //alert(document.body.scrollHeight);
        document.getElementById("divPic").style.position = 'absolute';
        document.getElementById("divPic").style.left = mousedownX + 20;
        document.getElementById("divPic").style.top = mousedownY;
        document.getElementById("divPic").style.display="";
        //alert(obj.style.height + " : " + obj.style.width);
    }

    function HideImage()
    {
        document.getElementById("divPic").style.display="none";
    }

function ViewDetails(id, confid)
{
    ViewWorkorderDetails(id, confid);
}
function ViewDetails(id, confid, tpe)
{
    ViewWorkorderDetails(id, confid, tpe);
}
var servertoday = new Date();

function displayDetails(str)
{
    var obj = document.getElementById(str);
    if (obj!= null)
    	display_prompt('image/pen.gif', 'Details', obj.outerHTML);
//    alert(obj.innerHTML);
}

function hideDetails()
{
    document.getElementById("prompt").style.display = "none";
}

function getCustomRecur(cid)
{
	if (cid != "") {
		if (ifrmPreloading != null)
			ifrmPreloading.window.location.href = "dispatcher/conferencedispatcher.asp?cmd=GetInstances&mode=21&frm=preload&cid=" + cid;
	}
}
function getRecur(cid)
{
//alert("in getrecur" + cid);
	if (cid != "") {
		if (ifrmPreloading != null)
			ifrmPreloading.window.location.href = "dispatcher/conferencedispatcher.asp?cmd=GetInstances&mode=22&frm=preload&cid=" + cid;
	}
}
function setConfInCalendar(f, instInfo)
{

    var confid = document.getElementById("<%=lblConfID.ClientID%>").value;
    if (confid == "")
        confid = document.getElementById("<%=lblConfUniqueID.ClientID%>").innerHTML;
	//url = "saveconfincal.asp?f=" + f + "&ii=" + confid;
	//alert(f);
	//Code Changed for FB 1410 - Start
//	url = "SetSessionOutXml.aspx?tp=saveconfincal.asp&ii=" + instInfo + "&f=" + f;
	url = "GetSessionOutXml.aspx?ii=" + instInfo + "&f=" + f;
	//Code Changed for FB 1410 - End
//	alert(url);
	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=600,height=350,top=0,left=0,resizable=no,scrollbars=no,status=no");
		winrtc.focus();
	} else {	// has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=600,height=350,top=0,left=0,resizable=no,scrollbars=no,status=no");
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=600,height=350,top=0,left=0,resizable=no,scrollbars=no,status=no");
	        winrtc.focus();
		}
	}
}

  function saveToOutlookCalendar (iscustomrecur, isrecr, f, instInfo) 
  {
	// outlook do not support custom recur. Maybe LN can, but current can not test.
	var confid = document.getElementById("<%=lblConfID.ClientID%>").value;
	//alert(confid);
	instanceInfo = "";
	//instInfo = document.getElementById("Recur").value;
	//alert(f + " : " + instInfo);
	if (parseInt(iscustomrecur,10)) { // ZD 101722
		getCustomRecur(confid);
	} else {
		//if (parseInt(isrecr))
		//	getRecur(confid);
		//else
			setConfInCalendar(f, confid);
	}
  }

  function Trim(x) // ZD 101326
  {
    return x.replace(/^\s+|\s+$/gm,'');
  }

  function getCookie(cname) // ZD 101326
  {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) 
      {
      var c = Trim(ca[i]);
      if (c.indexOf(name)==0) return c.substring(name.length,c.length);
      }
    return "";
  }

  function fnInvokeExportRefresh() // ZD 101326
  {
    var cook = getCookie("exportCookie");
    //alert(cook);
    if(cook == "1")
    {
        document.cookie = "exportCookie=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/";
        window.location.href = window.location.href;
    }
    setTimeout("fnInvokeExportRefresh()",100);
  }
  
  function pdfReport() //fogbugz case 38 and 386
    {
        document.getElementById('topDiv').style.position = 'fixed'; // ZD 101326
        document.getElementById('topDiv').style.zIndex = '10000';
        document.getElementById('topDiv').style.display = 'block';
        fnInvokeExportRefresh();

        var loc = document.location.href; 
        loc = loc.substring(0,loc.indexOf("ManageConference.aspx") - 0); //FB 1830 //ZD 101294 
         
        var htmlString = document.getElementById("tblMain").innerHTML;
        DataLoading(1);//ZD 101294
        //remove Actions table from PDF
        var toBeRemoved = document.getElementById("tblActions");
        var path = '<%=Session["OrgCSSPath"]%>';
        path = path.replace("../","");
        var imagepath = '<%=Session["OrgBanner1600Path"]%>'; //FB 1830        
        if (toBeRemoved != null)
            htmlString = htmlString.replace(toBeRemoved.innerHTML, "");
        //Remove Expand Collapse checkbox
        toBeRemoved = document.getElementById("tdExpandCollapse");
        
        if (toBeRemoved != null)
            htmlString = htmlString.replace(toBeRemoved.parentNode.innerHTML, "");
        //replace all doube " with single '        
        htmlString = htmlString.replace(new RegExp("\"","g"), "'");
        loc = "http://localhost" + loc.replace(loc.substring(0, loc.indexOf("/", 8)), "");   //fogbugz case 386 Saima
        //remove all image source relative paths with the absolute path
        //insert the banner on top of page and style sheet on top.
        //Code Changed for RSS
        //FB 1830
         if("<%=Session["ImageURL"]%>" == "")
         {
           htmlString = htmlString.replace(new RegExp("image/","g"), loc + "image/");
           //FB 2102 Start
           htmlString = "<html><link rel='stylesheet' type='text/css' href='" + loc + path +"' /><body><center><table><tr><td></td></tr></table>" + htmlString + "</center></body></html>";
          //htmlString = "<html><link rel='stylesheet' type='text/css' href='" + loc + path +"' /><body><center><table><tr><td><img src='" + loc + imagepath + "' width='100%' height='72'></td></tr></table>" + htmlString + "</center></body></html>";
          //FB 2102 End
        }
        else
        {
            if(path == "")            
                path = "/Organizations/Org_11/CSS/Mirror/Styles/main.css";
                
          var url = "<%=Session["ImageURL"]%>";
          htmlString = htmlString.replace(new RegExp("image/","g"), url + "/en/image/");
          htmlString = "<html><link rel='stylesheet' type='text/css' href='" + url + path +"' /><body><center>" + htmlString + "</center></body></html>";
        }
        if (document.getElementById("tempText") != null)
        {
            document.getElementById("tempText").value = "";
            document.getElementById("tempText").value = htmlString;
        }
        //alert(htmlString.length + " : " + document.getElementById("tempText").value.length);    
        setTimeout("DataLoading(0);",4000);//ZD 101294
        return true;
    }

  function ExpandAll()
  {
//    var obj = document.getElementById("img_LOC");
//    obj.src = obj.src.replace("plus", "minus");
        ShowHideRow("LOC", document.getElementById("img_LOC"),true);
        ShowHideRow("PAR", document.getElementById("img_PAR"),true);
        ShowHideRow("CustOpt", document.getElementById("Img_CustOpt"),true);  //Custom attribute fixes
        ShowHideRow("AV", document.getElementById("Img_AV"),true);
        ShowHideRow("AVWO", document.getElementById("Img_AVWO"),true);
        ShowHideRow("CATWO", document.getElementById("Img_CATWO"),true);
        ShowHideRow("HKWO", document.getElementById("Img_HKWO"),true);
        ShowHideRow("CON", document.getElementById("Img_CON"),true);//FB 2359//FB 2632
        ShowHideRow("ICONTROL", document.getElementById("img_ICONTROL"),true);//ZD 101098
  }
  function ShowHideRow(rType, obj, frmCheck)
  {
    var tempRow = document.getElementById("tr_" + rType);
    //var tempLbl = document.getElementById("lbl_" + rType);
    //alert(obj.src);
    if ( (tempRow != null) && (obj != null) )
    {
        if (frmCheck == true)
        {
            if (document.getElementById("chkExpandCollapse").checked)
            {
                obj.src = obj.src.replace("minus", "plus");
                tempRow.style.display = "none";
            }
            else
            {
                obj.src = obj.src.replace("plus", "minus");
                tempRow.style.display = "";
            }
        }
        if (frmCheck == false)
        {
            if (obj.src.indexOf("minus") > 0)
            {
                obj.src = obj.src.replace("minus", "plus");
                tempRow.style.display = "none";
                //tempLbl.style.display = "";
            }
            else
            {
                obj.src = obj.src.replace("plus", "minus");
                tempRow.style.display = "";
                //tempLbl.style.display = "none";
            }
        }
    }
  }
  
  function btnSetupAtMCU_Click()
  {
  
    if (confirm(SetUponbridge))
    {
        DataLoading(1);
        document.getElementById("<%=cmd.ClientID %>").value="1";
        document.frmSubmit.action="ManageConference.aspx?t=";
        document.frmSubmit.submit();
        return true;
    }
    else
        return false;
  }
  
  function btnAcceptReject_Click()
  {
    //FB 2438 - Start
    if ("<%=isAcceptDecline%>" == 1)
    {
        alert(ManageRoomInstance);
    }
    
    //alert("<%=Session["ConfID"] %>");
    window.location.href="ResponseConference.aspx?t=2&id=" + "<%=Session["ConfID"] %>" + "&req=" + "<%=Session["userID"] %>";
    //FB 2438 - End
  }
  
  function btnDeleteConference_Click()
  {
    //Code Edited for FB 1425 QA Bug -Start
  var msg;
  if('<%=Application["Client"]%>' == "MOJ")
  msg = "Are you sure you want to delete this hearing?";
  else
  msg = DeleteConf;
  
    if (confirm(msg))
    //Code Edited for FB 1425 QA Bug -End
    {
//        document.getElementById("<%=cmd.ClientID %>").value="2";
//        document.frmSubmit.action="ManageConference.aspx?t=2";
//        document.frmSubmit.submit();
        DataLoading(1);
        return true;
    }
    else
        return false;
  }
  function ChangeView(id)
  {
    if (id == "1")
    {
        document.getElementById("divNormal").style.display="";
        document.getElementById("divEndpoint").style.display="none";
    }
    else
    {
        document.getElementById("divEndpoint").style.display="";
        document.getElementById("divNormal").style.display="none";
    }        
  }
  function managelayout (dl, epid, epty)
    {
        dl ="<%=imgVideoLayout.ClientID %>"; //ZD 101294
    //	change_display_layout_prompt('image/pen.gif', 'Manage Display Layout', epid, epty, dl, 4, "", ""); 
    var ML = RSManageLayOut;
	    change_display_layout_prompt('image/pen.gif', ML, epid, epty, dl, 5, document.getElementById('<%=ImageFiles.ClientID%>').value + '|' + document.getElementById('<%=ImageFilesBT.ClientID%>').value, document.getElementById('<%=ImagesPath.ClientID%>').value);
    }

function change_display_layout_prompt(promptpicture, prompttitle, epid, epty, dl, rowsize, images, imgpath) 
{
    var tempEpid = epid;
	var title = new Array()
	title[0] = "Default ";
	title[1] = "Custom ";
	promptbox = document.createElement('div'); 
	promptbox.setAttribute ('id' , 'prompt');
	document.getElementsByTagName('body')[0].appendChild(promptbox);
	promptbox = eval("document.getElementById('prompt').style");

    //FB 2950 - Start
    promptbox.position = 'absolute'	
    promptbox.top = -120+mousedownY + 'px';
	promptbox.left = mousedownX + 'px'; 
	promptbox.width = rowsize * 100 + 'px';
	promptbox.border = 'outset 1 #bbbbbb' 
	promptbox.height = 400 + 'px';	
	promptbox.overflow ='auto';
	promptbox.backgroundColor = '#FFFFE6'; //ZD 100426
	//FB 2950 - End

	m = "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr valign='middle'><td width='22' height='22' style='text-indent:2;' class='titlebar'><img src='" + promptpicture + "' height='18' width='18' alt='Prompt'></td><td class='titlebar'>" + prompttitle + "</td></tr></table>" //ZD 100419
	m += "<table cellspacing='2' cellpadding='2' border='0' width='100%' class='promptbox'>";
	imagesary = images.split(":");
	rowNum = parseInt( (imagesary.length + rowsize - 2) / rowsize, 10 );
	m += "  <tr><td align='right' colspan='" + (rowsize * 2) + "'>"
	m += "    <input type='button' class='altMedium0BlueButtonFormat' value='<asp:Literal Text='<%$ Resources:WebResources, Submit%>' runat='server'></asp:Literal>' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveOrder(\"" + dl + "\", epid, \"" + epty + "\",\"" + tempEpid + "\");'>"
	m += "    <input type='button' class='altMedium0BlueButtonFormat' value='<asp:Literal Text='<%$ Resources:WebResources, Cancel%>' runat='server'></asp:Literal>' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='cancelthis();'>"
	m += "  </td></tr>"
	m += "	<tr>";
	m += "    <td colspan='" + (rowsize * 2) + "' align='left'><asp:Literal Text='<%$ Resources:WebResources, ManageConference_DisplayLayout %>' runat='server'></asp:Literal></td>";//FB 2579
	m += "  </tr>"
	m += "  <tr>"
	m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
	m += "  </tr>"

    //ZD 101869 start

    if (document.getElementById("hdnCodian") != null && document.getElementById("hdnCodian").value == "1") 
    {

        m += "<tr><td colspan='" + (rowsize * 2) + "'>";
        m += "<table>";
        m += "  <tr>";
        m += "    <td valign='middle'>";
        m += "      <input type='radio' name='layout' id='layout' value='100' onClick='epid=100'>";
        m += "    </td>";
        m += "    <td valign='middle' align='left' class='blackblodtext'>" + Defaultfamily + "</td>";
        m += "  </tr>";

        m += "  <tr>";
        m += "    <td  valign='middle'>";
        m += "      <input type='radio' name='layout' id='layout' value='101' onClick='epid=101'>";
        m += "    </td>";
        m += "    <td valign='middle' align='left' class='blackblodtext'>" + Family1 + "</td>";
        m += "    <td valign='middle'>";
        m += "      <img src='" + imgpath + "05.gif' width='57' height='43' alt='Layout'>";
        m += "    </td>";
        m += "    <td></td>";
        m += "    <td valign='middle'>";
        m += "      <img src='" + imgpath + "06.gif' width='57' height='43' alt='Layout'>";
        m += "    </td>";
        m += "    <td></td>";
        m += "    <td valign='middle'>";
        m += "      <img src='" + imgpath + "07.gif' width='57' height='43' alt='Layout'>";
        m += "    </td>";
        m += "    <td></td>";
        m += "    <td valign='middle'>";
        m += "      <img src='" + imgpath + "44.gif' width='57' height='43' alt='Layout'>";
        m += "    </td>";
        m += "  </tr>";

        m += "  <tr>"
        m += "    <td height='1'></td>";
        m += "  </tr>"

        m += "  <tr>";
        m += "    <td  valign='middle'>";
        m += "      <input type='radio' name='layout' id='layout' value='102' onClick='epid=102'>";
        m += "    </td>";
        m += "    <td valign='middle' align='left' class='blackblodtext'>" + Family2 + "</td>";
        m += "    <td valign='middle'>";
        m += "      <img src='" + imgpath + "01.gif' width='57' height='43' alt='Layout'>"; 
        m += "    </td>";
        m += "  </tr>";

        m += "  <tr>"
        m += "    <td height='1'></td>";
        m += "  </tr>"

        m += "  <tr>";
        m += "    <td  valign='middle'>";
        m += "      <input type='radio' name='layout' id='layout' value='103' onClick='epid=103'>";
        m += "    </td>";
        m += "    <td valign='middle' align='left' class='blackblodtext'>" + Family3 + "</td>";
        m += "    <td valign='middle'>";
        m += "      <img src='" + imgpath + "02.gif' width='57' height='43' alt='Layout'>";
        m += "    </td>";
        m += "  </tr>";

        m += "  <tr>"
        m += "    <td height='1'></td>";
        m += "  </tr>"


        m += "  <tr>";
        m += "    <td valign='middle'>";
        m += "      <input type='radio' name='layout' id='layout' value='104' onClick='epid=104'>";
        m += "    </td>";
        m += "    <td valign='middle' align='left' class='blackblodtext'>" + Family4 + "</td>";
        m += "    </td>";
        m += "    <td valign='middle'>";
        m += "      <img src='" + imgpath + "02.gif' width='57' height='43' alt='Layout' >";
        m += "    </td>";
        m += "    <td></td>";
        m += "    <td valign='middle'>";
        m += "      <img src='" + imgpath + "03.gif' width='57' height='43' alt='Layout'>";
        m += "    </td>";
        m += "    <td></td>";
        m += "    <td valign='middle'>";
        m += "      <img src='" + imgpath + "04.gif' width='57' height='43' alt='Layout'>";
        m += "    </td>";
        m += "    <td></td>";
        m += "    <td valign='middle'>";
        m += "      <img src='" + imgpath + "43.gif' width='57' height='43' alt='Layout'>";
        m += "    </td>";
        m += "  </tr>";

        m += "  <tr>"
        m += "    <td height='1'></td>";
        m += "  </tr>"

        m += "    <tr>";
        m += "    <td valign='middle'>";
        m += "      <input type='radio' name='layout' id='layout' value='105' onClick='epid=105'>";
        m += "    </td>";
        m += "    <td valign='middle' align='left' class='blackblodtext'>" + Family5 + "</td>";
        m += "    </td>";
        m += "    <td valign='middle'>";
        m += "      <img src='" + imgpath + "25.gif' width='57' height='43' alt='Layout'>";
        m += "    </td>";
        m += "  </tr>"

        m += "  <tr>"
        m += "    <td height='1'></td>";
        m += "  </tr>"

        m += "</table>";
        m += " </td></tr>"

        m += "  <tr>"
        m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
        m += "  </tr>"

    }
    //ZD 101869 End

	imgno = 0;
	for (i = 0; i < rowNum; i++) 
	{
		m += "  <tr>";
		for (j = 0; (j < rowsize) && (imgno < imagesary.length-1); j++) {
			
		
			m += "    <td valign='middle'>";
			m += "      <input type='radio' name='layout' id='layout' value='" + imagesary[imgno] + "' onClick='epid=" + imagesary[imgno] + ";'>";
			m += "    </td>";
			m += "    <td valign='middle'>";
			m += "      <img src='" + imgpath + imagesary[imgno] + ".gif' width='30' height='30'>";
			m += "    </td>";
			imgno ++;
		}
		m += "  </tr>";
	}
    
	m += "  <tr>";
	m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
	m += "  </tr>"
	m += "  <tr><td align='right' colspan='" + (rowsize * 2) + "'>"
	m += "    <input type='button' class='altMedium0BlueButtonFormat' value='<asp:Literal Text='<%$ Resources:WebResources, Submit%>' runat='server'></asp:Literal>' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveOrder(\"" + dl + "\", epid, \"" + epty + "\",\"" + tempEpid + "\");'>"
	m += "    <input type='button' class='altMedium0BlueButtonFormat' value='<asp:Literal Text='<%$ Resources:WebResources, Cancel%>' runat='server'></asp:Literal>' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='cancelthis();'>"
	m += "  </td></tr>"
	m += "</table>" 
	
	document.getElementById('prompt').innerHTML = m;
} 

function saveOrder(objid, id, epty, epid) 
{
//    alert(objid + " : " + id + " : " + epty + " : " + epid);
    if (id < 10)
        id = "0" + id;
	
	if (epty=="")
    {
        document.getElementById("<%=txtTempImage.ClientID %>").value=document.getElementById("<%=txtSelectedImage.ClientID %>").value;
        document.getElementById("<%=txtSelectedImage.ClientID %>").value = id;
	    document.getElementById("<%=cmd.ClientID %>").value="5";
        document.frmSubmit.action="ManageConference.aspx?t=5";
        document.frmSubmit.submit();
    }
    else
        if (epty != "1")
        {
	        document.getElementById("<%=txtTempImage.ClientID %>").value=document.getElementById("<%=txtSelectedImageEP.ClientID %>").value;
            document.getElementById("<%=txtSelectedImageEP.ClientID %>").value = id;
	        document.getElementById("<%=txtEndpointType.ClientID %>").value=epty + "," + epid;
            document.getElementById("<%=cmd.ClientID %>").value="5";
            document.frmSubmit.action="ManageConference.aspx?t=6"; 
            document.frmSubmit.submit();
        }
	document.getElementById(objid).src = document.getElementById("<%=ImagesPath.ClientID %>").value + document.getElementById("<%=txtSelectedImage.ClientID %>").value + ".gif";
     //if (document.getElementById("hdnCodian").value == "1" || document.getElementById("hdnLOSelection").value == "1" )
     {
            //document.getElementById("hdnCodian").value = id;

           // document.getElementById("hdnFamilyLayout").value = 1;
            if (id == 101) {

                document.getElementById("imgVideoDisplay").style.display = '';
                document.getElementById("imgVideoDisplay").src = "image/displaylayout/05.gif";
                document.getElementById("imgLayoutMapping6EP").style.display = '';
                document.getElementById("imgLayoutMapping6EP").src = "image/displaylayout/06.gif";
                document.getElementById("imgLayoutMapping7EP").style.display = '';
                document.getElementById("imgLayoutMapping7EP").src = "image/displaylayout/07.gif";
                document.getElementById("imgLayoutMapping8EP").style.display = '';
                document.getElementById("imgLayoutMapping8EP").src = "image/displaylayout/44.gif";
                document.getElementById("lblCodianLOEP").innerHTML = Family1;

                document.getElementById("imgVideoLayout").style.display = 'none';
//                document.getElementById("imgVideoLayout").src = "image/displaylayout/05.gif";
//                document.getElementById("imgLayoutMapping6").style.display = '';
//                document.getElementById("imgLayoutMapping6").src = "image/displaylayout/06.gif";
//                document.getElementById("imgLayoutMapping7").style.display = '';
//                document.getElementById("imgLayoutMapping7").src = "image/displaylayout/07.gif";
//                document.getElementById("imgLayoutMapping8").style.display = '';
//                document.getElementById("imgLayoutMapping8").src = "image/displaylayout/44.gif";
                document.getElementById("lblCodianLO").innerHTML = Family1; 
            }
            else if (id == 102) {
                document.getElementById("imgVideoDisplay").style.display = '';
                document.getElementById("imgVideoDisplay").src = "image/displaylayout/01.gif";
                document.getElementById("imgLayoutMapping6EP").style.display = 'none';
                document.getElementById("imgLayoutMapping7EP").style.display = 'none';
                document.getElementById("imgLayoutMapping8EP").style.display = 'none';
                document.getElementById("lblCodianLOEP").innerHTML = Family2; 

                document.getElementById("imgVideoLayout").style.display = 'none';
//                document.getElementById("imgVideoLayout").src = "image/displaylayout/01.gif";
//                document.getElementById("imgLayoutMapping6").style.display = 'none';
//                document.getElementById("imgLayoutMapping7").style.display = 'none';
//                document.getElementById("imgLayoutMapping8").style.display = 'none';
                document.getElementById("lblCodianLO").innerHTML = Family2; 
            }
            else if (id == 103) {

                document.getElementById("imgVideoDisplay").style.display = '';
                document.getElementById("imgVideoDisplay").src = "image/displaylayout/02.gif";
                document.getElementById("imgLayoutMapping6EP").style.display = 'none';
                document.getElementById("imgLayoutMapping7EP").style.display = 'none';
                document.getElementById("imgLayoutMapping8EP").style.display = 'none';
                document.getElementById("lblCodianLOEP").innerHTML = Family3; 

                document.getElementById("imgVideoLayout").style.display = 'none';
//                document.getElementById("imgVideoLayout").src = "image/displaylayout/02.gif";
//                document.getElementById("imgLayoutMapping6").style.display = 'none';
//                document.getElementById("imgLayoutMapping7").style.display = 'none';
//                document.getElementById("imgLayoutMapping8").style.display = 'none';
                document.getElementById("lblCodianLO").innerHTML = Family3; 
            }
            else if (id == 104) {
                document.getElementById("imgVideoDisplay").style.display = '';
                document.getElementById("imgVideoDisplay").src = "image/displaylayout/02.gif";
                document.getElementById("imgLayoutMapping6EP").style.display = '';
                document.getElementById("imgLayoutMapping6EP").src = "image/displaylayout/03.gif";
                document.getElementById("imgLayoutMapping7EP").style.display = '';
                document.getElementById("imgLayoutMapping7EP").src = "image/displaylayout/04.gif";
                document.getElementById("imgLayoutMapping8EP").style.display = '';
                document.getElementById("imgLayoutMapping8EP").src = "image/displaylayout/43.gif";
                document.getElementById("lblCodianLOEP").innerHTML = Family4; 

                document.getElementById("imgVideoLayout").style.display = 'none';
//                document.getElementById("imgVideoLayout").src = "image/displaylayout/02.gif";
//                document.getElementById("imgLayoutMapping6").style.display = '';
//                document.getElementById("imgLayoutMapping6").src = "image/displaylayout/03.gif";
//                document.getElementById("imgLayoutMapping7").style.display = '';
//                document.getElementById("imgLayoutMapping7").src = "image/displaylayout/04.gif";
//                document.getElementById("imgLayoutMapping8").style.display = '';
//                document.getElementById("imgLayoutMapping8").src = "image/displaylayout/43.gif";
                document.getElementById("lblCodianLO").innerHTML = Family4; 
            }
            else if (id == 105) {

                document.getElementById("imgVideoDisplay").style.display = '';
                document.getElementById("imgVideoDisplay").src = "image/displaylayout/25.gif";
                document.getElementById("imgLayoutMapping6EP").style.display = 'none';
                document.getElementById("imgLayoutMapping7EP").style.display = 'none';
                document.getElementById("imgLayoutMapping8EP").style.display = 'none';
                document.getElementById("lblCodianLOEP").innerHTML = Family5; 

                document.getElementById("imgVideoLayout").style.display = 'none';
//                document.getElementById("imgVideoLayout").src = "image/displaylayout/25.gif";
//                document.getElementById("imgLayoutMapping6").style.display = 'none';
//                document.getElementById("imgLayoutMapping7").style.display = 'none';
//                document.getElementById("imgLayoutMapping8").style.display = 'none';
                document.getElementById("lblCodianLO").innerHTML = Family5; 
            }
            else if (id == 100) {

                document.getElementById("imgVideoDisplay").style.display = 'none';
                document.getElementById("imgLayoutMapping6EP").style.display = 'none';
                document.getElementById("imgLayoutMapping7EP").style.display = 'none';
                document.getElementById("imgLayoutMapping8EP").style.display = 'none';
                document.getElementById("lblCodianLOEP").innerHTML = Defaultfamily; 

                document.getElementById("imgVideoLayout").style.display = 'none';
//                document.getElementById("imgLayoutMapping6").style.display = 'none';
//                document.getElementById("imgLayoutMapping7").style.display = 'none';
//                document.getElementById("imgLayoutMapping8").style.display = 'none';
                document.getElementById("lblCodianLO").innerHTML = Defaultfamily; 

            }
            else {
                document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + id + ".gif";
                document.getElementById("imgVideoDisplay").style.display = '';
                document.getElementById("imgLayoutMapping6EP").style.display = 'none';
                document.getElementById("imgLayoutMapping7EP").style.display = 'none';
                document.getElementById("imgLayoutMapping8EP").style.display = 'none';
                document.getElementById("lblCodianLOEP").innerHTML = ""; 

                document.getElementById("imgVideoLayout").src = "image/displaylayout/" + id + ".gif";
                document.getElementById("imgVideoLayout").style.display = '';
//                document.getElementById("imgLayoutMapping6").style.display = 'none';
//                document.getElementById("imgLayoutMapping7").style.display = 'none';
//                document.getElementById("imgLayoutMapping8").style.display = 'none';
                document.getElementById("lblCodianLO").innerHTML = ""; 
                //document.getElementById("hdnFamilyLayout").value = 0;
            }
        }
	cancelthis();
} 


function cancelthis()
{
	document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
	/*Code Commented for - FB 1400 - Start */
	//window.resizeTo(750,450);
}

function saveLayout() {
    if(document.getElementById("txtSelectedImage") != null)//ZD 101931
       var id = document.getElementById("txtSelectedImage").value;
    if (id == 101) {
        document.getElementById("imgVideoDisplay").style.display = '';
        document.getElementById("imgVideoDisplay").src = "image/displaylayout/05.gif";
        document.getElementById("imgLayoutMapping6EP").style.display = '';
        document.getElementById("imgLayoutMapping6EP").src = "image/displaylayout/06.gif";
        document.getElementById("imgLayoutMapping7EP").style.display = '';
        document.getElementById("imgLayoutMapping7EP").src = "image/displaylayout/07.gif";
        document.getElementById("imgLayoutMapping8EP").style.display = '';
        document.getElementById("imgLayoutMapping8EP").src = "image/displaylayout/44.gif";
        document.getElementById("lblCodianLOEP").innerHTML = Family1;

        document.getElementById("imgVideoLayout").style.display = 'none';
//        document.getElementById("imgVideoLayout").src = "image/displaylayout/05.gif";
//        document.getElementById("imgLayoutMapping6").style.display = '';
//        document.getElementById("imgLayoutMapping6").src = "image/displaylayout/06.gif";
//        document.getElementById("imgLayoutMapping7").style.display = '';
//        document.getElementById("imgLayoutMapping7").src = "image/displaylayout/07.gif";
//        document.getElementById("imgLayoutMapping8").style.display = '';
//        document.getElementById("imgLayoutMapping8").src = "image/displaylayout/44.gif";
        document.getElementById("lblCodianLO").innerHTML = Family1; 
    }
    else if (id == 102) {
        document.getElementById("imgVideoDisplay").style.display = '';
        document.getElementById("imgVideoDisplay").src = "image/displaylayout/01.gif";
        document.getElementById("imgLayoutMapping6EP").style.display = 'none';
        document.getElementById("imgLayoutMapping7EP").style.display = 'none';
        document.getElementById("imgLayoutMapping8EP").style.display = 'none';
        document.getElementById("lblCodianLOEP").innerHTML = Family2; 

        document.getElementById("imgVideoLayout").style.display = 'none';
//        document.getElementById("imgVideoLayout").src = "image/displaylayout/01.gif";
//        document.getElementById("imgLayoutMapping6").style.display = 'none';
//        document.getElementById("imgLayoutMapping7").style.display = 'none';
//        document.getElementById("imgLayoutMapping8").style.display = 'none';
        document.getElementById("lblCodianLO").innerHTML = Family2; 
    }
    else if (id == 103) {

        document.getElementById("imgVideoDisplay").style.display = '';
        document.getElementById("imgVideoDisplay").src = "image/displaylayout/02.gif";
        document.getElementById("imgLayoutMapping6EP").style.display = 'none';
        document.getElementById("imgLayoutMapping7EP").style.display = 'none';
        document.getElementById("imgLayoutMapping8EP").style.display = 'none';
        document.getElementById("lblCodianLOEP").innerHTML = Family3; 

        document.getElementById("imgVideoLayout").style.display = 'none';
//        document.getElementById("imgVideoLayout").src = "image/displaylayout/02.gif";
//        document.getElementById("imgLayoutMapping6").style.display = 'none';
//        document.getElementById("imgLayoutMapping7").style.display = 'none';
//        document.getElementById("imgLayoutMapping8").style.display = 'none';
        document.getElementById("lblCodianLO").innerHTML = Family3; 
    }
    else if (id == 104) {
        document.getElementById("imgVideoDisplay").style.display = '';
        document.getElementById("imgVideoDisplay").src = "image/displaylayout/02.gif";
        document.getElementById("imgLayoutMapping6EP").style.display = '';
        document.getElementById("imgLayoutMapping6EP").src = "image/displaylayout/03.gif";
        document.getElementById("imgLayoutMapping7EP").style.display = '';
        document.getElementById("imgLayoutMapping7EP").src = "image/displaylayout/04.gif";
        document.getElementById("imgLayoutMapping8EP").style.display = '';
        document.getElementById("imgLayoutMapping8EP").src = "image/displaylayout/43.gif";
        document.getElementById("lblCodianLOEP").innerHTML = Family4; 

        document.getElementById("imgVideoLayout").style.display = 'none';
//        document.getElementById("imgVideoLayout").src = "image/displaylayout/02.gif";
//        document.getElementById("imgLayoutMapping6").style.display = '';
//        document.getElementById("imgLayoutMapping6").src = "image/displaylayout/03.gif";
//        document.getElementById("imgLayoutMapping7").style.display = '';
//        document.getElementById("imgLayoutMapping7").src = "image/displaylayout/04.gif";
//        document.getElementById("imgLayoutMapping8").style.display = '';
//        document.getElementById("imgLayoutMapping8").src = "image/displaylayout/43.gif";
        document.getElementById("lblCodianLO").innerHTML = Family4; 
    }
    else if (id == 105) {

        document.getElementById("imgVideoDisplay").style.display = '';
        document.getElementById("imgVideoDisplay").src = "image/displaylayout/25.gif";
        document.getElementById("imgLayoutMapping6EP").style.display = 'none';
        document.getElementById("imgLayoutMapping7EP").style.display = 'none';
        document.getElementById("imgLayoutMapping8EP").style.display = 'none';
        document.getElementById("lblCodianLOEP").innerHTML = Family5; 

        document.getElementById("imgVideoLayout").style.display = 'none';
//        document.getElementById("imgVideoLayout").src = "image/displaylayout/25.gif";
//        document.getElementById("imgLayoutMapping6").style.display = 'none';
//        document.getElementById("imgLayoutMapping7").style.display = 'none';
//        document.getElementById("imgLayoutMapping8").style.display = 'none';
        document.getElementById("lblCodianLO").innerHTML = Family5; 
    }
    else if (id == 100) {

        document.getElementById("imgVideoDisplay").style.display = 'none';
        document.getElementById("imgLayoutMapping6EP").style.display = 'none';
        document.getElementById("imgLayoutMapping7EP").style.display = 'none';
        document.getElementById("imgLayoutMapping8EP").style.display = 'none';
        document.getElementById("lblCodianLOEP").innerHTML = Defaultfamily; 

        document.getElementById("imgVideoLayout").style.display = 'none';
//        document.getElementById("imgLayoutMapping6").style.display = 'none';
//        document.getElementById("imgLayoutMapping7").style.display = 'none';
//        document.getElementById("imgLayoutMapping8").style.display = 'none';
        document.getElementById("lblCodianLO").innerHTML = Defaultfamily; 

    }
    else {
        document.getElementById("imgVideoDisplay").src = "image/displaylayout/" + id + ".gif";
        document.getElementById("imgVideoDisplay").style.display = '';
        document.getElementById("imgLayoutMapping6EP").style.display = 'none';
        document.getElementById("imgLayoutMapping7EP").style.display = 'none';
        document.getElementById("imgLayoutMapping8EP").style.display = 'none';
        document.getElementById("lblCodianLOEP").innerHTML = ""; 

        document.getElementById("imgVideoLayout").src = "image/displaylayout/" + id + ".gif";
        document.getElementById("imgVideoLayout").style.display = '';
//        document.getElementById("imgLayoutMapping6").style.display = 'none';
//        document.getElementById("imgLayoutMapping7").style.display = 'none';
//        document.getElementById("imgLayoutMapping8").style.display = 'none';
        document.getElementById("lblCodianLO").innerHTML = ""; 
        //document.getElementById("hdnFamilyLayout").value = 0;
    }
   return true;
}

 
function CheckEndpoint()
{
    if (document.getElementById("<%=imgVideoLayout.ClientID %>") != null)
        document.getElementById("<%=imgVideoLayout.ClientID %>").src = document.getElementById("<%=ImagesPath.ClientID %>").value + document.getElementById("<%=txtSelectedImage.ClientID %>").value + ".gif";    

  //saveLayout();//ZD 101931
}
/*Code Added for - FB 1400 - Start */
function fnValidateEndTime()
{
    var args = fnValidateEndTime.arguments; //FB 1562
    if(args[0] == 'p')
    {
		if ( (document.getElementById("txtPExtTime").value == "") ) {
				document.getElementById("LblPExtTimeMsg").style.visibility = "visible";
				document.getElementById("LblPExtTimeMsg").innerText = "<asp:Literal Text="<%$ Resources:WebResources, Required%>" runat="server"></asp:Literal>";
				return (false);		
		}
	}
	else
	{
	    if ( (document.getElementById("txtExtendedTime").value == "") ) {
				document.getElementById("LblExtendedTimeMsg").style.visibility = "visible";
				document.getElementById("LblExtendedTimeMsg").innerText = "<asp:Literal Text="<%$ Resources:WebResources, Required%>" runat="server"></asp:Literal>";
				return (false);		
		}
	}
	return true;
}
/*Code Added for - FB 1400 - Start */
//Code added for FB 1391 -- Start
function CustomEditAlert()
{
    var msg = "Some instances of this series have a unique Start Time/End Time.  Edit All will globally change the Start Time/End Time for all instances of this series.  Press OK to proceed and enter a new Start Time/End Time, or press Cancel to edit individual instances."
    var act = true;
    
    if ("<%=isCustomEdit%>" == "Y" )
    {
        act = confirm(msg);
        if(!act)
            DataLoading(0);
        //return act;    
    }   

    if(act == true && parseInt('<%=Session["admin"]%>',10) > 0) // ZD 101722
        return fnSelectForm();
    else
        return act;
    //return true;
}
//Code added for FB 1391 -- End

//Code added for P2P
function fnchkValue()
{
        var args = fnchkValue.arguments;
        var txtt
        if(args)
        {
            if(args[0])      
              txtt = document.getElementById(args[0]);
            
            if(txtt)
            {
                if(txtt.value == "")
                {
                    alert(DashMsg)
                    return false;
                }
            }
         }
            
        return true;
}

function fnOpenEpt()
{
    var args = fnOpenEpt.arguments;
    
    if(args[0])
    {
        window.open("http://"+args[0], "EndPointDetails", "width=900,height=800,resizable=yes,scrollbars=yes,status=no");
        return false;
    }
}

function fnOpenRemote()
{
    var args = fnOpenRemote.arguments;
    
    if(args[0])
    {
        window.open("http://"+ args[0] + "/a_tvmon.htm", "Monitor", "width=650,height=350,resizable=yes,scrollbars=yes,status=no");
        return false;
    }
}

function fnOpenMsg()
{

    var args = fnOpenMsg.arguments;
    var rw;
    if(args[0])
      rw = document.getElementById(args[0]);
    
    if(rw)
    {      
        if(args[1] == "1")
         rw.style.display = "block";
        else if(args[1] == "0")
             rw.style.display = "none"; 
    }
}

function shwHostDetails() //FB 1958
{
  document.getElementById("viewHostDetails").style.display = 'block';
  return false;
}

var isIE = false; //ZD 100369 508 Issues
if (navigator.userAgent.indexOf('Trident') > -1)
    isIE = true;

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="stylesheet" type="text/css" href="css/myprompt.css" />
</head>
<body>
    <p>
        <br />
    </p>
    <%--EdIted For FB 1428 QA Bug--%>
    <input type="hidden" id="helpPage" value="66">
    <form id="frmSubmit" autocomplete="off" runat="server" method="post" onsubmit="return true">
    <%--ZD 101190--%>
    <%--ZD 101022 start--%>
    <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true">
        <Scripts>
            <asp:ScriptReference Path="~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>" />
        </Scripts>
    </asp:ScriptManager>
    <%--ZD 101022 End--%>
    <input type="hidden" id="timezone" runat="server" />
    <%--FB 1948--%>
    <%--<input type="hidden" name="hdnConceirgeSupp" id="hdnConceirgeSupp" runat="server" /> --%><%--FB 2359--%>
    <%--FB 2377--%>
    <%--FB 2274 Starts--%>
    <input type="hidden" id="hdnCrossEnableBufferZone" runat="server" />
    <input type="hidden" id="hdnCrossEnableEntity" runat="server" />
    <input type="hidden" id="hdnCrossfoodModule" runat="server" />
    <input type="hidden" id="hdnCrosshkModule" runat="server" />
    <input type="hidden" id="hdnCrossroomModule" runat="server" />
    <%--FB 2274 Ends--%>
    <%--FB 2446 - Start --%>
    <input type="hidden" id="hdnCrossEnableConfPassword" runat="server" />
    <input type="hidden" id="hdnCrossEnablePublicConf" runat="server" />
    <%--FB 2446 - End --%>
    <input type="hidden" runat="server" id="hdnconfOriginID" /><%--2457 exchange round trip--%>
    <input type="hidden" id="hdnEnableEM7" runat="server" /><%--FB 2598--%>
    <input type="hidden" id="hdnNetworkSwitching" runat="server" /><%--FB 2595--%>
    <input type="hidden" id="hdnTimeZoneId" runat="server" /><%--ZD 100602--%>
    <input type="hidden" id="hdnCloudConf" runat="server" />
    <input type="hidden" id="hdnIsVMR" runat="server" />
    <input type="hidden" id="hdnServiceType" runat="server" />
    <input type="hidden" id="hdnConfStart" runat="server" />
    <input type="hidden" id="hdnConfEnd" runat="server" /><%--ZD 100602 End--%>
    <input type="hidden" id="hdnWebExHostURL" runat="server" />
    <input type="hidden" id="hdnWebExPartyURL" runat="server" />
    <input type="hidden" id="hdnMCUSynchronous" runat="server" /><%--ZD 100036 End--%>
    <input type="hidden" id="hdnPermanentConf" runat="server" />
    <%--ZD 100522--%>
    <input type="hidden" id="hdnExpressConf" runat="server" />
    <%--ZD 101233--%>
    <input type="hidden" id="hdnEditForm" runat="server" />
    <%--ZD 101597--%>
    <input type="hidden" runat="server" id="hdnSetuptimeDisplay" />
    <%--ZD 101755 --%>
    <input type="hidden" runat="server" id="hdnTeardowntimeDisplay" />
    <%--ZD 101755 --%>
    <input id="hdnCodian" type="hidden" runat="server" value ="0"  name="hdnCodian"/> <%-- ZD 101971 Starts start--%>
    <%--ZD 100221 Starts--%>
    <div id="WebExDialog" style="display: none; width: 700px; border-style: solid; background: white;
        height: 190px;">
        <table>
            <tr>
                <td width="420px" style="background: #EAEAEA; height: 183px;">
                    <table width="470">
                        <tr>
                            <td width="140px">
                                <asp:Label class="blackblodtext" ID="lblWebEXMeetingStatus" runat="server" Text="Meeting Status" />
                            </td>
                            <td class="blackblodtext">
                                :
                            </td>
                            <td>
                                <asp:Label ID="WebEXMeetingStatus" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td width="140px">
                                <asp:Label class="blackblodtext" ID="lblstartdate" runat="server" Text="Starting Date" />
                            </td>
                            <td class="blackblodtext">
                                :
                            </td>
                            <td>
                                <asp:Label ID="WebEXstartdate" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td width="140px">
                                <asp:Label class="blackblodtext" ID="lblStartingTime" runat="server" Text="Starting Time" />
                            </td>
                            <td class="blackblodtext">
                                :
                            </td>
                            <td>
                                <asp:Label ID="WebEXStartingTime" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td width="140px">
                                <asp:Label class="blackblodtext" ID="Label5" runat="server" Text="Duration" />
                            </td>
                            <td class="blackblodtext">
                                :
                            </td>
                            <td>
                                <asp:Label ID="WebEXDuration" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td width="140px">
                                <asp:Label class="blackblodtext" ID="Label7" runat="server" Text="Host's Name" />
                            </td>
                            <td class="blackblodtext">
                                :
                            </td>
                            <td>
                                <asp:Label ID="WebEXHost" runat="server" />
                            </td>
                        </tr>
                        <tr>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <input type="button" id="btnClose1" runat="server" class="altShortBlueButtonFormat"
                                    onclick="javascript:fnHide();" value="<%$ Resources:WebResources, ManageConference_BtnClose%>" />
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table>
                        <tr style="height: 20px">
                        </tr>
                        <tr>
                            <td valign="top">
                                <img src="../image/webexicon.jpg" />
                            </td>
                            <td colspan="2">
                                <span class="blackblodtext">When it's time, start your<br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; meeting here.</span>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr style="height: 20px">
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td align="center" colspan="3">
                                <asp:Button runat="server" OnClick="Btn_WebEXStart" ID="WebExStart" class="altShortBlueButtonFormat"
                                    Text="Start" Style="width: 141px" />
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <%--ZD 100221 Ends--%>
    <div id="viewHostDetails" runat="server" align="center" style="left: 120px; top: 150px;
        position: absolute; height: 350px; visibility: visible; z-index: 3; display: none;
        width: 725px">
        <%--FB 1958--%>
        <asp:PlaceHolder ID="HostDetailHolder" runat="server"></asp:PlaceHolder>
    </div>
    <%--FB 2441 Starts--%>
    <div id="MuteAllEndpointDiv" style="left: 700px; top: 650px; display: none; position: absolute;
        height: 200px; visibility: visible; z-index: 3; width: 250px; background-color: #E1E1E1">
        <div style="height: 170px; left: 200px; overflow-y: scroll; background-color: #E1E1E1">
            <table>
                <tr>
                    <td align="center">
                        <span class="subtitleblueblodtext">
                            <asp:Literal Text="<%$ Resources:WebResources, ManageConference_SelectParticip%>"
                                runat="server"></asp:Literal></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DataGrid AutoGenerateColumns="false" ShowHeader="false" GridLines="None" ID="dgMuteALL"
                            runat="server">
                            <Columns>
                                <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="type" Visible="false"></asp:BoundColumn>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ID="chk_muteall" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="Name"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </td>
                </tr>
            </table>
        </div>
        <table>
            <tr>
                <td align="center">
                    <asp:Button ID="BtnSubmit" Text="<%$ Resources:WebResources, ManageConference_BtnSubmit%>"
                        CssClass="altMedium0BlueButtonFormat" OnClick="btnMuteAllExcept" runat="server">
                    </asp:Button>
                    <asp:Button ID="BtnClose" Text="<%$ Resources:WebResources, ManageConference_BtnClose%>"
                        CssClass="altMedium0BlueButtonFormat" runat="server" OnClientClick="javascript:return fnShowHide('0');">
                    </asp:Button>
                </td>
            </tr>
        </table>
    </div>
    <%--FB 2441 Ends--%>
    <center>
        <div align="center" style="width: 100%">
            <table width="100%" id="tblMain" name="tblMain">
                <tr>
                    <td>
                        <%--Added For FB 1428 QA Bug--%>
                        <div id="dataLoadingDIV" style="display: none" align="center">
                            <img border='0' src='image/wait1.gif' alt='Loading..' />
                        </div>
                        <%--ZD 100176--%>
                        <%--ZD 100678 End--%>
                        <asp:CheckBox ID="Refreshchk" runat="server" Style="display: none" />
                        <table align="center">
                            <%--Added for FF--%>
                            <tr nowrap>
                                <td align="center" colspan="3" nowrap>
                                    <h3>
                                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                                        <%--Added for FB 1428 START--%>
                                        <%-- Organization Css Module --%>
                                        <span id="Field15" runat="server">
                                            <asp:Literal Text="<%$ Resources:WebResources, ManageConference_Field15%>" runat="server"></asp:Literal></span>
                                        <%--Edited for FB 1428 END--%>
                                        <asp:TextBox ID="cmd" runat="server" BorderStyle="None" TabIndex="-1" BackColor="transparent"
                                            Width="0px" BorderWidth="0px"></asp:TextBox>
                                        <%--ZD 100369--%>
                                        <% if (Request.QueryString["t"] != null)
                   if (Request.QueryString["t"].ToString().Equals("hf"))
                    {
                       //window dressing
                                        %>
                                        <button id='btnClose' onclick='javascript:window.close();' class='altMedium0BlueButtonFormat'>
                                            <asp:Literal ID="Literal50" Text="<%$ Resources:WebResources, Close%>" runat="server"></asp:Literal></button>
                                        <%
                    }
           
                                        %>
                                    </h3>
                                </td>
                            </tr>
                            <%--Added for FF--%>
                        </table>
                    </td>
                    <%--Added For FB 1428 QA Bug--%>
                </tr>
                <tr>
                    <td align="center" colspan="3">
                        <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
                        <asp:Table runat="server" ID="tblForceTerminate" Visible="false">
                            <asp:TableRow>
                                <asp:TableCell CssClass="lblError" Width="100%">
                                    <%--FB 2579 Start--%>
                                    <asp:Literal ID="Literal49" Text="<%$ Resources:WebResources, DashBoard_WARNINGconfere%>"
                                        runat="server"></asp:Literal><br />
                                    <asp:Button Text="<%$ Resources:WebResources, ManageConference_ForceTerminate%>"
                                        CssClass="altLongBlueButtonFormat" runat="server" OnClientClick="javascript:DataLoading(1)"
                                        OnClick="ForceTerminate"></asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                        <%--<asp:Table Width="90%" runat="server">
                        <asp:TableRow Width="100%">
                            <asp:TableCell Width="100%">--%>
                        <%--Window Dressing--%>
                        <br />
                        <asp:Label ID="lblAlert" runat="server" CssClass="lblError" Width="90%"></asp:Label>
                        <%-- </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>--%>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="3">
                        <table width="100%" border="0">
                            <%--Edited for FF--%>
                            <tr>
                                <td align="center">
                                    <%--FB 2446--%>
                                    <%--Window Dressing--%>
                                    <asp:TextBox ID="lblConfID" runat="server" Width="0px" TabIndex="-1" BorderStyle="None"
                                        BackColor="transparent"></asp:TextBox>
                                    <%--ZD 100369--%>
                                    <table width="90%">
                                        <tr>
                                            <td align="left" width="120" class="blackblodtext" valign="top">
                                                <asp:Literal Text="<%$ Resources:WebResources, ManageConference_Title%>" runat="server"></asp:Literal>
                                            </td>
                                            <td style="width: 1px; vertical-align: top">
                                                <b>:</b>&nbsp;
                                            </td>
                                            <td align="left" width="150" style="padding-right: 4px; color: #046380; font-size: 11pt;
                                                font-family: Arial;">
                                                <asp:Label ID="lblConfName" runat="server" Font-Bold="true"></asp:Label>
                                                <asp:LinkButton ID="ConfTitle" runat="server">...</asp:LinkButton><%--FB 2508--%>
                                            </td>
                                            <td align="left" class="blackblodtext" valign="top" nowrap="nowrap">
                                                <asp:Literal Text="<%$ Resources:WebResources, ManageConference_UniqueID%>" runat="server"></asp:Literal>
                                            </td>
                                            <td style="width: 1px; vertical-align: top">
                                                <b>:</b>&nbsp;
                                            </td>
                                            <td align="left" colspan="2" style="height: 21px" width="200" valign="top">
                                                <asp:Label ID="lblConfUniqueID" runat="server" ForeColor="Red"></asp:Label>
                                            </td>
                                            <td align="left" colspan="1" rowspan="8" width="350px" valign="top" class="btprint">
                                                <%--Window Dressing--%>
                                                <asp:Table ID="tblActions" runat="server" BorderStyle="None" BorderWidth="0" CssClass="tableBody"
                                                    CellPadding="0" CellSpacing="0" Width="100%">
                                                    <asp:TableRow runat="server" CssClass="LinksHeader" Height="30px">
                                                        <asp:TableCell runat="server" VerticalAlign="Middle" HorizontalAlign="right">
                                                            <asp:Literal ID="Literal39" Text="<%$ Resources:WebResources, ConferenceList_td7head%>"
                                                                runat="server"></asp:Literal>&nbsp;&nbsp;&nbsp;</asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow ID="trAcceptReject" runat="server" Height="20px">
                                                        <asp:TableCell ID="TableCell3" runat="server">
                                            <a href="#" onclick="javascript:btnAcceptReject_Click()"><asp:Literal Text="<%$ Resources:WebResources, ManageConference_AcceptDecline%>" runat="server"></asp:Literal></a>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow ID="trClone" runat="server" Height="20px">
                                                        <asp:TableCell ID="TableCell4" runat="server">
                                                            <asp:LinkButton ID="btnClone" Text="<%$ Resources:WebResources, ManageConference_btnClone%>"
                                                                runat="server" OnClientClick="javascript:DataLoading(1);" OnClick="CloneConference"></asp:LinkButton>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow runat="server" ID="trCancel" Height="20px">
                                                        <asp:TableCell ID="TableCell2" runat="server">
                                                            <asp:LinkButton runat="server" Text="<%$ Resources:WebResources, ManageConference_btnDeleteConf%>"
                                                                ID="btnDeleteConf" OnClick="DeleteConference" OnClientClick="javascript:return btnDeleteConference_Click();"></asp:LinkButton>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow ID="trEdit" runat="server" Height="20px">
                                                        <asp:TableCell ID="TableCell5" runat="server">
                                                            <asp:Button Style="display: none;" runat="server" ID="Temp" OnClick="EditConference" />
                                                            <asp:LinkButton ID="btnEdit" runat="server" Text="<%$ Resources:WebResources, ManageConference_btnEdit%>"
                                                                OnClick="EditConference"></asp:LinkButton>
                                                        </asp:TableCell></asp:TableRow>
                                                    <asp:TableRow runat="server" Height="20px">
                                                        <asp:TableCell runat="server">
                                                            <asp:LinkButton ID="btnPDF" Text="<%$ Resources:WebResources, ManageConference_btnPDF%>"
                                                                runat="server" OnClick="ExportToPDF" OnClientClick="javascript:pdfReport();"></asp:LinkButton>
                                                            <%--ZD 100429--%><%--ZD 101294--%>
                                                        </asp:TableCell></asp:TableRow>
                                                    <asp:TableRow runat="server" Height="20px">
                                                        <asp:TableCell runat="server">
                                            <a href="#" onclick="javascript:printpage();"><asp:Literal Text="<%$ Resources:WebResources, ManageConference_Print%>" runat="server"></asp:Literal></a>
                                                        </asp:TableCell></asp:TableRow>
                                                    <asp:TableRow ID="TableRow4" runat="server" Visible="false" Height="20px">
                                                        <%--FB 2152--%>
                                                        <asp:TableCell ID="TableCell6" runat="server">
                                                            <% if (Recur.Text.Equals(""))
                                           { %>
                                                            <asp:LinkButton ID="btnOutlook" Text="<%$ Resources:WebResources, ManageConference_btnOutlook%>"
                                                                runat="server" OnClientClick="javascript:saveToOutlookCalendar('0','0','1','');return false;"></asp:LinkButton>
                                                            <% }
                                           else
                                           { %>
                                                            <asp:LinkButton ID="btnOutlookR" Text="<%$ Resources:WebResources, ManageConference_btnOutlookR%>"
                                                                runat="server" OnClientClick="javascript:saveToOutlookCalendar('0','1','2','');return false;"></asp:LinkButton>
                                                            <% } %>
                                                        </asp:TableCell></asp:TableRow>
                                                    <asp:TableRow ID="trMCU" runat="server" Height="20px">
                                                        <asp:TableCell ID="TableCell1" runat="server">
                                                            <a href="#" onclick="javascript:btnSetupAtMCU_Click();">
                                                                <asp:Literal ID="Literal43" Text="<%$ Resources:WebResources, ManageConference_SetuponMCU%>"
                                                                    runat="server"></asp:Literal></a>
                                                        </asp:TableCell></asp:TableRow>
                                                    <asp:TableRow ID="tableRow12" runat="server" Height="20px">
                                                        <%--Added For FB 1529--%>
                                                        <asp:TableCell ID="TableCell12" runat='server'>
                                                            <a href="javascript:location.reload(true);">
                                                                <asp:Literal ID="Literal44" Text="<%$ Resources:WebResources, ManageConference_Refresh%>"
                                                                    runat="server"></asp:Literal></a>
                                                        </asp:TableCell></asp:TableRow>
                                                    <asp:TableRow ID="tableRow1" runat="server" Height="20px">
                                                        <asp:TableCell ID="TableCell11" runat='server'>
                                                            <asp:LinkButton runat="server" ID="btnwebexconf" OnClientClick="javascript:fnshow(this); return false;">
                                                                <asp:Literal ID="Literal45" Text="<%$ Resources:WebResources, ManageConference_Webex%>"
                                                                    runat="server"></asp:Literal></asp:LinkButton>
                                                            <%--ZD 100513--%>
                                                        </asp:TableCell></asp:TableRow>
                                                </asp:Table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="blackblodtext" valign="top" width="120">
                                                <%--FB 2446--%>
                                                <%if ((Application["Client"] == "MOJ")){%>
                                                <%--Added For FB 1428--%>
                                                <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, ManageConference_CreatedBy%>"
                                                    runat="server"></asp:Literal>
                                                <%}else{ %>
                                                <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, DashBoard_Host%>" runat="server"></asp:Literal>
                                                <%} %>
                                            </td>
                                            <td style="width: 1px; vertical-align: top">
                                                <b>:</b>&nbsp;
                                            </td>
                                            <td align="left" width="150" valign="top">
                                                <asp:Label ID="lblConfHost" runat="server" Font-Bold="False"></asp:Label><asp:Label
                                                    ID="hdnConfHost" runat="server" Visible="false"></asp:Label><asp:ImageButton ID="imgHostDetails"
                                                        AlternateText="Host Details" runat="server" ImageUrl="image/FaceSheet.GIF" OnClientClick="javascript:return shwHostDetails()"
                                                        ToolTip="<%$ Resources:WebResources, ViewDetails%>" Style="cursor: pointer; vertical-align: middle;" /><%--FB 1958--%>
                                                <%--ZD 100419 --%>
                                            </td>
                                            <td align="left" class="blackblodtext" valign="top" width="150">
                                                <%--ZD 100419--%>
                                                <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, DashBoard_LastModifiedB%>"
                                                    runat="server"></asp:Literal>
                                            </td>
                                            <td style="width: 1px; vertical-align: top">
                                                <b>:</b>&nbsp;
                                            </td>
                                            <td align="left" colspan="2" style="height: 21px;" valign="top" width="200">
                                                <asp:Label ID="lblLastModifiedBy" runat="server"></asp:Label>&nbsp;
                                            </td>
                                            <asp:Label ID="hdnLastModifiedBy" runat="server" Visible="false"></asp:Label></tr>
                                        <tr>
                                            <td align="left" class="blackblodtext" valign="top" width="120">
                                                <asp:Literal Text="<%$ Resources:WebResources, ManageConference_Date%>" runat="server"></asp:Literal>
                                            </td>
                                            <%--FB 2446--%>
                                            <td style="width: 1px" valign="top">
                                                <b>:</b>&nbsp;
                                            </td>
                                            <td align="left" width="250">
                                                <asp:Label ID="lblConfDate" runat="server" Font-Bold="False"></asp:Label>
                                                <asp:Label ID="lblConfTime" runat="server" Font-Bold="False"></asp:Label>
                                                <div style="height: auto; width: 100%; overflow: auto" id="lblTimezoneDIV">
                                                    <asp:Label ID="lblTimezone" runat="server"></asp:Label></div>
                                                <%--Window Dressing--%>
                                                <asp:TextBox runat="server" ID="Recur" Text="" TabIndex="-1" TextMode="SingleLine"
                                                    BorderStyle="None" Width="0px" BackColor="transparent" BorderColor="transparent"
                                                    ForeColor="White" Height="10"></asp:TextBox>
                                                <%--ZD 100369--%>
                                                <!--26&05&00&PM&60#1&2&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1#7/24/2007&2&5&-1-->
                                            </td>
                                            <td align="left" class="blackblodtext" valign="top">
                                                <asp:Literal Text="<%$ Resources:WebResources, ManageConference_Duration%>" runat="server"></asp:Literal>
                                            </td>
                                            <td style="width: 1px" valign="top">
                                                <b>:</b>&nbsp;
                                            </td>
                                            <td align="left" colspan="2" style="height: 21px;" valign="top" width="200" nowrap>
                                                <asp:Label ID="lblConfDuration" runat="server"></asp:Label>
                                                <asp:Label ID="hdnConfDuration" runat="server" Visible="false"></asp:Label>
                                            </td>
                                        </tr>
                                        <%--code added for buffer zone -- start--%>
                                        <%if(!(Application["Client"] == "MOJ")){ %>
                                        <tr runat="server" id="bufferTableCell">
                                            <td align="left" class="blackblodtext" valign="top" nowrap style="height: 10px">
                                                <asp:Literal Text="<%$ Resources:WebResources, ManageConference_ConferenceStar%>"
                                                    runat="server"></asp:Literal>
                                            </td>
                                            <td style="width: 1px">
                                                <b>:</b>&nbsp;
                                            </td>
                                            <td align="left" width="150">
                                                <asp:Label ID="lblSetupDur" runat="server" Font-Bold="False"></asp:Label>
                                            </td>
                                            <td align="left" class="blackblodtext" valign="top" nowrap>
                                                <asp:Literal Text="<%$ Resources:WebResources, ManageConference_ConferenceEnd%>"
                                                    runat="server"></asp:Literal>
                                            </td>
                                            <td style="width: 1px">
                                                <b>:</b>&nbsp;
                                            </td>
                                            <td align="left" width="150" style="height: 10px;">
                                                <asp:Label ID="lblTearDownDur" runat="server" Font-Bold="False"></asp:Label>
                                            </td>
                                        </tr>
                                        <%}%>
                                        <%--code added for buffer zone -- End--%>
                                        <tr id="trBuffer" runat="server">
                                            <%--ZD 101755 start--%>
                                            <td id="tdSetup" runat="server" align="left" class="blackblodtext" valign="top" nowrap
                                                style="height: 10px">
                                                <%--ZD 100085 Starts--%>
                                                <asp:Literal Text="<%$ Resources:WebResources, ManageConference_tdSetup%>" runat="server"></asp:Literal>
                                            </td>
                                            <td id="tdsetupcol" runat="server" style="width: 1px">
                                                <b>:</b>&nbsp;
                                            </td>
                                            <td id="tdlblSetup" align="left" width="150">
                                                <asp:Label ID="lblSetupTimeinMin" runat="server" Font-Bold="False"></asp:Label>
                                            </td>
                                            <td id="tdteardowncol" runat="server" align="left" class="blackblodtext" valign="top"
                                                nowrap="nowrap">
                                                <asp:Literal Text="<%$ Resources:WebResources, ManageConference_TearDownmins%>" runat="server"></asp:Literal>
                                            </td>
                                            <td id="tdlblteardowncol" runat="server" style="width: 1px">
                                                <b>:</b>&nbsp;
                                            </td>
                                            <td id="tdlblteardown" runat="server" align="left" width="150" style="height: 10px;">
                                                <asp:Label ID="lblTearDownTimeinMin" runat="server" Font-Bold="False"></asp:Label>
                                            </td>
                                        </tr>
                                        <%--ZD 101755 End--%>
                                        <tr id="trMUCPreTime" runat="server">
                                            <td align="left" class="blackblodtext" valign="top" nowrap style="height: 10px">
                                                <asp:Literal Text="<%$ Resources:WebResources, ManageConference_MCUPreStartTi%>"
                                                    runat="server"></asp:Literal>
                                            </td>
                                            <td style="width: 1px">
                                                <b>:</b>&nbsp;
                                            </td>
                                            <td align="left" width="150">
                                                <asp:Label ID="lblMCUPreStart" runat="server" Font-Bold="False"></asp:Label>
                                            </td>
                                            <td align="left" class="blackblodtext" valign="top" nowrap>
                                                <asp:Literal Text="<%$ Resources:WebResources, ManageConference_MCUPreEndTime%>"
                                                    runat="server"></asp:Literal>
                                            </td>
                                            <td style="width: 1px">
                                                <b>:</b>&nbsp;
                                            </td>
                                            <td align="left" width="150" style="height: 10px;">
                                                <asp:Label ID="lblMCUPreEnd" runat="server" Font-Bold="False"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <%--ZD 100085 End--%>
                                            <td align="left" valign="top" width="120" class="blackblodtext">
                                                <%--FB 2446--%>
                                                <asp:Literal Text="<%$ Resources:WebResources, ManageConference_Status%>" runat="server"></asp:Literal>
                                            </td>
                                            <td style="width: 1px">
                                                <b>:</b>&nbsp;
                                            </td>
                                            <td align="left" valign="top">
                                                <asp:Label ID="lblStatus" runat="server" Font-Bold="False"></asp:Label>
                                            </td>
                                            <%if(!(Application["Client"] == "MOJ")){ %>
                                            <td align="left" valign="top" class="blackblodtext" id="tdType" runat="server">
                                                <asp:Literal Text="<%$ Resources:WebResources, ManageConference_tdType%>" runat="server"></asp:Literal>
                                            </td>
                                            <td style="width: 1px">
                                                <b>:</b>&nbsp;
                                            </td>
                                            <td colspan="2" align="left">
                                                <asp:Label ID="lblConfType" runat="server" Font-Bold="false"></asp:Label>
                                            </td>
                                            <%}%>
                                        </tr>
                                        <tr id="trPuPw" runat="server">
                                            <%--Edited for MOJ Phase 2 QA--%>
                                            <%--FB 2446 - Start --%>
                                            <td id="tdpu" runat="server" align="left" valign="top" width="120" class="blackblodtext">
                                                <asp:Literal Text="<%$ Resources:WebResources, DashBoard_Public%>" runat="server"></asp:Literal>
                                            </td>
                                            <td id="tdpub" runat="server" style="width: 1px">
                                                <b>:</b>&nbsp;
                                            </td>
                                            <td id="tdpu1" runat="server" align="left" valign="top">
                                                <asp:Label ID="lblPublic" runat="server" Font-Bold="False"></asp:Label><asp:Label
                                                    ID="lblRegistration" runat="server" Font-Bold="False"></asp:Label>
                                            </td>
                                            <td id="tdpw" runat="server" align="left" class="blackblodtext">
                                                <asp:Literal Text="<%$ Resources:WebResources, DashBoard_Password%>" runat="server"></asp:Literal>
                                            </td>
                                            <td id="tdpwd" runat="server" style="width: 1px">
                                                <b>:</b>&nbsp;
                                            </td>
                                            <td id="tdpw1" runat="server" align="left" style="height: 21px;" colspan="2">
                                                <asp:Label ID="lblPassword" runat="server"></asp:Label>&nbsp;
                                            </td>
                                            <%--FB 2446 - End --%>
                                        </tr>
                                        <tr>
                                            <%--FB 1926--%>
                                            <td id="tdRemainder" runat="server" align="left" valign="top" width="120" class="blackblodtext">
                                                <%--FB 2446--%>
                                                <asp:Literal ID="Literal8" Text="<%$ Resources:WebResources, ManageConference_tdRemainder%>"
                                                    runat="server"></asp:Literal>
                                            </td>
                                            <td id="tdRemainder1" runat="server" style="width: 1px" valign="top">
                                                <b>:</b>&nbsp;
                                            </td>
                                            <td id="tdRemainderselection" runat="server" align="left" valign="top">
                                                <%--FB 2694--%>
                                                <asp:Label ID="lblReminders" runat="server" Font-Bold="False"></asp:Label>&nbsp;
                                            </td>
                                            <%--FB 2501 Starts--%>
                                            <td id="trVNOC" runat="server" align="left" class="blackblodtext" valign="top">
                                                <asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, DashBoard_trVNOC%>"
                                                    runat="server"></asp:Literal>
                                            </td>
                                            <td style="width: 1px" valign="top">
                                                <b>:</b>&nbsp;
                                            </td>
                                            <td id="trVNOCoptor" runat="server" align="left" style="height: 21px;" colspan="2"
                                                valign="top">
                                                <%--FB 2670--%>
                                                <asp:Label ID="lblConfVNOC" runat="server"></asp:Label>&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <%--FB 2595 Starts--%>
                                            <td id="tdStartMode" runat="server" align="left" class="blackblodtext" valign="top">
                                                <asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, DashBoard_tdStartMode%>"
                                                    runat="server"></asp:Literal>
                                            </td>
                                            <td id="tdStartMode1" runat="server" style="width: 1px">
                                                <b>:</b>&nbsp;
                                            </td>
                                            <td id="tdStartModeSelection" runat="server" align="left">
                                                <asp:Label ID="lblStartMode" runat="server"></asp:Label>&nbsp;
                                            </td>
                                            <td id="tdSecured" runat="server" align="left" class="blackblodtext" valign="top">
                                                <asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, DashBoard_tdSecured%>"
                                                    runat="server"></asp:Literal>
                                            </td>
                                            <td id="tdSecured1" runat="server" style="width: 1px">
                                                <b>:</b>&nbsp;
                                            </td>
                                            <td id="tdSecuredSelection" runat="server" align="left" style="height: 21px;" colspan="2"
                                                valign="top">
                                                <asp:Label ID="lblSecured" runat="server"></asp:Label>&nbsp;
                                            </td>
                                        </tr>
                                        <%--FB 2501 Ends--%><%--FB 2595 Ends--%>
                                        <tr id="trFle" runat="server">
                                            <%--Edited for MOJ Phase 2 QA--%>
                                            <td align="left" valign="top" width="120" class="blackblodtext">
                                                <%--FB 2446--%>
                                                <asp:Literal ID="Literal9" Text="<%$ Resources:WebResources, ManageConference_Files%>"
                                                    runat="server"></asp:Literal>
                                            </td>
                                            <td style="width: 1px">
                                                <b>:</b>&nbsp;
                                            </td>
                                            <td align="left" colspan="4" valign="top">
                                                <asp:Label ID="lblFiles" runat="server" Font-Bold="False"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" width="120" class="blackblodtext">
                                                <%--FB 2446--%>
                                                <asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, DashBoard_Description%>"
                                                    runat="server"></asp:Literal>
                                            </td>
                                            <td style="width: 1px" valign="top">
                                                <b>:</b>&nbsp;
                                            </td>
                                            <td align="left" colspan="6" valign="top">
                                                <%--FB 2508--%>
                                                <asp:Label ID="lblDescription" runat="server" Font-Bold="False"></asp:Label>&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <%--Window Dressing--%>
                                            <td align="right" colspan="8" valign="top" style="font-weight: bold" id="tdExpandCollapse"
                                                class="subtitleblueblodtext">
                                                <input id="chkExpandCollapse" type="checkbox" onclick="javascript:ExpandAll()" class="btprint" /><asp:Literal
                                                    ID="Literal10" Text="<%$ Resources:WebResources, ManageConference_tdExpandCollapse%>"
                                                    runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <%--FB 1985--%>
                            <%if(Application["Client"].ToString().ToUpper() == "DISNEY") {%>
                            <tr>
                                <td id="tblAVExpand" colspan="3" runat="server" align="center">
                                    <%--Disney New Requirement--%>
                                    <table border="0" align="center" width="80px">
                                        <tr>
                                            <td id="Td1" align="left" onmouseover="javascript:return fnShowHideAVLink('1');"
                                                onmouseout="javascript:return fnShowHideAVLink('0');" runat="server">
                                                &nbsp;
                                                <asp:LinkButton ID="LnkAVExpand" TabIndex="-1" Style="display: none" runat="server"
                                                    Text="Expand" OnClick="fnShowEndpoint"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <%} %>
                            <tr>
                                <td colspan="3" align="center">
                                    <asp:Menu ID="Menu22" Orientation="Horizontal" StaticMenuItemStyle-CssClass="tab"
                                        StaticSelectedStyle-CssClass="selectedTab" CssClass="tabs" ItemWrap="true" OnMenuItemClick="Menu22_MenuItemClick"
                                        SkipLinkText="" runat="server">
                                        <Items>
                                            <%--ZD 100429--%>
                                            <asp:MenuItem Text="<div align='center' valign='middle' style='width:123;' tabindex='-1' onclick='javascript:DataLoading(1);'></div><br>"
                                                Value="0" />
                                            <%--Edited for FF--%>
                                            <%--ZD 100369--%>
                                            <asp:MenuItem Text="<div align='center' valign='middle' style='width:123' onclick='javascript:DataLoading(1);'>Endpoints</div><br>"
                                                Value="1" />
                                            <%--Edited for FF--%>
                                            <asp:MenuItem Text="<div align='center' style='width:123'  onclick='javascript:DataLoading(1);'>Resource<br>Availability</div>"
                                                Value="2" Selected="true" />
                                            <%--Edited for FF--%>
                                            <%--Code Added For FB 1422 - New Menu Item For Point to point Endpoint--%>
                                            <asp:MenuItem Text="<div align='center' valign='middle' style='width:123' onclick='javascript:DataLoading(1);'>Point-To-Point</div><br>"
                                                Value="1" />
                                            <%--FB 2694--%><%--FB 2769--%>
                                        </Items>
                                    </asp:Menu>
                                    <%-- FB 2050 --%>
                                    <div class="tabContents" style="width: 90%">
                                        <asp:MultiView ID="MultiView1" runat="server">
                                            <asp:View ID="NormalView" runat="server">
                                                <asp:Panel runat="server" Width="100%" ID="pnlNormal">
                                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                        <tr>
                                                            <td align="center">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" colspan="2" class="subtitleblueblodtext">
                                                                            <a href="" onkeydown="if(event.keyCode == 13){document.getElementById('img_LOC').click();return false;}"
                                                                                onmouseup="if(!isIE){this.childNodes[0].click();return false;}">
                                                                                <%--ZD 100369 508 Issue--%>
                                                                                <img border="0" id='img_LOC' src="image/loc/nolines_minus.gif" alt="Expand/Collapse"
                                                                                    onclick="ShowHideRow('LOC', this,false);return false;" /></a>
                                                                            <asp:Literal Text="<%$ Resources:WebResources, ManageConference_Locations%>" runat="server"></asp:Literal>
                                                                            <asp:Label ID="lblLocCount" runat="server" Font-Bold="True"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="tr_LOC">
                                                                        <%--ZD 100419--%>
                                                                        <td width="5%">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td align="left" style="font-weight: bold;" valign="top"> <%--ZD 103216--%>
                                                                             <asp:DataGrid Width="80%" ID="dgRoomHost" runat="server" AutoGenerateColumns="False" Style="border-collapse: separate;" GridLines="None"
                                                                                OnItemDataBound="dgRoomHost_ItemDataBound">
                                                                                <Columns>
                                                                                    <asp:BoundColumn DataField="RoomName" HeaderText="" HeaderStyle-Width="45%">
                                                                                    </asp:BoundColumn>
                                                                                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, DashBoard_Host%>" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="10%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Image ID="imgHost"  runat="server" ImageUrl= "~/en/image/blue_button.png" Width="12px"/>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, NoofAttendees%>" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"  HeaderStyle-Width="14%">
                                                                                        <ItemTemplate>
                                                                                            <table width="100%" >
                                                                                                <tr>
                                                                                                    <td width="35%"></td>
                                                                                                    <td width="30%" align="right">
                                                                                                    <asp:Label ID="lblNoofAttendees" runat="server" Font-Bold="false"></asp:Label>                                                                                                
                                                                                                    </td>
                                                                                                    <td width="35%"></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate> 
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderStyle-Width="35%">
                                                                                        <ItemTemplate>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                            <asp:Label ID="lblLocation" runat="server" Font-Bold="False"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr id="trPrt" runat="server">
                                                            <%--Edited for FB 1425 QA Bug--%>
                                                            <td align="center">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" colspan="4" class="subtitleblueblodtext">
                                                                            <a href="" onkeydown="if(event.keyCode == 13){document.getElementById('img_PAR').click();return false;}"
                                                                                onmouseup="if(!isIE){this.childNodes[0].click();return false;}">
                                                                                <%--ZD 100369 508 Issue--%>
                                                                                <img border="0" id='img_PAR' src="image/loc/nolines_minus.gif" alt="Expand/Collapse"
                                                                                    onclick="ShowHideRow('PAR', this,false);return false;" /></a>
                                                                            <asp:Literal ID="Literal41" Text="<%$ Resources:WebResources, ManageConference_Participants%>"
                                                                                runat="server"></asp:Literal><asp:Label ID="lblPartyCount" runat="server" Font-Bold="True"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="tr_PAR">
                                                                        <%--ZD 100419--%>
                                                                        <td align="center" style="font-weight: bold;" colspan="4" rowspan="3">
                                                                            <asp:Table ID="tblNoParty" runat="server" CellPadding="0" CellSpacing="0" Visible="False"
                                                                                Width="90%" BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px">
                                                                                <asp:TableRow ID="TableRow6" runat="server" CssClass="tableHeader" Height="30px">
                                                                                    <asp:TableCell ID="TableCell7" runat="server" CssClass="tableHeader" Text="<%$ Resources:WebResources, ConferenceOrders_Name%>"></asp:TableCell>
                                                                                    <asp:TableCell ID="TableCell8" runat="server" CssClass="tableHeader" Text="<%$ Resources:WebResources, ConferenceList_Email%>"></asp:TableCell>
                                                                                    <asp:TableCell ID="TableCell9" runat="server" CssClass="tableHeader" Text="<%$ Resources:WebResources, ManageConference_Status%>"></asp:TableCell></asp:TableRow>
                                                                                <%--Window Dressing--%>
                                                                                <asp:TableRow ID="TableRow7" runat="server" CssClass="lblError" Height="30px" HorizontalAlign="Center"
                                                                                    VerticalAlign="Middle">
                                                                                    <asp:TableCell ID="TableCell10" runat="server" ColumnSpan="3" CssClass="lblError">
                                                                                        <%if (Application["Client"] == "MOJ"){%>There are no participants in this hearing.<%}else{ %><asp:Literal
                                                                                            ID="Literal42" Text="<%$ Resources:WebResources, ManageConference_NoParty%>"
                                                                                            runat="server"></asp:Literal><%} %></asp:TableCell><%--Edited  For FB 1428--%></asp:TableRow>
                                                                            </asp:Table>
                                                                            <%--FB 2023--%>
                                                                            <asp:DataGrid runat="server" ID="partyGrid" OnSortCommand="SortGrid" AllowSorting="True"
                                                                                OnItemDataBound="partyGridBound" BorderColor="Blue" CellPadding="4" Font-Bold="False"
                                                                                AutoGenerateColumns="false" ForeColor="#333333" GridLines="None" Width="90%"
                                                                                BorderStyle="Solid" BorderWidth="1px" OnEditCommand="SendReminderToParticipant"
                                                                                Style="border-collapse: separate">
                                                                                <%--Edited for FF--%>
                                                                                <AlternatingItemStyle CssClass="tableBody" />
                                                                                <ItemStyle CssClass="tableBody" />
                                                                                <HeaderStyle CssClass="tableHeader" Height="30px" />
                                                                                <Columns>
                                                                                    <asp:BoundColumn DataField="partyID" Visible="false"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="partyFirstName" HeaderText="<%$ Resources:WebResources, ConferenceList_FirstName%>"
                                                                                        ItemStyle-CssClass="tableBody">
                                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="partyLastName" HeaderText="<%$ Resources:WebResources, ConferenceList_LastName%>"
                                                                                        ItemStyle-CssClass="tableBody">
                                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="partyEmail" HeaderText="<%$ Resources:WebResources, ConferenceList_Email%>"
                                                                                        ItemStyle-CssClass="tableBody">
                                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="partyStatus" HeaderText="<%$ Resources:WebResources, ManageConference_Status%>"
                                                                                        ItemStyle-CssClass="tableBody">
                                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="partyInvite" HeaderText="<%$ Resources:WebResources, ManageConference_InvitedAs%>"
                                                                                        ItemStyle-CssClass="tableBody">
                                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:ButtonColumn ButtonType="LinkButton" HeaderText="<%$ Resources:WebResources, ManageConference_Invitation%>"
                                                                                        CausesValidation="true" CommandName="Edit" Text="<%$ Resources:WebResources, ManageConference_ResendInvitati%>"
                                                                                        ItemStyle-CssClass="Link"></asp:ButtonColumn>
                                                                                </Columns>
                                                                                <%--ZD 100425--%>
                                                                            </asp:DataGrid>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <%--ZD 101098 START--%>
                                                                <table id="tblAttendancelist" runat="server" width="100%" border="0" cellpadding="0"
                                                                    cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" colspan="4" class="subtitleblueblodtext">
                                                                            <a href="" onkeydown="if(event.keyCode == 13){document.getElementById('img_ICONTROL').click();return false;}"
                                                                                onmouseup="if(!isIE){this.childNodes[0].click();return false;}">
                                                                                <img border="0" id='img_ICONTROL' src="image/loc/nolines_minus.gif" alt="Expand/Collapse"
                                                                                    onclick="ShowHideRow('ICONTROL', this,false);return false;" /></a> Attendance
                                                                            List
                                                                            <asp:Label ID="Label1" runat="server" Font-Bold="True"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="tr_ICONTROL">
                                                                        <td align="center" style="font-weight: bold;" colspan="4" rowspan="3">
                                                                            <asp:DataGrid runat="server" ID="partyattendancelistGrid" AllowSorting="True" BorderColor="Blue"
                                                                                CellPadding="4" Font-Bold="False" AutoGenerateColumns="false" ForeColor="#333333"
                                                                                GridLines="None" Width="90%" BorderStyle="Solid" BorderWidth="1px" Style="border-collapse: separate">
                                                                                <%--Edited for FF--%>
                                                                                <AlternatingItemStyle CssClass="tableBody" />
                                                                                <ItemStyle CssClass="tableBody" />
                                                                                <HeaderStyle CssClass="tableHeader" Height="30px" />
                                                                                <Columns>
                                                                                    <asp:BoundColumn DataField="partyID" Visible="false"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="partyFirstName" HeaderText="<%$ Resources:WebResources, ConferenceList_FirstName%>"
                                                                                        ItemStyle-CssClass="tableBody">
                                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="partyLastName" HeaderText="<%$ Resources:WebResources, ConferenceList_LastName%>"
                                                                                        ItemStyle-CssClass="tableBody">
                                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="partyEmail" HeaderText="<%$ Resources:WebResources, ConferenceList_Email%>"
                                                                                        ItemStyle-CssClass="tableBody">
                                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="partyConfdate" HeaderText="<%$ Resources:WebResources, Manage_iControlRoomLoginTime%>"
                                                                                        ItemStyle-CssClass="tableBody">
                                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="partyAttended" Visible="false"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="partyRoom" HeaderText="<%$ Resources:WebResources, ConferenceSetup_Room%>"
                                                                                        ItemStyle-CssClass="tableBody">
                                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                                    </asp:BoundColumn>
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <%--ZD 101098 END--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </asp:View>
                                            <asp:View ID="EndpointView" runat="server">
                                                <asp:Panel ID="pnlEndpoint" runat="server" Width="100%" Style="font-weight: bold;">
                                                    <asp:Table runat="server" ID="tblTerminalControl" Width="100%" Visible="false">
                                                        <asp:TableRow>
                                                            <asp:TableCell HorizontalAlign="center">
                                                                <table width="90%" border="0">
                                                                    <tr>
                                                                        <%-- Window Dressing--%>
                                                                        <td align="left" style="width: 10%" class="blackblodtext" runat="server" id="tdEVideoLayout"> <%--ZD 101931--%>
                                                                            <asp:Literal ID="Literal46" Text="<%$ Resources:WebResources, ManageConference_DisplayLayout%>"
                                                                                runat="server"></asp:Literal>
                                                                        </td>
                                                                        <%-- Code Modified FB 1400 - Changed allignments.Added TD tag - Start--%>
                                                                        <td align="center" valign="middle" style="width: 70px" nowrap="nowrap" runat="server" id="tdVideoLayout"> <%--ZD 101931--%>
                                                                            <asp:Label runat="server" ID="lblCodianLO" class="blackblodtext"></asp:Label>
                                                                            <asp:Image ID="imgVideoLayout" runat="server" Width="30" Height="30" AlternateText="VideoLayout" />
                                                                        </td>
                                                                        <%--ZD 101388--%>
                                                                        <td align="left" runat="server" id="tdEndpointLayout"> <%--ZD 101931--%>
                                                                            <input type="button" id="ConfLayoutSubmit" name="ConfLayoutSubmit" runat="server"
                                                                                value="<%$ Resources:WebResources, ManageConference_btnChangeEndpointLayout%>"
                                                                                class="altMedium0BlueButtonFormat" onclick="javascript:managelayout('', '01', '');" />
                                                                        </td>
                                                                        <%--ZD 101294--%>
                                                                        <%-- Code Modified FB 1400 - End--%>
                                                                        <%-- Window Dressing--%>
                                                                        <td align="right" class="blackblodtext" id="tdExndTmlbl" runat="server"> <%--ZD 102997--%>
                                                                            <asp:Literal Text="<%$ Resources:WebResources, ManageConference_ExtendEndTime %>"
                                                                                runat="server"></asp:Literal>
                                                                        </td>
                                                                        <td nowrap="nowrap" id="tdExndTm" runat="server"> <%--ZD 102997--%>
                                                                            <%--Modified during FB 1562--%>
                                                                            <asp:TextBox ID="txtExtendedTime" CssClass="altText" Width="100px" runat="server" ValidationGroup="SubmitTime"></asp:TextBox>&nbsp;
                                                                            <%-- Code Modified FB 1400 - Start--%>
                                                                            <asp:Button ID="btnExtendEndtime" CssClass="altMedium0BlueButtonFormat" OnClick="ExtendEndtime"
                                                                                ValidationGroup="SubmitTime" Text="<%$ Resources:WebResources, Submit %>" runat="server"
                                                                                OnClientClick="javascript:return fnValidateEndTime('m');" /><%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SubmitTime" ControlToValidate="txtExtendedTime"  ErrorMessage="<br>" runat="server" Display="dynamic"></asp:RequiredFieldValidator>--%>
                                                                            <br />
                                                                            <asp:Label ID="LblExtendedTimeMsg" Style="visibility: hidden" Text="" ForeColor="red"
                                                                                runat="server"></asp:Label><%-- Code Modified FB 1400 - End--%>
                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="SubmitTime"
                                                                                ControlToValidate="txtExtendedTime" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly2%>"
                                                                                ValidationExpression="\d+" runat="server" Display="dynamic"></asp:RegularExpressionValidator><asp:RangeValidator
                                                                                    ID="RangeValidator1" ValidationGroup="SubmitTime" ControlToValidate="txtExtendedTime"
                                                                                    ErrorMessage="<%$ Resources:WebResources, ExtendtimeRange%>" MinimumValue="0"
                                                                                    MaximumValue="360" Type="Integer" runat="server" Display="dynamic"></asp:RangeValidator>
                                                                        </td>
                                                                        <%-- Window Dressing--%>
                                                                        <td align="right" class="blackblodtext">
                                                                            <%--ZD 101233--%>
                                                                            <asp:Literal ID="Literal48" Text="<%$ Resources:WebResources, DashBoard_AutoRefresh30%>"
                                                                                runat="server"></asp:Literal>
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkAutoRefresh" runat="server" Checked="true" onclick="javascript:UpdateEndpointStatus();" />
                                                                        </td>
                                                                    </tr>
                                                                    <%--FB 2441 Starts--%>
                                                                    <tr>
                                                                        <td colspan="5" align="left" class="blackblodtext">
                                                                            <asp:LinkButton ID="lnkMuteAllExcept" Text="Mute All Party Except" runat="server"
                                                                                Visible="false" OnClientClick="javascript:return fnShowHide('1')"></asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                            <asp:LinkButton ID="lnkUnMuteAllParties" Enabled="true" runat="server" Text="UnMute All Parties"
                                                                                Visible="false" OnClick="UnMuteAllParties"></asp:LinkButton>&nbsp;&nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <%--FB 2441 Ends--%>
                                                                </table>
                                                            </asp:TableCell></asp:TableRow>
                                                    </asp:Table>
                                                    <asp:Table runat="server" ID="tblEndpoints" Width="100%">
                                                        <%--ZD 100419 ZD 100528--%>
                                                        <%--  Code Modified For FB 1699--%>
                                                        <asp:TableRow>
                                                            <asp:TableCell HorizontalAlign="center">
                                                                <%--  Code Modified For FB 1367-  Nathira - Start--%>
                                                                <asp:DataGrid runat="server" EnableViewState="true" OnItemDataBound="InitializeEndpoints"
                                                                    ID="dgEndpoints" AutoGenerateColumns="false" Style="border-collapse: separate"
                                                                    CellSpacing="0" CellPadding="4" GridLines="None" ShowFooter="true" OnEditCommand="EditEndpoint"
                                                                    OnDeleteCommand="DeleteTerminal" OnUpdateCommand="ConnectEndpoint" OnCancelCommand="MuteEndpoint"
                                                                    BorderColor="blue" BorderStyle="solid" BorderWidth="1" Width="90%">
                                                                    <%--Edited for FF--%>
                                                                    <%--  Code Modified For FB 1367-  Nathira - End--%>
                                                                    <AlternatingItemStyle CssClass="tableBody" />
                                                                    <ItemStyle CssClass="tableBody" />
                                                                    <FooterStyle CssClass="tableBody" />
                                                                    <HeaderStyle CssClass="tableHeader" Height="30px" />
                                                                    <%-- Window Dressing start--%>
                                                                    <EditItemStyle CssClass="tableBody" />
                                                                    <SelectedItemStyle CssClass="tableBody" Font-Bold="True" />
                                                                    <%-- Window Dressing end--%>
                                                                    <Columns>
                                                                        <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="type" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="displayLayout" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="mute" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="ImageURL" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Name" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center"
                                                                            HeaderText="<%$ Resources:WebResources, ManageConference_RmAttendee%>" HeaderStyle-CssClass="tableHeader"
                                                                            ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="EndpointName" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center"
                                                                            HeaderText="<%$ Resources:WebResources, ManageConference_EptName%>" HeaderStyle-CssClass="tableHeader"
                                                                            ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                                        <%-- Code Modified For FB 1371 - Removed datafeild=status - Nathira - Start--%>
                                                                        <asp:BoundColumn ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center"
                                                                            HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, ManageConference_EptStatus%>"
                                                                            Visible="false"></asp:BoundColumn>
                                                                        <%-- Code Modified For FB 1371 - Removed datafeild=status - Nathira - End--%>
                                                                        <asp:BoundColumn DataField="address" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center"
                                                                            HeaderText="<%$ Resources:WebResources, Address%>" HeaderStyle-CssClass="tableHeader"
                                                                            ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="addressType" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center"
                                                                            HeaderText="<%$ Resources:WebResources, ManageConference_AddressType%>" HeaderStyle-CssClass="tableHeader"
                                                                            ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="connectionType" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center"
                                                                            HeaderText="<%$ Resources:WebResources, ManageConference_ConnType%>" HeaderStyle-CssClass="tableHeader"
                                                                            ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="MCUName" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center"
                                                                            HeaderText="<%$ Resources:WebResources, ReportDetails_MCU%>" HeaderStyle-CssClass="tableHeader"
                                                                            ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="BridgeAddress" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center"
                                                                            HeaderText="<%$ Resources:WebResources, ManageConference_MCUAddress%>" HeaderStyle-CssClass="tableHeader"
                                                                            ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="BridgeAddressType" ItemStyle-HorizontalAlign="center"
                                                                            HeaderStyle-HorizontalAlign="center" HeaderText="<%$ Resources:WebResources, ManageConference_MCUAddressType%>"
                                                                            HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="DefaultProtocol" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center"
                                                                            HeaderText="<%$ Resources:WebResources, AddNewEndpoint_Protocol%>" HeaderStyle-CssClass="tableHeader"
                                                                            ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="EptOnlineStatus" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center"
                                                                            HeaderText="<%$ Resources:WebResources, ManageConference_EM7%>" HeaderStyle-CssClass="tableHeader"
                                                                            ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                                        <%--FB 2501 EM7--%>
                                                                        <asp:BoundColumn DataField="BridgeProfileName" ItemStyle-HorizontalAlign="center"
                                                                            HeaderStyle-HorizontalAlign="Center" HeaderText="<%$ Resources:WebResources, ManageConference_MCUProfile%>"
                                                                            HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody">
                                                                            <%--ZD 100298--%>
                                                                        </asp:BoundColumn>
                                                                        <%--ZD 104256 Starts--%>
                                                                         <asp:BoundColumn DataField="BridgePoolOrderName" ItemStyle-HorizontalAlign="center"
                                                                            HeaderStyle-HorizontalAlign="Center" HeaderText="<%$ Resources:WebResources, PoolOrder%>"
                                                                            HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody">
                                                                        </asp:BoundColumn>
                                                                        <%--ZD 104256 Ends--%>
                                                                        <%--FB 2839--%><asp:TemplateColumn HeaderText="<%$ Resources:WebResources, ManageConference_DisplayLayout%>"
                                                                            ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center" Visible="false"
                                                                            HeaderStyle-CssClass="tableHeader">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblisTelepresence" Style="visibility: hidden" Text='<%# DataBinder.Eval(Container, "DataItem.IsTelepresence") %>'
                                                                                    runat="server"></asp:Label><%--FB 2400--%>
                                                                                <asp:Image ID="imgVideoLayout" runat="server" AlternateText="Video Layout" Width="30"
                                                                                    Height="30" ImageUrl='<%# DataBinder.Eval(Container, "DataItem.ImageURL") %>'
                                                                                    Visible='<%# DataBinder.Eval(Container, "DataItem.type").ToString().Trim() != "2" %>' />
                                                                                <%--ZD 100419--%>
                                                                                <asp:Button CssClass="altMedium0BlueButtonFormat" ID="btnChangeEndpointLayout" runat="server"
                                                                                    Text="<%$ Resources:WebResources, ManageConference_btnChangeEndpointLayout%>" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn ItemStyle-HorizontalAlign="center" HeaderStyle-Width="100" ItemStyle-Width="100"
                                                                            HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, ManageConference_Actions%>"
                                                                            HeaderStyle-HorizontalAlign="center" FooterStyle-HorizontalAlign="left">
                                                                            <ItemTemplate>
                                                                                <table cellspacing="5" width="100%" border="0">
                                                                                    <tr width="100%" align="center">
                                                                                        <%--  Code Added For FB 1367- Nathira - Start--%><%--FB 3055 Start--%>
                                                                                        <%--ZD 100672- START--%>
                                                                                        <td>
                                                                                            <asp:LinkButton ID="btnCon" CommandName="Update" Text="<%$ Resources:WebResources, ManageConference_btnCon%>"
                                                                                                runat="server" Visible="false"></asp:LinkButton>
                                                                                        </td>
                                                                                        <%--ZD 100602--%>
                                                                                        <%--  Code Added For FB 1367-  Nathira - End--%>
                                                                                        <td>
                                                                                            <asp:LinkButton ID="btnEdit" Text="<%$ Resources:WebResources, ManageConference_btnEdit%>"
                                                                                                CommandName="Edit" runat="server" Visible='<%# (Request.QueryString["t"] != "hf") %>'></asp:LinkButton>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:LinkButton ID="btnMute" Text="<%$ Resources:WebResources, ManageConference_btnMute%>"
                                                                                                Visible="false" CommandName="Cancel" runat="server"></asp:LinkButton>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:LinkButton ID="btnDelete" Text="<%$ Resources:WebResources, ManageConference_btnDelete%>"
                                                                                                CommandName="Delete" runat="server" Visible='<%# (Request.QueryString["t"] != "hf") %>'></asp:LinkButton>
                                                                                        </td>
                                                                                        <%--ZD 100672- END--%>
                                                                                    </tr>
                                                                                </table>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                <asp:Button ID="Button1" CssClass="altLongBlueButtonFormat" Text="<%$ Resources:WebResources, ManageConference_btnAddNewEndpoint%>"
                                                                                    runat="server" OnClick="AddNewEndpoint" style="width:245px;" /><%--FB 3055 End--%>
                                                                                <%--ZD 100672--%><%--ZD 100602--%>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <%--FB 1650--%>
                                                                        <asp:BoundColumn DataField="CascadeLinkId" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="EndPointStatus" Visible="false"></asp:BoundColumn>
                                                                    </Columns>
                                                                </asp:DataGrid></asp:TableCell></asp:TableRow>
                                                    </asp:Table>
                                                    <asp:DropDownList CssClass="altLong0SelectFormat" Visible="false" ID="lstAddressType"
                                                        runat="server" DataTextField="Name" DataValueField="ID">
                                                    </asp:DropDownList>
                                                    <asp:Table runat="server" ID="tblP2PEndpoints" Width="100%" Visible="false">
                                                        <asp:TableRow>
                                                            <asp:TableCell HorizontalAlign="center" runat="server" Visible="false" ID="refreshCell">
                                                                <table width="90%" border="0">
                                                                    <tr>
                                                                        <td id="tdmessage" runat="server" align="left" class="blackblodtext">
                                                                            Broadcast Message
                                                                        </td>
                                                                        <td id="tdsendmessage" runat="server">
                                                                            <input type="text" class="altText" onkeyup="javascript:chkLimit(this,'50');" id="TxtMessageBoxAll"
                                                                                runat="server" />&nbsp;
                                                                            <asp:Button ID="SendMsgAll" CssClass="altMedium0BlueButtonFormat" Width="80px" Text="Send"
                                                                                runat="server" OnClientClick="javascript:return fnchkValue('TxtMessageBoxAll');"
                                                                                OnClick="BroadCastP2PMsg" />
                                                                        </td>
                                                                        <td align="left" class="blackblodtext" nowrap>
                                                                            <%--FB 1562--%>
                                                                            <asp:Literal ID="Literal51" Text="<%$ Resources:WebResources,  ManageConference_ExtendEndTime%>"
                                                                                runat="server"></asp:Literal>
                                                                        </td>
                                                                        <td nowrap>
                                                                            <asp:TextBox ID="txtPExtTime" CssClass="altText" runat="server" Width="50px" ValidationGroup="SubmitTime"></asp:TextBox>&nbsp;
                                                                            <asp:Button ID="btnPExtEndtime" CssClass="altMedium0BlueButtonFormat" Width="80px"
                                                                                OnClick="ExtendEndtime" ValidationGroup="SubmitTime" Text="<%$ Resources:WebResources, DashBoard_btnExtendEndtime%>"
                                                                                runat="server" OnClientClick="javascript:return fnValidateEndTime('p');" /><br />
                                                                            <asp:Label ID="LblPExtTimeMsg" Style="visibility: hidden" Text="" ForeColor="red"
                                                                                runat="server"></asp:Label><asp:RegularExpressionValidator ID="RegPExtTime" ValidationGroup="SubmitTime"
                                                                                    ControlToValidate="txtPExtTime" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly2%>"
                                                                                    ValidationExpression="\d+" runat="server" Display="dynamic"></asp:RegularExpressionValidator><asp:RangeValidator
                                                                                        ID="RangePExtTime" ValidationGroup="SubmitTime" ControlToValidate="txtPExtTime"
                                                                                        ErrorMessage="<%$ Resources:WebResources, ExtendtimeRange%>" MinimumValue="0"
                                                                                        MaximumValue="360" Type="Integer" runat="server" Display="dynamic"></asp:RangeValidator>
                                                                        </td>
                                                                        <td align="left" class="blackblodtext">
                                                                            <asp:Literal Text="<%$ Resources:WebResources,  DashBoard_AutoRefresh30%>" runat="server"></asp:Literal>
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="P2pAutoRef" runat="server" Checked="true" onclick="javascript:UpdateP2PEndpointStatus();" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:TableCell></asp:TableRow>
                                                        <asp:TableRow>
                                                            <%--ZD 100528--%>
                                                            <%--ZD 101133--%>
                                                            <asp:TableCell HorizontalAlign="center">
                                                                <%--Code Modified For FB 1422 - To display Point to point End Point Details - Start--%>
                                                                <asp:DataGrid runat="server" EnableViewState="true" ID="dgP2PEndpoints" AutoGenerateColumns="false"
                                                                    OnCancelCommand="SendP2PMessage" OnItemDataBound="InitializeP2PEndpoints" Style="border-collapse: separate"
                                                                    CellSpacing="0" CellPadding="4" GridLines="None" BorderColor="blue" BorderStyle="solid"
                                                                    BorderWidth="1" Width="90%" OnUpdateCommand="ConnectP2PEndpoint">
                                                                    <%--Edited for FF--%>
                                                                    <%-- Window Dressing start--%>
                                                                    <SelectedItemStyle CssClass="tableBody" Font-Bold="True" />
                                                                    <AlternatingItemStyle CssClass="tableBody" />
                                                                    <ItemStyle CssClass="tableBody" />
                                                                    <HeaderStyle CssClass="tableHeader" Height="30px" />
                                                                    <EditItemStyle CssClass="tableBody" />
                                                                    <Columns>
                                                                        <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="type" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="endpointID" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="name" HeaderStyle-CssClass="tableHeader" Visible="true"
                                                                            HeaderText="<%$ Resources:WebResources, ManageConference_RmAttendee%>"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="EndpointName" HeaderStyle-CssClass="tableHeader" Visible="true"
                                                                            HeaderText="<%$ Resources:WebResources, ManageConference_EptName%>"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="address" HeaderStyle-CssClass="tableHeader" Visible="false">
                                                                        </asp:BoundColumn>
                                                                        <asp:TemplateColumn ItemStyle-HorizontalAlign="center" HeaderStyle-Width="100" ItemStyle-Width="100"
                                                                            HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, AddNewEndpoint_Address%>"
                                                                            HeaderStyle-HorizontalAlign="center" FooterStyle-HorizontalAlign="left">
                                                                            <ItemTemplate>
                                                                                <asp:HyperLink Style="cursor: hand;" ID="EptWebsite" runat="server"></asp:HyperLink>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:BoundColumn DataField="addressType" HeaderStyle-CssClass="tableHeader" Visible="true"
                                                                            HeaderText="<%$ Resources:WebResources, ManageConference_AddressType%>"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="DefaultProtocol" HeaderStyle-CssClass="tableHeader" Visible="true"
                                                                            HeaderText="<%$ Resources:WebResources, AddNewEndpoint_Protocol%>"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Connect2" HeaderStyle-CssClass="tableHeader" Visible="true"
                                                                            HeaderText="<%$ Resources:WebResources, ManageConference_CallerCallee%>"></asp:BoundColumn>
                                                                        <asp:BoundColumn ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center"
                                                                            HeaderText="<%$ Resources:WebResources, ManageConference_EptStatus%>" HeaderStyle-CssClass="tableHeader">
                                                                        </asp:BoundColumn>
                                                                        <%-- Window Dressing end--%>
                                                                        <asp:TemplateColumn ItemStyle-HorizontalAlign="center" HeaderStyle-Width="170px"
                                                                            ItemStyle-Width="100" HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, InventoryManagement_Actions%>"
                                                                            HeaderStyle-HorizontalAlign="center" FooterStyle-HorizontalAlign="left">
                                                                            <ItemTemplate>
                                                                                <table cellspacing="5" width="160px" border="0">
                                                                                    <tr width="100%" align="center">
                                                                                        <td>
                                                                                            <asp:LinkButton ID="btnConP2P" CommandName="Update" Text="<%$ Resources:WebResources, ManageConference_btnCon%>"
                                                                                                runat="server"></asp:LinkButton>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:HyperLink ID="btnMessage" Style="cursor: hand;" Text="<%$ Resources:WebResources, ManageConference_btnMessage%>"
                                                                                                runat="server"></asp:HyperLink>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:HyperLink ID="EptMonitor" Style="cursor: hand;" Text="<%$ Resources:WebResources, ManageConference_EptMonitor%>"
                                                                                                runat="server"></asp:HyperLink>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="Messagediv" runat="server" style="display: none; z-index: 999; cursor: hand">
                                                                                        <td colspan="2" width="120px">
                                                                                            <div>
                                                                                                <table cellspacing="5" border="0">
                                                                                                    <tr width="100%" align="center">
                                                                                                        <td>
                                                                                                            <input type="text" height="35px" onkeyup="javascript:chkLimit(this,'50');" id="TxtMessageBox"
                                                                                                                runat="server" />
                                                                                                            <asp:Button ID="SendMsg" CssClass="altMedium0BlueButtonFormat" Text="<%$ Resources:WebResources, ManageConference_SendMsg%>"
                                                                                                                runat="server" CommandName="Cancel" />
                                                                                                            <%--<button id="btnClose" runat="server" onclick="javascript:fnOpenMsg('0')" class="altMedium0BlueButtonFormat" >Close </button>--%>
                                                                                                            <input type="button" id="btnClose" runat="server" onclick="javascript:fnOpenMsg('0')"
                                                                                                                class="altMedium0BlueButtonFormat" value="<%$ Resources:WebResources, ManageCustomAttribute_btnCancel%>"><%--FB 1562--%>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <%--Blue Status Project START--%>
                                                                        <asp:BoundColumn DataField="EndPointStatus" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="remoteEndpoint" Visible="false"></asp:BoundColumn>
                                                                        <%--Blue Status Project End--%>
                                                                        <asp:BoundColumn DataField="EptOnlineStatus" HeaderStyle-CssClass="tableHeader" Visible="true"
                                                                            HeaderText="<%$ Resources:WebResources, ManageConference_EM7%>"></asp:BoundColumn>
                                                                    </Columns>
                                                                </asp:DataGrid><%--Code Modified Fo FB 1422 End--%>
                                                            </asp:TableCell></asp:TableRow>
                                                    </asp:Table>
                                                    <h5>
                                                        <asp:Literal ID="Literal40" Text="<%$ Resources:WebResources, ManageConference_Alerts%>"
                                                            runat="server"></asp:Literal></h5>
                                                    <asp:Table runat="server" ID="tblNoEndpoints" Visible="false">
                                                        <asp:TableRow>
                                                            <asp:TableCell CssClass="lblError" Text="<%$ Resources:WebResources, EndpointList_lblNoEndpoints%>"></asp:TableCell></asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>
                                                                <asp:Button ID="btnAddNewEndpoint" CssClass="altLongBlueButtonFormat" Text="<%$ Resources:WebResources, ManageConference_btnAddNewEndpoint%>"
                                                                    runat="server" OnClick="AddNewEndpoint"  style="width:245px;"/></asp:TableCell></asp:TableRow>
                                                    </asp:Table>
                                                    <asp:Table ID="tblAlerts" Visible="false" runat="server" Width="100%">
                                                        <asp:TableRow>
                                                            <asp:TableCell HorizontalAlign="center">
                                                                <asp:DataGrid runat="server" EnableViewState="true" ID="dgAlerts" AutoGenerateColumns="false"
                                                                    Style="border-collapse: separate" CellSpacing="0" CellPadding="4" GridLines="None"
                                                                    BorderColor="blue" BorderStyle="solid" BorderWidth="1" Width="90%">
                                                                    <%--Edited for FF--%>
                                                                    <%--Window Dressing--%>
                                                                    <SelectedItemStyle CssClass="tableBody" Font-Bold="True" />
                                                                    <AlternatingItemStyle CssClass="tableBody" />
                                                                    <ItemStyle CssClass="tableBody" />
                                                                    <HeaderStyle CssClass="tableHeader" Height="30px" />
                                                                    <%--Window Dressing--%>
                                                                    <EditItemStyle CssClass="tableBody" />
                                                                    <Columns>
                                                                        <asp:BoundColumn DataField="AlertID" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Message" HeaderStyle-CssClass="tableHeader" Visible="true"
                                                                            HeaderText="<%$ Resources:WebResources, approvalstatus_Message%>"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Timestamp" HeaderStyle-CssClass="tableHeader" Visible="true"
                                                                            HeaderText="<%$ Resources:WebResources, TimeStamp%>"></asp:BoundColumn>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                                <asp:Label ID="lblNoAlerts" Text="<%$ Resources:WebResources, ManageConference_lblNoAlerts%>"
                                                                    CssClass="lblError" Visible="false" runat="server"></asp:Label></asp:TableCell></asp:TableRow>
                                                    </asp:Table>
                                                    <asp:TextBox ID="ImageFiles" runat="server" Text="" Width="0" Height="0" Style="display: none"></asp:TextBox><%--Edited for FF--%>
                                                    <asp:TextBox ID="ImageFilesBT" runat="server" Text="" Width="0" Height="0" Style="display: none"></asp:TextBox><%--Edited for FF--%>
                                                    <asp:TextBox ID="ImagesPath" runat="server" Text="" Width="0" Height="0" Style="display: none"></asp:TextBox><%--Edited for FF--%>
                                                    <asp:TextBox ID="txtSelectedImage" runat="server" Text="01" Width="0" Height="0"
                                                        Style="display: none"></asp:TextBox><%--Edited for FF--%>
                                                    <asp:TextBox ID="txtSelectedImageEP" runat="server" Text="01" Width="0" Height="0"
                                                        Style="display: none"></asp:TextBox><%--Edited for FF--%>
                                                    <asp:TextBox ID="txtTempImage" runat="server" Text="01" Width="0" Height="0" Style="display: none"></asp:TextBox><%--Edited for FF--%>
                                                        </asp:Panel>
                                            </asp:View>
                                            <asp:View ID="ResourceView" runat="server">
                                                <asp:Panel ID="pnlResource" runat="server" Width="100%" Height="120%">
                                                    <%--FB 1982--%>
                                                    <table width="100%">
                                                        <tr id="trAVCommonSettings" runat="server">
                                                            <td align="center">
                                                                <asp:DataGrid ID="dgBridgeResources" GridLines="none" AutoGenerateColumns="false"
                                                                    runat="server" Width="90%" Style="border-collapse: separate">
                                                                    <%--Edited for FF--%>
                                                                    <AlternatingItemStyle CssClass="tableBody" />
                                                                    <ItemStyle CssClass="tableBody" />
                                                                    <HeaderStyle CssClass="tableHeader" Height="30px" />
                                                                    <%--Window Dressing--%>
                                                                    <EditItemStyle CssClass="tableBody" />
                                                                    <Columns>
                                                                        <asp:BoundColumn DataField="BridgeID" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="BridgeName" HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, ManageConference_BridgeName%>"
                                                                            ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                        </asp:BoundColumn>
                                                                        <%-- FB 2050 --%>
                                                                        <asp:BoundColumn DataField="AudioPorts" HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, SufficientAudioOnlyPorts%>"
                                                                            Visible="true" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="VideoPorts" HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, ManageConference_AVPorts%>"
                                                                            Visible="true" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="CardsAvailable" HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, ManageConference_Ports%>"
                                                                            Visible="false" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Description" HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, ManageConference_Description%>"
                                                                            Visible="true" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                                <asp:DropDownList ID="lstBridges" runat="server" DataTextField="BridgeName" DataValueField="BridgeID"
                                                                    Visible="false">
                                                                </asp:DropDownList>
                                                                <%-- SelectedValue='<%# DataBinder.Eval(Container, "DataItem.BridgeID") %>'--%>
                                                                <asp:DropDownList runat="server" ID="lstBridgeCountV" DataTextField="BridgeID" DataValueField="BridgeCount"
                                                                    Visible="false">
                                                                </asp:DropDownList>
                                                                <asp:DropDownList runat="server" ID="lstBridgeCountA" DataTextField="BridgeID" DataValueField="BridgeCount"
                                                                    Visible="false">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr id="trp2pLinerate" runat="server" style="display: none;">
                                                            <td align="left" valign="top">
                                                                <span class="blackblodtext">
                                                                    <asp:Literal ID="Literal38" Text="<%$ Resources:WebResources, ManageConference_MaximumLineRa%>"
                                                                        runat="server"></asp:Literal></span>
                                                                <asp:Label ID="LblLineRate" runat="server" Font-Bold="False"></asp:Label>
                                                            </td>
                                                            <%--FB 1982--%>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </asp:View>
                                        </asp:MultiView></div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center">
                                    <asp:Table ID="tblAV" runat="server" Visible="true" Width="90%">
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="left" colspan="4" class="subtitleblueblodtext" valign="top">
                                                            <%--Edited for FB 1428--%>
                                                            <%if(Session["systemTimezone"] !=null)%>
                                                            <a href="" onkeydown="if(event.keyCode == 13){document.getElementById('Img_AV').click();return false;}"
                                                                onmouseup="if(!isIE){this.childNodes[0].click();return false;}">
                                                                <%--ZD 100369 508 Issue--%>
                                                                <img border="0" id='Img_AV' src="image/loc/nolines_minus.gif" alt="Expand/Collapse"
                                                                    onclick="ShowHideRow('AV', this, false);return false;" /></a>
                                                            <%if (Session["systemTimezone"] == "MOJ"){%>Audio & Video Hearing Parameters<%}else{ %>
                                                            <asp:Literal Text="<%$ Resources:WebResources, ManageConference_AVConfParam%>" runat="server"></asp:Literal>
                                                            <%} %>
                                                        </td>
                                                        <%--ZD 100419--%>
                                                    </tr>
                                                    <tr id="tr_AV" align="center">
                                                        <td align="center" style="font-weight: bold;" valign="top" colspan="4">
                                                            <table width="90%" cellspacing="1" cellpadding="1">
                                                                <%--Modification for FB 1982 - start--%>
                                                                <tr>
                                                                    <td class="blackblodtext" width="25%" align="left">
                                                                        <asp:Literal ID="Literal11" Text="<%$ Resources:WebResources, ManageConference_RestrictNetwor%>"
                                                                            runat="server"></asp:Literal>
                                                                    </td>
                                                                    <td style="width: 1px">
                                                                        <b>:</b>&nbsp;
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblNWAccess" runat="server" Font-Bold="False"></asp:Label>
                                                                    </td>
                                                                    <td width="25%" class="blackblodtext" align="left">
                                                                        <asp:Literal ID="Literal12" Text="<%$ Resources:WebResources, ManageConference_RestrictUsage%>"
                                                                            runat="server"></asp:Literal>
                                                                    </td>
                                                                    <td style="width: 1px">
                                                                        <b>:</b>&nbsp;
                                                                    </td>
                                                                    <td align="left" width="25%">
                                                                        <asp:Label ID="lblRestrictUsage" runat="server" Font-Bold="False"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="25%" align="left">
                                                                        <span class="blackblodtext">
                                                                            <asp:Literal ID="Literal13" Text="<%$ Resources:WebResources, ManageConference_MaximumVideoP%>"
                                                                                runat="server"></asp:Literal></span>
                                                                    </td>
                                                                    <td style="width: 1px">
                                                                        <b>:</b>&nbsp;
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblMaxVideoPorts" runat="server" Font-Bold="False"></asp:Label>
                                                                    </td>
                                                                    <td width="25%" align="left">
                                                                        <span class="blackblodtext">
                                                                            <asp:Literal ID="Literal14" Text="<%$ Resources:WebResources, ManageConference_MaximumAudioP%>"
                                                                                runat="server"></asp:Literal></span>
                                                                    </td>
                                                                    <td style="width: 1px">
                                                                        <b>:</b>&nbsp;
                                                                    </td>
                                                                    <td align="left" width="25%">
                                                                        <asp:Label ID="lblMaxAudioPorts" runat="server" Font-Bold="False"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 18px" width="25%" align="left">
                                                                        <span class="blackblodtext">
                                                                            <asp:Literal ID="Literal15" Text="<%$ Resources:WebResources, ManageConference_VideoCodecs%>"
                                                                                runat="server"></asp:Literal></span>
                                                                    </td>
                                                                    <td style="width: 1px">
                                                                        <b>:</b>&nbsp;
                                                                    </td>
                                                                    <td align="left" style="height: 18px">
                                                                        <asp:Label ID="lblVideoCodecs" runat="server" Font-Bold="False"></asp:Label>
                                                                        <asp:DropDownList ID="lstVideoCodecs" DataTextField="Name" DataValueField="ID" runat="server"
                                                                            Visible="false">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td style="height: 18px" width="25%" align="left">
                                                                        <span class="blackblodtext">
                                                                            <asp:Literal ID="Literal16" Text="<%$ Resources:WebResources, ManageConference_AudioCodecs%>"
                                                                                runat="server"></asp:Literal></span>
                                                                    </td>
                                                                    <td style="width: 1px">
                                                                        <b>:</b>&nbsp;
                                                                    </td>
                                                                    <td align="left" style="height: 18px" width="25%">
                                                                        <asp:Label ID="lblAudioCodecs" runat="server" Font-Bold="False"></asp:Label>
                                                                        <asp:DropDownList ID="lstAudioCodecs" DataTextField="Name" DataValueField="ID" runat="server"
                                                                            Visible="false">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="25%" style="height: 18px" align="left">
                                                                        <span class="blackblodtext">
                                                                            <asp:Literal ID="Literal17" Text="<%$ Resources:WebResources, ManageConference_DualStreamMod%>"
                                                                                runat="server"></asp:Literal></span>
                                                                    </td>
                                                                    <td style="width: 1px">
                                                                        <b>:</b>&nbsp;
                                                                    </td>
                                                                    <td align="left" style="height: 18px">
                                                                        <asp:Label ID="lblDualStreamMode" runat="server" Font-Bold="False"></asp:Label>
                                                                    </td>
                                                                    <td width="25%" style="height: 18px" align="left">
                                                                        <span class="blackblodtext">
                                                                            <asp:Literal ID="Literal18" Text="<%$ Resources:WebResources, ManageConference_Password%>"
                                                                                runat="server"></asp:Literal></span>
                                                                    </td>
                                                                    <td style="width: 1px">
                                                                        <b>:</b>&nbsp;
                                                                    </td>
                                                                    <td align="left" width="25%" style="height: 18px">
                                                                        <asp:Label ID="lblAVPassword" runat="server" Font-Bold="False" Text="<%$ Resources:WebResources, ManageConference_lblAVPassword%>"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 18px" width="25%" align="left">
                                                                        <span class="blackblodtext">
                                                                            <asp:Literal ID="Literal19" Text="<%$ Resources:WebResources, ManageConference_Encryption%>"
                                                                                runat="server"></asp:Literal></span>
                                                                    </td>
                                                                    <td style="width: 1px">
                                                                        <b>:</b>&nbsp;
                                                                    </td>
                                                                    <td align="left" rowspan="1">
                                                                        <asp:Label ID="lblEncryption" runat="server" Font-Bold="False"></asp:Label>
                                                                    </td>
                                                                    <td style="height: 18px" width="25%" align="left">
                                                                        <%--added for FB 1428 Start--%>
                                                                        <%if (Application["Client"] == "MOJ")
                                          {%>
                                                                        <span class="blackblodtext">
                                                                            <asp:Literal ID="Literal20" Text="<%$ Resources:WebResources, ManageConference_HearingonPort%>"
                                                                                runat="server"></asp:Literal></span>
                                                                    </td>
                                                                    <%}
                                      else
                                      {%>
                                                                    <span class="blackblodtext">
                                                                        <asp:Literal ID="Literal21" Text="<%$ Resources:WebResources, ManageConference_ConferenceonP%>"
                                                                            runat="server"></asp:Literal></span>
                                                        </td>
                                                        <%} %>
                                                        <%--added for FB 1428 End--%>
                                                        <td style="width: 1px">
                                                            <b>:</b>&nbsp;
                                                        </td>
                                                        <td align="left" rowspan="1" width="25%">
                                                            <asp:Label ID="lblConfOnPort" runat="server" Font-Bold="False"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="25%" valign="middle" align="left" id="tdVideoDisplay" runat="server">  <%--ZD 101931--%>
                                                            <span class="blackblodtext">
                                                                <asp:Literal ID="Literal22" Text="<%$ Resources:WebResources, ManageConference_VideoDisplay%>"
                                                                    runat="server"></asp:Literal></span>
                                                        </td>
                                                        <td style="width: 1px; vertical-align: middle" id="tdDisplay" runat="server"> <%--ZD 101931--%>
                                                            <b>:</b>&nbsp;
                                                        </td>
                                                        <td align="left" valign="top" id="tdimgVideoDisplay" runat="server"> <%--ZD 101931--%>
                                                            <asp:Label runat="server" ID="lblCodianLOEP" style="margin-bottom:10px; font-style:normal; font-weight:normal;" BorderWidth="2px" BorderColor="Transparent" ></asp:Label>
                                                            <asp:Image ID="imgVideoDisplay" runat="server" AlternateText="Video Dispaly Layout"
                                                                ImageUrl="image/displaylayout/01.gif" Height="30" Width="30" />
                                                                &nbsp;
                                                            <asp:Image ID="imgLayoutMapping6EP" runat="server" alt="Layout" Height="30" Width="30" Style="display: none" />
                                                            &nbsp;
                                                            <asp:Image ID="imgLayoutMapping7EP" runat="server" alt="Layout" Height="30" Width="30" Style="display: none" />
                                                            &nbsp;
                                                            <asp:Image ID="imgLayoutMapping8EP" runat="server" alt="Layout" Height="30" Width="30" Style="display: none" />
                                                        </td>
                                                        <%--ZD 100419--%>
                                                        
                                                        <td style="height: 18px" width="25%" valign="top" align="left">
                                                            <span class="blackblodtext">
                                                                <asp:Literal ID="Literal23" Text="<%$ Resources:WebResources, ConferenceSetup_MaximumLineRa%>"
                                                                    runat="server"></asp:Literal></span>
                                                        </td>
                                                        <td style="width: 1px; vertical-align: top">
                                                            <b>:</b>&nbsp;
                                                        </td>
                                                        <td align="left" rowspan="1" valign="top">
                                                            <asp:Label ID="lblMaxLineRate" runat="server" Font-Bold="False"></asp:Label>
                                                            <asp:DropDownList ID="lstLineRate" runat="server" DataTextField="LineRateName" DataValueField="LineRateID"
                                                                Visible="false">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="25%" valign="top" align="left">
                                                            <span class="blackblodtext">
                                                                <asp:Literal ID="Literal24" Text="<%$ Resources:WebResources, ManageConference_LectureMode%>"
                                                                    runat="server"></asp:Literal></span>
                                                        </td>
                                                        <td style="width: 1px">
                                                            <b>:</b>&nbsp;
                                                        </td>
                                                        <td align="left" valign="top">
                                                            <asp:Label ID="lblLectureMode" runat="server" Font-Bold="False"></asp:Label>
                                                        </td>
                                                        <td width="25%" valign="top" align="left">
                                                            <span class="blackblodtext">
                                                                <asp:Literal ID="Literal25" Text="<%$ Resources:WebResources, ManageConference_SingleDialin%>"
                                                                    runat="server"></asp:Literal></span>
                                                        </td>
                                                        <td style="width: 1px">
                                                            <b>:</b>&nbsp;
                                                        </td>
                                                        <td align="left" valign="top">
                                                            <asp:Label ID="lblSingleDialin" runat="server" Font-Bold="False"></asp:Label>
                                                        </td>
                                                        <%--FB 2377--%>
                                                    </tr>
                                                    <%-- FB 2501 FECC Starts --%>
                                                    <tr>
                                                        <td colspan="3">
                                                        </td>
                                                        <td align="left">
                                                            <span class="blackblodtext">
                                                                <asp:Literal ID="Literal26" Text="<%$ Resources:WebResources, ManageConference_FECC%>"
                                                                    runat="server"></asp:Literal></span>
                                                        </td>
                                                        <td style="width: 1px">
                                                            <b>:</b>&nbsp;
                                                        </td>
                                                        <td align="left">
                                                            <asp:Label ID="lblFECCMode" runat="server" Font-Bold="False"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%-- FB 2501 FECC Ends --%>
                                                </table>
                                                <%--Modification for FB 1982 - end--%>
                                            </asp:TableCell></asp:TableRow>
                                    </asp:Table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <%--FB 2359 Start--%>
                <%--FB 2377 Starts--%>
                <tr id="trEnableConcierge" runat="server">
                    <td colspan="3" align="center">
                        <asp:Table ID="tblConcierge" runat="server" Visible="true" Width="90%">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="left" colspan="4" class="subtitleblueblodtext" valign="top">
                                                <%if(Session["systemTimezone"] !=null)%>
                                                <a href="" onkeydown="if(event.keyCode == 13){document.getElementById('Img_CON').click();return false;}"
                                                    onmouseup="if(!isIE){this.childNodes[0].click();return false;}">
                                                    <%--ZD 100369 508 Issue--%>
                                                    <img border="0" id='Img_CON' src="image/loc/nolines_minus.gif" alt="Expand/Collapse"
                                                        onclick="ShowHideRow('CON', this, false);return false;" /></a>
                                                <%--ZD 100419--%>
                                                <asp:Literal ID="Literal37" Text="<%$ Resources:WebResources, ConferenceSetup_tdConcierge%>"
                                                    runat="server"></asp:Literal>
                                            </td>
                                            <%--FB 3023--%>
                                        </tr>
                                        <%--FB 2632 - Start--%>
                                        <tr id="tr_CON" align="center">
                                            <td align="center" valign="top" colspan="4">
                                                <table width="90%" cellspacing="1" cellpadding="1">
                                                    <tr id="trOnSiteAVSupport" runat="server">
                                                        <%--FB 2670--%>
                                                        <td style="height: 18px" width="25%" align="left" nowrap="nowrap">
                                                            <span class="blackblodtext">
                                                                <asp:Literal ID="Literal27" Text="<%$ Resources:WebResources, ManageConference_OnSiteAVSuppo%>"
                                                                    runat="server"></asp:Literal></span>
                                                        </td>
                                                        <td style="width: 1px">
                                                            <b>:</b>&nbsp;
                                                        </td>
                                                        <td align="left" style="height: 18px">
                                                            <label id="lblOnsiteAV" style="font-weight: normal;" runat="server">
                                                            </label>
                                                        </td>
                                                    </tr>
                                                    <tr id="trMeetandGreet" runat="server" nowrap="nowrap">
                                                        <%--FB 2670--%>
                                                        <td class="blackblodtext" width="25%" align="left">
                                                            <asp:Literal ID="Literal28" Text="<%$ Resources:WebResources, ManageConference_MeetandGreet%>"
                                                                runat="server"></asp:Literal>
                                                        </td>
                                                        <td style="width: 1px">
                                                            <b>:</b>&nbsp;
                                                        </td>
                                                        <td align="left">
                                                            <label id="lblmeet" style="font-weight: normal;" runat="server">
                                                            </label>
                                                        </td>
                                                    </tr>
                                                    <tr id="trConciergeMonitoring" runat="server" nowrap="nowrap">
                                                        <%--FB 2670--%>
                                                        <td style="height: 18px" width="25%" align="left">
                                                            <span class="blackblodtext">
                                                                <asp:Literal ID="Literal29" Text="<%$ Resources:WebResources, ManageConference_CallMonitoring%>"
                                                                    runat="server"></asp:Literal></span>
                                                            <%--FB 3023--%>
                                                        </td>
                                                        <td style="width: 1px">
                                                            <b>:</b>&nbsp;
                                                        </td>
                                                        <td align="left" style="height: 18px">
                                                            <label id="lblConciergeMonitoring" runat="server" style="font-weight: normal;">
                                                            </label>
                                                        </td>
                                                    </tr>
                                                    <tr id="trDedicatedVNOC" runat="server">
                                                        <td width="25%" align="left" nowrap="nowrap">
                                                            <span class="blackblodtext">
                                                                <asp:Literal ID="Literal30" Text="<%$ Resources:WebResources, ManageConference_DedicatedVNOC%>"
                                                                    runat="server"></asp:Literal></span>
                                                        </td>
                                                        <td style="width: 1px">
                                                            <b>:</b>&nbsp;
                                                        </td>
                                                        <td align="left">
                                                            <label id="lblVNOC" runat="server" style="font-weight: normal;">
                                                            </label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <%--FB 2632 - End --%>
                                        <%--FB 2377 - End--%>
                                    </table>
                                </asp:TableCell></asp:TableRow>
                        </asp:Table>
                    </td>
                </tr>
                <%--FB 2359 End--%>
                <%--Custom Attribute Fixes start --%>
                <tr id="trEnableCustom" runat="server">
                    <td align="center" colspan="3">
                        <asp:Table runat="server" ID="tblCustOpt" Width="100%" CellPadding="0" CellSpacing="0">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="center">
                                    <table width="90%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="left" colspan="3" class="subtitleblueblodtext" valign="top">
                                                <a href="" onkeydown="if(event.keyCode == 13){document.getElementById('Img_CustOpt').click();return false;}"
                                                    onmouseup="if(!isIE){this.childNodes[0].click();return false;}">
                                                    <%--ZD 100369 508 Issue--%>
                                                    <img border="0" id='Img_CustOpt' src="image/loc/nolines_minus.gif" alt="Expand/Collapse"
                                                        onclick="ShowHideRow('CustOpt', this, false);return false;" /></a>
                                                <%--ZD 100419--%>
                                                <asp:Literal ID="Literal31" Text="<%$ Resources:WebResources, ManageConference_CustomOptions%>"
                                                    runat="server"></asp:Literal>
                                                <asp:Label ID="lbl_CustOpt" runat="server" Font-Bold="True"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="tr_CustOpt">
                                            <td align="center" style="font-weight: normal;" valign="top" colspan="3" id="tdCustOpt"
                                                runat="server">
                                                <asp:Table ID="lblNoCustOption" Visible="false" runat="server" Width="90%">
                                                    <asp:TableRow CssClass="tableHeader">
                                                        <asp:TableCell Text="<%$ Resources:WebResources, ManageConference_Message%>" CssClass="tableHeader"
                                                            HorizontalAlign="center"></asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow Height="30px">
                                                        <asp:TableCell Text="<%$ Resources:WebResources, ManageConference_NoCustomOptio%>"
                                                            CssClass="lblError"></asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:TableCell></asp:TableRow>
                        </asp:Table>
                    </td>
                </tr>
                <%--Custom Attribute Fixes end --%>
                <tr>
                    <td align="center" colspan="3">
                        <asp:Table runat="server" ID="tblAVWO" Width="100%" CellPadding="0" CellSpacing="0">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="center">
                                    <table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
                                        <tr>
                                            <td align="left" colspan="6" class="subtitleblueblodtext" valign="top">
                                                <a href="" onkeydown="if(event.keyCode == 13){document.getElementById('Img_AVWO').click();return false;}"
                                                    onmouseup="if(!isIE){this.childNodes[0].click();return false;}">
                                                    <%--ZD 100369 508 Issue--%>
                                                    <img border="0" id='Img_AVWO' src="image/loc/nolines_minus.gif" alt="Expand/Collapse"
                                                        onclick="ShowHideRow('AVWO', this,false);return false;" />
                                                </a>
                                                <asp:Literal ID="Literal32" Text="<%$ Resources:WebResources, ManageConference_AudiovisualWor%>"
                                                    runat="server"></asp:Literal><%-- FB 2570 --%>
                                                <%--ZD 100419--%>
                                                <asp:Label ID="lblAVWO" runat="server" Font-Bold="True"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="tr_AVWO">
                                            <td align="left" valign="top" colspan="6">
                                                <table width="100%" cellspacing="1" cellpadding="1">
                                                    <tr>
                                                        <td width="100%" align="center">
                                                            <asp:DataGrid ID="dgAVWO" runat="server" AllowSorting="false" AutoGenerateColumns="false"
                                                                Width="90%" OnItemDataBound="AV_ItemDataBound" OnEditCommand="SendReminderToHost"
                                                                Style="border-collapse: Collapse;" BorderColor="Black" BorderWidth="1px">
                                                                <%--Edited for FF FB 2288--%>
                                                                <AlternatingItemStyle CssClass="tableBody" />
                                                                <ItemStyle CssClass="tableBody" />
                                                                <HeaderStyle CssClass="tableHeader" />
                                                                <Columns>
                                                                    <asp:BoundColumn DataField="ID" Visible="false" ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black"
                                                                        ItemStyle-BorderWidth="1px"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Name" HeaderText="<%$ Resources:WebResources, ConferenceOrders_Name%>"
                                                                        ItemStyle-CssClass="tableBody" ItemStyle-HorizontalAlign="Left" ItemStyle-BorderColor="Black"
                                                                        ItemStyle-BorderWidth="1px">
                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                    </asp:BoundColumn>
                                                                    <%--FB 2508--%>
                                                                    <asp:BoundColumn DataField="StartByDate" HeaderText="<%$ Resources:WebResources, ConferenceOrders_StartDate%>"
                                                                        ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="StartByTime" HeaderText="<%$ Resources:WebResources, EventLog_Time%>"
                                                                        ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="CompletedByDate" HeaderText="<%$ Resources:WebResources, ConferenceOrders_CompletedDate%>"
                                                                        ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="CompletedByTime" HeaderText="<%$ Resources:WebResources, EventLog_Time%>"
                                                                        ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="RoomName" HeaderText="<%$ Resources:WebResources, InventoryManagement_AssignedtoRoo%>"
                                                                        ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="AssignedToID" Visible="true" HeaderText="<%$ Resources:WebResources, ViewWorkorderDetails_Personincharge%>"
                                                                        ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Status" HeaderText="<%$ Resources:WebResources, ViewWorkorderDetails_Status%>"
                                                                        ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Comments" HeaderText="<%$ Resources:WebResources, ViewWorkorderDetails_Comments%>"
                                                                        ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                    </asp:BoundColumn>
                                                                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, ConferenceOrders_Details%>"
                                                                        HeaderStyle-CssClass="tableHeader" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                        <ItemTemplate>
                                                                            <a href="#" onclick="ViewDetails('<%#DataBinder.Eval(Container, "DataItem.ID") %>', '<%#DataBinder.Eval(Container, "DataItem.ConfID") %>')">
                                                                                <asp:Literal ID="Literal33" Text="<%$ Resources:WebResources, ManageConference_View%>"
                                                                                    runat="server"></asp:Literal></a>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:EditCommandColumn ButtonType="LinkButton" EditText="<%$ Resources:WebResources, ConferenceOrders_Sendreminder%>"
                                                                        HeaderText="<%$ Resources:WebResources, ConferenceOrders_Action%>" HeaderStyle-CssClass="tableHeader"
                                                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="Link"
                                                                        ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"></asp:EditCommandColumn>
                                                                    <asp:TemplateColumn Visible="true">
                                                                        <ItemTemplate>
                                                                            <asp:DataGrid ID="itemsGrid" runat="server" AutoGenerateColumns="False" GridLines="None"
                                                                                Width="95%" Visible="true" Style="border-collapse: collapse" BorderColor="Black"
                                                                                BorderWidth="1px">
                                                                                <%--Edited for FF--%>
                                                                                <%--Window Dressing start--%>
                                                                                <FooterStyle CssClass="tableBody" Font-Bold="True" />
                                                                                <SelectedItemStyle CssClass="tableBody" Font-Bold="True" />
                                                                                <EditItemStyle CssClass="tableBody" />
                                                                                <AlternatingItemStyle CssClass="tableBody" />
                                                                                <ItemStyle CssClass="tableBody" BorderColor="Black" BorderWidth="1px" />
                                                                                <HeaderStyle CssClass="tableHeader" Height="30px" />
                                                                                <Columns>
                                                                                    <asp:BoundColumn DataField="ID" Visible="False" ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black"
                                                                                        ItemStyle-BorderWidth="1px">
                                                                                        <HeaderStyle CssClass="tableBody" Font-Bold="true" Height="30px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Name" HeaderText="<%$ Resources:WebResources, ConferenceOrders_Name%>"
                                                                                        ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                                        <HeaderStyle CssClass="tableBody" Font-Bold="true" Height="30px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Price" HeaderText="<%$ Resources:WebResources, ViewWorkorderDetails_PriceUSD%>"
                                                                                        ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                                        <HeaderStyle CssClass="tableBody" Font-Bold="true" Height="30px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="SerialNumber" HeaderText="<%$ Resources:WebResources, SerialNo%>"
                                                                                        ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                                        <HeaderStyle CssClass="tableBody" Font-Bold="true" Height="30px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Image%>">
                                                                                        <HeaderStyle CssClass="tableBody" Font-Bold="true" Height="30px" />
                                                                                        <ItemTemplate>
                                                                                            <asp:Image runat="server" ID="imgItem" AlternateText="Work order Item" ImageUrl='<%# DataBinder.Eval(Container, "DataItem.Image") %>'
                                                                                                Width="30" Height="30" onmouseover="javascript:ShowImage(this)" onmouseout="javascript:HideImage()" />
                                                                                            <%--ZD 100419--%>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:BoundColumn DataField="Comments" HeaderText="<%$ Resources:WebResources, ViewWorkorderDetails_Comments%>"
                                                                                        ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                                        <HeaderStyle CssClass="tableBody" Font-Bold="true" Height="30px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="ServiceCharge" HeaderText="<%$ Resources:WebResources, EditConferenceOrder_ServiceCharge%>"
                                                                                        ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                                        <HeaderStyle Font-Bold="true" CssClass="tableBody" Height="30px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="DeliveryCost" HeaderText="<%$ Resources:WebResources, EditConferenceOrder_DeliveryCost%>"
                                                                                        ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                                        <HeaderStyle Font-Bold="true" CssClass="tableBody" Height="30px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, RequestedQuantity%>"
                                                                                        ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                                        <HeaderStyle CssClass="tableBody" Font-Bold="true" Height="30px" />
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblReqQuantity" runat="server" Width="30px" Text='<%# DataBinder.Eval(Container, "DataItem.QuantityRequested") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <%--Window Dressing end--%>
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                            <asp:Table ID="lblAVNoWorkOrder" Visible="false" runat="server" Width="90%" Style="border-collapse: separate">
                                                                <%--Edited for FF--%>
                                                                <asp:TableRow CssClass="tableHeader">
                                                                    <%--Window Dressing--%>
                                                                    <asp:TableCell Text="<%$ Resources:WebResources, ManageConference_Message%>" CssClass="tableHeader"
                                                                        HorizontalAlign="center"></asp:TableCell>
                                                                </asp:TableRow>
                                                                <asp:TableRow Height="30px">
                                                                    <%--Window Dressing--%>
                                                                    <asp:TableCell Text="<%$ Resources:WebResources, ManageConference_NoWorkOrders%>"
                                                                        CssClass="lblError"></asp:TableCell>
                                                                </asp:TableRow>
                                                            </asp:Table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:TableCell></asp:TableRow>
                        </asp:Table>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="3">
                        <center>
                            <asp:Table runat="server" ID="tblCATWO" Width="100%" CellPadding="0" CellSpacing="0">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="center">
                                        <table width="90%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="left" colspan="6" class="subtitleblueblodtext" valign="top">
                                                    <a href="" onkeydown="if(event.keyCode == 13){document.getElementById('Img_CATWO').click();return false;}"
                                                        onmouseup="if(!isIE){this.childNodes[0].click();return false;}">
                                                        <%--ZD 100369 508 Issue--%>
                                                        <img border="0" id='Img_CATWO' src="image/loc/nolines_minus.gif" alt="Expand/Collapse"
                                                            onclick="ShowHideRow('CATWO', this, false);return false;" />
                                                    </a>
                                                    <%--ZD 100419--%>
                                                    <asp:Literal Text="<%$ Resources:WebResources, ManageConference_CateringWorkO%>"
                                                        runat="server"></asp:Literal>
                                                    <asp:Label ID="lblCATWO" runat="server" Font-Bold="True"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="tr_CATWO">
                                                <td align="left" style="font-weight: bold;" valign="top" colspan="6">
                                                    <table width="100%" cellspacing="1" cellpadding="1">
                                                        <tr>
                                                            <td width="100%" align="center">
                                                                <asp:DataGrid ID="dgCATWO" runat="server" AutoGenerateColumns="False" OnSortCommand="SortGrid"
                                                                    AllowSorting="True" Width="90%" OnItemDataBound="CAT_ItemDataBound" OnEditCommand="SendReminderToHost"
                                                                    Style="border-collapse: collapse" BorderColor="Black" BorderWidth="1px">
                                                                    <%--Edited for FF FB 2288 FB 2050--%>
                                                                    <AlternatingItemStyle CssClass="tableBody" />
                                                                    <ItemStyle CssClass="tableBody" BorderColor="Black" BorderWidth="1px" />
                                                                    <HeaderStyle CssClass="tableHeader" BorderColor="Black" BorderWidth="1px" />
                                                                    <Columns>
                                                                        <asp:BoundColumn DataField="ID" Visible="false" ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black"
                                                                            ItemStyle-BorderWidth="1px"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Name" HeaderText="<%$ Resources:WebResources, ConferenceOrders_Name%>"
                                                                            ItemStyle-CssClass="tableBody" ItemStyle-HorizontalAlign="Left" ItemStyle-BorderColor="Black"
                                                                            ItemStyle-BorderWidth="1px">
                                                                            <HeaderStyle CssClass="tableHeader" />
                                                                        </asp:BoundColumn>
                                                                        <%--FB 2508--%>
                                                                        <asp:BoundColumn DataField="CompletedByDate" HeaderText="<%$ Resources:WebResources, ConferenceOrders_DeliverDate%>"
                                                                            ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                            <HeaderStyle CssClass="tableHeader" />
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="CompletedByTime" HeaderText="<%$ Resources:WebResources, ConferenceOrders_DeliverTime%>"
                                                                            ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                            <HeaderStyle CssClass="tableHeader" />
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="RoomName" HeaderText="<%$ Resources:WebResources, InventoryManagement_AssignedtoRoo%>"
                                                                            ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                            <HeaderStyle CssClass="tableHeader" />
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="AssignedToID" Visible="false" HeaderText="<%$ Resources:WebResources, ViewWorkorderDetails_Personincharge%>"
                                                                            ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                            <HeaderStyle CssClass="tableHeader" />
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Status" HeaderText="<%$ Resources:WebResources, ViewWorkorderDetails_Status%>"
                                                                            Visible="false" ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black"
                                                                            ItemStyle-BorderWidth="1px">
                                                                            <HeaderStyle CssClass="tableHeader" />
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Comments" HeaderText="<%$ Resources:WebResources, ViewWorkorderDetails_Comments%>"
                                                                            Visible="false" ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black"
                                                                            ItemStyle-BorderWidth="1px">
                                                                            <HeaderStyle CssClass="tableHeader" />
                                                                        </asp:BoundColumn>
                                                                        <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, ConferenceOrders_Details%>"
                                                                            HeaderStyle-CssClass="tableHeader">
                                                                            <ItemTemplate>
                                                                                <a href="#" onclick="ViewDetails('<%#DataBinder.Eval(Container, "DataItem.ID") %>', '<%#DataBinder.Eval(Container, "DataItem.ConfID") %>', '<%#DataBinder.Eval(Container, "DataItem.Type") %>')">
                                                                                    <asp:Literal ID="Literal34" Text="<%$ Resources:WebResources, ManageConference_View%>"
                                                                                        runat="server"></asp:Literal></a>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:EditCommandColumn ButtonType="LinkButton" EditText="<%$ Resources:WebResources, ConferenceOrders_Sendreminder%>"
                                                                            HeaderText="<%$ Resources:WebResources, ConferenceOrders_Action%>" HeaderStyle-CssClass="tableHeader"
                                                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="Link"
                                                                            ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"></asp:EditCommandColumn>
                                                                        <asp:TemplateColumn Visible="true">
                                                                            <ItemTemplate>
                                                                                <asp:DataGrid ID="itemsGrid" runat="server" AutoGenerateColumns="False" GridLines="None"
                                                                                    Width="95%" Visible="true" BorderColor="black" BorderStyle="Solid" BorderWidth="2"
                                                                                    Style="border-collapse: separate">
                                                                                    <%--Edited for FF--%>
                                                                                    <%--Window Dressing start--%>
                                                                                    <FooterStyle Font-Bold="True" />
                                                                                    <SelectedItemStyle Font-Bold="True" />
                                                                                    <EditItemStyle />
                                                                                    <AlternatingItemStyle />
                                                                                    <ItemStyle />
                                                                                    <HeaderStyle Height="30px" />
                                                                                    <Columns>
                                                                                        <asp:BoundColumn DataField="ID" Visible="False" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                                            <HeaderStyle CssClass="tableHeader" Font-Bold="true" Height="30px" />
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Name" HeaderText="<%$ Resources:WebResources, ConferenceOrders_Name%>"
                                                                                            ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                                            <HeaderStyle CssClass="tableHeader" Font-Bold="true" Height="30px" />
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Quantity" HeaderText="<%$ Resources:WebResources, EditInventory_tdHQuantity%>"
                                                                                            ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                                            <HeaderStyle Font-Bold="true" CssClass="tableHeader" Height="30px" />
                                                                                        </asp:BoundColumn>
                                                                                        <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, RequestedQuantity%>"
                                                                                            ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                                            <HeaderStyle CssClass="tableHeader" Font-Bold="true" Height="30px" />
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblReqQuantity" runat="server" Width="30px" Text='<%# DataBinder.Eval(Container, "DataItem.QuantityRequested") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateColumn>
                                                                                    </Columns>
                                                                                </asp:DataGrid>
                                                                                <%--Window Dressing end --%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                                <asp:Table ID="lblCATNoWorkOrder" Visible="false" runat="server" Width="90%">
                                                                    <asp:TableRow CssClass="tableHeader">
                                                                        <asp:TableCell Text="<%$ Resources:WebResources, ManageConference_Message%>" HorizontalAlign="center"
                                                                            CssClass="tableHeader"></asp:TableCell>
                                                                    </asp:TableRow>
                                                                    <asp:TableRow Height="30px">
                                                                        <%--Window Dressing --%>
                                                                        <asp:TableCell Text="<%$ Resources:WebResources, ManageConference_NoWorkOrders%>"
                                                                            CssClass="lblError"></asp:TableCell>
                                                                    </asp:TableRow>
                                                                </asp:Table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:TableCell></asp:TableRow>
                            </asp:Table>
                        </center>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="3">
                        <asp:Table runat="server" ID="tblHKWO" Width="100%" CellPadding="0" CellSpacing="0">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="center">
                                    <table width="90%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="left" colspan="6" class="subtitleblueblodtext" valign="top">
                                                <a href="" onkeydown="if(event.keyCode == 13){document.getElementById('Img_HKWO').click();return false;}"
                                                    onmouseup="if(!isIE){this.childNodes[0].click();return false;}">
                                                    <%--ZD 100369 508 Issue--%>
                                                    <img border="0" id='Img_HKWO' src="image/loc/nolines_minus.gif" alt="Expand/Collapse"
                                                        onclick="ShowHideRow('HKWO', this, false);return false;" />
                                                </a>
                                                <asp:Literal ID="Literal35" Text="<%$ Resources:WebResources, ManageConference_FacilityWorkO%>"
                                                    runat="server"></asp:Literal>
                                                <%-- FB 2570 --%>
                                                <%--ZD 100419--%>
                                                <asp:Label ID="lblHKWO" runat="server" Font-Bold="True"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="tr_HKWO">
                                            <td align="left" style="font-weight: normal;" valign="top" colspan="6">
                                                <table width="100%" cellspacing="1" cellpadding="1">
                                                    <tr>
                                                        <td width="100%" align="center">
                                                            <%--Window Dressing --%>
                                                            <asp:DataGrid ID="dgHKWO" runat="server" AutoGenerateColumns="False" CssClass="tableHeader"
                                                                GridLines="Both" Width="90%" OnItemDataBound="HK_ItemDataBound" OnEditCommand="SendReminderToHost"
                                                                OnCancelCommand="SendReminderToHost" OnUpdateCommand="SendReminderToHost" Style="border-collapse: collapse"
                                                                Font-Bold="false" BorderColor="Black" BorderWidth="1px">
                                                                <%--Editedfor FF--%>
                                                                <%--FB 2181 FB 2288--%>
                                                                <AlternatingItemStyle CssClass="tableBody" BorderColor="Black" BorderWidth="1px" />
                                                                <ItemStyle CssClass="tableBody" BorderColor="Black" BorderWidth="1px" />
                                                                <HeaderStyle CssClass="tableHeader" BorderColor="Black" BorderWidth="1px" />
                                                                <Columns>
                                                                    <asp:BoundColumn DataField="ID" Visible="false" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Name" HeaderText="<%$ Resources:WebResources, ConferenceOrders_Name%>"
                                                                        ItemStyle-CssClass="tableBody" ItemStyle-HorizontalAlign="Left">
                                                                        <%--FB 2508--%>
                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="CompletedByDate" HeaderText="<%$ Resources:WebResources, EditConferenceOrder_CompletedbyDa%>"
                                                                        ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="CompletedByTime" HeaderText="<%$ Resources:WebResources, EventLog_Time%>"
                                                                        ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="RoomName" HeaderText="<%$ Resources:WebResources, InventoryManagement_AssignedtoRoo%>"
                                                                        ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="AssignedToID" Visible="true" HeaderText="<%$ Resources:WebResources, ViewWorkorderDetails_Personincharge%>"
                                                                        ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Status" HeaderText="<%$ Resources:WebResources, ViewWorkorderDetails_Status%>"
                                                                        ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Comments" HeaderText="<%$ Resources:WebResources, ViewWorkorderDetails_Comments%>"
                                                                        ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                                                        <HeaderStyle CssClass="tableHeader" />
                                                                    </asp:BoundColumn>
                                                                    <asp:TemplateColumn Visible="true">
                                                                        <ItemTemplate>
                                                                            <asp:DataGrid ID="itemsGrid" runat="server" AutoGenerateColumns="False" GridLines="None"
                                                                                Width="95%" Visible="true" BorderColor="black" BorderStyle="Solid" BorderWidth="1"
                                                                                Style="border-collapse: separate">
                                                                                <%--Edited for FF--%>
                                                                                <%--Window Dressing start--%>
                                                                                <FooterStyle CssClass="tableBody" Font-Bold="True" />
                                                                                <SelectedItemStyle CssClass="tableBody" />
                                                                                <EditItemStyle CssClass="tableBody" />
                                                                                <AlternatingItemStyle CssClass="tableBody" />
                                                                                <ItemStyle CssClass="tableBody" />
                                                                                <HeaderStyle CssClass="tableHeader" Height="30px" />
                                                                                <Columns>
                                                                                    <asp:BoundColumn DataField="ID" Visible="False">
                                                                                        <HeaderStyle CssClass="tableHeader" Font-Bold="true" Height="30px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Name" HeaderText="<%$ Resources:WebResources, Task%>">
                                                                                        <HeaderStyle CssClass="tableHeader" Font-Bold="true" Height="30px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Price" HeaderText="<%$ Resources:WebResources, ViewWorkorderDetails_PriceUSD%>">
                                                                                        <HeaderStyle CssClass="tableHeader" Font-Bold="true" Height="30px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="SerialNumber" HeaderText="<%$ Resources:WebResources, SerialNo%>">
                                                                                        <HeaderStyle CssClass="tableHeader" Font-Bold="true" Height="30px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Image%>">
                                                                                        <HeaderStyle CssClass="tableHeader" Font-Bold="true" Height="30px" />
                                                                                        <ItemTemplate>
                                                                                            <asp:Image runat="server" ID="imgItem" AlternateText="Work Order Item" ImageUrl='<%# DataBinder.Eval(Container, "DataItem.Image") %>'
                                                                                                onmouseover="javascript:ShowImage(this)" onmouseout="javascript:HideImage()" />
                                                                                            <%--ZD 100419--%>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:BoundColumn DataField="Comments" HeaderText="<%$ Resources:WebResources, ViewWorkorderDetails_Comments%>">
                                                                                        <HeaderStyle CssClass="tableHeader" Font-Bold="true" Height="30px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Quantity" HeaderText="<%$ Resources:WebResources, EditInventory_tdHQuantity%>">
                                                                                        <HeaderStyle Font-Bold="true" CssClass="tableHeader" Height="30px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, RequestedQuantity%>">
                                                                                        <HeaderStyle Font-Bold="true" CssClass="tableHeader" Height="30px" />
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblReqQuantity" runat="server" Width="30px" Text='<%# DataBinder.Eval(Container, "DataItem.QuantityRequested") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                            <%--Window Dressing end--%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Details" HeaderStyle-CssClass="tableHeader" ItemStyle-BorderColor="Black"
                                                                        ItemStyle-BorderWidth="1px">
                                                                        <ItemTemplate>
                                                                            <a href="#" onclick="ViewDetails('<%#DataBinder.Eval(Container, "DataItem.ID") %>', '<%#DataBinder.Eval(Container, "DataItem.ConfID") %>')">
                                                                                <asp:Literal Text="<%$ Resources:WebResources, ManageConference_View%>" runat="server"></asp:Literal></a>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:EditCommandColumn ButtonType="LinkButton" EditText="<%$ Resources:WebResources, ConferenceOrders_Sendreminder%>"
                                                                        HeaderText="<%$ Resources:WebResources, ConferenceOrders_Action%>" HeaderStyle-CssClass="tableHeader"
                                                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="Link">
                                                                    </asp:EditCommandColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                            <asp:Table ID="lblHKNoWorkOrder" Visible="false" runat="server" Width="90%" Style="border-collapse: separate">
                                                                <%--Edited for FF--%>
                                                                <asp:TableRow CssClass="tableHeader">
                                                                    <%--Window Dressing --%>
                                                                    <asp:TableCell Text="<%$ Resources:WebResources, ManageConference_Message%>" CssClass="tableHeader"
                                                                        HorizontalAlign="center"></asp:TableCell>
                                                                </asp:TableRow>
                                                                <asp:TableRow Height="30px">
                                                                    <%--Window Dressing --%>
                                                                    <asp:TableCell Text="<%$ Resources:WebResources, ManageConference_NoWorkOrders%>"
                                                                        CssClass="lblError"></asp:TableCell>
                                                                </asp:TableRow>
                                                            </asp:Table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:TableCell></asp:TableRow>
                        </asp:Table>
                    </td>
                </tr>
            </table>
        </div>
        <%--ZD 101597--%>
        <div id="PopupFormList" align="center" style="position: absolute; overflow: hidden;
            border: 1px; width: 450px; display: none; top: 300px; left: 510px; height: 450px;">
            <table align="center" class="tableBody" width="80%" cellpadding="5" cellspacing="5">
                <tr class="tableHeader">
                    <td colspan="2" align="left" class="blackblodtext">
                        <asp:Literal Text="<%$ Resources:WebResources, ChooseFormHeading%>" runat="server"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <asp:RadioButtonList ID="rdEditForm" runat="server" onfocus="javascript:void(0);"
                            RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="1" CellPadding="3"
                            CellSpacing="3">
                            <asp:ListItem Text="<%$ Resources:WebResources, LongForm%>" Value="1"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:WebResources, ExpressForm%>" Value="2"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input id="btnOk" runat="server" type="button" class="altMedium0BlueButtonFormat"
                            onclick="javascript:return fnRedirectForm();" value="<%$ Resources:WebResources, MasterChildReport_cmdConfOk%>" />
                    </td>
                    <td>
                        <input id="btnCancel" runat="server" type="button" class="altMedium0BlueButtonFormat"
                            onclick="javascript:fnDivClose();" value="<%$ Resources:WebResources, Cancel%>" />
                    </td>
                </tr>
            </table>
        </div>
    </center>
    <script language="javascript" type="text/javascript">
    function calculateRecurText()
    {
        if (document.getElementById("<%=Recur.ClientID %>").value != "" ) 
        {
            AnalyseRecurStr(document.getElementById("<%=Recur.ClientID %>").value);
            
            st = calStart(atint[1], atint[2], atint[3]);
            document.getElementById("<%=lblTimezone.ClientID%>").innerHTML = "";
            et = calEnd(st, parseInt(atint[4], 10));
            document.getElementById("<%=lblTimezone.ClientID%>").innerHTML = recur_discription(document.getElementById("<%=Recur.ClientID %>").value, et
            , document.getElementById("timezone").value, Date(),"<%=Session["timeFormat"].ToString()%>","<%=Session["timezoneDisplay"].ToString()%>");//FB 1728 //FB 1948
            if(document.getElementById("lblTimezoneDIV").innerText.length > 200)
                document.getElementById("lblTimezoneDIV").style.height = "200";
            //ZD 100221 
            if(document.getElementById("WebEXstartdate") != null)
                document.getElementById("<%=WebEXstartdate.ClientID%>").innerHTML =  document.getElementById("<%=lblTimezone.ClientID%>").innerHTML;
        }
        else
            document.getElementById("lblTimezoneDIV").style.height = "20";
    }

    setTimeout("calculateRecurText()", 100);
    //Blue Status Project
    function ShowEpID()
    {
        if(arguments[1] != "")
            document.getElementById(arguments[0]).innerHTML = arguments[1];
    }
    </script>
    <%--FB 1985 --%>
    <script type="text/javascript">
        function fnShowHideAVLink() {
            var args = fnShowHideAVLink.arguments;
            var obj = eval(document.getElementById("LnkAVExpand"));
            var Conftype = eval(document.getElementById("hdnConfType"));
            if (Conftype.value != "7") {
                if (Conftype.value != "4") {
                    if (obj) {

                        if (args[0] == '1') {
                            obj.style.display = '';
                        }
                        else {
                            obj.style.display = 'none';
                            //obj.tabindex = "-1";

                        }
                    }
                }
            }
        }
        //FB 2441 Starts
        function fnShowHide(arg) {

            if (arg == '1')
                document.getElementById("MuteAllEndpointDiv").style.display = 'block';
            else
                document.getElementById("MuteAllEndpointDiv").style.display = 'none';

            return false;
        }
        //FB 2441 Ends
    </script>
    <div id="divPic" style="display: none; z-index: 1;">
        <img src="" name="myPic" width="200" height="200">
    </div>
    <iframe src="" name="ifrmPreloading" width="0" height="0" style="display: none">
        <%--Edited for FF--%>>
        <p>
            <asp:Literal ID="Literal36" Text="<%$ Resources:WebResources, ManageConference_Loadingpage%>"
                runat="server"></asp:Literal></p>
    </iframe>
    <asp:DropDownList Visible="false" ID="lstProtocol" runat="server" DataTextField="Name"
        DataValueField="ID">
    </asp:DropDownList>
    <asp:TextBox ID="txtTimeDifference" Visible="false" runat="server"></asp:TextBox><asp:TextBox
        ID="tempText" TextMode="multiline" Rows="4" runat="server" Width="0" Height="0"
        ForeColor="black" BackColor="transparent" BorderColor="transparent" Style="display: none"></asp:TextBox><%--Edited for FF--%>
    <asp:TextBox ID="txtEndpointType" runat="server" Width="0" Height="0" ForeColor="transparent"
        BackColor="transparent" BorderColor="transparent" Style="display: none"></asp:TextBox><%--Edited for FF--%>
    <asp:Button ID="btnRefreshEndpoints" runat="server" Visible="false" OnClick="UpdateEndpoints" />
    <asp:HiddenField ID="hdnConfStatus" runat="server" />
    <asp:HiddenField ID="hdnConfType" runat="server" />
    <asp:HiddenField ID="hdnConfLockStatus" runat="server" />
    <%--FB 2501 2012-12-07--%>
    </form>
</body>
</html>
<% 
 //   Response.Write(Request.QueryString["t"]);
if (Request.QueryString["t"] != null)
    if (!Request.QueryString["t"].ToString().Equals("hf"))
    { %>
<script language="javascript">
    CheckEndpoint();
    //FB Case 944, 936, 861 and 730 Saima
    function UpdateEndpointStatus() {

        if ((document.getElementById("dgEndpoints") != null && document.getElementById("chkAutoRefresh").checked == true)) {
            setTimeout("DataLoading(1);__doPostBack('btnRefreshEndpoints', '')", 30000);
        }
    }
    //FB Case 944, 936, 861 and 730 Saima
    function UpdateP2PEndpointStatus() {
        if ((document.getElementById("dgP2PEndpoints") != null) && (document.getElementById("P2pAutoRef").checked == true)) {

            var tim = '15000';

            var chk = document.getElementById("Refreshchk");

            if (chk) {
                if (chk.checked == true)
                    tim = '30000'
            }

            setTimeout("DataLoading(1);__doPostBack('btnRefreshEndpoints', '')", tim);
        }
    }


    if (document.getElementById("P2pAutoRef") != null)
        if ((document.getElementById("dgP2PEndpoints") != null) && (document.getElementById("P2pAutoRef").checked == true))
            UpdateP2PEndpointStatus();

    if (document.getElementById("chkAutoRefresh") != null)
        if ((document.getElementById("dgEndpoints") != null) && (document.getElementById("chkAutoRefresh").checked == true))
            UpdateEndpointStatus();
</script>
<%--<script language="javascript">

                   
                        if (document.getElementById("Menu1") != null)
                        {
                            document.getElementById("Menu1").innerHTML = document.getElementById("Menu1").innerHTML.replace("javascript:", "javascript:if (DataLoading(1)) ");
                        }
                    

</script>--%>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<%--ZD 101388 Commented--%>
<%--<!-- ZD 100288 Starts -->
<% if (Session["isExpressUser"].ToString() == "1"){%>
<!-- #INCLUDE FILE="inc/mainbottomNETExp.aspx" -->
<%}else{%>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<%}%>
<!-- ZD 100288 Ends -->--%>
<% }
   else
   { %>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<script language="javascript">
    window.resizeTo(1000, 700);
    document.getElementById("chkExpandCollapse").checked = true;
    ExpandAll();
</script>
<% } %>
<%--FB 2508 Start--%>
<script type="text/javascript">
    var label = document.getElementById('lblConfName');
    var caption = document.getElementById('ConfTitle');
    if (label.innerHTML.length > 50) {
        caption.title = label.innerHTML;
        label.innerHTML = label.innerHTML.substr(0, 50);
    }
    else {
        caption.style.visibility = 'hidden';
    }
</script>
<%--FB 2508 End--%>
<%--ZD 100428 START- Close the popup window using the esc key--%>
<script language="javascript" type="text/javascript">
    //ZD 101597
    document.onkeydown =  function(event) {
        if (event.keyCode == 27) {
            if (document.getElementById('viewHostDetails') != null)
                if (document.getElementById('viewHostDetails').style.display != 'none')
                    ClosePopUp();
                //ZD 100221 start
             else if (document.getElementById('WebExDialog') != null)
                    if (document.getElementById('WebExDialog').style.display != 'none')
                        fnHide();
                //ZD 100221 End    
            else if (document.getElementById('prompt') != null)
                cancelthis();
            else if (document.getElementById('btnClose') != null)
                document.getElementById('btnClose').click();
			//ZD 101597
            var obj = document.getElementById('PopupFormList');        
            if(obj != null &&  obj.style.display !='none')
                obj.style.display = 'none';
        }

        if (event.keyCode == 13) {
            if (window.parent.document.getElementById('ctl37_BtnUsrDetailClose') != null) {
                window.parent.document.getElementById('ctl37_BtnUsrDetailClose').focus();
            }
        }
    }

<%--ZD 100428 END--%>

<%--ZD 101294 Start--%>
    function printpage() {
        document.getElementById('menu1').style.display = 'none';
        document.getElementById('tblTopMenu').style.display = 'none';
        document.getElementById('topSpace').style.display = 'none';
        document.getElementById('topHeader').style.display = 'none';
        document.getElementById('menuTopLine').style.display = 'none'; // ZD 101326
        if (document.getElementById("tblExpMenu") != null)
            document.getElementById("tblExpMenu").style.display = "none";        
        //var color = document.getElementById('menuTopLine').style.backgroundColor;
        //document.getElementById('menuTopLine').style.backgroundColor = "transparent";
        window.print();
        if (navigator.userAgent.indexOf('Firefox') > -1)
            alert('Click ok to continue');
        window.location.href = window.location.href;
        /*
        document.getElementById('menuTopLine').style.backgroundColor = color;
        document.getElementById('menu1').style.display = 'block';
        document.getElementById('tblTopMenu').style.display = 'block';
        document.getElementById('menuTopLine').style.visiblity = 'visible';
        document.getElementById('topSpace').style.display = 'block';
        document.getElementById('topHeader').style.display = 'block';
        */
    }

    <%--ZD 101294 End--%>        
    if(document.addEventListener != null)//ZD 102008
        document.addEventListener("click",handler,true); //ZD 101597

    
//<%--ZD 101597 Start--%>    
function fnSelectForm() {  

    var elementRef = document.getElementById('rdEditForm');
    var inputElementArray = elementRef.getElementsByTagName('input');

    if ('<%=Session["hasConference"]%>' == '1' && '<%=Session["hasExpConference"]%>' == "1") {
        document.getElementById("PopupFormList").style.display = 'block';
        if (document.getElementById('rdEditForm_1') != null)
        document.getElementById('rdEditForm_1').checked = false;
        if (document.getElementById('rdEditForm_2') != null)
        document.getElementById('rdEditForm_2').checked = false;

        for (var i = 0; i < inputElementArray.length; i++) {
            var inputElement = inputElementArray[i];
            inputElement.checked = false;
        }
         return false;
    }
       
}

    function fnRedirectForm() {
        var args = fnRedirectForm.arguments;
        var hdnEditForm = document.getElementById("hdnEditForm");
        hdnEditForm.value = "";
        
        if (document.getElementById("rdEditForm_0").checked)
            hdnEditForm.value = "1";
        else if (document.getElementById("rdEditForm_1").checked)
            hdnEditForm.value = "2";
            
        if(hdnEditForm.value != "")
        {
            if (args[0] == "CL") {
                return CustomEditAlert("'" + args[1] + "'")
            }
            else {
                var btnTemp = document.getElementById("Temp");
                if (btnTemp != null)
                    btnTemp.click();

                DataLoading(1);
            }
            document.getElementById("PopupFormList").style.display = 'None';
        }
        else
            alert("Please select the form to edit a conference.");

        return false;
    }

     function fnDivClose() {
        var hdnEditForm = document.getElementById("hdnEditForm");
        hdnEditForm.value = "";
        document.getElementById("PopupFormList").style.display = 'None';
    }


    function handler(e)
    {
        var obj = document.getElementById('PopupFormList');
        var isoutofpopup = false; //ZD 104319
        if(obj.style.display !='none')
        {
            if(e.target.id != "Temp" && e.target.id != "btnCancel" && e.target.id != "btnOk" && e.target.id.indexOf('rdEditForm') < 0)
            {
                isoutofpopup = true; //ZD 104319              
            }

             //ZD 104319 - start
            if (e.target.htmlFor != undefined) {
                    if(e.target.htmlFor.indexOf('rdEditForm') < 0)
                        isoutofpopup = true;
                    else
                        isoutofpopup = false;
            }

            if (isoutofpopup == true) {
                e.stopPropagation();
                e.preventDefault();
            }
            //ZD 104319 - end
        }
    }
    //ZD 101971 Starts
    if (document.getElementById("errLabel") != null) 
    var obj = document.getElementById("errLabel");
    if (obj != null) {

    var strInput = obj.innerHTML.toUpperCase();
    
    if (strInput.indexOf("NOTE: THE SELECTED VIDEO DISPLAY LAYOUT IS NOT SUPPORTED BY THE MCU IN CONFERENCE") > -1) 
        obj.setAttribute("style", "color:red");

    if (strInput.indexOf("SELECCIONADA NO ES COMPATIBLE CON EL MCU") > -1)
        obj.setAttribute("style", "color:red");
    }
    //ZD 101971 Ends 
      
  //<%--ZD 101597 End--%>    
</script>

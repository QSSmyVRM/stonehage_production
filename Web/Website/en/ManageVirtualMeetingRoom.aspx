﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.en_ManageVirtualMeetingRoom" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->


<script type="text/javascript">

    function getYourOwnEmailList(i) {
        if (i == -2) {
            if (queryField("sb") > 0)
                url = "../en/emaillist2.aspx?t=e&frm=roomassist&wintype=ifr&fn=frmVirtualMettingRoom&n=";
            else
                url = "../en/emaillist2main.aspx?t=e&frm=roomassist&fn=frmVirtualMettingRoom&n=";
        }
        else {
            url = "../en/emaillist2main.aspx?t=e&frm=approver&fn=frmVirtualMettingRoom&n=" + i;
        }
        if (!window.winrtc) {	// has not yet been defined
            winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
            winrtc.focus();
        } else // has been defined
            if (!winrtc.closed) {     // still open
            winrtc.close();
            winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
            winrtc.focus();
        } else {
        winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
            winrtc.focus();
        }
    }

    //ZD 100522 Starts
    function OpenEndpointlist() {
        var selectedRooms = '';
        var url = window.location.href;
        var Openurl = window.location.href;
        var selectedRoomIds;
        if (document.getElementById("selectedloc").value != null)
            selectedRoomIds = document.getElementById("selectedloc").value;

        var url = "RoomSearch.aspx?rmsframe=" + selectedRoomIds + "&hf=1&type=&frm=VMRfrmSettings2";
        $('#popupdiv').fadeIn();
        $("#formRoomList").attr("src", url);
        $('#PopupRoomList').show();
        $('#PopupRoomList').bPopup({
            fadeSpeed: 'slow',
            followSpeed: 1500,
            modalColor: 'gray'
        });

        return true;
    }

    function fnTriggerFromPopup() {
        $('#popupdiv').fadeOut();
        $('#PopupRoomList').hide();
    }
    //ZD 100522 End


    function deleteAssistant() {//FB 2448
        eval("document.frmVirtualMettingRoom.AssistantID").value = "";
        eval("document.frmVirtualMettingRoom.Assistant").value = "";
    }

    function fnClose() {
        window.location.replace("manageroom.aspx?hf=&m=&pub=&d=&comp=&f=&frm=");
        return true;
    }

    function frmMainroom_Validator() {

        if (!Page_ClientValidate())
            return Page_IsValid;
        
        var passlen = "<%= PasswordCharLength%>"; //ZD 100522
        
        var txtroomname = document.getElementById('<%=txtRoomName.ClientID%>');
        if (txtroomname.value == "") {
            reqName.style.display = 'block';
            txtroomname.focus();
            return false;
        }
        else if (txtroomname.value.search(/^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@$%&~]*$/) == -1) {
            regRoomName.style.display = 'block';
            txtroomname.focus();
            return false;
        }
		//ZD 100522 Starts
        if ((document.getElementById("chkCreateMCU") != null && document.getElementById("chkCreateMCU").checked) || (document.getElementById("ChkDialoutLoc") != null && document.getElementById("ChkDialoutLoc").checked)) {
            if (document.getElementById("lstMCU") != null && document.getElementById("lstMCU").selectedIndex <= 0) {
                alert(SelectMCU);
                return false;
            }
            if (document.getElementById("ChkDialoutLoc") != null && document.getElementById("ChkDialoutLoc").checked) {
                if (document.getElementById("RoomList") != null && document.getElementById("RoomList").length <= 0) {
                    alert(SelectDialOutLocation);
                    return false;
                }
            }
        }

        //ZD 102918 - Start
        if (document.getElementById("chkCreateMCU") != null && document.getElementById("chkCreateMCU").checked) {
            if (document.getElementById("txtVMRID") != null && document.getElementById("txtVMRID").value == "") {
                alert(ReqVMRID);
                return false;
            }
        }
        //ZD 102918 - End
        //ZD 103263 - Start
        
        if (document.getElementById("chkEnableBJN") != null) 
        {
            if ((document.getElementById("chkEnableBJN").checked)) {                
                if (document.getElementById("txtVMRID").value == "") 
                {
                    ValidatorEnable(document.getElementById("ReqtxtVMRID"), true);
                    return false;
                }
                if (document.getElementById("hdnPasschange").value == "false") {
                    if ('<%=Session["VMRPW"]%>' == "") {
                        ValidatorEnable(document.getElementById("ReqtxtPassword1"), true);
                        return false;
                    }
                }
                else {
                    if (document.getElementById("txtPassword1").value == "") {
                        ValidatorEnable(document.getElementById("ReqtxtPassword1"), true);
                        return false;
                    }
                }                         
            }
            else 
            {
                ValidatorEnable(document.getElementById("ReqtxtPassword1"), false);
            }
        }
        //ZD 103263 - End

        EnableVMRIDValidation(); //ZD 103833
       
        var obj1 = document.getElementById("txtPassword1");
        if (obj1.value != "") 
        {
            if (obj1.value.indexOf(0) == "0") 
            {
                alert(PwdValidation);
                return false;
            }
        }

        if (obj1.value != "" && obj1.value.length < passlen) {
            alert(PwdRangeValid + passlen + PwdRangeValid1);
            return false;
        }
		//ZD 100522 End
        var hdnmultipledept = document.getElementById('<%=hdnMultipleDept.ClientID%>');
        var departmentlist = document.getElementById('<%=DepartmentList.ClientID%>');

        if (hdnmultipledept.value == 1) {
            if ((departmentlist.value == "") && (departmentlist.length > 0)) {
                isConfirm = confirm(RoomProfdept)
                if (isConfirm == false) {
                    return (false);
                }
            }
        }

        return (true);
    }
	
    // ZD 100753 Starts
    function PasswordChange(par) {
        if (document.getElementById('reqPassword1') != null) 
        {
            ValidatorEnable(document.getElementById('reqPassword1'), true);
        }

        document.getElementById("hdnPasschange").value = true;

        if (par == 1)
            document.getElementById("hdnPW1Visit").value = true;
        else
            document.getElementById("hdnPW2Visit").value = true;

        document.getElementById("hdnVMRpwd").value = document.getElementById("txtPassword1").value; 
        
    } 
 function fnTextFocus(xid,par) {
     
     var obj1 = document.getElementById("txtPassword1");
     var obj2 = document.getElementById("txtPassword2");
     
     if(document.getElementById("hdnPasschange").value == "false")
     {
         if(obj1.value == "" && obj2.value == "")
         {
             document.getElementById("txtPassword1").style.backgroundImage="";
             document.getElementById("txtPassword2").style.backgroundImage="";
             document.getElementById("txtPassword1").value="";
             document.getElementById("txtPassword2").value="";
         }
     }
     return false;
     
      var obj = document.getElementById(xid);
          
    if (par == 1) {
        if(document.getElementById("hdnPW2Visit") != null)
        {
            if(document.getElementById("hdnPW2Visit").value == "false")
            { 
                document.getElementById("txtPassword1").value = "";
                document.getElementById("txtPassword1_1").value="";
                document.getElementById("txtPassword2").value = "";
                document.getElementById("txtPassword1_2").value="";
            }else
            {
                document.getElementById("txtPassword1").value = "";
                document.getElementById("txtPassword1_1").value="";
            }
        }
        else
        {
            document.getElementById("txtPassword1").value = "";
            document.getElementById("txtPassword1_1").value="";
            document.getElementById("txtPassword2").value = "";
            document.getElementById("txtPassword1_2").value="";
        }
    }
       else{
           if(document.getElementById("hdnPW1Visit") != null)
           {
            if(document.getElementById("hdnPW1Visit").value == "false")
            { 
                document.getElementById("txtPassword1").value = "";
                document.getElementById("txtPassword1_1").value="";
                document.getElementById("txtPassword2").value = "";
                document.getElementById("txtPassword1_2").value="";
            }
            else{
                document.getElementById("txtPassword2").value = "";
                document.getElementById("txtPassword1_2").value="";
                }
           
           }
           else{
                document.getElementById("txtPassword2").value = "";
                document.getElementById("txtPassword1_2").value="";
                }
        }
        
         if(document.getElementById("cmpValPassword1")!= null)
        {
            ValidatorEnable(document.getElementById('cmpValPassword1'), false);
            ValidatorEnable(document.getElementById('cmpValPassword1'), true);
        }
         if(document.getElementById("cmpValPassword2")!= null)
        {
            ValidatorEnable(document.getElementById('cmpValPassword2'), false);
            ValidatorEnable(document.getElementById('cmpValPassword2'), true);
        }
    }
    // ZD 100753 Ends
	//ZD 100522 Starts
    function num_gen() {
        var num = randam()
        if (num.indexOf(0) == 0)
            num = num.replace(0, 1)
        
        if (document.getElementById("cmpValPassword") != null)
            document.getElementById("cmpValPassword").style.display = "none";
        if (document.getElementById("cmpValPassword1") != null)
            document.getElementById("cmpValPassword1").style.display = "none";
        if (document.getElementById("numPassword1") != null)
            document.getElementById("numPassword1").style.display = "none";

        document.getElementById("txtPassword1").style.backgroundImage = "";
        document.getElementById("txtPassword2").style.backgroundImage = "";
        document.getElementById("txtPassword1").value = "";
        document.getElementById("txtPassword2").value = "";

        var pass = document.getElementById("txtPassword1");
        var pass1 = document.getElementById("txtPassword2");

        if (pass != null) {
            pass.value = num;
        }

        if (pass1 != null) {
            pass1.value = num;
        }
        document.getElementById("hdnPasschange").value = true;
    }

    function randam() {
        
        var pwdCharLength = "<%= PasswordCharLength%>"; //ZD 100522
        
        var totalDigits = pwdCharLength; //ZD 100522
        var n = Math.random() * 10000;
        n = Math.round(n);
        n = n.toString();
        var pd = '';
        if (totalDigits > n.length) {
            for (i = 0; i < (totalDigits - n.length); i++) {
                pd += '0';
            }
        }
        return pd + n.toString();
    }
    //ZD 100522 End
    //ZD 103263 Start //ZD 103565 - Start
    function BJNFeature() {
        var EnableBJN = document.getElementById("chkEnableBJN");

        if ((EnableBJN != null) && (EnableBJN.checked == true)) {
            document.getElementById("trVMRIntExtNumber").style.visibility = 'hidden';
            document.getElementById("tdcreateMCU").style.visibility = 'hidden';
            document.getElementById("tdchkMCU").style.visibility = 'hidden';
            document.getElementById("chkCreateMCU").checked = false;
            document.getElementById("ImgVMRID").style.display = "none";
            document.getElementById("ImgBJNID").style.display = "inline";
            ValidatorEnable(document.getElementById("RegVMRID"), true); //ZD 103833
            ValidatorEnable(document.getElementById("RegVMRIDNumeric"), false); //ZD 103833
            
        }
        else {
            document.getElementById("trVMRIntExtNumber").style.visibility = 'visible';
            document.getElementById("tdcreateMCU").style.visibility = 'visible';
            document.getElementById("tdchkMCU").style.visibility = 'visible';
            document.getElementById("chkCreateMCU").checked = false;
            document.getElementById("ImgVMRID").style.display = "inline";
            document.getElementById("ImgBJNID").style.display = "none";
            ValidatorEnable(document.getElementById("RegVMRID"), false); //ZD 103833
            ValidatorEnable(document.getElementById("RegVMRIDNumeric"), true); //ZD 103833
        }
    }
    //ZD 103565 - End
    //ZD 103833 start
    function EnableVMRIDValidation() {
        var EnableBJN = document.getElementById("chkEnableBJN");
        if ((EnableBJN != null) && (EnableBJN.checked == true)) 
        {
            document.getElementById("RegVMRID").style.display == 'block';
            document.getElementById("RegVMRIDNumeric").style.display == 'none';
        }
        else {
            document.getElementById("RegVMRID").style.display == 'none';
            document.getElementById("RegVMRIDNumeric").style.display == 'block';
        }
    }
    //ZD 103833 End

    function CreateMCUFeature() {
        var CreateMCU = document.getElementById("chkCreateMCU");

        if ((CreateMCU != null) && (CreateMCU.checked == true)) {
            document.getElementById("trVMRIntExtNumber").style.visibility = 'visible';
            document.getElementById("trEnableBJN").style.visibility = 'hidden';
            document.getElementById("chkEnableBJN").checked = false;
        }
        else {
            document.getElementById("trEnableBJN").style.visibility = 'visible';
            document.getElementById("chkEnableBJN").checked = false;
        }
    }

    //ZD 103263 End
</script>

<script type="text/javascript" src="inc/functions.js"></script>
<script type="text/javascript" src="script/RoomSearch.js"></script>
<script type="text/javascript" src="script/errorList.js"></script>
<script type="text/javascript" src="extract.js"></script>
<script src="script/CallMonitorJquery/jquery.1.4.2.js" type="text/javascript"></script> 
<script type="text/javascript" src="script/CallMonitorJquery/json2.js" ></script>
<script type="text/javascript" src="script/CallMonitorJquery/jquery.bpopup-0.7.0.min.js"></script>
<script type="text/javascript" src="script/CallMonitorJquery/MonitorMCU.js"></script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Manage Virtual Meeting Room</title>
    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />
    <%--<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main.css" />--%>
    <%--ZD 100664 Start--%>
    <script type="text/javascript">
     $(document).ready(function() {
           $('.treeNode').click(function() {
            var target = $(this).attr('tag');
            if ($('#' + target).is(":visible")) {
                $('#' + target).hide("slow");
                $(this).attr('src', 'image/loc/nolines_plus.gif');
            } else {
                $('#' + target).show("slow");
                
                $(this).attr('src', 'image/loc/nolines_minus.gif');
            }

        });
        });
    </script>
    <%--ZD 100664 End--%>
</head>
<body>
    <form id="frmVirtualMettingRoom" runat="server">
    <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <input type="hidden" id="hdnMultipleDept" runat="server" />
    <input type="hidden" id="AssistantID" runat="server" />
    <input type="hidden" id="hdnRoomID" runat="server" />
    <input type="hidden" id="AssistantName" runat="server" />
    <%--ZD 100522 Start--%>
    <input type="hidden" id="hdnPermanetConfid" runat="server" /> 
    <input type="hidden" id="hdnVRMConfid" runat="server" /> 
    <%--ZD 100522 End--%>
    <input type="hidden" id="hdnEPID" runat="server" /> <%--ZD 103263--%>
    <%--ZD 100753 Starts--%>
    <input type="hidden" id="hdnPasschange" value="false" runat="server"/> 
    <input type="hidden" id="hdnPW1Visit" value="false" runat="server"/>
    <input type="hidden" id="hdnPW2Visit" value="false" runat="server"/>
    <input id="txtPassword1_1" runat="server" type="hidden" />
    <input id="txtPassword1_2" runat="server" type="hidden" />
    <input type="hidden" id="hdnVMRpwd" value="" runat="server"/>
    <input name="locstrname" type="hidden" id="locstrname" runat="server"  /> <!--Added room search-->
    <input name="selectedloc" type="hidden" id="selectedloc" runat="server"  /> <!--Added room search-->
    <%--ZD 100753 Ends--%>
    <input type="hidden" id="AssistantEmail" value="" runat="server"/> <%--ZD 100619--%>
    <input type="hidden" id="txtContactEmail" value="" runat="server"/> <%--ZD 100619--%>
    <div>
        <center>
            <table border="0" width="100%" cellpadding="2" cellspacing="2">
                <tr>
                    <td align="center">
                        <h3>
                            <asp:Label ID="lblTitle" runat="server"></asp:Label></h3> <%--FB 2994--%>
                        <br />
                        <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
                    </td>
                </tr>
                <%--ZD 100664 - Start --%>
                    <tr id="trHistory" runat="server">
                        <td>
                            <table width="100%" border="0">
                                <tr>
                                    <td valign="top" align="left" style="width: 2px">
                                    <span class="blackblodtext" style="vertical-align:top"><asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, History%>" runat="server"></asp:Literal></span>
                                        <a href="#" onmouseup="if(event.keyCode == 13){this.childNodes[0].click();return false;}" onkeydown="if(event.keyCode == 13){document.getElementById('imgHistory').click();return false;}" >
                                        <img id="imgHistory" src="image/loc/nolines_plus.gif" class="treeNode" style="border:none;" alt="Expand/Collapse" tag="tblHistory" /></a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                    <td> 
                      <div id="tblHistory" style="display:none; width:600px; float:left">
                        <table cellpadding="4" cellspacing="0" border="0" style="border-color:Gray; border-radius:15px; background-color:#ECE9D8"  width="75%" class="tableBody" align="center" >
                            <tr>
                                <td class="subtitleblueblodtext" align="center">
                                    <asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, ExpressConference_Details%>" runat="server"></asp:Literal>
                                </td>            
                            </tr>
                            <tr align="center">
                                <td align="left">
                                    <asp:DataGrid ID="dgChangedHistory" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowSorting="true" 
                                        BorderStyle="solid" BorderWidth="0" ShowFooter="False"  
                                        Width="100%" Visible="true" style="border-collapse:separate" >
                                        <SelectedItemStyle  CssClass="tableBody"/>
                                        <AlternatingItemStyle CssClass="tableBody" />
                                        <ItemStyle CssClass="tableBody"  />                        
                                        <FooterStyle CssClass="tableBody"/>
                                        <HeaderStyle CssClass="tableHeader" />
                                        <Columns>
                                            <asp:BoundColumn DataField="ModifiedUserId"  HeaderStyle-Width="0%" Visible="false" ItemStyle-BackColor="#ECE9D8" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="ModifiedDateTime" HeaderText="<%$ Resources:WebResources, Date%>" ItemStyle-BackColor="#ECE9D8" ></asp:BoundColumn>
                                            <asp:BoundColumn DataField="ModifiedUserName" HeaderText="<%$ Resources:WebResources, User%>"  ItemStyle-BackColor="#ECE9D8" ></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Description" HeaderText="<%$ Resources:WebResources, Description%>"  ItemStyle-BackColor="#ECE9D8" ></asp:BoundColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
                
                <%--ZD 100664 - End --%>
                <%--<tr>
                    <td align="left">
                        <table id="Table4" cellpadding="2" cellspacing="2" border="0" style="width: 100%">
                            <tr align="left">
                                <td style="width: 10%" align="left" valign="top" class="blackblodtext" nowrap>
                                    Last Modified by :
                                </td>
                                <td style="width: 85%" align="left" valign="top">
                                    <asp:Label ID="lblMUser" runat="server" CssClass="active"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%" align="left" valign="top" class="blackblodtext" nowrap>
                                    Last Modified at :
                                </td>
                                <td style="width: 85%" align="left" valign="top">
                                    <asp:Label ID="lblMdate" runat="server" CssClass="active"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>--%>
                <tr>
                    <td class="subtitleblueblodtext" align="left">
                        <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, ManageVirtualMeetingRoom_BasicConfigura%>" runat="server"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" width="100%" cellpadding="2" cellspacing="2" style="margin-left:20px">
                        <%-- ZD 100522 Start --%>
                            <tr>
                                <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                    <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, ManageVirtualMeetingRoom_RoomName%>" runat="server"></asp:Literal> <span class="reqfldText">*</span>
                                </td> 
                                <td style="width: 20%" align="left" valign="middle" nowrap="nowrap">
                                    <asp:TextBox ID="txtRoomName" runat="server" CssClass="altText"></asp:TextBox>
                                    &nbsp; <span style="font-weight:bold;" >
                                    <asp:Literal ID="Literal20" Text="<%$ Resources:WebResources, ManageVirtualMeetingRoom_VMRID%>" runat="server"></asp:Literal> </span> 
                                    <asp:TextBox ID="txtVMRID" runat="server" CssClass="altText" onblur="javascript:EnableVMRIDValidation();"></asp:TextBox> <%--ZD 103833--%>
                                    <asp:RequiredFieldValidator ID="ReqtxtVMRID" runat="server" ControlToValidate="txtVMRID" Enabled="false"
                                        Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="<%$ Resources:WebResources, Required%>"
                                        ValidationGroup="Submit"></asp:RequiredFieldValidator><%--ZD 103263--%>

                                    <asp:ImageButton id="ImgVMRID" src="image/info.png" runat="server" alt="Info" style="cursor:default"
                                                tooltip="<%$ Resources:WebResources, ManageVirtualMeetingRoom_VMRReqMsg%>" OnClientClick="javascript:return false;" /><%-- ZD 102590 --%>
                                    <asp:ImageButton id="ImgBJNID" src="image/info.png" runat="server" alt="Info" style="cursor:default;display:none"
                                                tooltip="<%$ Resources:WebResources, ManageVirtualMeetingRoom_BJNReqMsg%>" OnClientClick="javascript:return false;" /> <%-- ZD 103263 --%>
                                      <br />
                                    <asp:RequiredFieldValidator ID="reqName" runat="server" ControlToValidate="txtRoomName"
                                        Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="<%$ Resources:WebResources, Required%>"
                                        ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regRoomName" ControlToValidate="txtRoomName"
                                        Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true"
                                        ErrorMessage="<%$ Resources:WebResources, InvalidCharacters35%>"
                                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@$%&~]*$"></asp:RegularExpressionValidator><%-- ZD 103424 ZD 104000--%>

                                    <asp:regularexpressionvalidator id="RegVMRIDNumeric" controltovalidate="txtVMRID" CssClass="lblError" style="margin-left: 210px" 
                                                display="dynamic" runat="server" validationgroup="Submit"  SetFocusOnError="true"
                                                errormessage="<%$ Resources:WebResources, NumericValue2%>" validationexpression="\d+"></asp:regularexpressionvalidator>

                                    <asp:RegularExpressionValidator ID="RegVMRID" ControlToValidate="txtVMRID"  CssClass="lblError" 
                                        Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, NumericValue3%>"
                                        ValidationExpression="^(\d){9,18}$"></asp:RegularExpressionValidator><%-- ZD 103263 --%>
                                      
                                </td>
                                <td style="width: 10%" align="left" valign="top" class="blackblodtext">
                                    <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, ManageVirtualMeetingRoom_AssistantInChar%>" runat="server"></asp:Literal><span class="reqfldText">*</span><%--FB 2975--%>
                                </td>
                                <td style="width: 30%" align="left" valign="top">
                                    <asp:TextBox ID="Assistant" runat="server" CssClass="altText"></asp:TextBox>
                                    <a id="EditHref" href="javascript:getYourOwnEmailList(-2);" onmouseover="window.status='';return true;">
                                        <img border="0" src="image/edit.gif" alt="edit" width="17" height="15" style="cursor:pointer;" title="<asp:Literal Text='<%$ Resources:WebResources, AddressBook%>' runat='server'></asp:Literal>" /></a> <%--FB 2798--%>
                                    <a href="javascript: deleteAssistant();" onmouseover="window.status='';return true;">
                                        <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16" style="cursor:pointer;" title="<asp:Literal Text='<%$ Resources:WebResources, Delete%>' runat='server'></asp:Literal>" /></a> <%--FB 2798--%>
                                    <asp:RequiredFieldValidator ID="AssistantValidator" runat="server" ControlToValidate="Assistant"
                                        Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="<%$ Resources:WebResources, Required%>"
                                        ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%" align="left" valign="top" class="blackblodtext">                                    
                                    <asp:Literal ID="Literal19" Text="<%$ Resources:WebResources, ManageVirtualMeetingRoom_SelectMCU%>" runat="server"></asp:Literal>
                                </td>
                                <td style="width: 20%" align="left" valign="top">
                                    <asp:DropDownList CssClass="altSelectFormat" AutoPostBack="true" OnSelectedIndexChanged="DisplayBridgeDetails" ID="lstMCU" runat="server" DataTextField="BridgeName" DataValueField="BridgeID" ></asp:DropDownList> <%--OnSelectedIndexChanged="DisplayBridgeDetails"--%>
                                    <asp:ImageButton id="ImgMCU" src="image/info.png" runat="server" alt="Info" tooltip="<%$ Resources:WebResources, ManageVirtualMeetingRoom_SelectMCUMsg%>" OnClientClick="javascript:return false;" style="cursor:default" /><%-- ZD 102590 --%>
                                </td>
                                <td id="tdcreateMCU" runat="server" style="width: 10%" align="left" valign="top" class="blackblodtext">  <%--ZD 103263--%>                                   
                                    <asp:Literal ID="Literal18" Text="<%$ Resources:WebResources, ManageVirtualMeetingRoom_CreateMCU%>" runat="server"></asp:Literal>
                                </td>
                                <td id="tdchkMCU" runat="server" style="width: 30%" align="left" valign="top">  <%--ZD 103263--%>
                                    <asp:CheckBox ID="chkCreateMCU" runat="server" onclick="javascript:CreateMCUFeature();" />  <%--ZD 103263 --%>
                                </td>
                            </tr>
                            <%--ZD 103263 Starts--%>
                            <tr id="trEnableBJN" runat="server">
                                <td style="width: 15%" align="left" valign="top" class="blackblodtext">                                    
                                    <asp:Literal ID="Literal22" Text="<%$ Resources:WebResources, ManageVirtualMeetingRoom_EnableBJN%>" runat="server"></asp:Literal>
                                </td>
                                <td style="width: 20%" align="left" valign="top">
                                    <asp:CheckBox ID="chkEnableBJN" runat="server" onclick="javascript:BJNFeature();" />
                                </td>
                                <%--ZD 103933--%>
                            </tr>
                            <%--ZD 103263 Ends--%>
                            <tr id="trVMRIntExtNumber" runat="server"> <%--ZD 103263--%>
                                <td align="left" class="blackblodtext" valign="top">
                                    <asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, ManageVirtualMeetingRoom_Internalnumber%>" runat="server"></asp:Literal>
                                </td>
                                <td align="left" valign="top">
                                    <asp:TextBox ID="txtInternalnum" Enabled="false" MaxLength="25" runat="server"></asp:TextBox>
                                </td>
                                <td align="left" class="blackblodtext" valign="top">
                                    <asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, ManageVirtualMeetingRoom_Externalnumber%>" runat="server"></asp:Literal>
                                </td>
                                <td align="left" valign="top">
                                    <asp:TextBox ID="txtExternalnum" Enabled="false" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <%--ZD 100753 Starts--%>
                            <tr id="trVMRPass">
                                <asp:Panel ID="pnlPassword1" runat="server" >
                                <td class="blackblodtext" align="left" valign="top">
                                <asp:Literal ID="Literal8" Text="<%$ Resources:WebResources, VMRPassword %>" runat="server"></asp:Literal> <%--ZD 100806--%>
                                  </td>
                                <td valign="top" nowrap="nowrap" align="left">
                                    <asp:TextBox ID="txtPassword1" runat="server" onblur="PasswordChange(1)" onfocus="fnTextFocus(this.id,1)"  style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat;" CssClass="altText"></asp:TextBox>  <%--style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat"--%> <%--ZD 100522--%>
                                    <asp:Button runat="server" ID="btnGenPassword" width="210px" Text="<%$ Resources:WebResources, ManageVirtualMeetingRoom_GeneratePassword%>" autopostback="false" CssClass="altMedium0BlueButtonFormat" OnClientClick="javascript:num_gen(); return false;" /><%-- FB 676--%>
                                    <asp:ImageButton id="ImgGenPassword" src="image/info.png" runat="server" alt="Info" style="cursor:default"
                                                tooltip="<%$ Resources:WebResources, ManageVirtualMeetingRoom_GenpwdMsg%>" OnClientClick="javascript:return false;" /><%-- ZD 102590 --%>
                                    <br />
                                    <asp:CompareValidator ID="cmpValPassword1" runat="server" ControlToCompare="txtPassword2"
                                        ControlToValidate="txtPassword1" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_password%>"></asp:CompareValidator>
                                    <%--<asp:RegularExpressionValidator ID="numPassword1" runat="server" ErrorMessage="<br> & < and > are invalid characters." SetFocusOnError="True" ToolTip="<br> & < and > are invalid characters." ControlToValidate="txtPassword1" ValidationExpression="^[^<>&]*$" Display="Dynamic"></asp:RegularExpressionValidator>--%>
                                    <asp:RegularExpressionValidator ID="numPassword1" runat="server" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters12%>" SetFocusOnError="True" ToolTip="<%$ Resources:WebResources, InvalidCharacters12%>" ControlToValidate="txtPassword1" ValidationExpression="^([1-9])([0-9]\d+)" Display="Dynamic"></asp:RegularExpressionValidator> <%--ZD 100522--%>
                                    <asp:RequiredFieldValidator ID="ReqtxtPassword1" runat="server" ControlToValidate="txtPassword1" Enabled="false"
                                        Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="<%$ Resources:WebResources, Required%>"
                                        ValidationGroup="Submit"></asp:RequiredFieldValidator><%--ZD 103263--%>
                                    <%--<asp:RegularExpressionValidator style="margin-left:15px"   ID="numPassword1" ControlToValidate="txtPW1" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>Need one uppercase,one lowercase,one numeric value,no space and  \ ` &quot; / = [ ] & < > are invalid characters." ValidationExpression="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d]{6,}"></asp:RegularExpressionValidator> --%>
                                </td>
                                </asp:Panel>
                                <asp:Panel ID="pnlPassword2" runat="server">
                                <td class="blackblodtext" align="left" valign="top">
                                <asp:Literal ID="Literal9" Text="<%$ Resources:WebResources, ManageUserProfile_ConfirmPasswor %>" runat="server"></asp:Literal></td>
                                <td style="height: 20px" valign="top" align="left">
                                    <asp:TextBox ID="txtPassword2" runat="server" CssClass="altText" onblur="PasswordChange(2)" onfocus="fnTextFocus(this.id,2)" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat;" ></asp:TextBox> <br /> <%--style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat"--%> <%--ZD 100522--%>
                                    <asp:CompareValidator ID="cmpValPassword2" runat="server" ControlToCompare="txtPassword1"
                                        ControlToValidate="txtPassword2" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, PasswordNotMatch1%>"></asp:CompareValidator>
                                </td>
                                </asp:Panel>
                            </tr>
                            <%--ZD 100753 Ends--%>
                            <tr>
                                <td align="left" class="blackblodtext" valign="top">
                                    <asp:Literal ID="Literal17" Text="<%$ Resources:WebResources, ManageVirtualMeetingRoom_CreateConfTemp%>" runat="server"></asp:Literal>
                                </td>
                                <td align="left" valign="top">
                                     <asp:CheckBox ID="chkCreateTemplate" runat="server" />
                                </td>
                                <td id="tdVMRLink" runat="server" align="left" class="blackblodtext" valign="top">
                                    <asp:Literal ID="Literal10" Text="<%$ Resources:WebResources, ManageVirtualMeetingRoom_VMRLink%>" runat="server"></asp:Literal> <%--ZD 100806--%>
                                </td>
                                <td align="left" valign="top">
                                    <asp:TextBox CssClass="altText" ID="txtVMRLink" runat="server"  MaxLength="325" Rows="5" ></asp:TextBox>
                                </td>                               
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext" valign="top">                                    
                                    <asp:Literal ID="Literal16" Text="<%$ Resources:WebResources, ManageVirtualMeetingRoom_Dialout%>" runat="server"></asp:Literal>
                                </td>
                                <td align="left" valign="top">
                                     <asp:CheckBox ID="ChkDialoutLoc" runat="server" /><br />
                                </td>
                                <td align="left">
                                    &nbsp;
                                </td>
                                <td align="left" class="blackblodtext">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                            <td>
                            </td>
                            <td align="left">
                             <input name="addRooms" runat="server" type="button" id="addRooms" onclick="javascript:AddRooms();" style="display:none;" />
                                <select size="4" name="RoomList" runat="server" id="RoomList" class="treeSelectedNode" Rows="3"></select>
                                <a href="" onclick="javascript:return OpenEndpointlist();" onmouseover="window.status='';return true;" runat="server" id="roomclick"><img border="0" id="roomimage" src="image/roomselect.gif" alt="edit" style="width:17px;cursor:pointer;"  title="<asp:Literal Text='<%$ Resources:WebResources, SelectLocation%>' runat='server'></asp:Literal>" /></a>  <%--FB 2798 ZD 100429--%>                             
                                <input type="hidden" id="hdnLocation" runat="server" />
                                <input type="hidden" id="hdnDialLoc" runat="server" />
                            </td>
                            </tr>
                        <%-- ZD 100522 End--%>
                        </table>
                    </td>
                </tr>
                
                <%--FB 2994 Start--%>
                
                <tr style="display:none"> <%-- ZD 100522 --%>
                    <td class="subtitleblueblodtext" align="left">
                        <asp:Literal ID="Literal11" Text="<%$ Resources:WebResources, ManageVirtualMeetingRoom_Image%>" runat="server"></asp:Literal>
                    </td>
                </tr>
                <tr style="display:none"> <%-- ZD 100522 --%>
                    <td>
                        <table id="Table1" cellpadding="2" cellspacing="2" border="0" style="width: 90%; margin-left:20px">
                            <tr>
                                <td align="left" style="width:18%" valign="top" class="blackblodtext">
                                    <asp:Literal ID="Literal12" Text="<%$ Resources:WebResources, ManageVirtualMeetingRoom_RoomImage%>" runat="server"></asp:Literal>
                                </td>
                                <td colspan="3" align="left" class="blackblodtext"> 
                                    <div>
                                        <input type="text" class="file_input_textbox" readonly="readonly" value="<asp:Literal Text='<%$ Resources:WebResources, Nofileselected%>' runat='server'></asp:Literal>" />
                                        <div class="file_input_div">
                                            <input type="button" runat="server" value="<%$ Resources:WebResources, Browse%>" class="file_input_button" onclick="document.getElementById('roomfileimage').click();return false;"   /><%--FB 3055-Filter in Upload Files--%>
                                            <input type="file"  class="file_input_hidden" id="roomfileimage" accept="image/*" tabindex="-1" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this);fnValidateFileName(this.value,1);"/>
                                        </div>
                                    </div>
                                    <asp:RegularExpressionValidator ID="regroomfileimage" runat="server" Display="Dynamic" ValidationGroup="Submit1" ControlToValidate="roomfileimage" CssClass="lblError" ErrorMessage="<%$ Resources:WebResources, Filetypeinvalid%>" ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G)|(b|B)(m|M)(p|P))$"></asp:RegularExpressionValidator>
                                    <div><%--ZD 102277--%>
                                     <asp:RegularExpressionValidator ID="regFNVal1" ControlToValidate="roomfileimage"
                                                    Display="dynamic" runat="server" ValidationGroup="Submit1" SetFocusOnError="true"
                                                    ErrorMessage="<%$ Resources:WebResources, InvalidCharacters45%>"
                                                    ValidationExpression="^[^<>&#]*$"></asp:RegularExpressionValidator></div>
                                    <asp:Button ID="BtnUploadRmImg" CssClass="altLongBlueButtonFormat" runat="server"  style="margin-left:2px"
                                        Text="<%$ Resources:WebResources, ManageVirtualMeetingRoom_BtnUploadRmImg%>" OnClick="UploadRoomImage" ValidationGroup="Submit1" /> <%--FB 3055-Filter in Upload Files--%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="display:none"> <%-- ZD 100522 --%>
                    <td align="left" colspan = "4"> 
                        <div style="overflow-y: hidden; overflow-x: auto; height: auto; width: 600px; margin-left:18%">
                            <asp:DataGrid BorderColor="blue" BorderStyle="solid" BorderWidth="1" ID="dgItems"
                                AutoGenerateColumns="false" OnItemCreated="BindRowsDeleteMessage" OnDeleteCommand="RemoveImage"
                                runat="server" Width="70%" GridLines="None" Visible="false" Style="border-collapse: separate">
                                <HeaderStyle Height="30" CssClass="tableHeader" HorizontalAlign="Center" />
                                <AlternatingItemStyle CssClass="tableBody" />
                                <ItemStyle CssClass="tableBody" />
                                <FooterStyle CssClass="tableBody" />
                                <Columns>
                                    <asp:BoundColumn DataField="ImageName" Visible="true" HeaderText="<%$ Resources:WebResources, Name%>" HeaderStyle-CssClass="tableHeader"
                                        ItemStyle-Width="20%"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Image" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Imagetype" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="ImagePath" Visible="false"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Image" HeaderStyle-CssClass="tableHeader" ItemStyle-Width="40%" ItemStyle-HorizontalAlign="center">
                                        <ItemTemplate>
                                            <asp:Image ID="itemImage" AlternateText="Item Image" ImageUrl='<%# DataBinder.Eval(Container, "DataItem.ImagePath") %>'  
                                                Width="30" Height="30" runat="server" /> <%--ZD 100419--%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Actions%>" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnDelete" Text="<%$ Resources:WebResources, ManageVirtualMeetingRoom_btnDelete%>" CommandName="Delete" runat="server"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid><br />
                        </div>
                    </td>
                </tr>
                
                <%--FB 2994 End--%>
                
                <tr>
                    <td align="left">
                        <span class="subtitleblueblodtext"><asp:Literal ID="Literal13" Text="<%$ Resources:WebResources, ManageVirtualMeetingRoom_Department%>" runat="server"></asp:Literal></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="tbRoomsDept" cellpadding="2" cellspacing="2" border="0" style="width: 90%; margin-left:20px">
                            <tr>
                                <td width="16%" align="left" valign="top" class="blackblodtext">
                                    <asp:Literal ID="Literal14" Text="<%$ Resources:WebResources, ManageVirtualMeetingRoom_RoomsDepartmen%>" runat="server"></asp:Literal>
                                </td>
                                <td style="width: 75%" align="left" valign="top">
                                    <asp:ListBox ID="DepartmentList" runat="server" CssClass="altText" DataTextField="Name"
                                        DataValueField="ID" SelectionMode="multiple"></asp:ListBox>
                                      <%--ZD 100422 Start--%>
                                     <span style='color: #666666;'>* <asp:Literal ID="Literal15" Text="<%$ Resources:WebResources, SelectMultiDept%>" runat="server"></asp:Literal></span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="height: 8px">
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table id="tblButtons" cellpadding="2" cellspacing="2" border="0" style="width: 90%">
                            <tr>
                                <td align="center" style="width: 20%">
                                    <asp:Button ID="btnReset" runat="server" Text="<%$ Resources:WebResources, Reset%>" CssClass="altMedium0BlueButtonFormat"
                                        OnClick="ResetRoomProfile" />
                                </td>
                                <td align="center" style="width: 20%">
                                    <input name="Go" id="btnGoBack" runat="server" type="button" class="altMedium0BlueButtonFormat" onclick="javascript:fnClose();"
                                        value=" <%$ Resources:WebResources, GoBack %> " /><%--ZD 100428--%>
                                </td>
                                <td align="center" style="width: 20%">
                                    <asp:Button ID="btnSubmitAddNew" runat="server" ValidationGroup="Submit" Text="<%$ Resources:WebResources, ManageVirtualMeetingRoom_btnSubmitAddNew%>"
                                         OnClick="SetRoomProfile" OnClientClick="javascript:return frmMainroom_Validator()" style="width:200px" />
                                </td>
                                <td align="center" style="width: 20%">
                                    <asp:Button ID="btnSubmit" ValidationGroup="Submit" runat="server" Text="<%$ Resources:WebResources, ManageVirtualMeetingRoom_btnSubmit%>" Width="150pt"
                                         OnClick="SetRoomProfile" OnClientClick="javascript:return frmMainroom_Validator()" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center>
    </div>
    </form>
    <%--ZD 100522 Starts--%>
    <div id="PopupRoomList" class="rounded-corners" style="position: absolute;
    overflow:hidden; border: 0px;
    width: 98%; display: none;">    
    <iframe src="" id="formRoomList" name="formRoomList" style="height: 630px; border: 0px; overflow:hidden; width: 98%; overflow: hidden;"></iframe>
	</div>
	<%--ZD 100522 End--%>
</body>
</html>

<script type="text/javascript">
    document.onkeydown = function(evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
            if (document.getElementById("btnGoBack") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnGoBack").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };

    if (document.getElementById("hdnMultipleDept"))
        if (document.getElementById("hdnMultipleDept").value == "0") {
        if (document.getElementById("trRoomsDept") != null)
            document.getElementById("trRoomsDept").style.display = "none";
    }
    
    if (document.getElementById("hdnVMRpwd").value != "") {
        var val = document.getElementById("hdnVMRpwd").value;
        document.getElementById("txtPassword1").value = val;
        document.getElementById("txtPassword2").value = val;
    }
	//ZD 103263 Start
    if ((document.getElementById("chkEnableBJN") != null))
    {
        if ((document.getElementById("chkEnableBJN").checked)) {
            document.getElementById("ImgVMRID").style.display = "none";
            document.getElementById("ImgBJNID").style.display = "inline";
            ValidatorEnable(document.getElementById("RegVMRID"), true); //ZD 103833
            ValidatorEnable(document.getElementById("RegVMRIDNumeric"), false); //ZD 103833

        }
        else {
            document.getElementById("ImgVMRID").style.display = "inline";
            document.getElementById("ImgBJNID").style.display = "none";
            ValidatorEnable(document.getElementById("RegVMRID"), false); //ZD 103833
            ValidatorEnable(document.getElementById("RegVMRIDNumeric"), true); //ZD 103833
        }
    }
    else {
        ValidatorEnable(document.getElementById("RegVMRID"), false); //ZD 103833
        ValidatorEnable(document.getElementById("RegVMRIDNumeric"), true); //ZD 103833
    }
    //ZD 103263 End
</script>

<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->

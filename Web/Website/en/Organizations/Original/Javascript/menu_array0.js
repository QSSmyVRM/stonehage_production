/*ZD 100147 Start*/
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 ZD 100886 End*/
menunum = 0;
menus = new Array();
_d = document;
//ZD 102356
var calDefaultView = 2;
var hdnCalDefaultView = document.getElementById("hdnCalDefaultView");
if (hdnCalDefaultView != null)
    calDefaultView = hdnCalDefaultView.value;

function addmenu() {
    menunum++;
    menus[menunum] = menu;
}

function dumpmenus() {
    mt = "<script language=javascript>";
    for (a = 1; a < menus.length; a++) {
        mt += " menu" + a + "=menus[" + a + "];"
    }
    mt += "</script>";
    _d.write(mt)
}
// FB 3055 Ends


if (navigator.appVersion.indexOf("MSIE") > 0) // FB 2815
{
    effect = "Fade(duration=0.2); Alpha(style=0,opacity=88); Shadow(color='#777777', Direction=135, Strength=5)"
}
else {
    effect = "Shadow(color='#777777', Direction=135, Strength=5)"
}

effect = ""			// 2785.2

timegap = 500; followspeed = 5; followrate = 40;
suboffset_top = 10; suboffset_left = 10;

style1 = ["white", "Purple", "Purple", "#FFF7CE", "lightblue", 12, "normal", "bold", "Arial, Verdana", 4, , , "66ffff", "000099", "Purple", "white", , "ffffff", "000099"]

style1_2 = ["#041433", "#ffffff", "#046380", "#E0E0E0", "lightblue", 12, "normal", "bold", "Arial, Verdana", 4, "", "", "#66ffff", "#000099", "#041433", "", "", "", "#000099", ] // FB 2791 //ZD 100156

style2 = ["black", "#FFF7CE", "#046380", "#FFF7CE", "lightblue", 120, "normal", "bold", "Arial, Verdana", 4, "image/menuarrow.gif", , "#66ffff", "#000099", "#046380", "", , "", "000099"] // FB 2791

style2_2 = ["#5E5D5E", "#E0E0E0", "#ffffff", "#7c7c7c", "transparent", 12, "normal", "bold", "Arial, Verdana", 10, "", "", "66ffff", "000099", "#041433", "#ffffff", "", "#046380", "#000099", ] // FB 2791
style2_3 = ["#5E5D5E", "#C2C2C2", "#ffffff", "#7c7c7c", "transparent", 12, "normal", "bold", "Arial, Verdana", 10, "", "", "66ffff", "000099", "#041433", "#ffffff", "", "#046380", "#000099", ] // FB 2791

var menu1Val, menu5Val;

menu1Val = ["<div id='menuHome' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Home</div>", "SettingSelect2.aspx", , "Go to the Home page", 0]; // FB 2719
if (defaultConfTempJS == "0")//FB 1755
    menu5Val = "ConferenceSetup.aspx?t=n&op=1"; // FB 2719 ZD 101233
else
    menu5Val = "ConferenceSetup.aspx?t=t"; // FB 2719 ZD 101233

if (navigator.appVersion.indexOf("MSIE") > 0) // FB 2815 FB 2827
{

    menu_0 = ["mainmenu", 5, 250, 90, , , style1, 1, "center", effect, , 1, , , , , , , , , , ]			// 24 FB 2719
    menu_0_2 = ["mainmenu", 5, 250, 90, , , style1_2, 1, "center", effect, , 1, , , , , , , , , , ]		// 24//FB 1565 FB 2719
	//ZD 101388 Commented
    //if (isExpressUser != null)//FB 1779 FB 2827
    // if (isExpressUser == 1)
	//    menu_0_2 = ["mainmenu", 5, 250, 90, , , style1_2, 1, "center", effect, , 1, , , , , , , , , , ]

    menu_1 = menu1Val
    menu_2 = ["<div id='menuCal' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);' ><br><br><br><br>Conferences</div>", "show-menu=conferences", , "Conferences", 0] // FB 2719
    menu_3 = ["<div id='menuConf' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);' onkeypress='fnFireEvent(this)' ><br><br><br><br>New<br>Conference</div>", "show-menu=confsetup", , "Create a New Conference", 0]; // ZD 101233
    menu_4 = ["<div id='menuSet' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);' ><br><br><br><br>Settings</div>", "show-menu=settings", , "My Settings", 0] // FB 2719
    menu_5 = ["<div id='menuAdmin' tabindex='1' onfocus='fnOnFocus(this);' ><br><br><br><br>Administration</div>", "show-menu=organization", , "Organization", 0] // FB 2719
    menu_6 = ["<div id='menuSite' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);' ><br><br><br><br>Site</div>", "show-menu=site", , "Site", 0] // FB 2719
    menu_7 = ["<div id='menuCall' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Schedule A Call</div>", "ExpressConference.aspx?t=n", , "Schedule a Call", 0] //FB 1779 FB 2827
    //ZD 100167 START
	//menu_8 = ["<div id='menuReservation' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Reservation</div>", "ConferenceList.aspx?t=3", , "View Reservations", 0] //FB 1779 FB 2827
    menu_8 = ["<div id='menuReservation' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Reservation</div>", "ConferenceList.aspx?t=3", , "View Reservations", 0] //FB 1779 FB 2827
    menu_9 = ["<div id='menuInstantConference' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Instant Call</div>", "javascript:ModalPopUp();", , "Instant Conference", 0]
    //ZD 100167 END
    menu = new Array();
    menu = (if_str == "2") ? menu.concat(menu_0_2) : menu.concat(menu_0);

    for (i = 1; i <= mmm_num; i++) {
        menu = (mmm_int & (1 << (mmm_num - i))) ? (menu.concat(eval("menu_" + i))) : menu;
        if (i == 8 && document.getElementById('isCloud').value == '1' && document.getElementById('isExpressUser').value == '1') {
            menu = menu.concat(eval("menu_9"));
        }
    }
    addmenu();

    // ZD 101233 Start
    submenu0_0 = ["conferences", 116, , 100, 1, , style2, , "left", effect, , , , , , , , , , , , ]
    submenu0_0_2 = ["conferences", 116, , 100, 1, , style2_2, , "left", effect, , , , , , , , , , , , ]
    //ZD 102356
    if(calDefaultView == 1)
        submenu0_1 = ["Calendar", "personalCalendar.aspx?v=1&r=1&hf=&m=&pub=&d=&comp=&f=v", , "Personal Calendar", 0]
    else
        submenu0_1 = ["Calendar", "roomcalendar.aspx?v=1&r=1&hf=&d=&pub=&m=&comp=", , "Room Calendar", 0]

    submenu0_2 = ["Call Monitor", "MonitorMCU.aspx", , "Call Monitor", 0]
    submenu0_3 = ["Dashboard", "Dashboard.aspx", , "Dashboard", 0] //ZD 102054
    submenu0_4 = ["List", "ConferenceList.aspx?t=" + hasConferenceList, , "List", 0]

    submenu1_0 = ["confsetup", 116, , 120, 1, , style2, , "left", effect, , , , , , , , , , , , ]
    submenu1_0_2 = ["confsetup", 116, , 120, 1, , style2_2, , "left", effect, , , , , , , , , , , , ]
    submenu1_1 = ["Express Form", "ExpressConference.aspx?t=n", , "Express Form", 0] //ZD 102054
    submenu1_2 = ["Advanced Form", menu5Val, , "Advanced Form", 0] //ZD 102054
    // ZD 101233 End

    submenu2_0 = ["settings", 116, , 120, 1, , style2, , "left", effect, , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu2_0_2 = ["settings", 116, , 120, 1, , style2_2, , "left", effect, , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu2_1 = ["Account Settings", "ManageUserProfile.aspx", , "Edit preferences", 0] //ZD 101930
    submenu2_2 = ["Reports", "GraphicalReport.aspx", , "Reports", 0] //FB 2593 //FB 2885 Start
    submenu2_3 = ["Templates", "ManageTemplate.aspx", , "Manage Conference Templates", 0]
    submenu2_4 = ["Groups", "ManageGroup.aspx", , "manage group", 0]

    submenu3_0 = ["organization", 116, , 120, 1, , style2, , "left", effect, , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu3_0_2 = ["organization", 116, , 120, 1, , style2_2, , "left", effect, , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890

    submenu3_1 = ["Hardware", "show-menu=mcu", , "Manage Hardware", 0]
    submenu3_1_ = ["Hardware", "", , , 0]
    submenu3_2 = ["Locations", "show-menu=loc", , "Manage locations", 0]
    submenu3_2_ = ["Locations", "", , , 0]
    submenu3_3 = ["Users", "show-menu=usr", , "Manage users", 0]
    submenu3_4 = ["Options", "mainadministrator.aspx", , "Edit System Settings", 0]
    submenu3_5 = ["Settings", "OrganisationSettings.aspx", , "Edit Organization Settings", 0] // FB 2719
    submenu3_6 = ["Audiovisual", "show-menu=av", , , 0]
    submenu3_6_ = ["Audiovisual", "", , , 0] // FB 2570
    submenu3_7 = ["Catering", "show-menu=catr", , , 0]
    submenu3_7_ = ["Catering", "", , , 0]
    submenu3_8 = ["Facility", "show-menu=hk", , , 0]
    submenu3_8_ = ["Facility", "", , , 0] // FB 2570 // FB 2593 End

    submenu4_0 = ["site", 116, , 100, 1, , style2, , "left", effect, , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu4_0_2 = ["site", 116, , 100, 1, , style2_2, , "left", effect, , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu4_1 = ["Settings ", "SuperAdministrator.aspx", , "Edit system settings", 0]

    submenu3_1_0 = ["mcu", , , 140, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_1_0_2 = ["mcu", , , 140, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_1_1 = ["Endpoints", "EndpointList.aspx?t=", , "Setup Bridge", 0]
    submenu3_1_2 = ["Diagnostics", "EventLog.aspx", , "View hardware problem log", 0]
    submenu3_1_3 = ["MCUs", "ManageBridge.aspx", , "Edit Bridge Management", 0]
    submenu3_1_4 = ["MCU Load Balancing", "ManageMCUGroups.aspx", , "MCU Load Balancing", 0] //ZD 100040
    submenu3_1_5 = ["Audio Bridges", "ManageAudioAddOnBridges.aspx", , "Audio Bridge Management", 0] //FB 2023
    submenu3_1_6 = ["EM7", "EM7Dashboard.aspx", , "EM7", 0] //FB 2633

    submenu3_2_0 = ["loc", , , 100, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_2_0_2 = ["loc", , , 100, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_2_1 = ["Rooms", "manageroom.aspx?hf=&m=&pub=&d=&comp=&f=&frm=", , "Edit or set up conference room", 0]
    submenu3_2_2 = ["Tiers", "ManageTiers.aspx", , "Manage Tiers", 0]
    submenu3_2_3 = ["Floor Plans", "RoomFloor.aspx", , "Floor Plans", 0] //ZD 102123


    submenu3_3_0 = ["usr", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_3_0_2 = ["usr", , , 150, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_3_1 = ["Active Users", "ManageUser.aspx?t=1", , "Edit, search or create a new user", 0]
    submenu3_3_2 = ["Bulk Tool", "allocation.aspx", , "Manage Bulk User", 0]
    submenu3_3_3 = ["Departments", "ManageDepartment.aspx", , "Manage Department", 0]
    submenu3_3_4 = ["Guests", "ManageUser.aspx?t=2", , "Edit, search or create a new guest", 0]
    if (sso_int)
        submenu3_3_5 = ["Restore User", "ManageUser.aspx?t=3", , "Delete or Replaced a removed user", 0]
    else
        submenu3_3_5 = ["Inactive Users", "ManageUser.aspx?t=3", , "Delete or Replaced a removed user", 0]
    submenu3_3_6 = ["LDAP Directory Import", "LDAPImport.aspx", , "Setup Multiple Users", 0]
    submenu3_3_7 = ["LDAP Groups", "LDAPGroup.aspx", , "Setup Multiple Groups", 0]
    submenu3_3_8 = ["Roles", "manageuserroles.aspx", , "Edit, search or create user roles", 0]
    submenu3_3_9 = ["Templates", "ManageUserTemplatesList.aspx", , "Edit, search or create user templates", 0]

    submenu3_6_0 = ["av", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_6_0_2 = ["av", , , 150, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_6_1 = ["Audiovisual Inventories", "InventoryManagement.aspx?t=1", , "", 0] // FB 2570
    submenu3_6_2 = ["Work Orders", "ConferenceOrders.aspx?t=1", , "", 0]


    submenu3_7_0 = ["catr", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_7_0_2 = ["catr", , , 150, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_7_1 = ["Catering Menus", "InventoryManagement.aspx?t=2", , "", 0] // FB 2570
    submenu3_7_2 = ["Work Orders ", "ConferenceOrders.aspx?t=2", , "", 0]

    submenu3_8_0 = ["hk", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_8_0_2 = ["hk", , , 150, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_8_1 = ["Facility Services", "InventoryManagement.aspx?t=3", , "", 0] // FB 2570
    submenu3_8_2 = ["Work Orders  ", "ConferenceOrders.aspx?t=3", , "", 0]

    cur_munu_no = 1; cur_munu_str = "0_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            if (EnableCallMonitor == 0 && i == 2) continue; 
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    // ZD 101233 Starts
    cur_munu_no = 2; cur_munu_str = "1_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
    // ZD 101233 Ends

    cur_munu_no = 3; cur_munu_str = "2_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
    cur_munu_no = 4; cur_munu_str = "3_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        '<%Session["SettingsMenu"] = "1";%>'
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no] - 3; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        for (i = mms_num[cur_munu_no] - 2; i <= mms_num[cur_munu_no]; i++) {
            if (rf_int & 1)
                menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
            rf_int = rf_int / 2;
        }
        addmenu();
    }

    cur_munu_no = 5; cur_munu_str = "3_1_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            if (EnableAudioBridge == 0 && i == 5) continue; //FB 2023//ZD 100040
            if (EnableEM7Opt == 0 && i == 6) continue; // FB 2633//ZD 100040
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
    cur_munu_no = 6; cur_munu_str = "3_2_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
    cur_munu_no = 7; cur_munu_str = "3_3_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            if ((EnableCloudInstallation == 1 && EnableAdvancedUserOption == 1) && (UsrCrossAccess == 1 || UsrCrossAccess == 0) && i == 7) continue; //ZD 100164
                menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 8; cur_munu_str = "3_6_"; //FB 2593 //FB 2885 // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 9; cur_munu_str = "3_7_"; //FB 2593 //FB 2885 // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 10; cur_munu_str = "3_8_"; //FB 2593 //FB 2885 // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 11; cur_munu_str = "4_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
}
else {
    menu_0 = ["mainmenu", 5, 250, 110, , , style1, 1, "center", effect, , 1, , , , , , , , , , , ]			// 24 FB 2719
    menu_0_2 = ["mainmenu", 5, 250, 110, , , style1_2, 1, "center", effect, , 1, , , , , , , , , , , ]		// 24 // FB 2050 FB 2719
    menu_1 = menu1Val
    menu_2 = ["<div id='menuCal' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);'><br><br><br><br>Conferences</div>", "show-menu=conferences", , "Conferences", 0] // FB 2719
    menu_3 = ["<div id='menuConf' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);' onkeypress='fnFireEvent(this)' ><br><br><br><br>New<br>Conference</div>", "show-menu=confsetup", , "Create a New Conference", 0]; // ZD 101233
    menu_4 = ["<div id='menuSet' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);'><br><br><br><br>Settings</div>", "show-menu=settings", , "My Settings", 0] // FB 2719
    menu_5 = ["<div id='menuAdmin' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);' ><br><br><br><br>Administration</div>", "show-menu=organization", , "Organization", 0] // FB 2719
    menu_6 = ["<div id='menuSite' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);' ><br><br><br><br>Site</div>", "show-menu=site", , "Site", 0] // FB 2719
    menu_7 = ["<div id='menuCall' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Schedule A Call</div>", "ExpressConference.aspx?t=n", , "Schedule a Call", 0] //FB 1779 FB 2827
    //ZD 100167 START
	//menu_8 = ["<div id='menuReservation' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Reservation</div>", "ConferenceList.aspx?t=3", , "View Reservations", 0] //FB 1779 FB 2827
    menu_8 = ["<div id='menuReservation' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Reservation</div>", "ConferenceList.aspx?t=3", , "View Reservations", 0] //FB 1779 FB 2827
    menu_9 = ["<div id='menuInstantConference' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Instant Call</div>", "javascript:ModalPopUp();", , "Instant Conference", 0]
    //ZD 100167 END
    menu = new Array();
    menu = (if_str == "2") ? menu.concat(menu_0_2) : menu.concat(menu_0);

    for (i = 1; i <= mmm_num; i++) {
        menu = (mmm_int & (1 << (mmm_num - i))) ? (menu.concat(eval("menu_" + i))) : menu;
        if(i == 8 && document.getElementById('isCloud').value == '1' && document.getElementById('isExpressUser').value == '1') {
            menu = menu.concat(eval("menu_9"));
        }
    }
    addmenu();

    // ZD 101233 Start
    submenu0_0 = ["conferences", 116, , 100, 1, , style2, , "left", effect, , , , , , , , , , , , , ]
    submenu0_0_2 = ["conferences", 116, , 100, 1, , style2_2, , "left", effect, , , , , , , , , , , , , ]
    //ZD 102356
    if (calDefaultView == 1)
        submenu0_1 = ["Calendar", "personalCalendar.aspx?v=1&r=1&hf=&m=&pub=&d=&comp=&f=v", , "Personal Calendar", 0]
    else
        submenu0_1 = ["Calendar", "roomcalendar.aspx?v=1&r=1&hf=&d=&pub=&m=&comp=", , "Room Calendar", 0]
        
    submenu0_2 = ["Call Monitor", "MonitorMCU.aspx", , "Call Monitor", 0]
    submenu0_3 = ["Dashboard", "Dashboard.aspx", , "Dashboard", 0] //ZD 102054
    submenu0_4 = ["List", "ConferenceList.aspx?t=" + hasConferenceList, , "List", 0]

    submenu1_0 = ["confsetup", 116, , 120, 1, , style2, , "left", effect, , , , , , , , , , , , , ]
    submenu1_0_2 = ["confsetup", 116, , 120, 1, , style2_2, , "left", effect, , , , , , , , , , , , , ]
    submenu1_1 = ["Express Form", "ExpressConference.aspx?t=n", , "Express Form", 0] //ZD 102054
    submenu1_2 = ["Advanced Form", menu5Val, , "Advanced Form", 0] //ZD 102054
    // ZD 101233 End

    submenu2_0 = ["settings", 116, , 120, 1, , style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu2_0_2 = ["settings", 116, , 120, 1, , style2_2, , "left", effect, , , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu2_1 = ["Account Settings", "ManageUserProfile.aspx", , "Edit preferences", 0] //ZD 101930
    submenu2_2 = ["Reports", "GraphicalReport.aspx", , "Reports", 0] //FB 2593 //FB 2885 Starts
    submenu2_3 = ["Templates", "ManageTemplate.aspx", , "Manage Conference Templates", 0]
    submenu2_4 = ["Groups", "ManageGroup.aspx", , "manage group", 0]


    submenu3_0 = ["organization", 116, , 120, 1, , style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu3_0_2 = ["organization", 116, , 120, 1, , style2_2, , "left", effect, , , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890

    submenu3_1 = ["Hardware", "show-menu=mcu", , "Manage Hardware", 0]
    submenu3_1_ = ["Hardware", "", , , 0]
    submenu3_2 = ["Locations", "show-menu=loc", , "Manage locations", 0]
    submenu3_2_ = ["Locations", "", , , 0]
    submenu3_3 = ["Users", "show-menu=usr", , "Manage users", 0]
    submenu3_3_ = ["Users", "", , , 0]
    submenu3_4 = ["Options", "mainadministrator.aspx", , "Edit System Settings", 0]
    submenu3_5 = ["Settings", "OrganisationSettings.aspx", , "Edit Organization Settings", 0]// FB 2719
    submenu3_6 = ["Audiovisual", "show-menu=av", , , 0]
    submenu3_6_ = ["Audiovisual", "", , , 0] // FB 2570
    submenu3_7 = ["Catering", "show-menu=catr", , , 0]
    submenu3_7_ = ["Catering", "", , , 0]
    submenu3_8 = ["Facility", "show-menu=hk", , , 0]
    submenu3_8_ = ["Facility", "", , , 0] // FB 2570 // FB 2593 End

    submenu4_0 = ["site", 116, , 100, 1, , style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu4_0_2 = ["site", 116, , 100, 1, , style2_2, , "left", effect, , , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu4_1 = ["Settings ", "SuperAdministrator.aspx", , "Edit system settings", 0]

    submenu3_1_0 = ["mcu", , , 140, 1, "", style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_1_0_2 = ["mcu", , , 140, 1, "", style2_3, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_1_1 = ["Endpoints", "EndpointList.aspx?t=", , "Setup Bridge", 0]
    submenu3_1_2 = ["Diagnostics", "EventLog.aspx", , "View hardware problem log", 0]
    submenu3_1_3 = ["MCUs", "ManageBridge.aspx", , "Edit Bridge Management", 0]
    submenu3_1_4 = ["MCU Load Balancing", "ManageMCUGroups.aspx", , "MCU Load Balancing", 0] //ZD 100040
    submenu3_1_5 = ["Audio Bridges", "ManageAudioAddOnBridges.aspx", , "Audio Bridge Management", 0] //FB 2023
    submenu3_1_6 = ["EM7", "EM7Dashboard.aspx", , "EM7", 0] //FB 2633
    
    submenu3_2_0 = ["loc", , , 100, 1, "", style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_2_0_2 = ["loc", , , 100, 1, "", style2_3, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_2_1 = ["Rooms", "manageroom.aspx?hf=&m=&pub=&d=&comp=&f=&frm=", , "Edit or set up conference room", 0]
    submenu3_2_2 = ["Tiers", "ManageTiers.aspx", , "Manage Tiers", 0]
    submenu3_2_3 = ["Floor Plans", "RoomFloor.aspx", , "Floor Plans", 0] //ZD 102123


    submenu3_3_0 = ["usr", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_3_0_2 = ["usr", , , 150, 1, "", style2_3, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_3_1 = ["Active Users", "ManageUser.aspx?t=1", , "Edit, search or create a new user", 0]
    submenu3_3_2 = ["Bulk Tool", "allocation.aspx", , "Manage Bulk User", 0]
    submenu3_3_3 = ["Departments", "ManageDepartment.aspx", , "Manage Department", 0]
    submenu3_3_4 = ["Guests", "ManageUser.aspx?t=2", , "Edit, search or create a new guest", 0]
    if (sso_int)
        submenu3_3_5 = ["Restore User", "ManageUser.aspx?t=3", , "Delete or Replaced a removed user", 0]
    else
        submenu3_3_5 = ["Inactive Users", "ManageUser.aspx?t=3", , "Delete or Replaced a removed user", 0]
    submenu3_3_6 = ["LDAP Directory Import", "LDAPImport.aspx", , "Setup Multiple Users", 0]
    submenu3_3_7 = ["LDAP Groups", "LDAPGroup.aspx", , "Setup Multiple Groups", 0]
    submenu3_3_8 = ["Roles", "manageuserroles.aspx", , "Edit, search or create user roles", 0]
    submenu3_3_9 = ["Templates", "ManageUserTemplatesList.aspx", , "Edit, search or create user templates", 0]

    submenu3_6_0 = ["av", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_6_0_2 = ["av", , , 150, 1, "", style2_3, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_6_1 = ["Audiovisual Inventories", "InventoryManagement.aspx?t=1", , "", 0] // FB 2570
    submenu3_6_2 = ["Work Orders", "ConferenceOrders.aspx?t=1", , "", 0]

    submenu3_7_0 = ["catr", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_7_0_2 = ["catr", , , 150, 1, "", style2_3, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_7_1 = ["Catering Menus", "InventoryManagement.aspx?t=2", , "", 0] // FB 2570
    submenu3_7_2 = ["Work Orders ", "ConferenceOrders.aspx?t=2", , "", 0]

    submenu3_8_0 = ["hk", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_8_0_2 = ["hk", , , 150, 1, "", style2_3, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_8_1 = ["Facility Services", "InventoryManagement.aspx?t=3", , "", 0] // FB 2570
    submenu3_8_2 = ["Work Orders  ", "ConferenceOrders.aspx?t=3", , "", 0]

    cur_munu_no = 1; cur_munu_str = "0_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            if (EnableCallMonitor == 0 && i == 2) continue; 
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    // ZD 101233 Starts
    cur_munu_no = 2; cur_munu_str = "1_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
    // ZD 101233 Ends

    cur_munu_no = 3; cur_munu_str = "2_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 4; cur_munu_str = "3_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        '<%Session["SettingsMenu"] = "1";%>'
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no] - 3; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        for (i = mms_num[cur_munu_no] - 2; i <= mms_num[cur_munu_no]; i++) {
            if (rf_int & 1)
                menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
            rf_int = rf_int / 2;
        }
        addmenu();
    }

    cur_munu_no = 5; cur_munu_str = "3_1_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            if (EnableAudioBridge == 0 && i == 5) continue; //FB 2023//ZD 100040
            if (EnableEM7Opt == 0 && i == 6) continue; // FB 2633//ZD 100040
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
    cur_munu_no = 6; cur_munu_str = "3_2_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
    cur_munu_no = 7; cur_munu_str = "3_3_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            if ((EnableCloudInstallation == 1 && EnableAdvancedUserOption == 1) && (UsrCrossAccess == 1 || UsrCrossAccess == 0) && i == 7) continue; //ZD 100164
                menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 8; cur_munu_str = "3_6_"; //FB 2593 //FB 2885 // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 9; cur_munu_str = "3_7_"; //FB 2593 //FB 2885 // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 10; cur_munu_str = "3_8_"; //FB 2593 //FB 2885 // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 11; cur_munu_str = "4_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
}
dumpmenus()

function fnFireEvent(cur) {
    var code = window.event.keyCode;
    if(code == 13)
        cur.parentNode.parentNode.click();
}

function fnOnFocus(cur) {
    //if (cur.id != 'menuAdmin' && document.getElementById('menuAdmin') != null)
        //fnOnBlur(document.getElementById('menuAdmin'));
    cur.parentNode.onmouseover();
}

function fnOnBlur(cur) {
    cur.parentNode.onmouseout();
}

document.onkeydown = function(evt) {
    fnOnKeyDown(evt);
};

function fnOnKeyDown(evt) {
    startTimer(); //ZD 100732
    evt = evt || window.event;
    var keyCode = evt.keyCode;
    if (keyCode == 40) {
        var str = document.activeElement.id;
        var mainMenu = ["menuCal", "menuSet", "menuAdmin", "menuSite"];
        if (str.indexOf("menu") > -1) {
            for (var i = 0; i < mainMenu.length; i++) {
                if (str == mainMenu[i]) {
                    var calSubMenu = [["Calendar", "Call Monitor"],
                    ["Account Settings", "Reports", "Templates", "Groups"], //ZD 101930
                    ["Hardware", "Locations", "Users", "Options", "Settings", "Audiovisual", "Catering", "Facility"],
                    ["Settings "]];
                    for (j = 0; j < calSubMenu[i].length; j++) {
                        var obj = document.getElementById(calSubMenu[i][j]);
                        if (obj != null) {
                            if (i == 2 && j != 3 && j != 4) {
                                obj.parentNode.focus();
                            }
                            else
                                obj.parentNode.parentNode.focus();
                            break;
                        }
                    }
                }
            }
            return false;
        }
        if (document.activeElement.id == "tdtabnav1" || document.activeElement.id == "tdtabnav2") {
            var lst = "accountset,tabnav1,tabnav2,acclogout";
            var nodecount = 1;
            if (navigator.appName.indexOf("Internet Explorer") != -1) {
                nodecount = 0;
            }

            if (lst.indexOf(document.activeElement.childNodes[nodecount].id) > -1)
                tabnavigation();
        }
    }

    if (keyCode == 39) {
        var str = document.activeElement.childNodes[1].id;
        var parentId = document.activeElement.parentNode.id;
        var subMenu = ["Hardware", "Locations", "Users", "Audiovisual", "Catering", "Facility"]
        if (parentId.indexOf("menu") > -1) {
            for (var i = 0; i < subMenu.length; i++) {
                if (str == subMenu[i]) {
                    var calSubItem = [["Endpoints", "Diagnostics", "MCUs","MCUs Load Balance", "Audio Bridges", "EM7"],
                    ["Rooms", "Tiers"],
                    ["Active Users", "Bulk Tool", "Departments", "Guests", "Restore User", "Inactive Users", "LDAP Directory Import", "LDAP Groups", "Roles", "Templates"],
                    ["Audiovisual Inventories", "Work Orders"],
                    ["Catering Menus", "Work Orders "],
                    ["Facility Services", "Work Orders  "]];
                    for (j = 0; j < calSubItem[i].length; j++) {
                        var obj = document.getElementById(calSubItem[i][j]);
                        if (obj != null) {
                            obj.parentNode.parentNode.focus();
                            break;
                        }
                    }
                }
            }
            return false;
        }
    }
    if (keyCode == 37) {
        var lst = "aswitchorgMain";

        if (lst.indexOf(document.activeElement.id) > -1)
            tabnavigationleftarrow();
    }
    if (keyCode == 27) {
        EscClosePopup();
    }
}
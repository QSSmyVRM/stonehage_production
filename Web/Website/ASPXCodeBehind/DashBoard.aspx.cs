/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Reflection;
using DevExpress.Web;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;
using ExpertPdf.HtmlToPdf;
using System.Text;

namespace ns_MyVRM
{
    public partial class en_DashBoard : System.Web.UI.Page
    {

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        MyVRMNet.Util utilObj; //FB 2236

        public String confID = "";
        public String duration = "";
        protected String format = "MM/dd/yyyy";
        String tformat = "hh:mm tt";
        ns_InXML.InXML objInXML;
        String PageNo = "0"; //ZD 103782
        DataSet ds = null;
        protected String isCustomEdit = "";
        String strEndPointID = "";
        String strType = "";
        String lsttype = "";
        Boolean cntstats = false;
        Boolean prtlstats = false;
        Boolean bluStatus = false; //Blue Status
        private String listValue = "";

        protected String p2pStatus = "";//Code added for p2p Status
        Boolean initload = false;//Code added for p2p Status
        protected String language = "";//FB 1830
        string CascadeLink = ""; //FB 2528
        public bool VMRConf;//FB 2501
        string Filtertype = "";//FB 2639 - Search

        # region prviate DataMember

        protected System.Web.UI.WebControls.LinkButton LinkButton1;
        protected System.Web.UI.WebControls.LinkButton LinkButton2;
        protected System.Web.UI.WebControls.LinkButton LinkButton3;
        protected System.Web.UI.WebControls.LinkButton LinkButton4;
        protected System.Web.UI.WebControls.LinkButton LinkButton6;
        protected System.Web.UI.WebControls.LinkButton btnDeleteConf;
        protected System.Web.UI.WebControls.LinkButton btnEdit;
        protected System.Web.UI.WebControls.LinkButton btnAddEndpoint;
        protected System.Web.UI.WebControls.LinkButton btnPDF;

        protected System.Web.UI.WebControls.DropDownList DrpDwnListView;
        protected System.Web.UI.WebControls.DropDownList lstAddressType;
        protected System.Web.UI.WebControls.DropDownList lstProtocol;

        protected System.Web.UI.WebControls.Label LblError;
        protected System.Web.UI.WebControls.Label lblStatus;
        protected System.Web.UI.WebControls.Label lblConfName;
        protected System.Web.UI.WebControls.Label lblConfUniqueID;
        protected System.Web.UI.WebControls.Label lblhash;
        protected System.Web.UI.WebControls.Label LblTimeText;
        protected System.Web.UI.WebControls.Label LblTime;
        protected System.Web.UI.WebControls.Label lblPassword;
        protected System.Web.UI.WebControls.Label lblConfType;
        protected System.Web.UI.WebControls.Label lblConfDate;
        protected System.Web.UI.WebControls.Label lblConfTime;
        protected System.Web.UI.WebControls.Label lblSetupDur;
        protected System.Web.UI.WebControls.Label lblTearDownDur;
        protected System.Web.UI.WebControls.Label hdnConfDuration;
        protected System.Web.UI.WebControls.Label lblConfDuration;
        protected System.Web.UI.WebControls.Label lblDescription;
        protected System.Web.UI.WebControls.Label lblPublic;
        protected System.Web.UI.WebControls.Label lblRegistration;
        protected System.Web.UI.WebControls.Label lblTimezone;
        protected System.Web.UI.WebControls.Label lblLastModifiedBy;
        protected System.Web.UI.WebControls.Label hdnLastModifiedBy;
        protected System.Web.UI.WebControls.Label lblReminders;
        protected System.Web.UI.WebControls.Label lblConfVNOC;
        protected System.Web.UI.WebControls.Label lblStartMode;
        protected System.Web.UI.WebControls.Label lblFiles;
        protected System.Web.UI.WebControls.Label lblConfHost;
        protected System.Web.UI.WebControls.Label hdnConfHost;

        protected System.Web.UI.WebControls.Image imgVideoLayout;
        protected System.Web.UI.WebControls.ImageButton imgHostDetails;

        protected System.Web.UI.WebControls.DataGrid dgEndpoints;
        protected System.Web.UI.WebControls.DataGrid dgP2PEndpoints;
        protected DevExpress.Web.ASPxGridView.ASPxGridView grid2;


        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnlisttype;
        protected System.Web.UI.HtmlControls.HtmlInputHidden helpPage;
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtSelectionCount;
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtSearchType;
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtSortBy;
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtPublic;
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtConferenceSearchType;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnLayout;

        protected System.Web.UI.WebControls.HiddenField hdngridCount;
        protected System.Web.UI.WebControls.HiddenField hdnConfLockStatus;
        protected System.Web.UI.WebControls.HiddenField hdnConfStatus;
        protected System.Web.UI.WebControls.HiddenField hdnConfType;

        protected System.Web.UI.WebControls.Table tblEndpoints;
        protected System.Web.UI.WebControls.Table tblP2PEndpoints;
        protected System.Web.UI.WebControls.Table tblNoEndpoints;
        protected System.Web.UI.WebControls.Table tblForceTerminate;
        protected System.Web.UI.WebControls.TableRow trED;
        protected System.Web.UI.WebControls.TableCell tcAEP;
        protected System.Web.UI.WebControls.TableCell refreshCell;

        protected System.Web.UI.WebControls.TextBox lblConfID;
        protected System.Web.UI.WebControls.TextBox tempText;
        protected System.Web.UI.WebControls.TextBox txtEndpointType;
        protected System.Web.UI.WebControls.TextBox ImageFiles;
        protected System.Web.UI.WebControls.TextBox ImageFilesBT;
        protected System.Web.UI.WebControls.TextBox ImagesPath;
        protected System.Web.UI.WebControls.TextBox txtSelectedImage;
        protected System.Web.UI.WebControls.TextBox txtSelectedImageEP;
        protected System.Web.UI.WebControls.TextBox txtTempImage;
        protected System.Web.UI.WebControls.TextBox Recur;
        protected System.Web.UI.WebControls.TextBox txtTimeDifference;
        protected System.Web.UI.WebControls.TextBox txtExtendedTime;

        protected System.Web.UI.HtmlControls.HtmlInputText TxtMessageBoxAll;

        protected System.Web.UI.WebControls.PlaceHolder HostDetailHolder;

        protected System.Web.UI.HtmlControls.HtmlTableCell tdStartMode;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdStartMode1;//FB 2565
        protected System.Web.UI.HtmlControls.HtmlTableCell tdStartModeSelection;
        protected System.Web.UI.WebControls.Label lblSecured;//FB 2595
        protected System.Web.UI.HtmlControls.HtmlTableCell tdSecured;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdSecured1;//FB 2565
        protected System.Web.UI.HtmlControls.HtmlTableCell tdSecuredSelection;
        //FB 2441 Starts
        protected System.Web.UI.WebControls.DataGrid dgMuteALL;
        protected System.Web.UI.WebControls.LinkButton lnkMuteAllExcept;
        protected System.Web.UI.WebControls.LinkButton lnkUnMuteAllParties;
        protected System.Web.UI.HtmlControls.HtmlInputButton ConfLayoutSubmit;
        protected System.Web.UI.WebControls.Button btnExtendEndtime;
        //FB 2441 Ends
        //FB 2670 START
        protected System.Web.UI.HtmlControls.HtmlTableCell trVNOC;
        protected System.Web.UI.HtmlControls.HtmlTableCell trVNOCoptor;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdvnoccol;
        //FB 2670 END
        //FB 2694 Start
        protected System.Web.UI.HtmlControls.HtmlTableCell tdReminders;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdReminders1;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdReminderSelections;
        protected System.Web.UI.HtmlControls.HtmlTableRow trStModeNwStarte;
        protected System.Web.UI.HtmlControls.HtmlTableRow trPuPw;
        //FB 2694 End
        protected System.Web.UI.HtmlControls.HtmlAnchor A3;//FB 2664
        protected int NetworkSwitching = 0; //FB 2993

        // ZD #100093 & #100085 start
        protected System.Web.UI.WebControls.Label lblSetupTimeinMin;
        protected System.Web.UI.WebControls.Label lblTearDownTimeinMin;
        protected System.Web.UI.WebControls.Label lblMCUPreStart;
        protected System.Web.UI.WebControls.Label lblMCUPreEnd;
        protected System.Web.UI.HtmlControls.HtmlTableRow trBuffer;
        protected System.Web.UI.HtmlControls.HtmlTableRow trMUCPreTime;
        protected int enableBufferZone = 0; //ZD 100085 End
        protected System.Web.UI.HtmlControls.HtmlTableCell tdDescription; //ZD 100420

        //ZD 100602 Starts
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnTimeZoneId;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCloudConf;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnServiceType;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnIsVMR;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnConfStart;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnConfEnd;
        //ZD 100602 End
		//ZD 100036 Starts
        protected System.Web.UI.WebControls.TableCell TableCell13;
        protected System.Web.UI.WebControls.TableCell TableCell9;
        protected System.Web.UI.WebControls.TableCell TableCell8;
		//ZD 100036 End  
        protected System.Web.UI.WebControls.TableCell ss; //ZD 101133
        protected System.Web.UI.WebControls.TableCell TableCell5; //ZD 101133
        protected System.Web.UI.WebControls.TableCell TableCell6; //ZD 101133
		protected System.Web.UI.HtmlControls.HtmlInputHidden hdnExpressConf; //ZD 101233
        //ZD 101233 Start
        //protected System.Web.UI.HtmlControls.HtmlInputHidden hdnIsPublicConf; 
        protected string hasView = "";
        protected string hasEdit = "";
        protected string hasDelete = "";
        protected string hasManage = "";
        protected string hasClone = "";
        protected string hasMCUInfo = "";
        protected string hasExtendTime = "";
        //ZD 101233 End

        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEditForm; //ZD 101597        
        //ZD 101871 start
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCodian;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnPolycomMGC;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnPolycomRMX;
        //ZD 101871 End

        #endregion

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        #region Page load

        protected void Page_Load(object sender, EventArgs e)
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            utilObj = new MyVRMNet.Util(); //FB 2236

            objInXML = new ns_InXML.InXML();
            //String stDate = "";
            //String enDate = "";

            try
            {   //FF Fixes
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("dashboard.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                LinkButton1.Attributes["OnClick"] = "return true;";
                LinkButton2.Attributes["OnClick"] = "return true;";
                LinkButton3.Attributes["OnClick"] = "return true;";
                LinkButton4.Attributes["OnClick"] = "return true;";
                LinkButton6.Attributes["OnClick"] = "return true;";
                btnDeleteConf.Attributes["OnClick"] = "return true;";
                btnEdit.Attributes["OnClick"] = "return true;";
                btnAddEndpoint.Attributes["OnClick"] = "return true;";
                //ZD 101597
                if (Int32.Parse(Session["admin"].ToString()) > 0)
                    btnEdit.Attributes.Add("onclick", "javascript:return fnSelectForm();");
                //ZD 100036
                TableCell13.Visible = true;
                TableCell9.Visible = true;
                TableCell8.Visible = true;
                ListItem itemToRemove = null;//ZD 101233

                itemToRemove = DrpDwnListView.Items.FindByValue("1");
                if (itemToRemove != null && Session["hasReservations"] != null && Session["hasReservations"].ToString() == "0")
                    DrpDwnListView.Items.Remove(itemToRemove);
                itemToRemove = DrpDwnListView.Items.FindByValue("2");
                if (itemToRemove != null && Session["hasOngoing"] != null && Session["hasOngoing"].ToString() == "0")
                    DrpDwnListView.Items.Remove(itemToRemove);
                itemToRemove = DrpDwnListView.Items.FindByValue("3");
                if (itemToRemove != null && Session["hasOnMCU"] != null && Session["hasOnMCU"].ToString() == "0")
                    DrpDwnListView.Items.Remove(itemToRemove);

                if (DrpDwnListView.Items.Count == 0) // ZD 101388
                    DrpDwnListView.Items.Add(obj.GetTranslatedText("No items"));

                lsttype = "2";
                LblError.Visible = false;

                Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
                //tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");
                //FB 2588 Starts
                if (Session["timeFormat"].ToString().Equals("0"))
                    tformat = "HH:mm";
                else if (Session["timeFormat"].ToString().Equals("1"))
                    tformat = "hh:mm tt";
                else if (Session["timeFormat"].ToString().Equals("2"))
                    tformat = "HHmmZ";
                //FB 2588 Ends


                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() != "")
                        format = Session["FormatDateType"].ToString();
                }
                if (Session["language"] == null)//FB 1830
                    Session["language"] = "en";
                if (Session["language"].ToString() != "")
                    language = Session["language"].ToString();
                //FB 2530 Starts
                if (Session["muteAll"] == null)
                    Session["muteAll"] = "M";
                else if (Session["muteAll"].ToString() == "")
                    Session["muteAll"] = "M";

                if (Session["muteAll"].ToString() == "U")
                    LinkButton1.Text = obj.GetTranslatedText("Unmute All"); // ZD 103628
                else if (Session["muteAll"].ToString() == "M")
                    LinkButton1.Text = obj.GetTranslatedText("Mute All");
                LinkButton2.Attributes.Add("onclick", "javascript:managelayout('" + imgVideoLayout.ClientID + "','01','');return false;");
                //FB 2530 Ends

                //ZD 100085
                if (Session["EnableBufferZone"] != null)
                    int.TryParse(Session["EnableBufferZone"].ToString(), out enableBufferZone);


                if (DrpDwnListView.SelectedValue != "" && DrpDwnListView.SelectedValue != "2")
                {

                    //Conference Action Fix starts..
                    LinkButton1.Enabled = false;
                    //LinkButton2.Enabled = false;// FF START
                    LinkButton2.Attributes["OnClick"] = "return false;";
                    LinkButton2.ForeColor = System.Drawing.Color.Gray;
                    LinkButton2.Style.Add("cursor", "default");
                    //LinkButton2.Style["text-decoration"] = "none";
                    //LinkButton3.Enabled = false;
                    //LinkButton3.Attributes["OnClick"] = "return false;";
                    LinkButton3.Attributes.Remove("OnClick");
                    LinkButton3.ForeColor = System.Drawing.Color.Gray;
                    LinkButton3.Style.Add("cursor", "default");// FF End
                    LinkButton4.Enabled = false;//101610
                    LinkButton4.Attributes["OnClick"] = "return false;";
                    LinkButton4.ForeColor = System.Drawing.Color.Gray;
                    LinkButton4.Style.Add("cursor", "default");
                   
                    // FB 2573 Starts
                    btnAddEndpoint.Enabled = false;
                    btnAddEndpoint.Attributes["OnClick"] = "return false;";
                    btnAddEndpoint.ForeColor = System.Drawing.Color.Gray;
                    btnAddEndpoint.Style.Add("cursor", "default");
                    // FB 2573 Ends
                    lnkMuteAllExcept.Visible = false; // FB 2441
                    lnkUnMuteAllParties.Visible = false; //FB 2441
                    grid2.Columns[2].Visible = false;
                    dgEndpoints.Columns[17].Visible = false;//FB 2839 //ZD 104256

                    lsttype = "1";
                }
                else
                {
                    grid2.Columns[2].Visible = true;
                    dgEndpoints.Columns[18].Visible = false;//FB 2839 //ZD 104256
                    dgEndpoints.Columns[17].Visible = true;//FF //ZD 104256

                }
                //Conference Action Fix Ends..
            
                if (!IsPostBack)
                {
                    //Code added  -- Strat
                    if (Request.QueryString["listValue"] != null)
                        if (Request.QueryString["listValue"].ToString() != "")
                        {
                            listValue = Request.QueryString["listValue"].ToString();
                        }
                    // End

                    hdnlisttype.Value = DrpDwnListView.SelectedValue;

                    if (listValue != "")
                    {
                        hdnlisttype.Value = listValue;
                        DrpDwnListView.SelectedValue = listValue;
                    }

                    ChangeCalendarDate(null, null);

                    if (Request.QueryString["m"] != null)
                        if (Request.QueryString["m"].ToString().Equals("1"))
                        {
                            LblError.Text = obj.GetTranslatedText("Operation Successful!");
                            LblError.Visible = true;
                        }

                }

                hdngridCount.Value = grid2.VisibleRowCount.ToString();//Edited For FF...
                if (Session["DtDispList"] != null)
                {
                    grid2.DataSource = (DataTable)Session["DtDispList"];
                    grid2.DataBind();//FB 1679
                }

                //ZD 100085
                if (enableBufferZone == 1)
                    trBuffer.Attributes.Add("style", "display:");
                else
                    trBuffer.Attributes.Add("style", "display:none");

                //ZD 101388 Starts
                if (DrpDwnListView.SelectedValue == "")
                {
                    LinkButton1.Enabled = false;
                    LinkButton1.Attributes["OnClick"] = "return false;";
                    LinkButton1.ForeColor = System.Drawing.Color.Gray;
                    LinkButton1.Style.Add("cursor", "default");
                    LinkButton2.Enabled = false;
                    LinkButton2.Attributes["OnClick"] = "return false;";
                    LinkButton2.ForeColor = System.Drawing.Color.Gray;
                    LinkButton2.Style.Add("cursor", "default");
                    LinkButton3.Enabled = false;
                    //LinkButton3.Attributes["OnClick"] = "return false;";
                    LinkButton3.Attributes.Remove("OnClick");
                    LinkButton3.ForeColor = System.Drawing.Color.Gray;
                    LinkButton3.Style.Add("cursor", "default");
                    LinkButton4.Enabled = false;
                    LinkButton4.Attributes["OnClick"] = "return false;";
                    LinkButton4.ForeColor = System.Drawing.Color.Gray;
                    LinkButton4.Style.Add("cursor", "default");
                    LinkButton4.OnClientClick = null;

                    LinkButton6.Enabled = false;
                    LinkButton6.Attributes["OnClick"] = "return false;";
                    LinkButton6.ForeColor = System.Drawing.Color.Gray;
                    LinkButton6.Style.Add("cursor", "default");
                    
                    btnDeleteConf.Enabled = false;
                    btnDeleteConf.Attributes["OnClick"] = "return false;";
                    btnDeleteConf.ForeColor = System.Drawing.Color.Gray;
                    btnDeleteConf.Style.Add("cursor", "default");
                    btnDeleteConf.OnClientClick = null;
                    btnEdit.Enabled = false;
                    btnEdit.Attributes["OnClick"] = "return false;";
                    btnEdit.ForeColor = System.Drawing.Color.Gray;
                    btnEdit.Style.Add("cursor", "default");
                    btnPDF.Enabled = false;
                    btnPDF.ForeColor = System.Drawing.Color.Gray;
                    btnAddEndpoint.Enabled = false;
                    btnAddEndpoint.Attributes["OnClick"] = "return false;";
                    btnAddEndpoint.ForeColor = System.Drawing.Color.Gray;
                    btnAddEndpoint.Style.Add("cursor", "default");
                    btnAddEndpoint.OnClientClick = null;
                    A3.Style.Add("color", "Gray");
                    A3.Disabled = true;
                }
                //ZD 101388 End
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                //ZD 100263
                LblError.Text = obj.ShowSystemMessage();
                //LblError.Text = "DashBoard error1: " + ex.Message;
                log.Trace(ex.StackTrace + " DashBoard error : " + ex.Message);

            }

        }

        #endregion

        #region Bind List

        protected void ChangeCalendarDate(Object sender, EventArgs e)
        {

            try
            {

                if (hdnlisttype != null)
                {
                    switch (hdnlisttype.Value)
                    {
                        case "1": //Reservation
                            helpPage.Value = "110";
                            txtPublic.Value = "";
                            txtSearchType.Value = ns_MyVRMNet.vrmConfStatus.Scheduled;
                            //txtSearchType.Value = ns_MyVRMNet.vrmConfStatus.Scheduled + "," + ns_MyVRMNet.vrmConfStatus.OnMCU;
                            txtConferenceSearchType.Value = "";
                            Filtertype = ns_MyVRMNet.vrmSearchFilterType.Reservation;//FB 2639 - Search
                            BindReservations(txtConferenceSearchType.Value);
                            trED.Attributes.Add("style", "display:''");//Edited For FF...
                            tcAEP.Enabled = false;

                            break;
                        case "2": // Ongoing
                            helpPage.Value = "110";
                            txtSearchType.Value = ns_MyVRMNet.vrmConfStatus.Scheduled + "," + ns_MyVRMNet.vrmConfStatus.Ongoing; //ZD 100083
                            txtPublic.Value = "";
                            txtConferenceSearchType.Value = "1";
                            Filtertype = ns_MyVRMNet.vrmSearchFilterType.Ongoing;//FB 2639 - Search
                            BindReservations(txtConferenceSearchType.Value);
                            trED.Attributes.Add("style", "display:none");
                            tcAEP.Enabled = true;

                            break;
                            
                        //ZD 100036 START
                        case "3": // ON MCU
                            helpPage.Value = "110";
                            txtSearchType.Value = ns_MyVRMNet.vrmConfStatus.OnMCU;
                            txtPublic.Value = "";
                            txtConferenceSearchType.Value = "1";
                            Filtertype = ns_MyVRMNet.vrmSearchFilterType.OnMCU;
                            BindReservations(txtConferenceSearchType.Value);
                            trED.Attributes.Add("style", "display:''");
                            tcAEP.Enabled = false;
                            break;
                            //ZD 10036 END
                        
                        default:
                            if (lblConfID.Text != "")
                            {
                                if (Session["confid"] != null)
                                    Session["confid"] = lblConfID.Text;
                                else
                                    Session.Add("confid", lblConfID.Text);
                                DisplayConferenceDetails();
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                //ZD 100263
                LblError.Text = obj.ShowSystemMessage();
                //LblError.Text = "Room  error: " + ex.Message;
                log.Trace(ex.StackTrace + " Room  error : " + ex.Message);
            }
        }

        #endregion

        #region BindReservations
        /// <summary>
        /// BindReservations
        /// </summary>
        /// <param name="SearchType"></param>
        protected void BindReservations(String SearchType)
        {
            XmlDocument xmldoc = null;
            try
            {
                txtSortBy.Value = "3";

                string inXML = objInXML.SearchConference(Session["userID"].ToString(), txtSearchType.Value, "", "", "", "", SearchType, "", "", "", "", txtPublic.Value, "1", "", PageNo, txtSortBy.Value, "1", "", "", "", "0", "", false, Filtertype,""); //Custom Attribute Fixes//FB 2632//FB 2694 //FB 2822 //FB 2870 //FB 3006 //FB 2639 - Search TIK# 100037

                //ZD 100036 Starts
                if (SearchType == "1")
                    inXML = objInXML.SearchConference(Session["userID"].ToString(), txtSearchType.Value, "", "", "", "", SearchType, "", "", "", "", txtPublic.Value, "1", "", PageNo, txtSortBy.Value, "0", "", "", "", "0", "", false, Filtertype, "");
                                
                string outXML = obj.CallMyVRMServer("SearchConference", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //string outXML = obj.CallMyVRMServer("SearchAllConference", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 1679
                //ZD 100036 End

                if (outXML.IndexOf("<error>") < 0)
                {
                    xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//SearchConference/Conferences/Conference");
                    LoadConferenceList(nodes);
                }
                else
                {
                    LblError.Text = obj.ShowErrorMessage(outXML);
                    LblError.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("bindReservations: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region LoadConferenceList
        /// <summary>
        /// LoadConferenceList
        /// </summary>
        /// <param name="nodes"></param>
        /// <param name="dgList"></param>
        protected void LoadConferenceList(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                ds = new DataSet();
                foreach (XmlNode node in nodes)
                {
                    if (node.InnerXml.Trim().IndexOf("<Selected>") <= 0)
                    {
                        XmlNode nodeLoc = node.SelectSingleNode("//SearchConference/Conferences/Conference/Location");
                        nodeLoc.InnerXml = "<Selected><ID>-1</ID><Name>Other</Name></Selected>";
                    }
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.Auto);
                }
                DataView dv;
                DataTable dt = new DataTable();

                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;

                    if (Session["confid"] != null)
                        Session["confid"] = dt.Rows[0]["ConferenceID"].ToString();
                    else
                        Session.Add("confid", dt.Rows[0]["ConferenceID"].ToString());


                    String isRecur = dt.Rows[0]["IsRecur"].ToString();
                    if (isRecur == "0")
                    {
                        if (Session["confid"] != null)
                            Session["confid"] = dt.Rows[0]["ConferenceID"].ToString();
                        else
                            Session.Add("confid", dt.Rows[0]["ConferenceID"].ToString());
                    }
                    else
                    {
                        String confarray = dt.Rows[0]["ConferenceID"].ToString();
                        String[] confid = confarray.Split(',');

                        if (Session["confid"] != null)
                            Session["confid"] = confid[0];
                        else
                            Session.Add("confid", confid[0]);

                    }

                    //Conference Action Fix Starts ..
                    if (DrpDwnListView.SelectedValue != "" && DrpDwnListView.SelectedValue != "2" )
                    {
                        //LinkButton1.Enabled = false;// FF START
                        LinkButton1.Attributes["OnClick"] = "return false;";
                        LinkButton1.ForeColor = System.Drawing.Color.Gray;
                        LinkButton1.Style.Add("cursor", "default");
                        //LinkButton2.Enabled = false;
                        //Edited For FF...
                        LinkButton2.Attributes["OnClick"] = "return false;";
                        LinkButton2.ForeColor = System.Drawing.Color.Gray;
                        LinkButton2.Style.Add("cursor", "default");
                        //LinkButton3.Enabled = false;
                        //LinkButton3.Attributes["OnClick"] = "return false;";3
                        LinkButton3.Attributes.Remove("OnClick");
                        LinkButton3.ForeColor = System.Drawing.Color.Gray;
                        LinkButton3.Style.Add("cursor", "default");
                        LinkButton4.Enabled = false; //101610
                        LinkButton4.Attributes["OnClick"] = "return false;";
                        LinkButton4.ForeColor = System.Drawing.Color.Gray;
                        LinkButton4.Style.Add("cursor", "default"); // FF End

                        // FB 2573 Starts
                        btnAddEndpoint.Attributes["OnClick"] = "return false;";
                        btnAddEndpoint.ForeColor = System.Drawing.Color.Gray;
                        btnAddEndpoint.Style.Add("cursor", "default");
                        // FB 2573 Ends

                        grid2.Columns[2].Visible = false;
                        dgEndpoints.Columns[17].Visible = false;//FB 2839 //ZD 104256

                        lsttype = "1";
                    }
                    else if (DrpDwnListView.SelectedValue != "")
                    {
                        //ZD 101579
                        if ((dt.Rows[0]["IsHost"].ToString() == "1" && dt.Rows[0]["ConferenceStatus"].ToString() != "7") || Int32.Parse(Session["admin"].ToString()) >= 1
                            && (dt.Rows[0]["ConferenceType"].ToString() == "2" || dt.Rows[0]["ConferenceType"].ToString() == "6"))
                        {
                            LinkButton1.Enabled = true;
                            LinkButton1.ForeColor = System.Drawing.Color.Black;// FF
                            LinkButton1.Style.Add("cursor", "pointer");

                            LinkButton2.Enabled = true;
                            LinkButton2.ForeColor = System.Drawing.Color.Black;//
                            LinkButton2.Style.Add("cursor", "pointer");

                            LinkButton2.Enabled = true;
                            LinkButton2.ForeColor = System.Drawing.Color.Black;//
                            LinkButton2.Style.Add("cursor", "pointer");

                            LinkButton3.Enabled = true;
                            LinkButton3.ForeColor = System.Drawing.Color.Black;//
                            LinkButton3.Style.Add("cursor", "pointer");
                            LinkButton3.Attributes["OnClick"] = "javascript:return fnextendtime('1');return true;";//ZD 102997
                            txtExtendedTime.Enabled = true;//ZD 102997
                            btnExtendEndtime.Enabled = true;//ZD 102997

                            LinkButton4.ForeColor = System.Drawing.Color.Black;//
                            LinkButton4.Style.Add("cursor", "pointer");
                            LinkButton4.Enabled = true;

                            LinkButton6.Enabled = true;
                            LinkButton6.ForeColor = System.Drawing.Color.Black;//FF
                            LinkButton6.Style.Add("cursor", "pointer");

                            btnAddEndpoint.Attributes["OnClick"] = "return false;";
                            //FB 2573 Starts
                            btnAddEndpoint.Enabled = true;
                            btnAddEndpoint.ForeColor = System.Drawing.Color.Black;
                            btnAddEndpoint.Style.Add("cursor", "default");
                            btnAddEndpoint.Attributes["OnClick"] = "return false;";
                            // FB 2573 Ends
                            grid2.Columns[2].Visible = true;
                            dgEndpoints.Columns[17].Visible = false;//FB 2839 //ZD 104256
                        }
                        else
                        {
                            LinkButton1.Attributes["OnClick"] = "return false;";
                            LinkButton1.ForeColor = System.Drawing.Color.Gray;
                            LinkButton1.Style.Add("cursor", "default");
                            
                            LinkButton2.Attributes["OnClick"] = "return false;";
                            LinkButton2.ForeColor = System.Drawing.Color.Gray;
                            LinkButton2.Style.Add("cursor", "default");
                            
                            //LinkButton3.Attributes["OnClick"] = "return false;";
                            LinkButton3.Attributes.Remove("OnClick");
                            LinkButton3.ForeColor = System.Drawing.Color.Gray;
                            LinkButton3.Style.Add("cursor", "default");
                            LinkButton3.Enabled = false;

                            LinkButton4.Attributes["OnClick"] = "return false;";
                            LinkButton4.ForeColor = System.Drawing.Color.Gray;
                            LinkButton4.Style.Add("cursor", "default");
                            LinkButton4.Enabled = false;

                            LinkButton6.Attributes["OnClick"] = "return false;";
                            LinkButton6.ForeColor = System.Drawing.Color.Gray;
                            LinkButton6.Style.Add("cursor", "default");
                            LinkButton6.Enabled = false;

                            btnAddEndpoint.Attributes["OnClick"] = "return false;";
                            btnAddEndpoint.ForeColor = System.Drawing.Color.Gray;
                            btnAddEndpoint.Style.Add("cursor", "default");

                            grid2.Columns[2].Visible = false;
                            dgEndpoints.Columns[17].Visible = false; //ZD 104256
                        }
                    }


                    
                    btnDeleteConf.Enabled = true;
                    btnDeleteConf.ForeColor = System.Drawing.Color.Black;
                    btnDeleteConf.Style.Add("cursor", "pointer");
                    btnAddEndpoint.Enabled = true;
                    // FB 2573 Starts
                    //btnAddEndpoint.ForeColor = System.Drawing.Color.Black;
                    //btnAddEndpoint.Style.Add("cursor", "default");
                    //btnEdit.Enabled = true;
                    // FB 2573 Ends
                    btnEdit.ForeColor = System.Drawing.Color.Black;
                    btnEdit.Style.Add("cursor", "pointer");
                    btnPDF.Enabled = false;
                    btnPDF.ForeColor = System.Drawing.Color.Gray;//FB 2664
                    btnPDF.Attributes["OnClick"] = "return false;";

                    //Conference Action Fix Ends..
                    DisplayConferenceDetails();

                }
                else
                {
                    //Conference Action Fix..
                    //LinkButton1.Enabled = false;// FF
                    LinkButton1.Attributes["OnClick"] = "return false;";
                    LinkButton1.ForeColor = System.Drawing.Color.Gray;
                    LinkButton1.Style.Add("cursor", "default");
                    //LinkButton2.Enabled = false;
                    LinkButton2.Attributes["OnClick"] = "return false;";
                    LinkButton2.ForeColor = System.Drawing.Color.Gray;
                    LinkButton2.Style.Add("cursor", "default");
                    //LinkButton3.Enabled = false;
                    //LinkButton3.Attributes["OnClick"] = "return false;";
                    LinkButton3.Attributes.Remove("OnClick");
                    LinkButton3.ForeColor = System.Drawing.Color.Gray;
                    LinkButton3.Style.Add("cursor", "default");
                    //LinkButton4.Enabled = false;
                    LinkButton4.Attributes["OnClick"] = "return false;";
                    LinkButton4.ForeColor = System.Drawing.Color.Gray;
                    LinkButton4.Style.Add("cursor", "default");

                    LinkButton6.Enabled = false;
                    LinkButton6.Attributes["OnClick"] = "return false;";
                    LinkButton6.ForeColor = System.Drawing.Color.Gray;
                    LinkButton6.Style.Add("cursor", "default");
                    //btnDeleteConf.Enabled = false;
                    btnDeleteConf.Attributes["OnClick"] = "return false;";
                    btnDeleteConf.ForeColor = System.Drawing.Color.Gray;
                    btnDeleteConf.Style.Add("cursor", "default");
                    //btnEdit.Enabled = false;
                    btnEdit.Attributes["OnClick"] = "return false;";
                    btnEdit.ForeColor = System.Drawing.Color.Gray;
                    btnEdit.Style.Add("cursor", "default");
                    btnPDF.Enabled = false;
                    btnPDF.ForeColor = System.Drawing.Color.Gray;//FB 2664
                    //btnAddEndpoint.Enabled = false;//FF
                    btnAddEndpoint.Attributes["OnClick"] = "return false;";
                    btnAddEndpoint.ForeColor = System.Drawing.Color.Gray;
                    btnAddEndpoint.Style.Add("cursor", "default");
                    A3.Style.Add("color", "Gray");// FB 2664

                }
                //FB 2664 start
                int n = dt.Rows.Count;
                if (n == 0)
                {
                    ClearLabel();
                    A3.Disabled = true;
                    A3.Style.Add("color", "Gray");
                    A3.Attributes.Remove("onclick");
                }//FB 2837
                //else
                //{
                    //A3.Style.Add("color", "Blue");

                //}
                //FB 2664 End

                if (Session["DtDispList"] != null)
                    Session["DtDispList"] = dt;
                else
                    Session.Add("DtDispList", dt);

                grid2.DataSource = (DataTable)Session["DtDispList"];
                grid2.DataBind();
                //FB 1958 - Start
                if (ds.Tables.Count == 0)
                    imgHostDetails.Visible = false;
                else
                    imgHostDetails.Visible = true;
                //FB 1958 - End


            }
            catch (Exception ex)
            {
                log.Trace("conferenceList: " + ex.Message + " : " + ex.StackTrace);
            }
        }
        #endregion

        #region Mute Endpoint

        protected void MuteEndpoint(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <confID>" + lblConfID.Text + "</confID>";
                inXML += "  <endpointID>" + e.Item.Cells[0].Text + "</endpointID>";
                inXML += "  <terminalType>" + e.Item.Cells[1].Text + "</terminalType>";
                if (e.Item.Cells[3].Text.Equals("1"))
                    inXML += "  <mute>0</mute>";
                else
                    inXML += "  <mute>1</mute>";
                inXML += "  <muteAll>1</muteAll>";//FB 2530
                inXML += "</login>";
                String outXML = obj.CallCOM2("MuteTerminal", inXML, Application["RTC_ConfigPath"].ToString());
                LblError.Visible = true;
                if (outXML.IndexOf("<error>") >= 0)
                    LblError.Text = obj.ShowErrorMessage(outXML);
                else
                {
                    //outXML = obj.CallCOM("MuteTerminal", inXML, Application["COM_ConfigPath"].ToString());
                    outXML = obj.CallMyVRMServer("MuteTerminal", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027
                    Session.Add("confid", lblConfID.Text);
                    //LoadEndpoints();
                    LblError.Text = obj.GetTranslatedText("Operation Successful!");
                    LblError.Visible = true;
                    DisplayConferenceDetails();
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("Mute Endpoint :" + ex.StackTrace);
                LblError.Text = obj.ShowSystemMessage();
                //LblError.Text = ex.StackTrace;
                LblError.Visible = true;
            }
        }

        //FB 2530 Start
        protected void MuteEndpointAll(Object sender, EventArgs e)
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();
                inXML += "<userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "<confID>" + lblConfID.Text + "</confID>";
                inXML += "<endpointID>0</endpointID>";
                inXML += "<terminalType>0</terminalType>";

                if (Session["muteAll"].ToString() == "M")
                    inXML += "<mute>1</mute>";
                else
                    inXML += "<mute>0</mute>";

                inXML += "<muteAll>2</muteAll>";
                inXML += "</login>";
                String outXML = obj.CallCOM2("MuteTerminal", inXML, Application["RTC_ConfigPath"].ToString());
                LblError.Visible = true;
                if (outXML.IndexOf("<error>") >= 0)
                    LblError.Text = obj.ShowErrorMessage(outXML);
                else
                {
                    outXML = obj.CallMyVRMServer("MuteTerminal", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027
                    Session.Add("confid", lblConfID.Text);
                    LblError.Text = obj.GetTranslatedText("Operation Successful!");
                    LblError.Visible = true;

                    if (Session["muteAll"].ToString() == "M")
                    {
                        Session["muteAll"] = "U";
                        LinkButton1.Text = obj.GetTranslatedText("Unmute All"); // ZD 103628
                    }
                    else
                    {
                        Session["muteAll"] = "M";
                        LinkButton1.Text = obj.GetTranslatedText("Mute All");
                    }
                    DisplayConferenceDetails();

                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("MuteEndpointAll" + ex.StackTrace);
                LblError.Text = obj.ShowSystemMessage();
                //LblError.Text = ex.StackTrace;
                LblError.Visible = true;
            }
        }
        //FB 2530 End

        #endregion

        #region ConnectEndpoint

        protected void ConnectEndpoint(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <confID>" + lblConfID.Text + "</confID>";
                inXML += "  <endpointID>" + e.Item.Cells[0].Text + "</endpointID>";
                inXML += "  <terminalType>" + e.Item.Cells[1].Text + "</terminalType>";
                if (e.Item.Cells[7].Text.Equals("Connected"))
                    inXML += "  <connectOrDisconnect>0</connectOrDisconnect>";
                else
                    inXML += "  <connectOrDisconnect>1</connectOrDisconnect>";
                inXML += "</login>";
                String outXML = obj.CallCOM2("ConnectDisconnectTerminal", inXML, Application["RTC_ConfigPath"].ToString());
                LblError.Visible = true;
                if (outXML.IndexOf("<error>") >= 0)
                    LblError.Text = obj.ShowErrorMessage(outXML);
                else
                {
                    //outXML = obj.CallCOM("ConnectDisconnectTerminal", inXML, Application["COM_ConfigPath"].ToString()); //FB 2027
                    Session.Add("confid", lblConfID.Text);
                    //LoadEndpoints();
                    LblError.Text = obj.GetTranslatedText("Operation Successful!");
                    LblError.Visible = true;
                    DisplayConferenceDetails();
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("ConnectEndpoint" + ex.StackTrace);
                LblError.Text = obj.ShowSystemMessage();
                //LblError.Text = ex.StackTrace;
                LblError.Visible = true;
            }
        }

        #endregion

        #region DeleteTerminal

        protected void DeleteTerminal(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                //FB 2528 Starts
                StringBuilder inXML = new StringBuilder();
                String outXML = "";
                if (e.Item.Cells[7].Text.Equals("Connected"))
                {
                    inXML.Append("<login>");
                    inXML.Append(obj.OrgXMLElement());
                    inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                    inXML.Append("<confID>" + lblConfID.Text + "</confID>");
                    inXML.Append("<endpointID>" + e.Item.Cells[0].Text + "</endpointID>");
                    inXML.Append("<terminalType>" + e.Item.Cells[1].Text + "</terminalType>");
                    inXML.Append("<connectOrDisconnect>0</connectOrDisconnect>");
                    inXML.Append("</login>");
                    outXML = obj.CallCOM2("ConnectDisconnectTerminal", inXML.ToString(), Application["RTC_ConfigPath"].ToString());
                }
                //FB 2528 Ends

                //FB 2027 - Starts
                inXML = new StringBuilder();
                outXML = ""; //FB 2528
                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<confID>" + lblConfID.Text + "</confID>");
                inXML.Append("<endpointID>" + e.Item.Cells[0].Text + "</endpointID>");
                inXML.Append("<terminalType>" + e.Item.Cells[1].Text + "</terminalType>");
                inXML.Append("</login>");
                outXML = obj.CallCOM2("DeleteTerminal", inXML.ToString(), Application["RTC_ConfigPath"].ToString());
                LblError.Visible = true;
                if (outXML.IndexOf("<error>") >= 0)
                    LblError.Text = obj.ShowErrorMessage(outXML);
                else
                {
                    //outXML = obj.CallCOM("DeleteTerminal", inXML, Application["COM_ConfigPath"].ToString());
                    outXML = obj.CallMyVRMServer("DeleteTerminal", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                    //FB 2027 - End 
                    Session.Add("confid", lblConfID.Text);

                }
                DisplayConferenceDetails();
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("DeleteTerminal" + ex.StackTrace);
                LblError.Text = obj.ShowSystemMessage();
                //LblError.Text = ex.StackTrace;
                LblError.Visible = true;
            }
        }

        #endregion

        #region AddNewEndpoint
        protected void AddNewEndpoint(Object sender, EventArgs e)
        {
            try
            {
                //ZD 100602 Starts
                Session.Remove("stdate");
                Session.Remove("endate");
                Session.Remove("conftzone");
                Session.Remove("cloudconf");
                Session.Remove("conftype");
                Session.Remove("confservicetype");

                Session.Add("stdate", hdnConfStart.Value);
                Session.Add("endate", hdnConfEnd.Value);
                Session.Add("conftzone", hdnTimeZoneId.Value);
                Session.Add("cloudconf", hdnCloudConf.Value);
                Session.Add("conftype", hdnConfType.Value);
                Session.Add("confservicetype", hdnServiceType.Value);

                Session.Add("confid", lblConfID.Text);
                if (Session["EndpointID"] == null)
                    Session.Add("EndpointID", "new");
                else
                    Session["EndpointID"] = "new";
                Session["ConfID"] = lblConfID.Text;
                //Response.Redirect("AddTerminalEndpoint.aspx?epid=new&cid=" + lblConfID.Text + "&tpe=U");
                //Response.Redirect("EndpointList.aspx?t=TC");//FB 1552
                Response.Redirect("AddNewEndpoint.aspx?tp=cc"); //FB 1552
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("AddNewEndpoint" + ex.StackTrace);
                LblError.Text = obj.ShowSystemMessage();
                //LblError.Text = ex.StackTrace;
                LblError.Visible = true;
            }
        }

        #endregion

        #region EditEndpoint

        protected void EditEndpoint(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                string epid = e.Item.Cells[0].Text.Trim(); //FB 1650
                String tpe = e.Item.Cells[1].Text.Trim();
                if (e.Item.Cells[1].Text.Trim().Equals("2"))
                    tpe = "R";
                if (e.Item.Cells[1].Text.Trim().Equals("1"))
                    tpe = "U";

                if (e.Item.Cells[1].Text.Trim().Equals("4")) //FB 1650
                {
                    epid = e.Item.Cells[17].Text.Trim(); //FB 1650
                }
                else
                {
                    epid = e.Item.Cells[0].Text.Trim(); //FB 1650
                }

                if (Session["EndpointID"] == null) //FB 1650
                    Session.Add("EndpointID", epid);
                else
                    Session["EndpointID"] = epid;

                Response.Redirect("AddTerminalEndpoint.aspx?tp=cc&t=&tpe=" + tpe); //FB 2530
            }
            catch (System.Threading.ThreadAbortException) { } //FB 1650
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("EditEndpoint" + ex.StackTrace);
                LblError.Text = obj.ShowSystemMessage();
                //LblError.Text = ex.StackTrace;
                LblError.Visible = true;
            }
        }

        #endregion

        #region Row Created
        protected void ASPxGridView1_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
        {
            try
            {

                if (e.RowType == DevExpress.Web.ASPxGridView.GridViewRowType.Data)
                {
                    if (e.KeyValue != null)
                    {

                        HyperLink lnkConf = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "btnViewDetailsDev") as HyperLink;
                        Label IsRecur = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "lblIsRecur") as Label;
                        String confID = e.KeyValue.ToString().Trim();
                        if (IsRecur.Text.Trim() == "1")
                        {
                            String[] recur = confID.Split(',');
                            confID = recur[0].Trim();
                        }

                        IsRecur.Attributes.Add("style", "display:none");

                        if (lnkConf != null)
                            lnkConf.Attributes.Add("onclick", "javascript:checkconf('" + confID + "');");

                        Image img = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "Image1") as Image;
                        HtmlControl tdimage = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "tdimage") as HtmlControl;
                        if (img != null)
                        {
                            if (lsttype == "1")
                            {
                                img.Attributes.Add("style", "display:none;");
                                tdimage.Attributes.Add("style", "display:none;");

                            }
                            else
                            {
                                cntstats = false;
                                prtlstats = false;
                                chkConnection(confID);
                                if (cntstats)
                                    img.Attributes.Add("src", "Image/green_button.png");
                                else if (bluStatus)//Blue status
                                    img.Attributes.Add("src", "Image/blue_button.png");
                                else //FB  1679
                                    img.Attributes.Add("src", "Image/red_button.png");

                            }

                        }




                    }
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                LblError.Text = obj.ShowSystemMessage();
                LblError.Visible = true;
                //LblError.Text = "Room search error6: " + ex.Message;
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);

            }
        }
        #endregion

        #region Custom Call Back

        protected void Grid2_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            //GridPage = int.Parse(e.Parameters);

            //grid2.SettingsPager.PageSize = GridPage;
            //grid2.DataBind();
        }

        #endregion

        #region DataBound

        protected void Grid2_DataBound(object sender, EventArgs e)
        {

            //grid2.JSProperties["cpPageCount"] = grid2.PageCount;
        }

        #endregion

        #region Export Pdf

        protected void ExportToPDF(object sender, EventArgs e)
        {
            try
            {
                while (tempText.Text.Equals(""))
                {
                    Response.Write("here");
                }
                String strScript = "<script language=\"javascript\">document.frmSubmit.Submit()</script>";
                RegisterClientScriptBlock("addItems", strScript);
                PdfConverter pdfConverter = new PdfConverter();
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;
                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentOptions.LeftMargin = 5;
                pdfConverter.PdfDocumentOptions.RightMargin = 5;
                pdfConverter.PdfDocumentOptions.TopMargin = 5;
                pdfConverter.PdfDocumentOptions.BottomMargin = 5;
                pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;

                pdfConverter.PdfDocumentOptions.ShowHeader = false;

                pdfConverter.PdfFooterOptions.FooterText = "myVRM Version " + Application["Version"].ToString() + ",(c)Copyright " + Application["CopyrightsDur"].ToString() + " myVRM.com. All Rights Reserved."; //FB 1648
                pdfConverter.PdfFooterOptions.FooterTextColor = System.Drawing.Color.Blue;
                pdfConverter.PdfFooterOptions.DrawFooterLine = false;
                pdfConverter.PdfFooterOptions.PageNumberText = obj.GetTranslatedText("Page");
                pdfConverter.PdfFooterOptions.ShowPageNumber = true;
                int upYear = obj.GetYear(Session["systemDate"].ToString());
                int upMonth = obj.GetMonth(Session["systemDate"].ToString());
                int upDay = obj.GetDay(Session["systemDate"].ToString());
                int upHour = obj.GetHour(Session["systemTime"].ToString());
                int upMinute = obj.GetMinute(Session["systemTime"].ToString());
                String upSet = obj.GetTimeSet(Session["systemTime"].ToString());
                DateTime UserTime = new DateTime(upYear, upMonth, upDay, upHour, upMinute, 0);
                pdfConverter.PdfFooterOptions.FooterText += "\n" + UserTime.ToString("MMMM dd, yyyy, hh:mm tt ") + Session["systemTimeZone"].ToString();
                pdfConverter.LicenseKey = "kGb8Fr0gg2spzZd/SluMYY+Bv/RxuZmz6thATnaDnlkD20HkaCEyR4X+P9QqabXI";
                //Added for FB 1428 Start
                string txtHtml = "";
                if (Application["Client"].ToString().ToUpper() == "MOJ")
                {
                    txtHtml = tempText.Text; //"<html><body><TBODY><TR><TD align=middle colSpan=3><H3><SPAN id=lblHeader>Manage Conference</SPAN><INPUT id=cmd style='BORDER-RIGHT: 0px; BORDER-TOP: 0px; BORDER-LEFT: 0px; WIDTH: 0px; BORDER-BOTTOM: 0px' name=cmd> </H3></TD></TR><TR><TD align=middle colSpan=3><BR><BR><B><SPAN id=lblAlert style='COLOR: red'></SPAN></B></TD></TR><TR><TD align=middle colSpan=3><TABLE width='90%' border=0><TBODY><TR><TD><INPUT class=btprint id=lblConfID style='BORDER-LEFT-COLOR: white; BORDER-BOTTOM-COLOR: white; COLOR: white; BORDER-TOP-STYLE: none; BORDER-TOP-COLOR: white; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BACKGROUND-COLOR: white; BORDER-RIGHT-COLOR: white; BORDER-BOTTOM-STYLE: none' value=2155 name=lblConfID> <TABLE><TBODY><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' align=right width=100>Title:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' align=left width=300><SPAN id=lblConfName style='FONT-WEIGHT: bold; FONT-SIZE: medium; COLOR: green; FONT-FAMILY: Verdana'>VetPro</SPAN> </TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' align=right>Unique ID:</TD><TD style='HEIGHT: 21px' align=left width=200 colSpan=2><SPAN id=lblConfUniqueID style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: red; FONT-FAMILY: Verdana'>6067</SPAN></TD><TD class=btprint style='FONT-WEIGHT: bold; FONT-SIZE: medium; COLOR: green; FONT-FAMILY: Verdana' vAlign=top align=left width=200 rowSpan=8><TABLE id=tblActions style='BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; WIDTH: 100%; BORDER-BOTTOM: blue 1px solid; BORDER-COLLAPSE: collapse' borderColor=blue cellSpacing=0 cellPadding=0 border=0><TBODY><TR class=tableHeader style='HEIGHT: 25px'><TD class=tableHeader>Actions</TD></TR><TR id=trCancel style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BACKGROUND-COLOR: #e0e0e0; BORDER-RIGHT-COLOR: gray'><TD id=TableCell2><A onclick=javascript:btnDeleteConference_Click() href='#'>Cancel</A> </TD></TR><TR id=trClone style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BORDER-RIGHT-COLOR: gray'><TD id=TableCell4><A id=btnClone href='javascript:__doPostBack('btnClone','')'>Clone</A></TD></TR><TR id=trEdit style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BACKGROUND-COLOR: #e0e0e0; BORDER-RIGHT-COLOR: gray'><TD id=TableCell5><A id=btnEdit href='javascript:__doPostBack('btnEdit','')'>Edit</A></TD></TR><TR style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BORDER-RIGHT-COLOR: gray'><TD><A id=btnPDF onclick=javascript:pdfReport(); href='javascript:__doPostBack('btnPDF','')'>Export to PDF</A></TD></TR><TR style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BACKGROUND-COLOR: #e0e0e0; BORDER-RIGHT-COLOR: gray'><TD><A id=btnPrint onclick=javascript:printpage(); href='javascript:__doPostBack('btnPrint','')'>Print</A></TD></TR><TR id=TableRow4 style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BORDER-RIGHT-COLOR: gray'><TD id=TableCell6><A id=btnOutlook onclick='javascript:saveToOutlookCalendar('0','0','1','');' href='javascript:__doPostBack('btnOutlook','')'>Save to Outlook</A></TD></TR></TBODY></TABLE></TD></TR><TR><TD style='FONT-WEIGHT: bold' align=right width=100>Host:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' align=left width=300><SPAN id=lblConfHost style='FONT-WEIGHT: normal'><A href='mailto:vrmadmin@expeditevcs.com'>VRM Administrator</A></SPAN></TD><TD style='FONT-WEIGHT: bold' align=right>Password:</TD><TD style='HEIGHT: 21px' align=left width=200 colSpan=2><SPAN id=lblPassword style='FONT-SIZE: x-small; FONT-FAMILY: Verdana'></SPAN>&nbsp;</TD></TR><TR><TD style='FONT-WEIGHT: bold' vAlign=top align=right width=100>Date:</TD><TD style='FONT-WEIGHT: normal; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=left width=300><SPAN id=lblConfDate style='FONT-WEIGHT: normal'></SPAN><SPAN id=lblConfTime style='FONT-WEIGHT: normal'>(GMT-05:00) EST</SPAN> <SPAN id=lblTimezone>Custom Date Selection: 9/13/2007, 9/20/2007, 9/27/2007</SPAN><INPUT id=Recur style='BORDER-LEFT-COLOR: transparent; BORDER-BOTTOM-COLOR: transparent; COLOR: white; BORDER-TOP-STYLE: none; BORDER-TOP-COLOR: transparent; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 10px; BORDER-RIGHT-COLOR: transparent; BORDER-BOTTOM-STYLE: none' value=26&amp;03&amp;00&amp;PM&amp;90#5#9/13/2007&amp;9/20/2007&amp;9/27/2007 name=Recur> <!--26&05&00&PM&60#1&2&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1#7/24/2007&2&5&-1--></TD><TD style='FONT-WEIGHT: bold' vAlign=top align=right>Duration:</TD><TD style='HEIGHT: 21px' vAlign=top align=left width=200 colSpan=2><SPAN id=lblConfDuration style='FONT-WEIGHT: normal; FONT-SIZE: x-small; FONT-FAMILY: Verdana'></SPAN></TD></TR><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana; HEIGHT: 18px' vAlign=top align=right width=100>Status:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana; HEIGHT: 18px' vAlign=top align=left><SPAN id=lblStatus style='FONT-WEIGHT: normal'>Scheduled</SPAN></TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=right>Type:</TD><TD align=left colSpan=2><SPAN id=lblConfType style='FONT-WEIGHT: normal'>Room Conference</SPAN> </TD></TR><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=right width=100>Public:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=left colSpan=4><SPAN id=lblPublic style='FONT-WEIGHT: normal'>No</SPAN> <SPAN id=lblRegistration style='FONT-WEIGHT: normal'></SPAN></TD></TR><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=right width=100>Files:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=left colSpan=5><SPAN id=lblFiles style='FONT-WEIGHT: normal'></SPAN></TD></TR><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=right width=100>Description:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=left colSpan=5><SPAN id=lblDescription style='FONT-WEIGHT: normal'>Laura Graves, x6969</SPAN>&nbsp;</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: green; FONT-FAMILY: Verdana' vAlign=top align=right colSpan=3><INPUT class=btprint id=chkExpandCollapse onclick=javascript:ExpandAll() type=checkbox>Collapse All</TD></TR><TR><TD align=middle colSpan=3><TABLE cellSpacing=0 cellPadding=0 width='90%' border=0><TBODY><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: medium; COLOR: green; FONT-FAMILY: Verdana' align=left colSpan=2><IMG id=img_LOC onclick='ShowHideRow('LOC', this,false)' src='image/loc/nolines_minus.gif' border=0>Locations <SPAN id=lblLocCount style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: #404040; FONT-FAMILY: Verdana'>(1 rooms)</SPAN> </TD></TR><TR id=tr_LOC><TD width='5%'>&nbsp;</TD><TD style='FONT-WEIGHT: bold' vAlign=top align=left><SPAN id=lblLocation style='FONT-WEIGHT: normal'>Ohio &gt; Chillicothe &gt; <A onclick='javascript:chkresource('97')' href='#'>Chillicothe b1 r285 Training Room</A><BR></SPAN></TD></TR></TBODY></TABLE></TD></TR><TR><TD align=middle colSpan=3><TABLE cellSpacing=0 cellPadding=0 width='90%' border=0><TBODY><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: medium; COLOR: green; FONT-FAMILY: Verdana' align=left colSpan=4><IMG id=img_PAR onclick='ShowHideRow('PAR', this,false)' src='image/loc/nolines_minus.gif' border=0>Participants <SPAN id=lblPartyCount style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: #404040; FONT-FAMILY: Verdana'>(No participants)</SPAN></TD></TR><TR id=tr_PAR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' align=middle colSpan=4 rowSpan=3><TABLE id=tblNoParty style='BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; WIDTH: 90%; BORDER-BOTTOM: blue 1px solid; BORDER-COLLAPSE: collapse' borderColor=blue cellSpacing=0 cellPadding=0 border=0><TBODY><TR class=tableHeader id=TableRow6 style='HEIGHT: 30px'><TD class=tableHeader id=TableCell7>Name</TD><TD class=tableHeader id=TableCell8>Email</TD><TD class=tableHeader id=TableCell9>Status</TD></TR><TR id=TableRow7 style='FONT-SIZE: x-small; FONT-FAMILY: Verdana; HEIGHT: 30px; BACKGROUND-COLOR: #e0e0e0' vAlign=center align=middle><TD id=TableCell10 colSpan=3>There are no participants in this hearing.</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR><TR><TD align=middle colSpan=3></TD></TR><TR><TD align=middle colSpan=3></TD></TR><TR><TD align=middle colSpan=3><CENTER></CENTER></TD></TR><TR><TD align=middle colSpan=3></TD></TR></TBODY></body></html>";
                }
                else
                {
                    //Added for FB 1428 End
                    txtHtml = tempText.Text; //"<html><body><TBODY><TR><TD align=middle colSpan=3><H3><SPAN id=lblHeader>Manage Conference</SPAN><INPUT id=cmd style='BORDER-RIGHT: 0px; BORDER-TOP: 0px; BORDER-LEFT: 0px; WIDTH: 0px; BORDER-BOTTOM: 0px' name=cmd> </H3></TD></TR><TR><TD align=middle colSpan=3><BR><BR><B><SPAN id=lblAlert style='COLOR: red'></SPAN></B></TD></TR><TR><TD align=middle colSpan=3><TABLE width='90%' border=0><TBODY><TR><TD><INPUT class=btprint id=lblConfID style='BORDER-LEFT-COLOR: white; BORDER-BOTTOM-COLOR: white; COLOR: white; BORDER-TOP-STYLE: none; BORDER-TOP-COLOR: white; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BACKGROUND-COLOR: white; BORDER-RIGHT-COLOR: white; BORDER-BOTTOM-STYLE: none' value=2155 name=lblConfID> <TABLE><TBODY><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' align=right width=100>Title:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' align=left width=300><SPAN id=lblConfName style='FONT-WEIGHT: bold; FONT-SIZE: medium; COLOR: green; FONT-FAMILY: Verdana'>VetPro</SPAN> </TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' align=right>Unique ID:</TD><TD style='HEIGHT: 21px' align=left width=200 colSpan=2><SPAN id=lblConfUniqueID style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: red; FONT-FAMILY: Verdana'>6067</SPAN></TD><TD class=btprint style='FONT-WEIGHT: bold; FONT-SIZE: medium; COLOR: green; FONT-FAMILY: Verdana' vAlign=top align=left width=200 rowSpan=8><TABLE id=tblActions style='BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; WIDTH: 100%; BORDER-BOTTOM: blue 1px solid; BORDER-COLLAPSE: collapse' borderColor=blue cellSpacing=0 cellPadding=0 border=0><TBODY><TR class=tableHeader style='HEIGHT: 25px'><TD class=tableHeader>Actions</TD></TR><TR id=trCancel style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BACKGROUND-COLOR: #e0e0e0; BORDER-RIGHT-COLOR: gray'><TD id=TableCell2><A onclick=javascript:btnDeleteConference_Click() href='#'>Cancel</A> </TD></TR><TR id=trClone style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BORDER-RIGHT-COLOR: gray'><TD id=TableCell4><A id=btnClone href='javascript:__doPostBack('btnClone','')'>Clone</A></TD></TR><TR id=trEdit style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BACKGROUND-COLOR: #e0e0e0; BORDER-RIGHT-COLOR: gray'><TD id=TableCell5><A id=btnEdit href='javascript:__doPostBack('btnEdit','')'>Edit</A></TD></TR><TR style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BORDER-RIGHT-COLOR: gray'><TD><A id=btnPDF onclick=javascript:pdfReport(); href='javascript:__doPostBack('btnPDF','')'>Export to PDF</A></TD></TR><TR style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BACKGROUND-COLOR: #e0e0e0; BORDER-RIGHT-COLOR: gray'><TD><A id=btnPrint onclick=javascript:printpage(); href='javascript:__doPostBack('btnPrint','')'>Print</A></TD></TR><TR id=TableRow4 style='FONT-WEIGHT: bold; FONT-SIZE: x-small; BORDER-LEFT-COLOR: gray; BORDER-BOTTOM-COLOR: gray; COLOR: blue; BORDER-TOP-COLOR: gray; HEIGHT: 20px; BORDER-RIGHT-COLOR: gray'><TD id=TableCell6><A id=btnOutlook onclick='javascript:saveToOutlookCalendar('0','0','1','');' href='javascript:__doPostBack('btnOutlook','')'>Save to Outlook</A></TD></TR></TBODY></TABLE></TD></TR><TR><TD style='FONT-WEIGHT: bold' align=right width=100>Host:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' align=left width=300><SPAN id=lblConfHost style='FONT-WEIGHT: normal'><A href='mailto:vrmadmin@expeditevcs.com'>VRM Administrator</A></SPAN></TD><TD style='FONT-WEIGHT: bold' align=right>Password:</TD><TD style='HEIGHT: 21px' align=left width=200 colSpan=2><SPAN id=lblPassword style='FONT-SIZE: x-small; FONT-FAMILY: Verdana'></SPAN>&nbsp;</TD></TR><TR><TD style='FONT-WEIGHT: bold' vAlign=top align=right width=100>Date:</TD><TD style='FONT-WEIGHT: normal; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=left width=300><SPAN id=lblConfDate style='FONT-WEIGHT: normal'></SPAN><SPAN id=lblConfTime style='FONT-WEIGHT: normal'>(GMT-05:00) EST</SPAN> <SPAN id=lblTimezone>Custom Date Selection: 9/13/2007, 9/20/2007, 9/27/2007</SPAN><INPUT id=Recur style='BORDER-LEFT-COLOR: transparent; BORDER-BOTTOM-COLOR: transparent; COLOR: white; BORDER-TOP-STYLE: none; BORDER-TOP-COLOR: transparent; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 10px; BORDER-RIGHT-COLOR: transparent; BORDER-BOTTOM-STYLE: none' value=26&amp;03&amp;00&amp;PM&amp;90#5#9/13/2007&amp;9/20/2007&amp;9/27/2007 name=Recur> <!--26&05&00&PM&60#1&2&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1#7/24/2007&2&5&-1--></TD><TD style='FONT-WEIGHT: bold' vAlign=top align=right>Duration:</TD><TD style='HEIGHT: 21px' vAlign=top align=left width=200 colSpan=2><SPAN id=lblConfDuration style='FONT-WEIGHT: normal; FONT-SIZE: x-small; FONT-FAMILY: Verdana'></SPAN></TD></TR><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana; HEIGHT: 18px' vAlign=top align=right width=100>Status:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana; HEIGHT: 18px' vAlign=top align=left><SPAN id=lblStatus style='FONT-WEIGHT: normal'>Scheduled</SPAN></TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=right>Type:</TD><TD align=left colSpan=2><SPAN id=lblConfType style='FONT-WEIGHT: normal'>Room Conference</SPAN> </TD></TR><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=right width=100>Public:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=left colSpan=4><SPAN id=lblPublic style='FONT-WEIGHT: normal'>No</SPAN> <SPAN id=lblRegistration style='FONT-WEIGHT: normal'></SPAN></TD></TR><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=right width=100>Files:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=left colSpan=5><SPAN id=lblFiles style='FONT-WEIGHT: normal'></SPAN></TD></TR><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=right width=100>Description:</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' vAlign=top align=left colSpan=5><SPAN id=lblDescription style='FONT-WEIGHT: normal'>Laura Graves, x6969</SPAN>&nbsp;</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: green; FONT-FAMILY: Verdana' vAlign=top align=right colSpan=3><INPUT class=btprint id=chkExpandCollapse onclick=javascript:ExpandAll() type=checkbox>Collapse All</TD></TR><TR><TD align=middle colSpan=3><TABLE cellSpacing=0 cellPadding=0 width='90%' border=0><TBODY><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: medium; COLOR: green; FONT-FAMILY: Verdana' align=left colSpan=2><IMG id=img_LOC onclick='ShowHideRow('LOC', this,false)' src='image/loc/nolines_minus.gif' border=0>Locations <SPAN id=lblLocCount style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: #404040; FONT-FAMILY: Verdana'>(1 rooms)</SPAN> </TD></TR><TR id=tr_LOC><TD width='5%'>&nbsp;</TD><TD style='FONT-WEIGHT: bold' vAlign=top align=left><SPAN id=lblLocation style='FONT-WEIGHT: normal'>Ohio &gt; Chillicothe &gt; <A onclick='javascript:chkresource('97')' href='#'>Chillicothe b1 r285 Training Room</A><BR></SPAN></TD></TR></TBODY></TABLE></TD></TR><TR><TD align=middle colSpan=3><TABLE cellSpacing=0 cellPadding=0 width='90%' border=0><TBODY><TR><TD style='FONT-WEIGHT: bold; FONT-SIZE: medium; COLOR: green; FONT-FAMILY: Verdana' align=left colSpan=4><IMG id=img_PAR onclick='ShowHideRow('PAR', this,false)' src='image/loc/nolines_minus.gif' border=0>Participants <SPAN id=lblPartyCount style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: #404040; FONT-FAMILY: Verdana'>(No participants)</SPAN></TD></TR><TR id=tr_PAR><TD style='FONT-WEIGHT: bold; FONT-SIZE: x-small; COLOR: black; FONT-FAMILY: Verdana' align=middle colSpan=4 rowSpan=3><TABLE id=tblNoParty style='BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; WIDTH: 90%; BORDER-BOTTOM: blue 1px solid; BORDER-COLLAPSE: collapse' borderColor=blue cellSpacing=0 cellPadding=0 border=0><TBODY><TR class=tableHeader id=TableRow6 style='HEIGHT: 30px'><TD class=tableHeader id=TableCell7>Name</TD><TD class=tableHeader id=TableCell8>Email</TD><TD class=tableHeader id=TableCell9>Status</TD></TR><TR id=TableRow7 style='FONT-SIZE: x-small; FONT-FAMILY: Verdana; HEIGHT: 30px; BACKGROUND-COLOR: #e0e0e0' vAlign=center align=middle><TD id=TableCell10 colSpan=3>There are no participants in this conference.</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR><TR><TD align=middle colSpan=3></TD></TR><TR><TD align=middle colSpan=3></TD></TR><TR><TD align=middle colSpan=3><CENTER></CENTER></TD></TR><TR><TD align=middle colSpan=3></TD></TR></TBODY></body></html>"; //Edited fo FB 1428
                }

                //pdfConverter.LicenseKey = "put your serial number here";
                byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlString(txtHtml);
                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.AddHeader("Content-Type", "binary/octet-stream");
                String downloadName = lblConfName.Text + ".pdf";
                response.AddHeader("Content-Disposition", "attachment; filename=MyVRM_Conference.pdf; size=" + downloadBytes.Length.ToString());
                //                Response.BinaryWrite(downloadBytes);
                response.BinaryWrite(downloadBytes);
                //                response.Flush();
                response.End();
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("ExportToPDF" + ex.StackTrace);
                LblError.Text = obj.ShowSystemMessage();
                //LblError.Text = ex.StackTrace;
                LblError.Visible = true;
            }
        }

        #endregion

        #region Initilize End Points

        protected void InitializeEndpoints(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    //Endpoint Count fix...
                    Label lblCount = (Label)e.Item.FindControl("lblCount");
                    Label lblCountongoing = (Label)e.Item.FindControl("lblCountonGoing");
                    DataView dv = null;
                    dv = new DataView((DataTable)Session["DtEptList"]);
                    lblCount.Text = dv.Table.Rows.Count.ToString();
                    lblCountongoing.Text = dv.Table.Rows.Count.ToString();
                    Label lblDetails = (Label)e.Item.FindControl("lblCountText");
                    Label lblDetailsongoing = (Label)e.Item.FindControl("lblCountOngoingText");
                    lblDetails.Text = "Total Endpoints:";
                    lblDetailsongoing.Text = obj.GetTranslatedText("Total Endpoints:");

                }


                //Disabling endpoints Fix..
                if (DrpDwnListView.SelectedValue == "1")
                {
                    if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                    {
                        dgEndpoints.Columns[17].Visible = false;//FB 2839 //ZD 104256

                        //dgEndpoints.Columns[17].Visible = true;

                        #region commented
                        //LinkButton btnDelete = ((LinkButton)e.Item.FindControl("btnDelete"));
                        //btnDelete.Enabled = false;
                        //LinkButton btnCon = ((LinkButton)e.Item.FindControl("btnCon"));
                        //btnCon.Enabled = false;
                        //LinkButton btnEdit = ((LinkButton)e.Item.FindControl("btnEdit"));
                        //btnEdit.Enabled = false;
                        #endregion
                    }
                }

                //if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                //{
                //    if (hdnlisttype.Value == "1")
                //    { 
                //    LinkButton btnDelete = ((LinkButton)e.Item.FindControl("btnDelete"));
                //    LinkButton btnDelete = ((LinkButton)e.Item.FindControl("btnDelete"));
                //    }
                //}
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    ((LinkButton)e.Item.FindControl("btnDelete")).Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to delete this endpoint?")+"')");
                    ((LinkButton)e.Item.FindControl("btnMute")).Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to mute/unmute this endpoint?")+"')");

                    if (e.Item.Cells[3].Text.Equals("1"))
                        ((LinkButton)e.Item.FindControl("btnMute")).Text = obj.GetTranslatedText("Unmute"); // ZD 103628
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnChangeLayout");
                    if (btnTemp != null)
                        btnTemp.Attributes.Add("onclick", "javascript:managelayout('" + ((Image)e.Item.FindControl("imgVideoLayout")).ClientID + "','" + e.Item.Cells[0].Text + "','" + e.Item.Cells[1].Text + "');return false;");
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("InitializeEndpoint" + ex.StackTrace);
                LblError.Text = obj.ShowSystemMessage();
                //LblError.Text = ex.StackTrace;
                LblError.Visible = true;
            }
        }

        #endregion

        #region display conference details

        private void DisplayConferenceDetails()
        {
            try
            {
                //Response.Write("in details");
                string inXML = "<login><userID>" + Session["userID"].ToString() + "</userID>" + obj.OrgXMLElement() + "<selectType>7</selectType><selectID>" + Session["ConfID"].ToString() + "</selectID></login>";//Organization Module Fixes
                string inXMLTime = "<login><userID>" + Session["userID"].ToString() + "</userID>" + obj.OrgXMLElement() + "</login>";//Organization Module Fixes
                //Response.Write(obj.Transfer(inXML));
                string outXML;
                string outXMLTime;
                outXML = obj.CallMyVRMServer("GetOldConference", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                //outXMLTime = obj.CallCOM("GetSystemDateTime", inXMLTime, Application["COM_ConfigPath"].ToString());
                outXMLTime = obj.CallMyVRMServer("GetSystemDateTime", inXMLTime, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                
                Session.Add("outXML", outXML);
                //Response.Write(obj.Transfer(outXML));
                XmlDocument xmldoc = new XmlDocument();
                outXML = outXML.Replace("& ", "&amp; ");
                xmldoc.LoadXml(outXML);


                XmlDocument xmldocTime = new XmlDocument();
                outXMLTime = outXMLTime.Replace("& ", "&amp; ");
                xmldocTime.LoadXml(outXMLTime);
                LblTimeText.Text = obj.GetTranslatedText("Last Updated:");


                // buffer zone Start
                string setupDur = "0";
                string tearDownDur = "0";
                Double setupDuration = Double.MinValue;
                Double tearDuration = Double.MinValue;
                String tformat = "hh:mm tt";
                //tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");
                //FB 2588 Starts
                if (Session["timeFormat"].ToString().Equals("0"))
                    tformat = "HH:mm";
                else if (Session["timeFormat"].ToString().Equals("1"))
                    tformat = "hh:mm tt";
                else if (Session["timeFormat"].ToString().Equals("2"))
                    tformat = "HHmmZ";
                //FB 2588 Ends
                // buffer zone End

                LblTime.Text = DateTime.Parse("06/06/2006 " + xmldocTime.SelectSingleNode("systemDateTime/systemTime").InnerText).ToString(tformat);//myVRMNet.NETFunctions.GetFormattedDate(xmldocTime.SelectSingleNode("systemDateTime/systemDate").InnerText) + " " +

                //ZD 100085 Starts
                int MCUSetupTime = 0;
                if (xmldoc.SelectSingleNode("conference/confInfo/McuSetupTime") != null)
                    int.TryParse(xmldoc.SelectSingleNode("conference/confInfo/McuSetupTime").InnerText.Trim(), out MCUSetupTime);
                lblMCUPreStart.Text = MCUSetupTime.ToString();
                if (xmldoc.SelectSingleNode("conference/confInfo/MCUTeardonwnTime") != null)
                    lblMCUPreEnd.Text = xmldoc.SelectSingleNode("conference/confInfo/MCUTeardonwnTime").InnerText.Trim();
                //ZD 100085 End

                string recurring = "0";
                if (xmldoc.SelectSingleNode("/conference/confInfo/recurring") != null)
                    recurring = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/recurring").InnerText);

                if (recurring.Equals("1"))
                {

                    XmlNode usrnode = xmldoc.SelectSingleNode("conference/userInfo");

                    if (usrnode != null)
                        usrnode.InnerXml += "<userId>" + Session["userID"].ToString() + "</userId>";

                    string recOutxml = obj.CallMyVRMServer("GetIfDirtyorPast", xmldoc.InnerXml, Application["MyVRMServer_ConfigPath"].ToString());

                    if (recOutxml != "")
                    {
                        if (recOutxml.IndexOf("<error>") < 0)
                        {
                            xmldoc.LoadXml(recOutxml);
                        }
                    }
                }
                /* *** Recurring Fixes for Editing dirty conference 1391 - start **** */

                if (xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/customInstance") != null)
                {
                    if (xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/customInstance").InnerText == "1")
                        isCustomEdit = "Y";
                }
                /* *** Recurring Fixes for Editing dirty conference 1391 - end **** */

                //Recurrence Fixes - Edit With Some instances in past (FB 1131) - end

                /* *** code added for buffer zone *** -- Start */

                if (xmldoc.SelectSingleNode("//conference/confInfo/bufferZone/SetupDur") != null)
                {
                    if (xmldoc.SelectSingleNode("//conference/confInfo/bufferZone/SetupDur").InnerText != "")
                    {
                        setupDur = xmldoc.SelectSingleNode("//conference/confInfo/bufferZone/SetupDur").InnerText;
                    }
                }

                if (xmldoc.SelectSingleNode("//conference/confInfo/bufferZone/TearDownDur") != null)
                {
                    if (xmldoc.SelectSingleNode("//conference/confInfo/bufferZone/TearDownDur").InnerText != "")
                    {
                        tearDownDur = xmldoc.SelectSingleNode("//conference/confInfo/bufferZone/TearDownDur").InnerText;
                    }
                }

                Double.TryParse(setupDur, out setupDuration);
                Double.TryParse(tearDownDur, out tearDuration);
                lblSetupTimeinMin.Text = setupDuration.ToString(); //ZD 100602
                lblTearDownTimeinMin.Text = tearDuration.ToString();//ZD 100602

                //ZD 101871 start
                int Bridetype = 0; 
                if (xmldoc.SelectSingleNode("//conference/confInfo/ConfSlaveBridgeType") != null)
                {
                    if (xmldoc.SelectSingleNode("//conference/confInfo/ConfSlaveBridgeType").InnerText != "")
                    {
                        Bridetype = int.Parse(xmldoc.SelectSingleNode("//conference/confInfo/ConfSlaveBridgeType").InnerText);
                    }
                }
                if (Bridetype == ns_MyVRMNet.MCUType.PolycomRMX2000)
                {
                    hdnPolycomRMX.Value = "1";
                    hdnPolycomMGC.Value = "0";
                    hdnCodian.Value = "0";
                }
                else if (Bridetype == ns_MyVRMNet.MCUType.PolycomMGC100 || Bridetype == ns_MyVRMNet.MCUType.PolycomMGC25 || Bridetype == ns_MyVRMNet.MCUType.PolycomMGC50)
                {
                    hdnPolycomMGC.Value = "1";
                    hdnCodian.Value = "0";
                    hdnPolycomRMX.Value = "0";
                }
                else if (Bridetype == ns_MyVRMNet.MCUType.CodianMCU4200 || Bridetype == ns_MyVRMNet.MCUType.CodianMCU4500 || Bridetype == ns_MyVRMNet.MCUType.CodianMSE8000Series)
                {
                    hdnCodian.Value = "1";
                    hdnPolycomRMX.Value = "0";
                    hdnPolycomMGC.Value = "0";
                }
                else
                {
                    hdnCodian.Value = "0";
                    hdnPolycomRMX.Value = "0";
                    hdnPolycomMGC.Value = "0";
                }
                //ZD 101871 End

                //ZD 101871 End
                /* *** code added for buffer zone *** -- End */

                lblConfName.Text = obj.GetTranslatedText("Title") + ": " + obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/confName").InnerText); //ZD 100425
                lblPassword.Text = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/confPassword").InnerText);
                lblConfID.Text = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/confID").InnerText);
                lblConfUniqueID.Text = obj.GetTranslatedText("Conference ID") + ": " + obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/confUniqueID").InnerText); //ZD 100425
                //lblhash.Text = "#"; //ZD 100425
                lblhash.Text = "    "; //ZD 100425
                lblStatus.Text = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/Status").InnerText);
                lblConfType.Text = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/createBy").InnerText);
                hdnConfType.Value = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/createBy").InnerText);
                hdnConfStatus.Value = lblStatus.Text;
                //ZD 100513 Starts
                string OBTPconf = "0";
                if (xmldoc.SelectSingleNode("//conference/confInfo/isOBTP") != null)
                    OBTPconf = xmldoc.SelectSingleNode("//conference/confInfo/isOBTP").InnerText;
                if (lblConfType.Text == "2" && OBTPconf == "1")
                    lblConfType.Text = "9";
                //ZD 100513 Ends
                //FB 2501 Starts
                VMRConf = false;
				//ZD 100602 Starts
                hdnIsVMR.Value = "0";
                if (xmldoc.SelectSingleNode("//conference/confInfo/isVMR") != null)
                {
                    if (xmldoc.SelectSingleNode("//conference/confInfo/isVMR").InnerText.Equals("1"))
                    {
                        hdnIsVMR.Value = "1"; //ZD 100602
                        VMRConf = true;
                    }
                }
				//ZD 100602 End

                //ZD 101233 START
                hdnExpressConf.Value = "0";
                if (xmldoc.SelectSingleNode("//conference/confInfo/isExpressConference") != null)
                {
                    if (xmldoc.SelectSingleNode("//conference/confInfo/isExpressConference").InnerText.Equals("1"))
                        hdnExpressConf.Value = "1";
                }
                //ZD 101233 END
                
                //FB 2501 Ends
                int isSynchronousConf = 0;
                if (xmldoc.SelectSingleNode("//conference/confInfo/IsSynchronous") != null)
                {
                    if (xmldoc.SelectSingleNode("//conference/confInfo/IsSynchronous").InnerText != "")
                    {
                         int.TryParse(xmldoc.SelectSingleNode("//conference/confInfo/IsSynchronous").InnerText, out isSynchronousConf);
                    }
                }

                //ZD 100036 - Starts
                if (DrpDwnListView.SelectedValue == "3")
                {
                    TableCell13.Visible = false;
                    TableCell9.Visible = false;

                    if (isSynchronousConf == 1)
                    {
                        TableCell8.Visible = true;
                    }
                    else
                    {
                        TableCell8.Visible = false;
                    }
                }
                //ZD 100036 - End

                
                //ZD 100602 Starts
                hdnCloudConf.Value = "0";
                if (xmldoc.SelectSingleNode("//conference/confInfo/CloudConferencing") != null)
                {
                    if (xmldoc.SelectSingleNode("//conference/confInfo/CloudConferencing").InnerText.Equals("1"))
                        hdnCloudConf.Value = xmldoc.SelectSingleNode("//conference/confInfo/CloudConferencing").InnerText;
                }

                hdnServiceType.Value = "-1"; //ZD 100152
                if (xmldoc.SelectSingleNode("//conference/confInfo/ServiceType") != null)
                {
                    //if (xmldoc.SelectSingleNode("//conference/confInfo/ServiceType").InnerText.Equals("1")) //ZD 100152
                        hdnServiceType.Value = xmldoc.SelectSingleNode("//conference/confInfo/ServiceType").InnerText;
                }

                if (xmldoc.SelectSingleNode("//conference/confInfo/ConfBufferStartTime") != null)
                {
                    hdnConfStart.Value = xmldoc.SelectSingleNode("//conference/confInfo/ConfBufferStartTime").InnerText.Trim();
                }
                if (xmldoc.SelectSingleNode("//conference/confInfo/ConfBufferEndTime") != null)
                {
                    hdnConfEnd.Value = xmldoc.SelectSingleNode("//conference/confInfo/ConfBufferEndTime").InnerText.Trim();
                }
                //ZD 100602 Ends

                //FB 2501 Dec10 Start
                if (xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/lockStatus") != null)
                    hdnConfLockStatus.Value = xmldoc.SelectSingleNode("/conference/confInfo/advAVParam/lockStatus").InnerText;
                else
                    hdnConfLockStatus.Value = "0";

                if (hdnConfLockStatus.Value == "1" || VMRConf) //ZD 100602
                    btnAddEndpoint.Visible = false;
                else
                    btnAddEndpoint.Visible = true; //FB 2568
                //FB 2501 Dec10 End
                /* *** Recurrence Fixes - commented for edit With Some instances in past (FB 1131) - start *** */
                //string recurring = "0";
                //if (xmldoc.SelectSingleNode("/conference/confInfo/recurring") != null)
                //    recurring = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/recurring").InnerText);
                /* *** Recurrence Fixes - commented for edit With Some instances in past (FB 1131) - end *** */

                switch (lblStatus.Text.ToString())
                {
                    case ns_MyVRMNet.vrmConfStatus.Scheduled:
                        lblStatus.Text = obj.GetTranslatedText("Scheduled");
                        tblEndpoints.Visible = true;
                        break;
                    case ns_MyVRMNet.vrmConfStatus.Pending:
                        lblStatus.Text = obj.GetTranslatedText("Pending");
                        break;
                    case ns_MyVRMNet.vrmConfStatus.Terminated:
                        lblStatus.Text = obj.GetTranslatedText("Terminated");
                        break;
                    case ns_MyVRMNet.vrmConfStatus.Ongoing:
                        lblStatus.Text = obj.GetTranslatedText("Ongoing");
                        tblEndpoints.Visible = true;
                        break;
                    case ns_MyVRMNet.vrmConfStatus.OnMCU:
                        lblStatus.Text = obj.GetTranslatedText("On MCU");
                        break;
                    case ns_MyVRMNet.vrmConfStatus.Completed:
                        lblStatus.Text = obj.GetTranslatedText("Completed");
                        break;
                    case ns_MyVRMNet.vrmConfStatus.Deleted:
                        lblStatus.Text = obj.GetTranslatedText("Deleted");
                        break;
                    case ns_MyVRMNet.vrmConfStatus.WaitList://ZD 102532
                        lblStatus.Text = obj.GetTranslatedText("Wait List");
                        break;
                    default:
                        lblStatus.Text = obj.GetTranslatedText("Undefined");
                        break;
                }
				//ZD 101579
                Boolean displayDetails = false;
                switch (lblConfType.Text.ToString())
                {
                    case ns_MyVRMNet.vrmConfType.RoomOnly:
                        displayDetails = false;//ZD 101579
                        //Added for FB 1428 Start
                        if (Application["Client"].ToString().ToUpper() == "MOJ")
                            lblConfType.Text = "Room Hearing";
                        else
                            //Added for FB 1428 End
                            lblConfType.Text = obj.GetTranslatedText("Room Conference");
                        tdStartMode.Visible = false;
                        tdStartMode1.Visible = false;//FB 2565
                        tdStartModeSelection.Visible = false;
                        //FB 2595
                        tdSecured.Visible = false;
                        tdSecured1.Visible = false;//FB 2565
                        tdSecuredSelection.Visible = false;
                        trMUCPreTime.Visible = false; //ZD 100085
                        tdDescription.Width = "117.5px";//ZD 100420
                        break;
                    case ns_MyVRMNet.vrmConfType.AudioOnly:
                        displayDetails = false;//ZD 101579
                        if (Application["Client"].ToString().ToUpper() == "MOJ")
                            lblConfType.Text = "Audio Only Hearing";
                        else
                            lblConfType.Text =obj.GetTranslatedText("Audio Only Conference");
                        //FB 2501 Starts
                        if (VMRConf)
                        {
                            tdStartMode.Visible = false;
                            tdStartMode1.Visible = false;//FB 2565
                            tdStartModeSelection.Visible = false;
                            trMUCPreTime.Visible = false; //ZD 100085
                            tdDescription.Width = "117.5px";//ZD 100420

                        }
                        else
                        {
                            tdStartMode.Visible = true;
                            tdStartMode1.Visible = true;//FB 2565
                            tdStartModeSelection.Visible = true;
                            trMUCPreTime.Visible = true; //ZD 100085
                            tdDescription.Width = "168px";//ZD 100420
                        }
                        //FB 2501 Ends
                        break;
                    case ns_MyVRMNet.vrmConfType.AudioVideo:
                        displayDetails = true;//ZD 101579
                        if (Application["Client"].ToString().ToUpper() == "MOJ")
                            lblConfType.Text = "Audio/Video Hearing";
                        else
                            lblConfType.Text = obj.GetTranslatedText("Audio/Video Conference");
                        //FB 2501 Starts
                        if (VMRConf)
                        {
                            tdStartMode.Visible = false;
                            tdStartMode1.Visible = false;//FB 2565
                            tdStartModeSelection.Visible = false;
                            trMUCPreTime.Visible = false; //ZD 100085
                            tdDescription.Width = "117.5px";//ZD 100420

                        }
                        else
                        {
                            tdStartMode.Visible = true;
                            tdStartMode1.Visible = true;//FB 2565
                            tdStartModeSelection.Visible = true;
                            trMUCPreTime.Visible = true; //ZD 100085
                            tdDescription.Width = "168px";//ZD 100420
                        }
                        //FB 2501 Ends
                        break;
                    case ns_MyVRMNet.vrmConfType.P2P:
                        displayDetails = false;//ZD 101579
                        if (Application["Client"].ToString().ToUpper() == "MOJ")
                            lblConfType.Text = "Point-To-Point Hearing";
                        else
                            lblConfType.Text = obj.GetTranslatedText("Point-To-Point Conference");
                        tdStartMode.Visible = false;
                        tdStartMode1.Visible = false;//FB 2565
                        tdStartModeSelection.Visible = false;
                        trMUCPreTime.Visible = false; //ZD 100085
                        tdDescription.Width = "117.5px";//ZD 100420

                        break;
                    //FB 2694 Start
                    case ns_MyVRMNet.vrmConfType.HotDesking:
                        displayDetails = false;//ZD 101579
                        lblConfType.Text = obj.GetTranslatedText("Hotdesking");
                        trStModeNwStarte.Attributes.Add("style", "display:none;");                        
                        trPuPw.Attributes.Add("style", "display:none;");
                        tdReminders.Attributes.Add("style", "display:none;");
                        tdReminders1.Attributes.Add("style", "display:none;");
                        tdReminderSelections.Attributes.Add("style", "display:none;");
                        trMUCPreTime.Visible = false; //ZD 100085
                        tdDescription.Width = "117.5px";//ZD 100420

                        break;
                    //FB 2694 End
                    case ns_MyVRMNet.vrmConfType.OBTP:
                        displayDetails = false;//ZD 101579
                        if (Application["Client"].ToString().ToUpper() == "MOJ")
                            lblConfType.Text = "OBTP Hearing";
                        else
                            lblConfType.Text = obj.GetTranslatedText("OBTP Conference");
                        if (VMRConf)
                        {
                            tdStartMode.Visible = false;
                            tdStartMode1.Visible = false;
                            tdStartModeSelection.Visible = false;
                            trMUCPreTime.Visible = false;
                            tdDescription.Width = "117.5px";

                        }
                        else
                        {
                            tdStartMode.Visible = true;
                            tdStartMode1.Visible = true;
                            tdStartModeSelection.Visible = true;
                            trMUCPreTime.Visible = true;
                            tdDescription.Width = "168px";
                        }
                        break;
                    default:
                        displayDetails = false;//ZD 101579
                        lblConfType.Text = obj.GetTranslatedText("Undefined");
                        break;

                }
                //ZD 101579
                if (DrpDwnListView.SelectedValue == "2")
                {
                    if (displayDetails)
                    {
                        LinkButton1.Enabled = true;
                        LinkButton1.ForeColor = System.Drawing.Color.Black;// FF
                        LinkButton1.Style.Add("cursor", "pointer");

                        LinkButton2.Enabled = true;
                        LinkButton2.ForeColor = System.Drawing.Color.Black;//
                        LinkButton2.Style.Add("cursor", "pointer");

                        LinkButton2.Enabled = true;
                        LinkButton2.ForeColor = System.Drawing.Color.Black;//
                        LinkButton2.Style.Add("cursor", "pointer");

                        LinkButton3.Enabled = true;
                        LinkButton3.ForeColor = System.Drawing.Color.Black;
                        LinkButton3.Style.Add("cursor", "pointer");
                        LinkButton3.Attributes["OnClick"] = "javascript:return fnextendtime('1');return true;";//ZD 102997
                        txtExtendedTime.Enabled = true;//ZD 102997
                        btnExtendEndtime.Enabled = true;//ZD 102997

                        LinkButton4.ForeColor = System.Drawing.Color.Black;//
                        LinkButton4.Style.Add("cursor", "pointer");
                        LinkButton4.Enabled = true;
                        //btnAddEndpoint.Attributes["OnClick"] = "return false;";

                        btnAddEndpoint.Enabled = true;
                        btnAddEndpoint.ForeColor = System.Drawing.Color.Black;
                        btnAddEndpoint.Style.Add("cursor", "pointer");
                        btnAddEndpoint.Attributes["OnClick"] = "return true;";//Modification

                        LinkButton6.Enabled = true;
                        LinkButton6.ForeColor = System.Drawing.Color.Black;//FF
                        LinkButton6.Style.Add("cursor", "pointer");

                    }
                    else
                    {
                        LinkButton1.Attributes["OnClick"] = "return false;";
                        LinkButton1.ForeColor = System.Drawing.Color.Gray;
                        LinkButton1.Style.Add("cursor", "default");

                        LinkButton2.Attributes["OnClick"] = "return false;";
                        LinkButton2.ForeColor = System.Drawing.Color.Gray;
                        LinkButton2.Style.Add("cursor", "default");

                        LinkButton3.Enabled = true;
                        LinkButton3.ForeColor = System.Drawing.Color.Black;
                        LinkButton3.Style.Add("cursor", "pointer");
                        LinkButton3.Attributes["OnClick"] = "javascript:return fnextendtime('1');return true;";//ZD 102997
                        txtExtendedTime.Enabled = true;//ZD 102997
                        btnExtendEndtime.Enabled = true;//ZD 102997

                        LinkButton4.Attributes["OnClick"] = "return false;";
                        LinkButton4.ForeColor = System.Drawing.Color.Gray;
                        LinkButton4.Style.Add("cursor", "default");
                        LinkButton4.Enabled = false;

                        btnAddEndpoint.Attributes["OnClick"] = "return false;";
                        btnAddEndpoint.ForeColor = System.Drawing.Color.Gray;
                        btnAddEndpoint.Style.Add("cursor", "default");


                        LinkButton6.Attributes.Add("Disabled", "Disabled");
                        LinkButton6.Attributes["OnClick"] = "return false;";
                        LinkButton6.ForeColor = System.Drawing.Color.Gray;
                        LinkButton6.Style.Add("cursor", "default");

                    }
                }

                if (xmldoc.SelectSingleNode("/conference/confInfo/immediate").InnerText.Equals("1"))
                {

                    //FB 2588 Starts
                    DateTime immdStartDate = DateTime.MinValue;

                    immdStartDate = Convert.ToDateTime(xmldoc.SelectSingleNode("/conference/confInfo/ImmediateConfDate").InnerText);                                     

                    int sIHour = Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/ImmediateConfHour").InnerText);
                    int sIMin = Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/ImmediateConfMin").InnerText);
                    string Iset = xmldoc.SelectSingleNode("/conference/confInfo/ImmediateConfSet").InnerText;

                    if ((Iset.ToUpper().Equals("PM")) && (sIHour != 12))
                        sIHour += 12;

                    //FB 2588 Ends
                    //FB 1774 - Start
                    DateTime startDate = DateTime.MinValue;
                    startDate = Convert.ToDateTime(xmldoc.SelectSingleNode("/conference/confInfo/startDate").InnerText);
                    int syear = startDate.Year; // Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/startDate").InnerText.Split('/')[2]);
                    int smonth = startDate.Month; //Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/startDate").InnerText.Split('/')[0]);
                    int sday = startDate.Day; //Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/startDate").InnerText.Split('/')[1]);
                    //FB 1774 - End
                    int sHour = Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/startHour").InnerText);
                    int sMin = Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/startMin").InnerText);
                    string sSet = xmldoc.SelectSingleNode("/conference/confInfo/startSet").InnerText;
                    if ((sSet.ToUpper().Equals("PM")) && (sHour != 12))
                        sHour += 12;

                    DateTime sDate = DateTime.Parse(smonth + "/" + sday + "/" + syear + " " + sHour + ":" + sMin + " " + sSet);  //buffer zone

                    DateTime sIDate = DateTime.Parse(smonth + "/" + sday + "/" + syear + " " + sIHour + ":" + sIMin + " " + Iset); //FB 2588
                    int dur = Convert.ToInt32(xmldoc.SelectSingleNode("/conference/confInfo/durationMin").InnerText);//FB 2960
                    lblConfDate.Text = myVRMNet.NETFunctions.GetFormattedDate(sDate);
                    lblConfTime.Text = myVRMNet.NETFunctions.GetFormattedTime(sIDate.ToShortTimeString(), Session["timeFormat"].ToString()); //FB 2588
                    lblSetupDur.Text = myVRMNet.NETFunctions.GetFormattedDate(sDate) + " "+ myVRMNet.NETFunctions.GetFormattedTime(sIDate.ToShortTimeString(), Session["timeFormat"].ToString()); //FB 2960
                    DateTime confendDateTime = sIDate.AddMinutes(dur);//FB 2960
                    lblTearDownDur.Text = myVRMNet.NETFunctions.GetFormattedDate(confendDateTime) + " " + myVRMNet.NETFunctions.GetFormattedTime(confendDateTime.ToShortTimeString(), Session["timeFormat"].ToString()); //FB 2960
                    //int dur = Convert.ToInt32(xmldoc.SelectSingleNode("/conference/confInfo/durationMin").InnerText);//FB 2960
                    hdnConfDuration.Text = dur.ToString();
                    lblConfDuration.Text = obj.GetProperValue((dur / 60) + " " + obj.GetTranslatedText("hrs") + " " + (dur % 60) + " " + obj.GetTranslatedText("mins"));//ZD 100288 //ZD 100528
                }
                else
                {
                    if (recurring.Equals("1"))
                    {
                        string recurstr = xmldoc.SelectSingleNode("/conference/confInfo/appointmentTime").InnerXml;
                        recurstr += xmldoc.SelectSingleNode("/conference/confInfo/recurrencePattern").InnerXml;
                        if (xmldoc.SelectNodes("/conference/confInfo/recurrenceRange").Count != 0)
                            recurstr += xmldoc.SelectSingleNode("/conference/confInfo/recurrenceRange").InnerXml;
                        recurstr = "<recurstr>" + recurstr + "</recurstr>";
                        //Response.Write(obj.Transfer(recurstr));
                        string tzstr = "<TimeZone>" + xmldoc.SelectSingleNode("/conference/confInfo/timezones").InnerXml + "</TimeZone>";
                        string rst = obj.AssembleRecur(recurstr, tzstr);
                        string[] rst_array = rst.Split('|');
                        string recur = rst_array[0];
                        /* *** Code added by Offshore for fb Issue 1073 DateFormat -Start **** */

                        string recDtString = "";
                        string tempRec = "";

                        String[] recDateArr = recur.Split('#');

                        recur = "";
                        if (recDateArr.Length > 0)
                        {
                            tempRec = recDateArr[recDateArr.Length - 1];
                            if (tempRec != "")
                            {
                                String[] dtsArr = tempRec.Split('&');

                                if (dtsArr.Length > 0)
                                {
                                    for (int lp = 0; lp < dtsArr.Length; lp++)
                                    {
                                        if (dtsArr[lp].IndexOf("/") > 0)
                                        {
                                            dtsArr[lp] = myVRMNet.NETFunctions.GetFormattedDate(dtsArr[lp]);
                                        }
                                        if (recDtString == "")
                                            recDtString = dtsArr[lp];
                                        else
                                            recDtString += "&" + dtsArr[lp];
                                    }
                                }
                            }
                            for (int lp = 0; lp < recDateArr.Length - 1; lp++)
                            {
                                if (recur == "")
                                    recur = recDateArr[lp];
                                else
                                    recur += "#" + recDateArr[lp];
                            }
                            recur += "#" + recDtString;
                        }

                        btnDeleteConf.Text = obj.GetTranslatedText("Delete All");
                        btnEdit.Text = obj.GetTranslatedText("Edit All");
                        btnEdit.Attributes.Add("onclick", "javascript:return CustomEditAlert();");// FB 1391
                        //ZD 101597
                        if (Int32.Parse(Session["admin"].ToString()) > 0)
                        {
                            btnEdit.Attributes.Add("onclick", "javascript:return fnSelectForm('CL');");
                        }

                        /* *** Code added by Offshore for fb Issue 1073 DateFormat - End **** */

                        Recur.Text = recur;
                        string SelectedTimeZoneName = rst_array[1];
                        if (xmldoc.SelectNodes("//conference/confInfo/recurrenceRange").Count != 0)//Conference console fix...
                            lblConfDate.Text = myVRMNet.NETFunctions.GetFormattedDate(xmldoc.SelectSingleNode("/conference/confInfo/recurrenceRange/startDate").InnerText);

                        lblConfDuration.Text = obj.GetTranslatedText("N/A"); //FB 1133
						//ZD 100085 Starts
                        string startHour = "0", startMin = "0", startSet = "AM";
                        int duration = 0;

                        if (xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/startHour") != null)
                        {
                            if (xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/startHour").InnerText != "")
                            {
                                startHour = xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/startHour").InnerText;
                            }
                        }
                        if (xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/startMin") != null)
                        {
                            if (xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/startMin").InnerText != "")
                            {
                                startMin = xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/startMin").InnerText;
                            }
                        }
                        if (xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/startSet") != null)
                        {
                            if (xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/startSet").InnerText != "")
                            {
                                startSet = xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/startSet").InnerText;
                            }
                        }

                        if (xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/durationMin") != null)
                            int.TryParse(xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/durationMin").InnerText, out duration);

                        //Double.TryParse(durationMin, out duration);

                        DateTime sTime = Convert.ToDateTime(startHour + ":" + startMin + " " + startSet);
                        DateTime endTime = sTime.AddMinutes(duration);

                        //setupTime = setupTime.AddMinutes(setupDuration);
                        //string sTime = setupTime.ToString(tformat);

                        //DateTime endTime = Convert.ToDateTime(startHour + ":" + startMin + " " + startSet);
                        //endTime = endTime.AddMinutes(-tearDuration);
                        //string tTime = endTime.ToString(tformat);

                        lblConfTime.Text = sTime.ToString(tformat);//Conference Console Fix...
                        lblSetupDur.Text = sTime.ToString(tformat);
                        lblTearDownDur.Text = endTime.ToString(tformat);
						//ZD 100085 End
                    }
                    else
                    {
                        btnDeleteConf.Text = obj.GetTranslatedText("Delete");
                        btnEdit.Text = obj.GetTranslatedText("Edit");
                        //FB 1774 - Start
                        DateTime startDate = DateTime.MinValue;
                        startDate = Convert.ToDateTime(xmldoc.SelectSingleNode("/conference/confInfo/startDate").InnerText);
                        int syear = startDate.Year; // Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/startDate").InnerText.Split('/')[2]);
                        int smonth = startDate.Month; //Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/startDate").InnerText.Split('/')[0]);
                        int sday = startDate.Day; //Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/startDate").InnerText.Split('/')[1]);
                        //FB 1774 - End
                        int sHour = Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/startHour").InnerText);
                        int sMin = Convert.ToInt16(xmldoc.SelectSingleNode("/conference/confInfo/startMin").InnerText);
                        string sSet = xmldoc.SelectSingleNode("/conference/confInfo/startSet").InnerText;
                        if ((sSet.ToUpper().Equals("PM")) && (sHour != 12))
                            sHour += 12;
                        //DateTime sDate = new DateTime(syear, smonth, sday, sHour, sMin, 0);
                        DateTime sDate = DateTime.Parse(smonth + "/" + sday + "/" + syear + " " + sHour + ":" + sMin + " " + sSet);  //buffer zone
                        //Code changed by Offshore for FB Issue 1073 -- Start
                        //lblConfDate.Text = sDate.ToString("MM/dd/yyyy"); //.Split(' ')[0];
                        lblConfDate.Text = myVRMNet.NETFunctions.GetFormattedDate(sDate); //.Split(' ')[0];
                        //Code changed by Offshore for FB Issue 1073 -- End
                        lblConfTime.Text = myVRMNet.NETFunctions.GetFormattedTime(sDate.ToShortTimeString(), Session["timeFormat"].ToString());//.Split(' ')[1].Split(':')[0] + ":" + sDate.ToString().Split(' ')[1].Split(':')[1]) + " " + sDate.ToString().Split(' ')[2]; // Convert.ToUInt16(sHour.ToString(), 10) + ":" + Convert.ToUInt16(sMin.ToString(), 10) + " " + sSet; FB 1425
                        int dur = Convert.ToInt32(xmldoc.SelectSingleNode("/conference/confInfo/durationMin").InnerText);
                        lblConfDuration.Text = obj.GetProperValue((dur / 60) + " " + obj.GetTranslatedText("hrs") + " " +(dur % 60) + " " + obj.GetTranslatedText("mins"));//ZD 100288
                        hdnConfDuration.Text = dur.ToString();

                        DateTime systemDate = Convert.ToDateTime(Session["systemDate"].ToString());
                        syear = systemDate.Year;// Convert.ToInt16(Session["systemDate"].ToString().Split('/')[2]);
                        smonth = systemDate.Month;// Convert.ToInt16(Session["systemDate"].ToString().Split('/')[0]);
                        sday = systemDate.Day;// Convert.ToInt16(Session["systemDate"].ToString().Split('/')[1]);
                        sHour = systemDate.Hour;// Convert.ToInt16(Session["systemTime"].ToString().Split(':')[0]);
                        sMin = systemDate.Minute;// Convert.ToInt16(Session["systemTime"].ToString().Split(':')[1].Split(' ')[0]);

                        

                        DateTime sysDate = new DateTime(syear, smonth, sday, sHour, sMin, 0);

                        TimeSpan ts = sDate.Subtract(sysDate);
                        double temp = (ts.Days * 24) + ts.Hours;
                        txtTimeDifference.Text = temp.ToString();

                        //code added/changed for buffer zone --Start
						//ZD 100085 Starts
                        int duration = 0;
                        if (xmldoc.SelectSingleNode("//conference/confInfo/durationMin") != null)
                            int.TryParse(xmldoc.SelectSingleNode("//conference/confInfo/durationMin").InnerText, out duration);

                        //DateTime setupStartDateTime = sDate.AddMinutes(setupDuration);
                        lblSetupDur.Text = myVRMNet.NETFunctions.GetFormattedDate(sDate) + " " + sDate.ToString(tformat);
                        DateTime endDateTime = sDate.AddMinutes(duration);
                        //DateTime teardownStartDateTime = endDateTime.AddMinutes(-tearDuration);

                        lblTearDownDur.Text = myVRMNet.NETFunctions.GetFormattedDate(endDateTime) + " " + endDateTime.ToString(tformat);
						//ZD 100085 End
                    }
                }
                char[] splitter = { '/' };
                lblDescription.Text = utilObj.ReplaceOutXMLSpecialCharacters(obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/description").InnerText), 2); //FB 2236
                //ZD 100288 Start
                if (lblDescription.Text.Trim() == "N/A")
                    lblDescription.Text = obj.GetTranslatedText("N/A");
                //ZD 100288 End
                if (xmldoc.SelectSingleNode("/conference/confInfo/publicConf").InnerText.Equals("0"))
                    lblPublic.Text = obj.GetTranslatedText("No");
                else
                {
                    lblPublic.Text = obj.GetTranslatedText("Yes");
                    if (xmldoc.SelectSingleNode("/conference/confInfo/dynamicInvite").InnerText.Equals("1"))
                        lblRegistration.Text = obj.GetTranslatedText("(Open for Registration)");
                    else // FB 2050
                        lblRegistration.Text = ""; // FB 2050
                }
                string tzID = xmldoc.SelectSingleNode("/conference/confInfo/timeZone").InnerText;
                hdnTimeZoneId.Value = tzID; //ZD 100602
                string tzName = "";
                XmlNodeList nodes = xmldoc.SelectNodes("/conference/confInfo/timezones/timezone");
                int length = nodes.Count;
                for (int i = 0; i < length; i++)
                    if (nodes[i].SelectSingleNode("timezoneID").InnerText.Equals(tzID))
                        tzName = nodes[i].SelectSingleNode("timezoneName").InnerText;
                if (!xmldoc.SelectSingleNode("/conference/confInfo/immediate").InnerText.Equals("1"))
                    lblTimezone.Text = obj.GetProperValue(tzName);

                if (Session["timezoneDisplay"].ToString() == "0") //FB 1425
                    lblTimezone.Text = "";

                lblLastModifiedBy.Text = obj.GetProperValue(xmldoc.SelectSingleNode("conference/confInfo/loginlastModifiedByName").InnerText);//ZD 103445
                hdnLastModifiedBy.Text = xmldoc.SelectSingleNode("conference/confInfo/lastModifiedById").InnerText;

                //FB 2501 Starts
                if (xmldoc.SelectSingleNode("/conference/confInfo/isReminder") != null)
                {
                    lblReminders.Text = obj.GetTranslatedText("Yes");
                    if (xmldoc.SelectSingleNode("/conference/confInfo/isReminder").InnerText == "0")
                        lblReminders.Text = obj.GetTranslatedText("No");
                }
                
                if (Session["EnableVNOCselection"].ToString() == "1" && Session["EnableDedicatedVNOC"].ToString() == "1")
                {
                    trVNOC.Visible = true;
                    trVNOCoptor.Visible = true;
                    lblConfVNOC.Text = obj.GetTranslatedText("N/A");
                    XmlNodeList ConfVNOCnodes = xmldoc.SelectNodes("//conference/confInfo/ConciergeSupport/ConfVNOCOperators/VNOCOperator");
                    if (ConfVNOCnodes.Count > 0)
                    {
                        lblConfVNOC.Text = "";
                        for (int i = 0; i < ConfVNOCnodes.Count; i++)
                        {
                            {
                                if (ConfVNOCnodes[i].InnerText != null)
                                {
                                    if (lblConfVNOC.Text == "")
                                        lblConfVNOC.Text = ConfVNOCnodes[i].InnerText.Trim();
                                    else
                                        lblConfVNOC.Text += ",\n" + ConfVNOCnodes[i].InnerText.Trim();
                                }
                            }
                        }
                    }

                }
                else
                {
                    trVNOC.Attributes.Add("style", "display: none;");
                    trVNOCoptor.Attributes.Add("style", "display: none;");
                    tdvnoccol.Attributes.Add("style", "display: none;");
                }

                string StartMode = "0";
                StartMode = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/StartMode").InnerText);

                if (StartMode == "0")
                    lblStartMode.Text = obj.GetTranslatedText("Automatic");
                else
                    lblStartMode.Text = obj.GetTranslatedText("Manual");

                //FB 2501 Ends

                //FB 2595 Start //FB 2993 Starts
                if (Session["NetworkSwitching"] != null)
                {
                    int.TryParse(Session["NetworkSwitching"].ToString(), out NetworkSwitching);
                    if (NetworkSwitching==2)
                    {
                        tdSecured.Visible = true;
                        tdSecured1.Visible = true;//FB 2565
                        tdSecuredSelection.Visible = true;
                    }
                    else
                    {
                        tdSecured.Visible = false;
                        tdSecured1.Visible = false;//FB 2565
                        tdSecuredSelection.Visible = false;
                    }
                }
                if (xmldoc.SelectSingleNode("/conference/confInfo/Secured") != null)
                {
                    if (xmldoc.SelectSingleNode("/conference/confInfo/Secured").InnerText.Equals("1"))
                        lblSecured.Text = obj.GetTranslatedText("NATO Secret");
                    else 
                        lblSecured.Text = obj.GetTranslatedText("NATO Unclassified");
                }
                //FB 2595 Ends //FB 2993 Ends

                nodes = xmldoc.SelectNodes("//conference/confInfo/fileUpload/file");
                lblFiles.Text = "";
                //Response.Write("here");
                foreach (XmlNode node in nodes)
                    if (!node.InnerText.Equals(""))
                    {
                        //FB 1830
                        String fileName = getUploadFilePath(node.InnerText);
                        String fPath = node.InnerText;
                        int startIndex = fPath.IndexOf(@"\en\");
                        if ((language == "en" || fPath.IndexOf(@"\en\") > 0) && startIndex > 0)
                        {
                            fPath = fPath.Replace("\\", "/");
                            int len = fPath.Length - 1;
                            //Response.Write(fPath + " : " + startIndex + " : " + len);
                            fPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 1) + "en" + fPath.Substring(startIndex + 3);//FB 1830
                        }
                        else
                            fPath = "../Image/" + fileName;

                        //fPath = fPath.Replace("\\", "/");
                        //int startIndex = fPath.IndexOf("/" + language + "/");//FB 1830
                        //int len = fPath.Length - 1;
                        ////Response.Write(fPath + " : " + startIndex + " : " + len);
                        //fPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 3) + fPath.Substring(startIndex + 3);//FB 1830

                        lblFiles.Text += "<a href='" + fPath + "' target='_blank'>" + fileName + "</a>, ";
                    }
                if (lblFiles.Text.Length > 0)
                    lblFiles.Text = lblFiles.Text.Substring(0, lblFiles.Text.Length - 2);
                else
                    lblFiles.Text = obj.GetTranslatedText("N/A"); //ZD 100288
                lblConfHost.Text = "<a href='mailto:" + xmldoc.SelectSingleNode("conference/confInfo/hostEmail").InnerText + "'>" + xmldoc.SelectSingleNode("conference/confInfo/hostName").InnerText + "</a>";
                // Code added for the Bug # 74- mpujari
                hdnConfHost.Text = xmldoc.SelectSingleNode("conference/confInfo/hostId").InnerText;

                //ZD 101233 Start
                int hasView = 0;
                int hasEdit = 0;
                int hasDelete = 0;
                int hasManage = 0;
                int hasClone = 0;
                int hasMCUInfo = 0;
                int hasExtendTime = 0;
                int filterType = 0;

                if (DrpDwnListView.SelectedValue == "2")
                    filterType = 2;
                else if (DrpDwnListView.SelectedValue == "3")
                    filterType = 8;
                else
                    filterType = 3;

                obj.CheckConferenceRights(filterType, ref hasView, ref hasManage, ref hasExtendTime, ref hasMCUInfo, ref hasEdit, ref hasDelete, ref hasClone);

                ViewState["hasView"] = hasView.ToString();
                ViewState["hasManage"] = hasManage.ToString();
                ViewState["hasEdit"] = hasEdit.ToString();
                ViewState["hasDelete"] = hasDelete.ToString();
                ViewState["hasClone"] = hasClone.ToString();
                ViewState["hasExtendTime"] = hasExtendTime.ToString();

                int adminLevel = 0, loginUser = 0, hostUserId = 0;
                int.TryParse(Session["admin"].ToString(), out adminLevel);
                int.TryParse(Session["userID"].ToString(), out loginUser);
                int.TryParse(hdnConfHost.Text, out hostUserId);

                if (adminLevel <= 1 && loginUser != hostUserId)
                {
                    btnDeleteConf.Attributes["OnClick"] = "return false;";
                    btnDeleteConf.ForeColor = System.Drawing.Color.Gray;
                    btnDeleteConf.Style.Add("cursor", "default");
                    //btnDeleteConf.OnClientClick = null;
                    btnDeleteConf.Enabled = false;
                    //ZD 101579
                    //LinkButton6.Attributes["OnClick"] = "return false;";
                    //LinkButton6.ForeColor = System.Drawing.Color.Gray;
                    //LinkButton6.Style.Add("cursor", "default");
                    ////LinkButton6.OnClientClick = null;
                    //LinkButton6.Enabled = false;


                    btnEdit.Attributes["OnClick"] = "return false;";
                    btnEdit.ForeColor = System.Drawing.Color.Gray;
                    btnEdit.Style.Add("cursor", "default");
                    //btnEdit.OnClientClick = null;
                    btnEdit.Enabled = false;

                    A3.Disabled = true;
                    A3.Style.Add("color", "Gray");
                    A3.Style.Add("cursor", "default");
                }
                else
                {
                    btnDeleteConf.Attributes["OnClick"] = "return true;";
                    btnDeleteConf.ForeColor = System.Drawing.Color.Black;
                    btnDeleteConf.Style.Add("cursor", "pointer");
                    //btnDeleteConf.OnClientClick = null;
                    btnDeleteConf.Enabled = true;
                    //ZD 101579
                    //LinkButton6.Attributes["OnClick"] = "return true;";
                    //LinkButton6.ForeColor = System.Drawing.Color.Black;
                    //LinkButton6.Style.Add("cursor", "pointer");
                    ////LinkButton6.OnClientClick = null;
                    //LinkButton6.Enabled = true;


                    btnEdit.Attributes["OnClick"] = "return true;";
                    btnEdit.ForeColor = System.Drawing.Color.Black;
                    btnEdit.Style.Add("cursor", "pointer");
                    //btnEdit.OnClientClick = null;
                    btnEdit.Enabled = true;
                    if (Int32.Parse(Session["admin"].ToString()) > 0)
                        btnEdit.Attributes.Add("onclick", "javascript:return fnSelectForm();");
                    
                    A3.Disabled = false;
                    A3.Style.Add("color", "#5555FF");
                    A3.Style.Add("cursor", "pointer");
                }
                if (hasManage == 0)
                {
                    A3.Disabled = true;
                    A3.Style.Add("color", "Gray");
                    A3.Attributes.Remove("onclick");
                    A3.Style.Add("cursor", "default");
                }
                else //ZD 101388
                {
                    A3.Disabled = false;
                    A3.Style.Add("color", "#5555FF");
                    A3.Style.Add("cursor", "pointer");
                }
                if (hasManage == 0)
                {
                    //LinkButton3.Attributes["OnClick"] = "return false;";
                    LinkButton3.Attributes.Remove("OnClick");
                    LinkButton3.ForeColor = System.Drawing.Color.Gray;
                    LinkButton3.Style.Add("cursor", "default");
                    LinkButton3.OnClientClick = null;
                    LinkButton3.Enabled = false;


                    btnEdit.Attributes["OnClick"] = "return false;";
                    btnEdit.ForeColor = System.Drawing.Color.Gray;
                    btnEdit.Style.Add("cursor", "default");
                    btnEdit.OnClientClick = null;
                    btnEdit.Enabled = false;

                    btnDeleteConf.Attributes["OnClick"] = "return false;";
                    btnDeleteConf.ForeColor = System.Drawing.Color.Gray;
                    btnDeleteConf.Style.Add("cursor", "default");
                    btnDeleteConf.OnClientClick = null;
                    btnDeleteConf.Enabled = false;

                    LinkButton4.Attributes["OnClick"] = "return false;";
                    LinkButton4.ForeColor = System.Drawing.Color.Gray;
                    LinkButton4.Style.Add("cursor", "default");
                    LinkButton4.OnClientClick = null;
                    LinkButton4.Enabled = false;


                    LinkButton1.Attributes["OnClick"] = "return false;";
                    LinkButton1.ForeColor = System.Drawing.Color.Gray;
                    LinkButton1.Style.Add("cursor", "default");
                    LinkButton2.Enabled = false;
                    LinkButton2.OnClientClick = null;


                    LinkButton2.Attributes["OnClick"] = "return false;";
                    LinkButton2.ForeColor = System.Drawing.Color.Gray;
                    LinkButton2.Style.Add("cursor", "default");
                    LinkButton2.OnClientClick = null;
                    LinkButton2.Enabled = false;


                    LinkButton6.Enabled = false;
                    LinkButton6.Attributes["OnClick"] = "return false;";
                    LinkButton6.ForeColor = System.Drawing.Color.Gray;
                    LinkButton6.Style.Add("cursor", "default");

                    btnPDF.Enabled = false;
                    btnPDF.ForeColor = System.Drawing.Color.Gray;
                    btnPDF.OnClientClick = null;
                    btnPDF.Enabled = false;

                    btnAddEndpoint.Enabled = false;
                    btnAddEndpoint.Attributes["OnClick"] = "return false;";
                    btnAddEndpoint.ForeColor = System.Drawing.Color.Gray;
                    btnAddEndpoint.Style.Add("cursor", "default");
                    btnAddEndpoint.OnClientClick = null;
                    btnAddEndpoint.Enabled = false;

                    A3.Disabled = true;
                    A3.Style.Add("color", "Gray");
                    A3.Attributes.Remove("onclick");

                }

                if (hasExtendTime == 0)
                {
                    //LinkButton3.Attributes["OnClick"] = "return false;";
                    LinkButton3.Attributes.Remove("OnClick");
                    LinkButton3.ForeColor = System.Drawing.Color.Gray;
                    LinkButton3.Style.Add("cursor", "default");
                    LinkButton3.OnClientClick = null;
                    LinkButton3.Enabled = false;
                }
                if (hasEdit == 0)
                {
                    btnEdit.Attributes["OnClick"] = "return false;";
                    btnEdit.ForeColor = System.Drawing.Color.Gray;
                    btnEdit.Style.Add("cursor", "default");
                    btnEdit.OnClientClick = null;
                    btnEdit.Enabled = false;
                }
                if (hasDelete == 0)
                {
                    btnDeleteConf.Attributes["OnClick"] = "return false;";
                    btnDeleteConf.ForeColor = System.Drawing.Color.Gray;
                    btnDeleteConf.Style.Add("cursor", "default");
                    btnDeleteConf.OnClientClick = null;
                    btnDeleteConf.Enabled = false;

                    LinkButton4.Attributes["OnClick"] = "return false;";
                    LinkButton4.ForeColor = System.Drawing.Color.Gray;
                    LinkButton4.Style.Add("cursor", "default"); // FF End
                    LinkButton4.OnClientClick = null;
                    LinkButton4.Enabled = false;
                }
                //ZD 101233 End
                

                ShowHostDetails(); //FB 1958
                LoadEndpoints();

            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("DisplayConferenceDetails" + ex.Message);
                LblError.Text = obj.ShowSystemMessage();
                LblError.Visible = true;
                //LblError.Text = "Details: " + ex.StackTrace;
            }
        }

        #endregion

        #region Load EndPoints

        protected void LoadEndpoints()
        {
            try
            {
                if (hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.P2P) && lblStatus.Text == "Ongoing")
                {
                    //p2pStatus = GetP2pStatus();//Blue Status Project
                    refreshCell.Visible = false; //ZD 101133
                    btnAddEndpoint.Enabled = false;
                    ss.Visible = false; //ZD 101133
                    TableCell5.Visible = false; //ZD 101133
                    TableCell6.Visible = false; //ZD 101133
                    //ZD 101579
                    
                    LinkButton4.ForeColor = System.Drawing.Color.Black;//
                    LinkButton4.Style.Add("cursor", "pointer");
                    LinkButton4.Enabled = true;

                }
                else
                {
                    refreshCell.Visible = false;//GP Fixes
                    //ZD 101579
                    ss.Visible = true;
                    //ZD 101931 start
                    if (Session["ShowVideoLayout"] != null && !string.IsNullOrEmpty(Session["ShowVideoLayout"].ToString()) && (Session["ShowVideoLayout"].ToString() == "1" || Session["ShowVideoLayout"].ToString() == "0"))
                        TableCell5.Attributes.Add("style", "Display:;");
                    else
                        TableCell5.Attributes.Add("style", "Display:None;");
                    //ZD 101931 End
                    TableCell6.Visible = true; 
                }
                GetVideoLayouts();

                String inXML = "<login>";
                inXML += obj.OrgXMLElement();
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <confID>" + lblConfID.Text.Split(',')[0] + "</confID>";
                inXML += "</login>";
                
                //String outXML = obj.CallCOM("GetTerminalControl", inXML, Application["COM_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("GetTerminalControl", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027(GetTerminalControl)
                
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlDocument xmldocEP = new XmlDocument();
                XmlNodeList nodesEP = xmldoc.SelectNodes("//terminalControl/confInfo/terminals/terminal");
                XmlNodeList nodes = xmldoc.SelectNodes("//terminalControl/confInfo/terminals/terminal");
                String outXMLEP = "<Endpoints>";
                obj.BindAddressType(lstAddressType);
                foreach (XmlNode nodeEP in nodesEP)
                {
                    inXML = "<GetConferenceEndpoint>";
                    inXML += obj.OrgXMLElement();
                    inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                    inXML += "  <ConfID>" + lblConfID.Text + "</ConfID>";
                    //Blue Status Project Code Review
                    //if (hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.P2P) && lblStatus.Text == "Ongoing" && Refreshchk.Checked == true) 
                    //    inXML += "  <EndpointID></EndpointID>";
                    //else
                    inXML += "  <EndpointID>" + nodeEP.SelectSingleNode("endpointID").InnerText + "</EndpointID>";
                    String tpe = "U";
                    if (nodeEP.SelectSingleNode("type").InnerText.Equals("2"))
                        tpe = "R";
                    if (nodeEP.SelectSingleNode("type").InnerText.Equals("4"))
                        tpe = "C";
                    //Blue Status Project Code Review
                    //if (hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.P2P) && lblStatus.Text == "Ongoing" && Refreshchk.Checked == true)
                    //    inXML += "  <Type></Type>";
                    //else
                    inXML += "  <Type>" + tpe + "</Type>";
                    inXML += "</GetConferenceEndpoint>";
                    outXML = obj.CallMyVRMServer("GetConferenceEndpoint", inXML, Application["MyVRMserver_ConfigPath"].ToString());
                    if (outXML.IndexOf("<error>") < 0)
                    {
                        xmldocEP.LoadXml(outXML);
                        //ZD 101133  Start
                        if (hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.P2P) && lblStatus.Text == obj.GetTranslatedText("Ongoing"))
                        {
                            if (xmldocEP.SelectSingleNode("//GetConferenceEndpoint/Endpoint/Connect2").InnerText.Equals("1"))
                            {
                                if (xmldocEP.SelectSingleNode("//GetConferenceEndpoint/confmessage").InnerText.Equals("1"))
                                {
                                    //refreshCell.Visible = true; // ZD 101189
                                    refreshCell.Visible = false;
                                }
                            }
                        }
                        //ZD 101133 End
                        outXMLEP += "<Endpoint>" + nodeEP.InnerXml;
                        outXMLEP += xmldocEP.SelectSingleNode("//GetConferenceEndpoint/Endpoint").InnerXml + "</Endpoint>";
                    }
                }

                outXMLEP += "</Endpoints>";
                xmldocEP = new XmlDocument();
                xmldocEP.LoadXml(outXMLEP);
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                //FB 2400 start
                String adds = "";
                nodes = xmldocEP.SelectNodes("//Endpoints/Endpoint/MultiCodec");
                foreach (XmlNode node in nodes)
                {
                    adds = "";
                    XmlNodeList nodess = node.SelectNodes("Address");
                    Int32 j = 0;
                    foreach (XmlNode snode in nodess)
                    {
                        if (j > 0)
                            adds += "�";

                        adds += snode.InnerText;
                        j = j + 1;
                    }
                    node.InnerText = adds;
                }
                //FB 2400 End

                nodes = xmldocEP.SelectNodes("//Endpoints/Endpoint");
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv = null;
                DataTable dt = null;



                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;
                    if (!dt.Columns.Contains("ImageURL"))
                        dt.Columns.Add("ImageURL");
                    obj.BindVideoProtocols(lstProtocol);
                    bool adminStatus = true;
                    LblError.Text = "";
                    foreach (DataRow dr in dt.Rows)
                    {
                        try
                        {
                            dr["addressType"] = lstAddressType.Items.FindByValue(dr["addressType"].ToString()).Text;
                        }
                        catch (Exception ex)
                        {
                            dr["addressType"] = "Undefined";
                            log.Trace(ex.Message + " : " + ex.StackTrace);
                        }
                        try
                        {
                            dr["DefaultProtocol"] = "IP";
                            if (dr["BridgeAddressType"].ToString().Equals("4"))
                                dr["DefaultProtocol"] = "ISDN";
                            if (dr["BridgeAddressType"].ToString().Equals("5"))
                                dr["DefaultProtocol"] = "MPI";

                            dr["BridgeAddressType"] = lstAddressType.Items.FindByValue(dr["BridgeAddressType"].ToString()).Text;
                        }
                        catch (Exception ex)
                        {
                            dr["BridgeAddressType"] = "Undefined";
                            log.Trace(ex.Message + " : " + ex.StackTrace);
                        }
                        switch (dr["connectionType"].ToString())
                        {
                            case ns_MyVRMNet.vrmConnectionTypes.DialIn:
                                dr["connectionType"] = obj.GetTranslatedText("Dial-in to Conference"); //ZD 100619 
                                break;
                            case ns_MyVRMNet.vrmConnectionTypes.DialOut:
                            case ns_MyVRMNet.vrmConnectionTypes.DialOutOld:
                                dr["connectionType"] = obj.GetTranslatedText("Dial-out to Location"); //ZD 100619
                                break;
                            case ns_MyVRMNet.vrmConnectionTypes.Direct:
                                dr["connectionType"] = obj.GetTranslatedText("Direct");
                                break;
                        }


                        if (dr["Name"].ToString() != "")
                        {
                            if (dr["Name"].ToString().Trim().StartsWith(","))
                                dr["Name"] = dr["Name"].ToString().Trim().Remove(0, 1);
                        }
                        /*
                                            String eptText = "<a href='javascript:void(0);' onclick=javascript:viewEndpoint('" + dr["ID"].ToString() + "');>" + dr["Name"].ToString() + "</a> ";
                                            dr["Name"] = eptText;*/

                        if (dr["EndpointName"].ToString().Trim().Equals(""))
                            dr["EndpointName"] = dr["Name"];

                        if (!dt.Columns.Contains("MCUName"))
                            dt.Columns.Add("MCUName");
                        if (!hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.P2P)) //FB 1462
                        {
                            inXML = "";
                            inXML += "<login>";
                            inXML += obj.OrgXMLElement();
                            inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                            inXML += "</login>";
                            outXML = obj.CallMyVRMServer("GetBridgeList", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                            XmlDocument xmldocMCU = new XmlDocument();
                            xmldocMCU.LoadXml(outXML);
                            XmlNodeList nodesMCU = xmldocMCU.SelectNodes("//bridgeInfo/bridges/bridge");

                            foreach (XmlNode node in nodesMCU)
                            {

                                if (node.SelectSingleNode("ID").InnerText.Trim().Equals(dr["BridgeID"].ToString().Trim()))
                                {

                                    dr["MCUName"] = "<a href='#' onclick=\"javascript:ViewBridgeDetails('" + node.SelectSingleNode("ID").InnerText + "');return false;\">" + node.SelectSingleNode("name").InnerText;


                                    if (node.SelectSingleNode("userstate") != null)
                                    {
                                        if (node.SelectSingleNode("userstate").InnerText == "I")
                                        {
                                            if (LblError.Text == "")
                                            {
                                                LblError.Text += "Error:MCU (" + node.SelectSingleNode("name").InnerText + ") Admin User - " + node.SelectSingleNode("administrator").InnerText.Trim() + " is In-Active. Please make sure the user is Active.";
                                            }
                                            else
                                            {
                                                if (!LblError.Text.Contains(node.SelectSingleNode("name").InnerText))
                                                    LblError.Text += "<br>Error:MCU (" + node.SelectSingleNode("name").InnerText + ") Admin User - " + node.SelectSingleNode("administrator").InnerText.Trim() + " is In-Active. Please make sure the user is Active.";
                                            }

                                            adminStatus = false;
                                        }
                                        else if (node.SelectSingleNode("userstate").InnerText == "D")
                                        {
                                            if (LblError.Text == "")
                                            {
                                                LblError.Text += "Error:MCU (" + node.SelectSingleNode("name").InnerText + ") admin User has been deleted";
                                            }
                                            else
                                            {
                                                if (!LblError.Text.Contains(node.SelectSingleNode("name").InnerText))
                                                    LblError.Text += "<br>Error:MCU (" + node.SelectSingleNode("name").InnerText + ") admin User has been deleted";
                                            }

                                            adminStatus = false;
                                        }
                                    }

                                }
                            }
                        }
                        dr["BridgeAddress"] = dr["BridgePrefix"].ToString() + " " + dr["BridgeAddress"].ToString();
                        if (dr["displayLayout"].ToString().Trim().Equals("") || dr["displayLayout"].ToString().Trim().Equals("0"))
                            dr["displayLayout"] = txtSelectedImage.Text;
                        dr["ImageURL"] = ImagesPath.Text + Int32.Parse(dr["displayLayout"].ToString()).ToString("00") + ".gif";


                        strEndPointID = dr["endpointID"].ToString();
                        strType = dr["type"].ToString();
                    }

                    if (Session["DtEptList"] != null)
                        Session["DtEptList"] = dt;
                    else
                        Session.Add("DtEptList", dt);

                    DataView dvSort = null;
                    dvSort = new DataView(dt);
                    //Edited For FF..
                    string RoomAttendee = "";

                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("TRIDENT")) // ZD 102431
                    {
                        RoomAttendee = "<span style='font-family:Wingdings; font-weight:bold;'>&#233;</span>";
                    }
                    else
                    {
                        RoomAttendee = "<span  style='font-family:Wingdings; font-weight:bolder; font-size:x-large;'>&#8593;</span>";
                    }

                    dvSort.Sort = "Name ASC";
                    dgEndpoints.Columns[5].HeaderText = obj.GetTranslatedText("Room/Attendee")+"<br>"+obj.GetTranslatedText("Name")+RoomAttendee;
                    dgEndpoints.Columns[6].HeaderText = obj.GetTranslatedText("Endpoint")+"<br>"+obj.GetTranslatedText("Name");
                    dgEndpoints.Columns[11].HeaderText = obj.GetTranslatedText("MCU");

                    ViewState["SortAscending"] = true;

                    if (!dt.Columns.Contains("CascadeLinkId")) //FB 1650
                        dt.Columns.Add(new DataColumn("CascadeLinkId"));

                    if (!dt.Columns.Contains("EndPointStatus")) //FB 1650 - Endpoint status issue
                        dt.Columns.Add(new DataColumn("EndPointStatus"));

                    dgEndpoints.DataSource = dvSort;
                    //FB 2027 -Starts
                    try
                    {
                        dgEndpoints.DataBind();
                    }
                    catch
                    {
                        dgEndpoints.CurrentPageIndex = 0;
                        dgEndpoints.DataBind();
                    }
                    //FB 2027 - End

                    // FB 2441 Starts
                    if (!hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.P2P)) 
                    {
                        dt.DefaultView.RowFilter = "isMonitorDMA=0";
                        dgMuteALL.DataSource = dt.DefaultView;
                    }
                    dgMuteALL.DataBind();
                    // FB 2441 Ends

                    if (LblError.Text.Trim() != "")
                    {
                        LblError.Visible = true;
                    }


                    EptStatus();


                    tblP2PEndpoints.Visible = false;

                    if (hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.P2P))
                    {

                        foreach (DataRow dr in dt.Rows)
                        {
                            if (Convert.ToString(dr["connect2"]) == "1")
                                dr["connect2"] = obj.GetTranslatedText("Caller");
                            else if (Convert.ToString(dr["connect2"]) == "0")
                                dr["connect2"] = obj.GetTranslatedText("Callee");
                            else
                                dr["connect2"] = "";
                        }

                        //if (!dt.Columns.Contains("EndPointStatus")) //Blue Status Project
                        //    dt.Columns.Add(new DataColumn("EndPointStatus"));

                        if (!dt.Columns.Contains("remoteEndpoint")) //Blue Status Project
                            dt.Columns.Add(new DataColumn("remoteEndpoint"));

                        dgP2PEndpoints.DataSource = dt;
                        dgP2PEndpoints.DataBind();
                        tblP2PEndpoints.Visible = true;
                        tblEndpoints.Visible = false;

                        //Blue Point Status START
                        String remEp = "";
                        foreach (DataGridItem dgi in dgP2PEndpoints.Items)
                        {
                            if (dgi.Cells[12].Text.Trim() != "")
                                p2pStatus = dgi.Cells[12].Text.Trim();

                            if (dgi.Cells[13].Text.Trim() != "")
                                remEp = dgi.Cells[13].Text.Trim();

                            if (dgi.Cells[13].Text.Trim().Contains("&nbsp;"))
                                remEp = "";

                            if (dgi.ItemType.Equals(ListItemType.Item) || dgi.ItemType.Equals(ListItemType.AlternatingItem))
                            {
                                LinkButton btConnect = ((LinkButton)dgi.FindControl("btnConP2P"));
                                HyperLink lnkmsg = ((HyperLink)dgi.FindControl("btnMessage"));
                                lnkmsg.Visible = false; // ZD 101189
                                HyperLink lnkmonit = ((HyperLink)dgi.FindControl("EptMonitor"));
                                lnkmonit.Visible = false;

                                switch (p2pStatus)
                                {
                                    case ns_MyVRMNet.vrmEndPointConnectionSatus.disconnect:
                                        dgi.Cells[10].Text = obj.GetTranslatedText("Disconnected");
                                        dgi.Cells[10].BackColor = System.Drawing.Color.Red;
                                        dgi.Cells[10].Font.Bold = true;
                                        btConnect.Text = obj.GetTranslatedText("Connect");
                                        lnkmsg.Attributes.Add("style", "display:none");
                                        if (remEp != "")
                                        {
                                            dgi.Cells[10].Attributes.Add("onmouseover", "javascript:ShowEpID('" + dgi.Cells[10].ClientID + "','" + remEp + "');return false;");
                                            dgi.Cells[10].Attributes.Add("onmouseout", "javascript:ShowEpID('" + dgi.Cells[10].ClientID + "','Disconnected');return false;");
                                        }
                                        break;
                                    case ns_MyVRMNet.vrmEndPointConnectionSatus.Connecting:
                                        dgi.Cells[10].Text = obj.GetTranslatedText("Connecting");
                                        dgi.Cells[10].BackColor = System.Drawing.Color.Yellow;
                                        dgi.Cells[10].Font.Bold = true;
                                        if (remEp != "")
                                        {
                                            dgi.Cells[10].Attributes.Add("onmouseover", "javascript:ShowEpID('" + dgi.Cells[10].ClientID + "','" + remEp + "');return false;");
                                            dgi.Cells[10].Attributes.Add("onmouseout", "javascript:ShowEpID('" + dgi.Cells[10].ClientID + "','Connecting');return false;");

                                        }
                                        break;
                                    case ns_MyVRMNet.vrmEndPointConnectionSatus.Connected:
                                        dgi.Cells[10].Text = obj.GetTranslatedText("Connected");
                                        dgi.Cells[10].BackColor = System.Drawing.Color.LimeGreen;
                                        dgi.Cells[10].Font.Bold = true;
                                        if (remEp != "")
                                        {
                                            dgi.Cells[10].Attributes.Add("onmouseover", "javascript:ShowEpID('" + dgi.Cells[10].ClientID + "','" + remEp + "');return false;");
                                            dgi.Cells[10].Attributes.Add("onmouseout", "javascript:ShowEpID('" + dgi.Cells[10].ClientID + "','Connected');return false;");

                                        }
                                        btConnect.Text = obj.GetTranslatedText("Disconnect");
                                        break;
                                    case ns_MyVRMNet.vrmEndPointConnectionSatus.Online:
                                        dgi.Cells[10].Text = obj.GetTranslatedText("Online");
                                        dgi.Cells[10].BackColor = System.Drawing.Color.FromArgb(32, 108, 255);
                                        if (remEp != "")
                                        {
                                            dgi.Cells[10].Attributes.Add("onmouseover", "javascript:ShowEpID('" + dgi.Cells[10].ClientID + "','" + remEp + "');return false;");
                                            dgi.Cells[10].Attributes.Add("onmouseout", "javascript:ShowEpID('" + dgi.Cells[10].ClientID + "','Online');return false;");

                                        }
                                        dgi.Cells[10].Font.Bold = true;
                                        break;
                                    case "-1":
                                        dgi.Cells[10].Text = obj.GetTranslatedText("Unreachable.");
                                        btConnect.Attributes.Add("style", "display:none");
                                        lnkmsg.Attributes.Add("style", "display:none");
                                        break;
                                    default:
                                        dgi.Cells[10].Text = obj.GetTranslatedText("Status being updated.");
                                        btConnect.Attributes.Add("style", "display:none");
                                        lnkmsg.Attributes.Add("style", "display:none");
                                        break;
                                }
                            }
                        }
                        //Blue Point Status End


                        LinkButton1.Enabled = false;
                        LinkButton2.Attributes["OnClick"] = "return false;";
                        LinkButton2.ForeColor = System.Drawing.Color.Gray;
                        LinkButton2.Style.Add("cursor", "default");
                        LinkButton6.Attributes.Remove("OnClick"); //.Attributes["OnClick"] = "return false;";
                        LinkButton6.ForeColor = System.Drawing.Color.Gray;
                        LinkButton6.Style.Add("cursor", "default");
                        btnAddEndpoint.Enabled = false; // FB 2573
                    }

                    tblNoEndpoints.Visible = false;



                }
                else
                {
                    tblNoEndpoints.Visible = true;
                    tblEndpoints.Visible = false;
                    tblP2PEndpoints.Visible = false;//ZD 101579

                }


            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("LoadEndpoint" + ex.StackTrace);
                LblError.Text = obj.ShowSystemMessage();
                //LblError.Text = ex.StackTrace;
                LblError.Visible = true;
            }
        }

        #endregion

        #region connect status details


        private String ConnectStatusDetails(String strEndPointID1, String strType1)
        {
            String strConStatus = "";
            String inXML = "";
            String outXMLNew = "";
            inXML += "<GetTerminalStatus>";
            inXML += "  <login>";
            inXML += "      <userID>" + Session["userID"].ToString() + "</userID>";
            inXML += "      <confID>" + lblConfID.Text + "</confID>";
            inXML += "      <endpointID>" + strEndPointID1 + "</endpointID>";
            inXML += "      <terminalType>" + strType1 + "</terminalType>";
            inXML += "  </login>";
            inXML += "</GetTerminalStatus>";
            outXMLNew = obj.CallCOM2("GetTerminalStatus", inXML, Application["RTC_ConfigPath"].ToString());

            if (outXMLNew.IndexOf("<error>") >= 0)
            {
                strConStatus = "-1";
            }
            else
            {
                Session.Add("confid", lblConfID.Text);
                XmlDocument xmldoc2 = new XmlDocument();
                xmldoc2.LoadXml(outXMLNew);
                strConStatus = xmldoc2.SelectSingleNode("//GetTerminalStatus/connectionStatus").InnerText;
            }
            return strConStatus;

        }

        #endregion

        #region getuploaded files
        protected string getUploadFilePath(string fpn)
        {
            string fPath = String.Empty;
            if (fpn.Equals(""))
                fPath = "";
            else
            {
                char[] splitter = { '\\' };
                string[] fa = fpn.Split(splitter[0]);
                if (fa.Length.Equals(0))
                    fPath = "";
                else
                    fPath = fa[fa.Length - 1];
            }
            return fPath;
        }

        #endregion

        #region Change layout

        protected void ChangeVideoDisplay(Object sender, EventArgs e)
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                if (hdnLayout.Value.Equals("5")) // Change ALl layout
                {
                    inXML += "  <confID>" + lblConfID.Text + "</confID>";
                    inXML += "  <endpointID>0</endpointID>";
                    inXML += "  <terminalType>0</terminalType>";
                    inXML += "  <displayLayout>" + txtSelectedImage.Text + "</displayLayout>";
                    inXML += "  <displayLayoutAll>1</displayLayoutAll>";//FB 3073
                    //inXML += "  <displayLayoutAll>3</displayLayoutAll>";//FB 2530
                }
                else if (hdnLayout.Value.Equals("6")) // Endpoint Layout
                {
                    inXML += "  <confID>" + lblConfID.Text + "</confID>";
                    inXML += "  <endpointID>" + txtEndpointType.Text.Split(',')[1] + "</endpointID>";
                    inXML += "  <terminalType>" + txtEndpointType.Text.Split(',')[0] + "</terminalType>";
                    inXML += "  <displayLayout>" + txtSelectedImageEP.Text + "</displayLayout>";
                    inXML += "  <displayLayoutAll>2</displayLayoutAll>";//FB 2530
                }
                inXML += "</login>";
                String outXML = obj.CallCOM2("DisplayTerminal", inXML, Application["RTC_ConfigPath"].ToString());
                LblError.Visible = true;
                if (outXML.IndexOf("<error>") >= 0)
                {
                    if (hdnLayout.Value.Equals("5"))
                    {
                        txtSelectedImage.Text = txtTempImage.Text;
                    }
                    if (hdnLayout.Value.Equals("6"))
                    {
                        txtSelectedImageEP.Text = txtTempImage.Text;
                    }
                    LblError.Text = obj.ShowErrorMessage(outXML);
                }
                else
                {
                    //outXML = obj.CallCOM("DisplayTerminal", inXML, Application["COM_ConfigPath"].ToString());
                    outXML = obj.CallMyVRMServer("DisplayTerminal", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027
                    Session.Add("confid", lblConfID.Text);
                    DisplayConferenceDetails();
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("ChangeVideoDisplay" + ex.StackTrace);
                LblError.Text = obj.ShowSystemMessage();
                //LblError.Text = ex.StackTrace;
                LblError.Visible = true;
            }
        }
        protected void ExtendEndtime(Object sender, EventArgs e)
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <confInfo>";
                inXML += "      <confID>" + lblConfID.Text + "</confID>";
                inXML += "      <retry>0</retry>";
                inXML += "      <extendEndTime>" + txtExtendedTime.Text + "</extendEndTime>";
                inXML += "  </confInfo>";
                inXML += "</login>";
                log.Trace(txtExtendedTime.Text);
                //FB 2528 - Starts
                //ZD 100819 Start
                String outXML = obj.CallMyVRMServer("CheckForExtendTimeConflicts", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                LblError.Visible = true;
                if (outXML.IndexOf("<error>") >= 0)
                    LblError.Text = obj.ShowErrorMessage(outXML);
                else
                {
                    outXML = obj.CallCOM2("SetTerminalControl", inXML, Application["RTC_ConfigPath"].ToString());
                    //FB 2528 - End
                    LblError.Visible = true;
                    if (outXML.IndexOf("<error>") >= 0)
                        LblError.Text = obj.ShowErrorMessage(outXML);
                    else
                    {
                        //FB 2528 - Starts
                        //outXML = obj.CallCOM2("SetTerminalControl", inXML, Application["RTC_ConfigPath"].ToString()); //Fb 1521   
                        outXML = obj.CallMyVRMServer("SetTerminalControl", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                        //FB 2528 - End

                        Session.Add("confid", lblConfID.Text);
                        DisplayConferenceDetails();

                    }
                }
                //ZD 100819 End
                txtExtendedTime.Text = "";

            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("ExtendEndTime" + ex.StackTrace);
                LblError.Text = obj.ShowSystemMessage();
                //LblError.Text = ex.StackTrace;
                LblError.Visible = true;
            }
        }

        #endregion

        #region Delete Conference


        protected void DeleteConference(Object sender, EventArgs e)
        {
            try
            {
                log.Trace("In delete conf");

                string inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><delconference><conference><confID>" + lblConfID.Text + "</confID><reason></reason></conference></delconference>";
                //ZD 102901 Start
                if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "YES")
                    inXML += "<isExchange>1</isExchange>";
                else if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "MOD")
                    inXML += "<isExchange>2</isExchange>";
                else
                    inXML += "<isExchange>0</isExchange>";
                //ZD 102901 End
                inXML += "</login>";
                
                string outXML = String.Empty;
                //Response.Write(obj.Transfer(inXML));
                if ((!hdnConfStatus.Value.Equals(ns_MyVRMNet.vrmConfStatus.OnMCU) && !hdnConfStatus.Value.Equals(ns_MyVRMNet.vrmConfStatus.Ongoing)) || (hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.RoomOnly))) //FB Case 909 and FB 1088
                    outXML = obj.CallMyVRMServer("DeleteConference", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                else
                    outXML = "<success>1</success>";
                if (outXML.IndexOf("<error>") >= 0)
                {
                    LblError.Visible = true;
                    LblError.Text = obj.ShowErrorMessage(outXML);
                }
                else
                {

                    //ZD 102973 Starts
                    if (Application["External"].ToString() != "")
                    {
                        String inEXML = "";
                        inEXML = "<SetExternalScheduling>";
                        inEXML += "<confID>" + lblConfID.Text + "</confID>";
                        inEXML += "</SetExternalScheduling>";

                        String outExml = obj.CallCommand("SetExternalScheduling", inEXML);
                    }
                    //ZD 103095
                    if ((!hdnConfStatus.Value.Equals(ns_MyVRMNet.vrmConfStatus.OnMCU) && !hdnConfStatus.Value.Equals(ns_MyVRMNet.vrmConfStatus.Ongoing) && !hdnConfStatus.Value.Equals(ns_MyVRMNet.vrmConfStatus.WaitList)) || (hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.RoomOnly)))
                    {
                        String inxml = "<DeleteCiscoICAL>" + obj.OrgXMLElement();
                        inxml += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                        inxml += "<ConfID>" + lblConfID.Text + "</ConfID>";
                        inxml += "</DeleteCiscoICAL>";

                        obj.CallCommand("DeleteCiscoICAL", inxml);

                        inxml = "<DeleteParticipantICAL>";
                        inxml += obj.OrgXMLElement();
                        inxml += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                        inxml += "<ConfID>" + lblConfID.Text + "</ConfID>";
                        if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "YES")
                            inxml += "<isExchange>1</isExchange>";
                        else if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "MOD")
                            inxml += "<isExchange>2</isExchange>";
                        else
                            inxml += "<isExchange>0</isExchange>";
                        inxml += "</DeleteParticipantICAL>";
                        obj.CallCommand("DeleteParticipantICAL", inxml);
                    }
                    //ZD 102973 Ends

                    if (Session["foodModule"].ToString().Equals("1") || Session["roomModule"].ToString().Equals("1") || Session["hkModule"].ToString().Equals("1"))
                    {
                        inXML = "";
                        inXML += "<login>";
                        inXML += obj.OrgXMLElement();//Organization Module Fixes
                        inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                        inXML += "  <ConferenceID>" + lblConfID.Text + "</ConferenceID>";
                        inXML += "  <WorkorderID>0</WorkorderID>";
                        inXML += "</login>";
                        //Response.Write(obj.Transfer(inXML));
                        outXML = obj.CallMyVRMServer("DeleteWorkOrder", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    }
                    if ((hdnConfStatus.Value.Equals(ns_MyVRMNet.vrmConfStatus.OnMCU) || hdnConfStatus.Value.Equals(ns_MyVRMNet.vrmConfStatus.Ongoing)) && (!hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.RoomOnly))) //FB Case 909
                    {
                        log.Trace("in if 1");
                        LblError.Visible = true;
                        LblError.Text = obj.GetTranslatedText("Operation Successful!");
                        inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><conferenceID>" + lblConfID.Text + "</conferenceID> ";
                        //ZD 102901 Start
                        if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "YES")
                            inXML += "<isExchange>1</isExchange>";
                        else if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "MOD")
                            inXML += "<isExchange>2</isExchange>";
                        else
                            inXML += "<isExchange>0</isExchange>";
                        //ZD 102901 End
                        
                        //ZD 102973 Satrts
                        if (hdnConfStatus.Value.Equals(ns_MyVRMNet.vrmConfStatus.OnMCU))
                            inXML += "<FromService>0</FromService>"; //RPRM
                        else
                            inXML += "<FromService>1</FromService>"; //DMA
                        //ZD 102973 Ends

                        inXML +="</login>";//Organization Module Fixes
                        outXML = obj.CallCOM2("TerminateConference", inXML, Application["RTC_ConfigPath"].ToString());
                        if (outXML.IndexOf("<error>") >= 0)
                        {
                            LblError.Visible = true;
                            LblError.Text = obj.ShowErrorMessage(outXML);
                            tblForceTerminate.Visible = true;
                        }
                        else
                        {
                            outXML = obj.CallMyVRMServer("TerminateConference", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 202
                            //ALLDEV-830 start
                            String inxml = "<DeleteCiscoICAL>" + obj.OrgXMLElement();
                            inxml += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                            inxml += "<ConfID>" + lblConfID.Text + "</ConfID>";
                            inxml += "</DeleteCiscoICAL>";

                            obj.CallCommand("DeleteCiscoICAL", inxml);


                            inxml = "<DeleteParticipantICAL>";
                            inxml += obj.OrgXMLElement();
                            inxml += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                            inxml += "<ConfID>" + lblConfID.Text + "</ConfID>";
                            if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "YES")
                                inxml += "<isExchange>1</isExchange>";
                            else if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "MOD")
                                inxml += "<isExchange>2</isExchange>";
                            else
                                inxml += "<isExchange>0</isExchange>";
                            inxml += "</DeleteParticipantICAL>";
                            obj.CallCommand("DeleteParticipantICAL", inxml);
                            //ALLDEV-830 End
                            Response.Redirect("Dashboard.aspx?m=1&t=2", true);
                        }
                    }
                    else
                    {
                        log.Trace("In else main");
                        if (Session["errMsg"] != null) //FB Case 943
                        {
                            Session["errMsg"] = "";
                            Session["errMsg"] = null;
                        }
                        log.Trace(hdnConfStatus.Value.Trim() + " : " + ns_MyVRMNet.vrmConfStatus.Ongoing);
                        if (hdnConfStatus.Value.Trim().Equals(ns_MyVRMNet.vrmConfStatus.Ongoing))
                        {
                            Response.Redirect("ConferenceList.aspx?m=1&t=2", true);
                        }
                        else
                        {
                            Response.Redirect("Dashboard.aspx?m=1&listValue=" + DrpDwnListView.SelectedValue, true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("Delete : " + ex.Message);
                LblError.Text = obj.ShowSystemMessage();
                LblError.Visible = true;
                //LblError.Text = "Delete: " + ex.StackTrace;
            }
        }

        protected void ForceTerminate(Object sender, EventArgs e)
        {
            try
            {
                String inXML = "<ForceConfDelete>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <confID>" + lblConfID.Text + "</confID>";
                //ZD 101872 Start
                if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "YES")
                    inXML += "<isExchange>1</isExchange>";
                else if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "MOD")
                    inXML += "<isExchange>2</isExchange>";
                else
                    inXML += "<isExchange>0</isExchange>";
                //ZD 101872 End
                inXML += "</ForceConfDelete>";
                String outXML = obj.CallMyVRMServer("ForceConfDelete", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //ZD 100152
                //ALLDEV-830 start
                String inxml = "<DeleteCiscoICAL>" + obj.OrgXMLElement();
                inxml += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                inxml += "<ConfID>" + lblConfID.Text + "</ConfID>";
                inxml += "</DeleteCiscoICAL>";

                obj.CallCommand("DeleteCiscoICAL", inxml);


                inxml = "<DeleteParticipantICAL>";
                inxml += obj.OrgXMLElement();
                inxml += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                inxml += "<ConfID>" + lblConfID.Text + "</ConfID>";
                if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "YES")
                    inxml += "<isExchange>1</isExchange>";
                else if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "MOD")
                    inxml += "<isExchange>2</isExchange>";
                else
                    inxml += "<isExchange>0</isExchange>";
                inxml += "</DeleteParticipantICAL>";
                obj.CallCommand("DeleteParticipantICAL", inxml);
                //ALLDEV-830 End
                if (outXML.IndexOf("<error>") >= 0)
                {
                    LblError.Visible = true;
                    LblError.Text = obj.ShowErrorMessage(outXML);
                }
                else
                {
                    Response.Redirect("ConferenceList.aspx?t=2&m=1");
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("ForceDelete" + ex.StackTrace);
                LblError.Text = obj.ShowSystemMessage();
                LblError.Visible = true;
                //LblError.Text = "Delete: " + ex.StackTrace;
            }

        }

        protected void HideForceTerminate(Object sender, EventArgs e)
        {
            try
            {
                tblForceTerminate.Visible = false;

            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("HideForceTerminate" + ex.StackTrace);
                LblError.Text = obj.ShowSystemMessage();
                LblError.Visible = true;
                //LblError.Text = "Delete: " + ex.StackTrace;
            }

        }

        #endregion

        #region Sort Order

        string SortField
        {

            get
            {
                object o = ViewState["SortField"];
                if (o == null)
                {
                    return String.Empty;
                }
                return (string)o;
            }

            set
            {
                if (value == SortField)
                {
                    SortAscending = !SortAscending;
                }
                ViewState["SortField"] = value;
            }
        }
        bool SortAscending
        {

            get
            {
                object o = ViewState["SortAscending"];
                if (o == null)
                {
                    return true;
                }
                return (bool)o;
            }

            set
            {
                ViewState["SortAscending"] = value;
            }
        }
        protected void SortGrid(Object src, DataGridSortCommandEventArgs e)
        {
            String arrwimg = "";
            try
            {

                dgEndpoints.CurrentPageIndex = 0;
                SortField = e.SortExpression;


                DataView dv = null;
                dv = new DataView((DataTable)Session["DtEptList"]);

                DataTable dt = dv.Table;


                dv.Sort = SortField;
                string RoomAttendee = "";




                if (!SortAscending)
                {
                    dv.Sort += " DESC";
                    //Edited For FF..
                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("TRIDENT")) // ZD 102431
                    {
                        arrwimg = "<span style='font-family:Wingdings; font-weight:bold;'>&#234;</span>";
                    }
                    else
                    {
                        arrwimg = "<span  style='font-family:Wingdings; font-weight:bolder; font-size:x-large;'>&#8595;</span>";
                    }
                }
                else
                {
                    dv.Sort += " ASC";
                    //Edited For FF..
                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("TRIDENT")) // ZD 102431
                    {
                        arrwimg = "<span style='font-family:Wingdings; font-weight:bold;'>&#233;</span>"; // ZD 102431
                    }
                    else
                    {
                        arrwimg = "<span  style='font-family:Wingdings; font-weight:bolder; font-size:x-large;'>&#8593;</span>";
                    }
                }

                if (SortField == "Name")
                {
                    dgEndpoints.Columns[5].HeaderText = obj.GetTranslatedText("Room/Attendee") + "<br>" + obj.GetTranslatedText("Name") + arrwimg;
                    dgEndpoints.Columns[6].HeaderText = obj.GetTranslatedText("Endpoint") + "<br>" + obj.GetTranslatedText("Name");
                    dgEndpoints.Columns[11].HeaderText = "MCU";
                }
                else if (SortField == "EndpointName")
                {
                    dgEndpoints.Columns[6].HeaderText = obj.GetTranslatedText("Endpoint") + "<br>" + obj.GetTranslatedText("Name") + arrwimg;
                    dgEndpoints.Columns[5].HeaderText = obj.GetTranslatedText("Room/Attendee") + "<br>" + obj.GetTranslatedText("Name");
                    dgEndpoints.Columns[11].HeaderText = "MCU";
                }
                else
                {
                    dgEndpoints.Columns[11].HeaderText = "MCU " + arrwimg;
                    dgEndpoints.Columns[5].HeaderText = obj.GetTranslatedText("Room/Attendee") + "<br>" + obj.GetTranslatedText("Name");
                    dgEndpoints.Columns[6].HeaderText = obj.GetTranslatedText("Endpoint") + "<br>" + obj.GetTranslatedText("Name");
                }

                dgEndpoints.DataSource = dv;
                dgEndpoints.DataBind();
                EptStatus();
            }
            catch (Exception ex)
            { }
        }

        #endregion

        #region  get video layouts


        protected void GetVideoLayouts()
        {
            try
            {
                ImagesPath.Attributes.Add("style", "display:none");
                txtSelectedImage.Attributes.Add("style", "display:none");
                ImageFiles.Attributes.Add("style", "display:none");
                ImageFilesBT.Attributes.Add("style", "display:none");
                ImagesPath.Text = "image/displaylayout/";
                if (Application["Client"].ToString().ToUpper().Equals("BTBOCES"))
                    ImagesPath.Text += "BTBoces/";
                ImageFiles.Text = "";
                //Response.Write(Server.MapPath(ImagesPath.Text));
                foreach (string file in Directory.GetFiles(Server.MapPath(ImagesPath.Text)))
                    ImageFiles.Text += file.Replace(Server.MapPath(ImagesPath.Text), "").Replace(".gif", "") + ":";
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " ||| " + ex.Message);
            }
        }

        #endregion

        #region Edit Conference
        protected void EditConference(Object src, EventArgs e)
        {
            //ZD 101233 START
            int isExpress = 0, isAdvancedForm = 0, isAdmin = 0; //ZD 102200
            bool isExpressUser = false; //ZD 101233

            //ZD 102200 Starts
            if (Session["hasConference"] != null)
                int.TryParse(Session["hasConference"].ToString(), out isAdvancedForm);
            if (Session["hasExpConference"] != null)
                int.TryParse(Session["hasExpConference"].ToString(), out isExpress);
            if (Session["admin"] != null)
                int.TryParse(Session["admin"].ToString(), out isAdmin);
            //ZD 102200 Ends

            if (Session["isExpressUser"] != null && Session["isExpressUserAdv"] != null && Session["isExpressManage"] != null)
            {
                if (Session["isExpressUser"].ToString() == "1" || Session["isExpressUserAdv"].ToString() == "1" || Session["isExpressManage"].ToString() == "1")
                    isExpressUser = true;
            }
            //ZD 101233 END

            if (src.GetType().Name == "LinkButton" && ((LinkButton)src).Text.IndexOf("Delete &") >= 0)//FB 1133
            {
                DeleteConference(src, e);//FB 1133
            }
            else if ((src.GetType().Name == "LinkButton" && ((LinkButton)src).Text.IndexOf("Edit") >= 0) 
                || ( src.GetType().Name == "Button" &&  ((Button)src).ID.IndexOf("Temp") >= 0))//FB 1133 //ZD 101597
            {
                Session.Add("ConfID", lblConfID.Text);
               
                //ZD 101597 //ZD 102200 Starts
                if (isAdvancedForm == 1 && isExpress == 1)
                {
                    if (isAdmin > 0)
                    {
                        if (hdnEditForm.Value == "1")
                            Response.Redirect("ConferenceSetup.aspx?t=");
                        else if (hdnEditForm.Value == "2")
                            Response.Redirect("ExpressConference.aspx?t=");
                    }
                    else
                    {
                        if (hdnExpressConf.Value == "0" && !isExpressUser)
                            Response.Redirect("ConferenceSetup.aspx?t=");
                        else
                            Response.Redirect("ExpressConference.aspx?t=");
                    }
                }
                else if (isAdvancedForm == 1)
                    Response.Redirect("ConferenceSetup.aspx?t=");
                else if (isExpress == 1)
                    Response.Redirect("ExpressConference.aspx?t=");
                //ZD 101233 END //ZD 102200 Ends
            }
        }

        #endregion

        #region Check Connection

        private void chkConnection(String confid)
        {
            String inXML = "";
            String inMsg = "";
            String outXML = "";
            String sts = "";
            try
            {
                inXML = "<login><userID>11</userID><confID>" + confid + "</confID></login>";
                //outXML = obj.CallCOM("GetTerminalControl", inXML, Application["COM_ConfigPath"].ToString());
                outXML = obj.CallMyVRMServer("GetTerminalControl", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027(GetTerminalControl)
                XmlDocument xmldocEP = new XmlDocument();
                xmldocEP.LoadXml(outXML);
                XmlNodeList nodesEP = xmldocEP.SelectNodes("//terminalControl/confInfo/terminals/terminal");
                Int32 cnt = nodesEP.Count;
                Int32 cntsts = 0;
                foreach (XmlNode nodeEP in nodesEP)
                {
                    if (nodeEP.SelectSingleNode("status") != null)
                    {
                        sts = nodeEP.SelectSingleNode("status").InnerText;

                        //if (sts == "1")//Blue Status
                        if (sts == "2")//Blue Status
                        {
                            cntsts++;
                            prtlstats = true;
                        }
                        else if (sts == "3") //Blue Status
                        {
                            bluStatus = true;
                        }

                    }
                }
                // FB 1679 Added  START
                cntstats = false;
                if (cnt == cntsts)
                {
                    if (cnt > 0)
                        cntstats = true;
                }
                // FB 1679 Added  END
            }
            catch (Exception ex)
            { }
        }

        #endregion

        #region Pagination of endpoint table
        public void paging(object sender, DataGridPageChangedEventArgs e)
        {
            dgEndpoints.CurrentPageIndex = e.NewPageIndex;
            DataView dv = null;
            dv = new DataView((DataTable)Session["DtEptList"]);
            DataTable dt = dv.Table;
            dgEndpoints.DataSource = dv;
            dgEndpoints.DataBind();
            LoadEndpoints();//FB 2071
        }
        #endregion

        #region EPT status

        public void EptStatus()
        {
            bool isMonitorDMA = false; // FB 2441
            bool isEndpointConnected = false;
            int RPRMMCU = 0;
            try
            {
                CascadeLink = "";//FB 2528
                foreach (DataGridItem dgi in dgEndpoints.Items)
                {

                    if (Request.QueryString["hf"] != null)
                    {
                        if (Request.QueryString["hf"].ToString().Equals("1"))
                        {

                            LinkButton btnMute = ((LinkButton)dgi.FindControl("btnMute"));
                            LinkButton btnEdit = ((LinkButton)dgi.FindControl("btnEdit"));
                            LinkButton btnDelete = ((LinkButton)dgi.FindControl("btnDelete"));

                            btnMute.Visible = false;
                            btnEdit.Visible = false;
                            btnDelete.Visible = false;
                        }
                    }
                    //FB 2528 Starts
                    CascadeLink = dgi.Cells[1].Text;
                    if (CascadeLink != null)
                    {
                        if (CascadeLink == "4")
                        {
                            LinkButton btnMute = ((LinkButton)dgi.FindControl("btnMute"));
                            LinkButton btnEdit = ((LinkButton)dgi.FindControl("btnEdit"));
                            LinkButton btnDelete = ((LinkButton)dgi.FindControl("btnDelete"));
                            LinkButton btnTempCon = ((LinkButton)dgi.FindControl("btnCon"));
                            LinkButton btnChangeLayout = (LinkButton)dgi.FindControl("btnChangeLayout");

                            if (btnMute != null)
                            {
                                btnMute.Enabled = false;
                                btnMute.ForeColor = System.Drawing.Color.Gray;
                                btnMute.Attributes.Remove("onclick");
                            }
                            if (btnEdit != null)
                            {
                                btnEdit.Enabled = false;
                                btnEdit.ForeColor = System.Drawing.Color.Gray;
                            }
                            if (btnDelete != null)
                            {
                                btnDelete.Enabled = false;
                                btnDelete.ForeColor = System.Drawing.Color.Gray;
                                btnDelete.Attributes.Remove("onclick");
                            }
                            if (btnTempCon != null)
                            {
                                btnTempCon.Enabled = false;
                                btnTempCon.ForeColor = System.Drawing.Color.Gray;
                            }
                            if (btnChangeLayout != null)
                            {
                                btnChangeLayout.Enabled = false;
                                btnChangeLayout.ForeColor = System.Drawing.Color.Gray;
                                btnChangeLayout.Attributes.Remove("onclick");
                                btnChangeLayout.OnClientClick = null; //FB 2441
                            }
                        }
                    }
                    //FB 2528 Ends

                    
                    //FB 2441 Starts
                    if (dgi.Cells[20].Text.Trim() == "1")//FB 2839
                        isMonitorDMA = true;
                    if (ViewState["hasManage"] != null && ViewState["hasManage"].ToString() == "0") //ZD 101388
                    {
                        LinkButton btnMute = ((LinkButton)dgi.FindControl("btnMute"));
                        LinkButton btnEdit = ((LinkButton)dgi.FindControl("btnEdit"));
                        LinkButton btnDelete = ((LinkButton)dgi.FindControl("btnDelete"));
                        LinkButton btnTempCon = ((LinkButton)dgi.FindControl("btnCon"));
                        LinkButton btnLayout = (LinkButton)dgi.FindControl("btnChangeLayout");//FB 2441
                        if (btnMute != null)
                        {
                            btnMute.Enabled = false;
                            btnMute.ForeColor = System.Drawing.Color.Gray;
                            btnMute.Attributes["OnClick"] = "return false;";// FB 2441
                        }
                        if (btnEdit != null)
                        {
                            btnEdit.Enabled = false;
                            btnEdit.ForeColor = System.Drawing.Color.Gray;
                            btnEdit.Attributes["OnClick"] = "return false;";// FB 2441
                        }
                        if (btnDelete != null)
                        {
                            btnDelete.Enabled = false;
                            btnDelete.ForeColor = System.Drawing.Color.Gray;
                            btnDelete.Attributes["OnClick"] = "return false;"; //FB 2441
                        }
                        if (btnTempCon != null)
                        {
                            btnTempCon.Enabled = false;
                            btnTempCon.ForeColor = System.Drawing.Color.Gray;
                        }
						//FB 2441 II Starts
						if (btnLayout != null)
                        {
                            btnLayout.Enabled = false;
                            btnLayout.ForeColor = System.Drawing.Color.Gray;
                            btnLayout.Attributes.Remove("onclick");
                            btnLayout.OnClientClick = null;
                            btnLayout.Style.Add("cursor", "default");
                        }
                        //LinkButton3.Attributes["OnClick"] = "return false;";
                        LinkButton3.Attributes.Remove("OnClick");
                        LinkButton3.ForeColor = System.Drawing.Color.Gray;
                        LinkButton3.Style.Add("cursor", "default");
                        LinkButton3.OnClientClick = null;
                        LinkButton3.Enabled = false;

                        txtExtendedTime.Enabled = false;
                        btnExtendEndtime.Enabled = false;

                        LinkButton2.Attributes["OnClick"] = "return false;";
                        LinkButton2.ForeColor = System.Drawing.Color.Gray;
                        LinkButton2.Enabled = false;
                        LinkButton2.Style.Add("cursor", "default");

                        LinkButton1.Attributes["OnClick"] = "return false;";
                        LinkButton1.ForeColor = System.Drawing.Color.Gray;
                        LinkButton1.Enabled = false;
                        LinkButton1.Style.Add("cursor", "default");


                        LinkButton6.Enabled = false;
                        LinkButton6.Attributes["OnClick"] = "return false;";
                        LinkButton6.ForeColor = System.Drawing.Color.Gray;
                        LinkButton6.Style.Add("cursor", "default");

                        lnkMuteAllExcept.Attributes["OnClick"] = "return false;";
                        lnkMuteAllExcept.OnClientClick = null;
                        lnkMuteAllExcept.ForeColor = System.Drawing.Color.Gray;
                        lnkMuteAllExcept.Enabled = false;
                        lnkMuteAllExcept.Style.Add("cursor", "default");

                        lnkUnMuteAllParties.Attributes["OnClick"] = "return false;";
                        lnkUnMuteAllParties.ForeColor = System.Drawing.Color.Gray;
                        lnkUnMuteAllParties.Enabled = false;
                        lnkUnMuteAllParties.Style.Add("cursor", "default");

                        btnAddEndpoint.Enabled = false;
                        btnAddEndpoint.ForeColor = System.Drawing.Color.Gray;
                        lnkUnMuteAllParties.Attributes["OnClick"] = "return false;";
						//FB 2441 II Ends
                    }
                    
                    //FB 2441 Ends

                    ////ZD 102997 Starts
                    if (isMonitorDMA)
                    {
                        LinkButton btnLayout = (LinkButton)dgi.FindControl("btnChangeLayout");//FB 2441
                        LinkButton btnEdit = ((LinkButton)dgi.FindControl("btnEdit"));
                        LinkButton3.Attributes.Remove("OnClick");
                        LinkButton3.ForeColor = System.Drawing.Color.Gray;
                        LinkButton3.Style.Add("cursor", "default");
                        LinkButton3.OnClientClick = null;
                        LinkButton3.Enabled = false;

                        txtExtendedTime.Enabled = false;
                        btnExtendEndtime.Enabled = false;

                        if (btnLayout != null)
                        {
                            btnLayout.Enabled = false;
                            btnLayout.ForeColor = System.Drawing.Color.Gray;
                            btnLayout.Attributes.Remove("onclick");
                            btnLayout.OnClientClick = null;
                            btnLayout.Style.Add("cursor", "default");
                        }
                        if (btnEdit != null)
                        {
                            btnEdit.Enabled = false;
                            btnEdit.ForeColor = System.Drawing.Color.Gray;
                            btnEdit.Attributes["OnClick"] = "return false;";// FB 2441
                        }
                    }
                    ////ZD 102997 Ends
                    if (!hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.P2P))
                    {
                        String strConnectionStatus = dgi.Cells[20].Text.Trim(); //FB 1650 Endpoint status issue//FB 2839 //ZD 104256
                        //strConnectionStatus = ConnectStatusDetails(dgi.Cells[0].Text, dgi.Cells[1].Text); //Commented during FB 1650
                        if (dgi.ItemType.Equals(ListItemType.Item) || dgi.ItemType.Equals(ListItemType.AlternatingItem))
                        {
                            LinkButton btnTempCon = ((LinkButton)dgi.FindControl("btnCon"));
                            if (strConnectionStatus.Equals(ns_MyVRMNet.vrmEndPointConnectionSatus.Connected))
                                btnTempCon.Text = obj.GetTranslatedText("Disconnect");
                        }
                        switch (strConnectionStatus)
                        {
                            case ns_MyVRMNet.vrmEndPointConnectionSatus.disconnect:
                                dgi.Cells[7].Text = obj.GetTranslatedText("Disconnected");
                                dgi.Cells[7].BackColor = System.Drawing.Color.Red;
                                dgi.Cells[7].Font.Bold = true;
                                break;
                            case ns_MyVRMNet.vrmEndPointConnectionSatus.Connecting:
                                dgi.Cells[7].Text = obj.GetTranslatedText("Connecting");
                                dgi.Cells[7].BackColor = System.Drawing.Color.Yellow;
                                dgi.Cells[7].Font.Bold = true;
                                break;
                            case ns_MyVRMNet.vrmEndPointConnectionSatus.Connected:
                                dgi.Cells[7].Text = obj.GetTranslatedText("Connected");
                                dgi.Cells[7].BackColor = System.Drawing.Color.LimeGreen; //code changed for FB 1371
                                dgi.Cells[7].Font.Bold = true;
                                break;
                            case "-1":
                                dgi.Cells[7].Text = obj.GetTranslatedText("N/A");
                                //dgi.Cells[7].BackColor = System.Drawing.Color.LightBlue;
                                break;
                            default:
                                dgi.Cells[7].Text = "";
                                break;
                        }
                    }
                }

                if (hdnConfStatus.Value.Equals(ns_MyVRMNet.vrmConfStatus.Ongoing))
                {
                    //Response.Write("in if");
                    dgEndpoints.Columns[6].Visible = true;
                    dgEndpoints.Columns[9].Visible = true;
                    dgEndpoints.Columns[7].Visible = true;
                    String strConnectionStatus = "";

                    foreach (DataGridItem dgi in dgEndpoints.Items)
                    {
                        strConnectionStatus = dgi.Cells[20].Text.Trim();//FB 2581 //FB 2839 //ZD 104256
                        if (dgi.Cells[20].Text.Trim() == "13")
                            RPRMMCU = 13;
                        if (dgi.ItemType.Equals(ListItemType.Item) || dgi.ItemType.Equals(ListItemType.AlternatingItem))
                        {
                            LinkButton btnTemp = ((LinkButton)dgi.FindControl("btnMute"));
                            if (btnTemp != null && strConnectionStatus == ns_MyVRMNet.vrmEndPointConnectionSatus.Connected)//FB 2581
                            {
                                btnTemp.Visible = true;
                                if (!isEndpointConnected)
                                    isEndpointConnected = true;
                            }

                            btnTemp = (LinkButton)dgi.FindControl("btnChangeLayout");
                            //ZD 101931 start
                            if (Session["ShowVideoLayout"] != null && !string.IsNullOrEmpty(Session["ShowVideoLayout"].ToString()) && (Session["ShowVideoLayout"].ToString() == "1" || Session["ShowVideoLayout"].ToString() == "0"))
                            {
                                if (btnTemp != null &&  strConnectionStatus == ns_MyVRMNet.vrmEndPointConnectionSatus.Connected)//FB 2581
                                  btnTemp.Visible = true;
                            }
                            else
                                btnTemp.Visible = false;
                            //ZD 101931 End
						// FB 2441 II Starts
                        }

                        if (RPRMMCU == 13)
                        {
                            LinkButton btnTempCon = ((LinkButton)dgi.FindControl("btnCon"));
                            LinkButton btnEdit = ((LinkButton)dgi.FindControl("btnEdit"));
                            
                            btnTempCon.Visible = false;
                            btnEdit.Visible = false;
                        }
						// FB 2441 II Ends
                    }
                    //FB 2441
                    if (isEndpointConnected && RPRMMCU == 13)//Link button is visible only for RPRM Mcu Type and any endpoint is connected.
                    {
                        lnkMuteAllExcept.Visible = true;
                        lnkUnMuteAllParties.Visible = true;
                    }
                    //GetConferenceAlerts();
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("EptStatus" + ex.Message);
                LblError.Text = obj.ShowSystemMessage();
                //LblError.Text = ex.Message; //FB 1650
                LblError.Visible = true;
            }
        }

        #endregion

        #region Setup on MCU

        protected void btnSetupAtMCU_Click(object sender, EventArgs e)
        {
            try
            {
                string inXML = String.Empty;
                inXML = "<Conference><confID>" + lblConfID.Text + "</confID>"+ obj.OrgXMLElement() +"</Conference>"; //ZD 101657
                string outXML = String.Empty;
                //ZD 101657 Starts
                if ((!hdnConfStatus.Value.Equals(ns_MyVRMNet.vrmConfStatus.OnMCU) && !hdnConfStatus.Value.Equals(ns_MyVRMNet.vrmConfStatus.Ongoing)) && (hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.AudioVideo)) || (hdnConfType.Value.Equals(ns_MyVRMNet.vrmConfType.OBTP)))
                    outXML = obj.CallMyVRMServer("CheckDialNumber", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                else
                    outXML = "<success>1</success>";

                if (outXML.IndexOf("<error>") >= 0)
                {
                    LblError.Visible = true;
                    LblError.Text = obj.ShowErrorMessage(outXML);
                    return;
                }
                //ZD 101657 Ends

                outXML = obj.CallCOM2("SetConferenceOnMcu", inXML, Application["RTC_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    LblError.Visible = true;
                    LblError.Text = obj.ShowErrorMessage(outXML);
                }
                else
                {
                    LblError.Text = obj.GetTranslatedText("Operation Successful!");
                    LblError.Visible = true;
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("Submit MCU" + ex.StackTrace);
                LblError.Text = obj.ShowSystemMessage();
                LblError.Visible = true;
                //LblError.Text = "Submit: " + ex.StackTrace;
            }
        }

        #endregion

        #region Codes for P2P

        protected String GetP2pStatus()
        {
            String stus = "";
            XmlDocument doc = null;
            try
            {

                String inXML = "";
                inXML += "<login>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <confID>" + lblConfID.Text + "</confID>";
                inXML += "  <endpointID></endpointID>";
                inXML += "  <terminalType></terminalType>";
                inXML += "</login>";
                //Response.Write(obj.Transfer(inXML));
                // <login> <userID>11</userID> <confID>15</confID> <endpointID></endpointID> <terminalType></terminalType> <displayLayout>01</displayLayout></login>
                String outXML = obj.CallCOM2("GetP2PConfStatus", inXML, Application["RTC_ConfigPath"].ToString());
                LblError.Visible = true;
                if (outXML.IndexOf("<error>") >= 0)
                    LblError.Text = obj.ShowErrorMessage(outXML);
                else
                {
                    doc = new XmlDocument();
                    doc.LoadXml(outXML);
                    if (doc.SelectSingleNode("GetP2PConfStatus/connectionStatus") != null)
                        stus = doc.SelectSingleNode("GetP2PConfStatus/connectionStatus").InnerText;
                }

            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("GetP2PStatus" + ex.StackTrace);
                LblError.Text = obj.ShowSystemMessage();
                //LblError.Text = ex.StackTrace;
                LblError.Visible = true;
            }

            return stus;
        }//Code added for P2P status

        protected void SendP2PMessage(Object sender, DataGridCommandEventArgs e)//Code added for P2P status
        {
            String text = "";
            try
            {
                System.Web.UI.HtmlControls.HtmlInputText msg = (System.Web.UI.HtmlControls.HtmlInputText)e.Item.FindControl("TxtMessageBox");

                if (msg != null)
                    text = msg.Value;

                String inXML = "";
                inXML += "<login>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <confID>" + lblConfID.Text + "</confID>";
                inXML += "  <endpointID>" + e.Item.Cells[0].Text + "</endpointID>";
                inXML += "  <terminalType>" + e.Item.Cells[1].Text + "</terminalType>";
                inXML += "  <messageText>" + text + "</messageText>";
                inXML += "</login>";
                //Response.Write(obj.Transfer(inXML));
                // <login> <userID>11</userID> <confID>15</confID> <endpointID></endpointID> <terminalType></terminalType> <displayLayout>01</displayLayout></login>
                String outXML = obj.CallCOM2("SendMessageToEndpoint", inXML, Application["RTC_ConfigPath"].ToString());
                LblError.Visible = true;
                if (outXML.IndexOf("<error>") >= 0)
                    LblError.Text = obj.ShowErrorMessage(outXML);
                else
                {
                    LblError.Text = obj.GetTranslatedText("Operation Successful!");
                    LblError.Visible = true;
                    //FB Case 903 - Saima ends here
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("SendP2PMessage" + ex.StackTrace);
                LblError.Text = obj.ShowSystemMessage();
                //LblError.Text = ex.StackTrace;
                LblError.Visible = true;
            }
        }

        protected void BroadCastP2PMsg(Object sender, EventArgs e)
        {
            try
            {


                String inXML = "";
                inXML += "<login>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <confID>" + lblConfID.Text + "</confID>";
                inXML += "  <messageText>" + TxtMessageBoxAll.Value + "</messageText>";
                inXML += "</login>";
                String outXML = obj.CallCOM2("SendMessageToConference", inXML, Application["RTC_ConfigPath"].ToString());
                LblError.Visible = true;
                if (outXML.IndexOf("<error>") >= 0)
                    LblError.Text = obj.ShowErrorMessage(outXML);
                else
                {
                    LblError.Text = obj.GetTranslatedText("Operation Successful!");
                    LblError.Visible = true;
                }


            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("BroadCastP2PMsg" + ex.StackTrace);
                LblError.Text = obj.ShowSystemMessage();
                LblError.Visible = true;
                //LblError.Text = "Delete: " + ex.StackTrace;
            }
        }

        protected void InitializeP2PEndpoints(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                //Response.Write(e.Item.ItemType);
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {

                    HyperLink eptWeb = (HyperLink)e.Item.FindControl("EptWebsite");
                    System.Web.UI.HtmlControls.HtmlTableRow msgTR = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("Messagediv");
                    System.Web.UI.HtmlControls.HtmlInputButton canclTR = (System.Web.UI.HtmlControls.HtmlInputButton)e.Item.FindControl("btnClose");
                    System.Web.UI.HtmlControls.HtmlInputText txtmsg = (System.Web.UI.HtmlControls.HtmlInputText)e.Item.FindControl("TxtMessageBox");
                    HyperLink eptMonitor = (HyperLink)e.Item.FindControl("EptMonitor");


                    if (eptWeb != null)
                    {
                        eptWeb.Text = e.Item.Cells[5].Text;
                        eptWeb.Attributes.Add("onclick", "javascript:fnOpenEpt('" + e.Item.Cells[5].Text.Trim() + "')"); //ZD 100420
                    }

                    if (eptMonitor != null)
                        eptMonitor.Attributes.Add("onclick", "javascript:fnOpenRemote('" + e.Item.Cells[5].Text.Trim() + "')"); //ZD 100420

                    if (lblStatus.Text == "Ongoing")
                    {

                        LinkButton btConnect = (LinkButton)e.Item.FindControl("btnConP2P");
                        Button btnms = (Button)e.Item.FindControl("SendMsg");
                        HyperLink lnkmsg = (HyperLink)e.Item.FindControl("btnMessage");

                        btConnect.Attributes.Add("onclick", "return confirm('"+obj.GetTranslatedText("Are you sure you want to Connect/disconnect this endpoint?")+"')");
                        if (obj.GetTranslatedText("Callee") == e.Item.Cells[9].Text) // ZD 101189
                            btConnect.Style.Add("visibility", "hidden");

                        if (lnkmsg != null && msgTR != null)
                            lnkmsg.Attributes.Add("onclick", "javascript:fnOpenMsg('" + msgTR.ClientID + "','1')");

                        if (canclTR != null && msgTR != null)
                            canclTR.Attributes.Add("onclick", "javascript:fnOpenMsg('" + msgTR.ClientID + "','0')");

                        if (btnms != null && txtmsg != null)
                            btnms.Attributes.Add("onclick", "javascript:return fnchkValue('" + txtmsg.ClientID + "')");
                        //Blue Status Project
                        //switch (p2pStatus)
                        //{
                        //    case ns_MyVRMNet.vrmEndPointConnectionSatus.disconnect:
                        //        e.Item.Cells[10].Text = "Disconnected";
                        //        e.Item.Cells[10].BackColor = System.Drawing.Color.Red;
                        //        e.Item.Cells[10].Font.Bold = true;
                        //        btConnect.Text = "Connect";
                        //        lnkmsg.Attributes.Add("style", "display:none");
                        //        break;
                        //    case ns_MyVRMNet.vrmEndPointConnectionSatus.Connecting:
                        //        e.Item.Cells[10].Text = "Connecting";
                        //        e.Item.Cells[10].BackColor = System.Drawing.Color.Yellow;
                        //        e.Item.Cells[10].Font.Bold = true;
                        //        break;
                        //    case ns_MyVRMNet.vrmEndPointConnectionSatus.Connected:
                        //        e.Item.Cells[10].Text = "Connected";
                        //        e.Item.Cells[10].BackColor = System.Drawing.Color.LimeGreen;
                        //        e.Item.Cells[10].Font.Bold = true;
                        //        btConnect.Text = "Disconnect";
                        //        break;
                        //    case "-1":
                        //        e.Item.Cells[10].Text = "Status not available.";
                        //        btConnect.Attributes.Add("style", "display:none");
                        //        lnkmsg.Attributes.Add("style", "display:none");
                        //        break;
                        //    default:
                        //        e.Item.Cells[10].Text = "Status being updated.";
                        //        btConnect.Attributes.Add("style", "display:none");
                        //        lnkmsg.Attributes.Add("style", "display:none");
                        //        break;
                        //}
                    }
                    else
                    {
                        dgP2PEndpoints.Columns[10].Visible = false;
                        dgP2PEndpoints.Columns[11].Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("P2PEndpoint" + ex.StackTrace);
                LblError.Text = obj.ShowSystemMessage();
                //LblError.Text = ex.StackTrace;
                LblError.Visible = true;
            }
        }//Code added for P2p Status

        protected void ConnectP2PEndpoint(Object sender, DataGridCommandEventArgs e)//Code added for P2P status
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <confID>" + lblConfID.Text + "</confID>";
                inXML += "  <endpointID>" + e.Item.Cells[0].Text + "</endpointID>";
                inXML += "  <terminalType>" + e.Item.Cells[1].Text + "</terminalType>";
                if (e.Item.Cells[10].Text.Equals("Connected"))
                    inXML += "  <connectOrDisconnect>0</connectOrDisconnect>";
                else
                    inXML += "  <connectOrDisconnect>1</connectOrDisconnect>";
                inXML += "</login>";
                String outXML = obj.CallCOM2("ConnectDisconnectTerminal", inXML, Application["RTC_ConfigPath"].ToString());
                LblError.Visible = true;
                if (outXML.IndexOf("<error>") >= 0)
                    LblError.Text = obj.ShowErrorMessage(outXML);
                else
                {
                    LblError.Text = obj.GetTranslatedText("Operation Successful!");
                    LblError.Visible = true;
                    //FB Case 903 - Saima ends here
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("ConnectP2PEndpoint" + ex.StackTrace);
                LblError.Text = obj.ShowSystemMessage();
                //LblError.Text = ex.StackTrace;
                LblError.Visible = true;
            }
        }

        #endregion

        //FB 1958
        #region ShowHostDetails
        /// <summary>
        /// Bind data to be printed
        /// </summary>
        protected void ShowHostDetails()
        {
            try
            {
                Control HostDetails = LoadControl(ResolveUrl(@"ViewUserDetails.ascx"));
                HostDetailHolder.Controls.Add(HostDetails);
                ns_MyVRM.en_ViewUserDetails userDetail = HostDetailHolder.Controls[0] as ns_MyVRM.en_ViewUserDetails;
                userDetail.BindUserData();
            }
            catch (Exception ex)
            {
                log.Trace("ShowHostDetails" + ex.Message);
            }
        }

        #endregion

        //FB 2441 Starts
        protected void btnMuteAllExcept(Object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<confID>" + lblConfID.Text + "</confID>");
                inXML.Append("<mode>1</mode>"); //0-UnMuteAllParties 1-MuteAllPartiesExcept
                inXML.Append("<endpoints>");

                for (int i = 0; i < dgMuteALL.Items.Count; i++)
                {
                    CheckBox btnMute = ((CheckBox)dgMuteALL.Items[i].FindControl("chk_muteall"));
                    if (btnMute.Checked)
                    {
                        inXML.Append("<endpoint>");
                        inXML.Append("<endpointId>" + dgMuteALL.Items[i].Cells[0].Text + "</endpointID>");
                        inXML.Append("<terminalType>" + dgMuteALL.Items[i].Cells[1].Text + "</terminalType>");
                        inXML.Append("</endpoint>");
                    }
                }
                inXML.Append("</endpoints>");
                inXML.Append("</login>");

                String outXML = obj.CallCOM2("MuteUnMuteParties", inXML.ToString(), Application["RTC_ConfigPath"].ToString());
                LblError.Visible = true;
                if (outXML.IndexOf("<error>") >= 0)
                    LblError.Text = obj.ShowErrorMessage(outXML);
                else
                {
                    outXML = obj.CallMyVRMServer("MuteUnMuteParticipants", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                    Session.Add("confid", lblConfID.Text);
                    LoadEndpoints();
                    LblError.Text = obj.GetTranslatedText("Operation Successful!");
                    LblError.Visible = true;
                }
            }
            catch (Exception ex)
            {
                LblError.Text = obj.ShowSystemMessage();
                log.Trace(ex.StackTrace);
                LblError.Visible = true;
            }
        }


        #region UnMuteAllParties
        /// <summary>
        /// UnMuteAllParties
        /// </summary>
        /// <param name="src"></param>
        /// <param name="e"></param>
        protected void UnMuteAllParties(Object src, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                String outXML = "";
                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<confID>" + lblConfID.Text + "</confID>");
                inXML.Append("<mode>0</mode>"); //0-UnMuteAllParties 1-MuteAllPartiesExcept
                inXML.Append("</login>");

                outXML = obj.CallCommand("MuteUnMuteParties", inXML.ToString());
                LblError.Visible = true;

                if (outXML.IndexOf("<error>") >= 0)
                    LblError.Text = obj.ShowErrorMessage(outXML);
                else
                {
                    outXML = obj.CallMyVRMServer("MuteUnMuteParticipants", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                    Session.Add("confid", lblConfID.Text);
                    LoadEndpoints();
                    LblError.Text = obj.GetTranslatedText("Operation Successful!");
                    LblError.Visible = true;
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("UnMuteAllPArties" + ex.StackTrace);
                LblError.Text = obj.ShowSystemMessage();
                //LblError.Text = ex.StackTrace;
                LblError.Visible = true;
            }
        }
        #endregion
        //FB 2441 Ends

        // FB 2664 start
        #region ClearLabel
        protected void ClearLabel()
        {

            lblConfHost.Text = "";
            lblLastModifiedBy.Text = "";
            lblConfDate.Text = "";
            lblConfTime.Text = "";
            lblTimezone.Text = "";
            lblConfDuration.Text = "";
            lblSetupDur.Text = "";
            lblTearDownDur.Text = "";
            lblConfType.Text = "";
            lblStatus.Text = "";
            lblPublic.Text = "";
            lblPassword.Text = "";
            lblReminders.Text = "";
            lblConfVNOC.Text = "";
            lblStartMode.Text = "";
            lblSecured.Text = "";
            lblFiles.Text = "";
            lblDescription.Text = "";
            lblSetupTimeinMin.Text = ""; //ZD 100085
            lblTearDownTimeinMin.Text = "";
            lblMCUPreStart.Text = "";
            lblMCUPreEnd.Text = "";
        }
        #endregion
        // FB 2664 End
    }
}


  
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text;
using System.Xml;
using System.Collections;

public partial class en_RoomFloor : System.Web.UI.Page
{
    protected System.Web.UI.HtmlControls.HtmlInputText txtFileName;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRoomIds;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnTierName;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnHoldState;
    protected System.Web.UI.HtmlControls.HtmlInputHidden markedValues;
    protected System.Web.UI.HtmlControls.HtmlInputHidden markCount;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRowInd;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSelectedRoom;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnHoldPopup;
    protected System.Web.UI.HtmlControls.HtmlInputHidden imgDiff;
    protected System.Web.UI.HtmlControls.HtmlImage planMap;
    protected System.Web.UI.HtmlControls.HtmlInputFile fleFloorPlan;
    protected System.Web.UI.HtmlControls.HtmlGenericControl lblNoData;
    protected System.Web.UI.HtmlControls.HtmlGenericControl noImgText;
    protected System.Web.UI.HtmlControls.HtmlGenericControl grdNoData;
    protected System.Web.UI.WebControls.Label errLabel;
    protected System.Web.UI.WebControls.Label errLabel2;
    protected System.Web.UI.WebControls.GridView grdPlanList;
    protected System.Web.UI.WebControls.GridView planMark;
    protected System.Web.UI.HtmlControls.HtmlInputButton btnReset;
    protected System.Web.UI.HtmlControls.HtmlInputButton btnSubmit;
    public string editText = "";
    public string actionText = "";

    myVRMNet.NETFunctions obj;
    ns_Logger.Logger log;
    

    public en_RoomFloor()
    {
        obj = new myVRMNet.NETFunctions();
        log = new ns_Logger.Logger();
    }

    #region InitializeCulture
    protected override void InitializeCulture()
    {
        if (Session["UserCulture"] != null)
        {
            UICulture = Session["UserCulture"].ToString();
            Culture = Session["UserCulture"].ToString();
            base.InitializeCulture();
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("RoomFloor.aspx", Request.Url.AbsoluteUri.ToLower());

            if (Session["admin"].ToString() == "3")
            {
                btnReset.Visible = false;
                btnSubmit.Visible = false;
                editText = obj.GetTranslatedText("View");
                actionText = obj.GetTranslatedText("Action");
            }
            else
            {
                editText = obj.GetTranslatedText("Edit");
                actionText = obj.GetTranslatedText("Actions");
            }

            if (!IsPostBack)
            {
                DataTable dt = new DataTable();
                dt.Clear();
                dt.Columns.Add("no", typeof(int));
                dt.Columns.Add("roomid", typeof(string));
                dt.Columns.Add("roomname", typeof(string));
                Session["markTable"] = dt;
                bindFloorPlanImages();
                planMap.Visible = false;
            }
            hdnHoldState.Value = "0";
            
        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();
            log.Trace(ex.StackTrace + "  " + ex.Message);
        }

    }

    protected void bindFloorPlanImages()
    {
        try
        {
            XmlDocument xd = new XmlDocument();
            XmlNodeList nodes = null;
            StringBuilder inXML = new StringBuilder();
            DataTable tblFloorPlan = new DataTable();
            ArrayList colNames = new ArrayList();
            inXML.Append("<GetFloorPlans>");
            inXML.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
            inXML.Append("<isAdmin>1</isAdmin>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("</GetFloorPlans>");
            string outXML = obj.CallMyVRMServer("GetFloorPlans", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
            xd.LoadXml(outXML);
            nodes = xd.SelectNodes("//GetFloorPlans/FloorPlan");
            colNames.Add("FloorPlanID");
            colNames.Add("FloorPlanName");
            colNames.Add("FloorPlanImage");
            colNames.Add("IdentifiedLocations");
            colNames.Add("PlanRoomIDs");
            colNames.Add("Tier1Name");
            colNames.Add("Tier2Name");
            tblFloorPlan = obj.LoadDataTable(nodes, colNames);
            if (tblFloorPlan.Rows.Count == 0)
                grdNoData.Visible = true;
            else
                grdNoData.Visible = false;
            string tierInUse = "";
            for (int i = 0; i < tblFloorPlan.Rows.Count; i++)
            {
                if (tierInUse == "")
                    tierInUse = tblFloorPlan.Rows[i]["Tier1ID"].ToString() + "||" + tblFloorPlan.Rows[i]["Tier2ID"].ToString();
                else
                    tierInUse += "~~" + tblFloorPlan.Rows[i]["Tier1ID"].ToString() + "||" + tblFloorPlan.Rows[i]["Tier2ID"].ToString();
            }
            Session["floorPlanTiers"] = tierInUse;
            grdPlanList.DataSource = tblFloorPlan;  
            grdPlanList.DataBind();
            ViewState["floorPlanList"] = tblFloorPlan;
        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();
            //errLabel.Text = ex.StackTrace + "  " + ex.Message;
            log.Trace(ex.StackTrace + "  " + ex.Message);
        }

    }

    protected void uploadImage(object sender, EventArgs e)
    {
        List<string> strExtension = null;
        try
        {
            string filename = Path.GetFileName(fleFloorPlan.Value);
            string filextn = Path.GetExtension(filename);
            strExtension = new List<string> { ".jpg", ".jpeg", ".png", ".gif" };
            bool filecontains = strExtension.Contains(filextn, StringComparer.OrdinalIgnoreCase);
            if (!filecontains)
            {
                errLabel.Text = obj.GetTranslatedText("Please select a valid Image Format like JPG GIF or PNG");
                //errLabel.Visible = true;
                return;
            }
            txtFileName.Value = obj.GetTranslatedText("No file selected");
            MemoryStream memStream = new MemoryStream();
            System.Drawing.Image myImage = System.Drawing.Image.FromStream(fleFloorPlan.PostedFile.InputStream);
            if (myImage.Width > 400 || myImage.Height > 300)
            {
                byte[] imgArray;
                imgArray = obj.Imageresize(myImage.Width, myImage.Height, 400, 300, myImage);
                memStream = new MemoryStream(imgArray);
            }
            else
            {
                myImage.Save(memStream, ImageFormat.Jpeg);
            }
            ViewState["curImage"] = Convert.ToBase64String(memStream.ToArray(), 0, memStream.ToArray().Length);
            ViewState["flrPlanId"] = "new";
            resetData();
            planMap.Visible = true;
            planMap.Src = "data:image/jpg;base64," + Convert.ToBase64String(memStream.ToArray(), 0, memStream.ToArray().Length);
            noImgText.Visible = false;
            lblNoData.Visible = true;
        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();
            log.Trace(ex.StackTrace + "  " + ex.Message);
        }

    }


    protected void RemoveMarkedRow(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            string c = e.CommandName;
            GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int index = gvr.RowIndex;
            DataTable markTable = (DataTable)Session["markTable"];
            markTable.Rows[index].Delete();
            string roomids = "";
            for (int i = 0; i < markTable.Rows.Count; i++)
            {
                markTable.Rows[i]["no"] = i + 1;

                if (markTable.Rows[i]["roomid"].ToString() != "")
                {
                    if (roomids == "")
                        roomids = markTable.Rows[i]["roomid"].ToString();
                    else
                        roomids += "," + markTable.Rows[i]["roomid"].ToString();
                }
            }
            hdnRoomIds.Value = roomids;
            Session["FloorPlanRoomIds"] = roomids;
            Session["markTable"] = markTable;
            if (markTable.Rows.Count > 0)
                lblNoData.Visible = false;
            else
                lblNoData.Visible = true;
            planMark.DataSource = markTable;
            planMark.DataBind();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "maskWin", "<script>fnPostResponse();</script>", false);
        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();
            log.Trace(ex.StackTrace + "  " + ex.Message);
        }


    }

    protected void addNewMark(object sender, EventArgs e)
    {
        try
        {
            int id = Convert.ToInt32(markCount.Value) - 1;
            DataTable markTable = (DataTable)Session["markTable"];
            DataRow markRow = markTable.NewRow();
            markRow["no"] = id;
            markRow["roomid"] = "";
            markRow["roomname"] = "";
            markTable.Rows.Add(markRow);
            Session["markTable"] = markTable;
            if (markTable.Rows.Count > 0)
                lblNoData.Visible = false;
            else
                lblNoData.Visible = true;
            planMark.DataSource = markTable;
            planMark.DataBind();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "maskWin", "<script>fnPostResponse();</script>", false);
        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();
            log.Trace(ex.StackTrace + "  " + ex.Message);
        }

    }


    protected void updateSelectedRoom(object sender, EventArgs e)
    {
        try
        {
            int ind = -1;
            Int32.TryParse(hdnRowInd.Value, out ind);
            string[] selecList = hdnSelectedRoom.Value.Split(new string[] { "||" }, StringSplitOptions.None);
            DataTable markTable = (DataTable)Session["markTable"];
            markTable.Rows[ind]["roomid"] = selecList[0];
            markTable.Rows[ind]["roomname"] = selecList[1];
            if (hdnRoomIds.Value == "")
            {
                hdnTierName.Value = selecList[2];
                Session["SelectedTierName"] = selecList[2];
            }
            string roomids = "";
            for (int i = 0; i < markTable.Rows.Count; i++)
            {
                if (markTable.Rows[i]["roomid"].ToString() != "")
                {
                    if (roomids == "")
                        roomids = markTable.Rows[i]["roomid"].ToString();
                    else
                        roomids += "," + markTable.Rows[i]["roomid"].ToString();
                }
            }
            hdnRoomIds.Value = roomids;
            Session["FloorPlanRoomIds"] = roomids;
            Session["markTable"] = markTable;
            planMark.DataSource = markTable;
            planMark.DataBind();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "maskWin", "<script>fnPostResponse();</script>", false);
        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();
            log.Trace(ex.StackTrace + "  " + ex.Message);
        }
    }


    protected void submitFloorPlan(object sender, EventArgs e)
    {
        try
        {
            //MemoryStream mem = new MemoryStream();
            //DataTable markTable = (DataTable)Session["markTable"];
            StringBuilder inXML = new StringBuilder();
            inXML.Append("<SetFloorPlan>");
            inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("<FloorPlan>");
            inXML.Append("<FloorPlanID>" + ViewState["flrPlanId"].ToString() + "</FloorPlanID>");
            inXML.Append("<FloorPlanName></FloorPlanName>");
            inXML.Append("<FloorPlanImage>" + ViewState["curImage"].ToString() + "</FloorPlanImage>");
            inXML.Append("<IdentifiedLocations>" + imgDiff.Value + "</IdentifiedLocations>");
            inXML.Append("<PlanRoomIDs>" + hdnRoomIds.Value + "</PlanRoomIDs>");
            inXML.Append("<Tier1ID>" + hdnTierName.Value.Split(',')[0] + "</Tier1ID>");
            inXML.Append("<Tier2ID>" + hdnTierName.Value.Split(',')[1] + "</Tier2ID>");
            inXML.Append("</FloorPlan>");
            inXML.Append("</SetFloorPlan>");
            string outXML = obj.CallMyVRMServer("SetFloorPlan", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
            if (outXML.ToLower().IndexOf("error") > -1)
            {
                hdnHoldState.Value = "0";
                errLabel.Text = obj.ShowErrorMessage(outXML);
            }
            else
            {
                errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                planMap.Src = "";
                planMap.Visible = false;
                resetData();
                noImgText.Visible = true;
                lblNoData.Visible = true;
                bindFloorPlanImages();
            }
        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();
            log.Trace(ex.StackTrace + "  " + ex.Message);
        }
    }

    protected void importSelectedPlan(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int index = gvr.RowIndex;
            string c = e.CommandName;
            if (c == "editPlan")
            {
                string[] roomDetail = null;
                string roomIds = "";
                DataTable dtPlanList = (DataTable) ViewState["floorPlanList"];
                string imgContent = (dtPlanList.Rows[index]["FloorPlanImage"]).ToString();
                ViewState["flrPlanId"] = (dtPlanList.Rows[index]["FloorPlanID"]).ToString();
                markedValues.Value = (dtPlanList.Rows[index]["IdentifiedLocations"]).ToString();
                string roomList = (dtPlanList.Rows[index]["PlanRoomIDStatus"]).ToString();
                string[] arrRoomList = roomList.Split(new string[] { "||" }, StringSplitOptions.None);
                hdnRoomIds.Value = "";
                hdnTierName.Value = (dtPlanList.Rows[index]["Tier1ID"]).ToString() + "," + (dtPlanList.Rows[index]["Tier2ID"]).ToString();
                Session["SelectedTierName"] = (dtPlanList.Rows[index]["Tier1ID"]).ToString() + "," + (dtPlanList.Rows[index]["Tier2ID"]).ToString();
                DataTable dt = (DataTable)Session["markTable"];
                dt.Rows.Clear();
                DataRow markRow;
                for (int i = 0; i < arrRoomList.Length; i++)
                {
                    roomDetail = arrRoomList[i].Split(new string[] { "~~" }, StringSplitOptions.None);
                    markRow = dt.NewRow();
                    markRow["no"] = (i + 1).ToString();
                    markRow["roomid"] = roomDetail[0];
                    markRow["roomname"] = roomDetail[1];
                    dt.Rows.Add(markRow);
                    if (roomIds == "")
                    {
                        roomIds = roomDetail[0];
                    }
                    else
                    {
                        roomIds += "," + roomDetail[0];
                    }
                }
                hdnRoomIds.Value = roomIds;
                lblNoData.Visible = false;
                Session["FloorPlanRoomIds"] = roomIds;
                Session["markTable"] = dt;
                planMark.DataSource = dt;
                planMark.DataBind();
                markCount.Value = (dt.Rows.Count + 1).ToString();
                noImgText.Visible = false;
                MemoryStream ms = new MemoryStream(Convert.FromBase64String(imgContent));
                ViewState["curImage"] = imgContent;
                planMap.Visible = true;
                planMap.Src = "data:image/jpg;base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);
                ms.Dispose();
                hdnHoldState.Value = "1";
            }

            if (c == "deletePlan")
            {
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<DeleteFloorPlan>");
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append(obj.OrgXMLElement());
                DataTable dtPlanList = (DataTable)ViewState["floorPlanList"];
                string flrPlanId = (dtPlanList.Rows[index]["FloorPlanID"]).ToString();
                inXML.Append("<FloorPlanID>" + flrPlanId + "</FloorPlanID>");
                inXML.Append("</DeleteFloorPlan>");
                string outXML = obj.CallMyVRMServer("DeleteFloorPlan", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.ToLower().IndexOf("error") > -1)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    hdnHoldState.Value = "0";
                }
                else
                {
                    errLabel2.Text = obj.GetTranslatedText("Operation Successful!");
                    hdnHoldPopup.Value = "1";
                    bindFloorPlanImages();
                    if (ViewState["flrPlanId"] != null && flrPlanId == ViewState["flrPlanId"].ToString())
                    {
                        resetData();
                        planMap.Src = "";
                        planMap.Visible = false;
                        noImgText.Visible = true;
                    }
                    else
                        hdnHoldState.Value = "0";
                }
            }
        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();
            //errLabel.Text = ex.StackTrace + "  " + ex.Message;
            log.Trace(ex.StackTrace + "  " + ex.Message);
        }
    }

    protected void resetData()
    {
        try
        {
            Session["SelectedTierName"] = "";
            Session["FloorPlanRoomIds"] = "";
            markCount.Value = "1";
            markedValues.Value = "";
            hdnTierName.Value = "";
            hdnRoomIds.Value = "";
            DataTable markTable = (DataTable)Session["markTable"];
            markTable.Rows.Clear();
            Session["markTable"] = markTable;
            planMark.DataSource = markTable;
            planMark.DataBind();
        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();
            //errLabel.Text = ex.StackTrace + "  " + ex.Message;
            log.Trace(ex.StackTrace + "  " + ex.Message);
            return;
        }
    }


}
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.Data;
using System.Xml;
using System.Text;
using System.Web.UI.WebControls;
using System.Threading;

namespace ns_SettingSelect
{

    /// <summary>
    /// Summary description for SettingSelect2.
    /// </summary>
    public partial class SettingSelect1 : System.Web.UI.Page
    {
        protected ns_InXML.InXML objInXML;
        ASPIL.VRMServer myvrmCom;
        ns_Logger.Logger log;
        
        protected System.Web.UI.WebControls.LinkButton btnCalendar;
        protected System.Web.UI.WebControls.LinkButton btnConference;
        protected System.Web.UI.WebControls.LinkButton btnAVInvManagement;
        protected System.Web.UI.WebControls.LinkButton btnConferenceConsole; 
        protected System.Web.UI.WebControls.LinkButton btnCATMenuManagement;
        protected System.Web.UI.WebControls.LinkButton btnHKGroupManagement;
        protected System.Web.UI.WebControls.LinkButton btnNewAVWorkorder;
        protected System.Web.UI.WebControls.LinkButton btnNewCATWorkorder;
        protected System.Web.UI.WebControls.LinkButton btnNewHKWorkorder;

        protected System.Web.UI.WebControls.DataGrid dgTemplates;
        protected System.Web.UI.WebControls.DataGrid dgHistory;
        protected System.Web.UI.WebControls.DataGrid dgAVWO;
        protected System.Web.UI.WebControls.DataGrid dgCATWO;
        protected System.Web.UI.WebControls.DataGrid dgHKWO;
        protected System.Web.UI.WebControls.DataGrid dgSearch;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label lblNoHKWO;
        protected System.Web.UI.WebControls.Table actionTable;
        protected System.Web.UI.WebControls.Table tblTemplates;
        protected System.Web.UI.WebControls.Table tblWorOrders;

        protected System.Web.UI.WebControls.ImageButton imgTemplate;
        protected System.Web.UI.WebControls.ImageButton imgHistory;
        protected System.Web.UI.WebControls.ImageButton imgAVWO;
        protected System.Web.UI.WebControls.ImageButton imgCATWO;
        protected System.Web.UI.WebControls.ImageButton imgHKWO;
        protected System.Web.UI.WebControls.ImageButton imgSearch;

        protected System.Web.UI.WebControls.TableRow trTemplates1;
        protected System.Web.UI.WebControls.TableRow trHistory1;
        protected System.Web.UI.WebControls.TableRow trAVWO1;
        protected System.Web.UI.WebControls.TableRow trCATWO1;
        protected System.Web.UI.WebControls.TableRow trHKWO1;
        protected System.Web.UI.WebControls.TableRow trSearch1;

        protected System.Web.UI.WebControls.Label lblNoAVWO;
        protected System.Web.UI.WebControls.Label lblNoCATWO;
        protected System.Web.UI.WebControls.Label lblNoSearch;
        protected System.Web.UI.WebControls.Label lblNoTemplate;
        protected System.Web.UI.WebControls.Label lblNoHistory;
        protected System.Web.UI.WebControls.Label lblSearchTemplate;

        protected System.Web.UI.HtmlControls.HtmlInputHidden txtLAlerts;
        protected System.Web.UI.HtmlControls.HtmlInputHidden feedTicker; //Ticker

        //Organization/CSS Module -- Start
        protected System.Web.UI.HtmlControls.HtmlGenericControl Field1;
        protected System.Web.UI.HtmlControls.HtmlGenericControl Field2;
        protected System.Web.UI.HtmlControls.HtmlGenericControl Field3;
        CustomizationUtil.CSSReplacementUtility cssUtil;
        //Organization/CSS Module -- End
        
        bool hasCalendar;
        bool hasConference;
        bool hasTemplates;
        bool hasHistory;
        bool hasAVWO;
        bool hasCATWO;
        bool hasHKWO;
        bool hasAVManagement;
        bool hasCATManagement;
        bool hasHKManagement;
        bool hasSearch;
        bool isGeneral;
        myVRMNet.NETFunctions obj;
        String tformat = "hh:mm tt";
        private String orgMsg = "";
        private String usrMsg = "";
        private String emailMsg = "";

        public SettingSelect1()
        {
            obj = new myVRMNet.NETFunctions();
            objInXML = new ns_InXML.InXML();
            myvrmCom = new ASPIL.VRMServer();
            log = new ns_Logger.Logger();
        }
        private void Page_Init()
        {
            obj = new myVRMNet.NETFunctions();
            objInXML = new ns_InXML.InXML();
            myvrmCom = new ASPIL.VRMServer();
            log = new ns_Logger.Logger();
        }
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("SettingSelect1.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
            Session["FormatDateType"] = ((Session["FormatDateType"] == null) ? "1" : Session["FormatDateType"]);
            Session["timeZoneDisplay"] = ((Session["timeZoneDisplay"] == null) ? "1" : Session["timeZoneDisplay"]);
            tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");

            if (!IsPostBack)
            {
                try
                {
                    //UI Text Change
                    String fieldText = "";
                    cssUtil = new CustomizationUtil.CSSReplacementUtility();
                    //FB 1830 -Translation - Start
                    cssUtil.ApplicationPath = Server.MapPath(".."); 
                    if (Session["isMultiLingual"] != null)
                    {
                        if (Session["isMultiLingual"].ToString() != "1")
                        {

                            fieldText = cssUtil.GetUITextForControl("SettingSelect2.aspx", "Field1");
                            Field1.InnerText = fieldText;

                            fieldText = cssUtil.GetUITextForControl("SettingSelect2.aspx", "Field2");
                            Field2.InnerText = fieldText;

                            fieldText = cssUtil.GetUITextForControl("SettingSelect2.aspx", "Field3");
                            Field3.InnerText = fieldText;

                            fieldText = cssUtil.GetUITextForControl("SettingSelect2.aspx", "CreateNewConference");
                            btnConference.Text = fieldText;
                            //Organization/CSS Module - Create folder for UI Settings --- End
                        }
                    }
					//FB 1830 -Translation - End
                    //code added for Ticker -- Start
                    myVRMNet.RSSXMLGenerator RSSObj = null;

                    if (obj == null)
                        obj = new myVRMNet.NETFunctions();


                    if (RSSObj == null)
                        RSSObj = new myVRMNet.RSSXMLGenerator();

                    String tickermarq = "";
                    if (Session["tickerStatus"] != null)
                    {
                        if (Session["tickerStatus"].ToString().Trim() == "0")
                        {
                            if (Session["tickerDisplay"].ToString().Trim() == "0")
                            {
                                tickermarq = RSSObj.TickerGenerator(Session["userID"].ToString(), Session["tickerDisplay"].ToString(), Session["FormatDateType"].ToString(), Session["timeFormat"].ToString());
                            }
                            else
                            {
                                string url = Session["rssFeedLink"].ToString();
                                tickermarq = RSSObj.FeedTickerGenerator(url, Session["userID"].ToString(), "0");
                            }
                        }
                    }
                    //feedTicker
                    Session.Add("ticker", tickermarq);

                    String tickermarq1 = "";
                    if (Session["tickerStatus1"] != null)
                    {
                        if (Session["tickerStatus1"].ToString().Trim() == "0")
                        {
                            if (Session["tickerDisplay1"].ToString().Trim() == "0")
                            {
                                tickermarq = RSSObj.TickerGenerator(Session["userID"].ToString(), Session["tickerDisplay1"].ToString(), Session["FormatDateType"].ToString(), Session["timeFormat"].ToString());
                            }
                            else
                            {
                                string url = Session["rssFeedLink1"].ToString();
                                tickermarq = RSSObj.FeedTickerGenerator(url, Session["userID"].ToString(), "1");
                            }
                        }
                    }
                    //feedTicker
                    Session.Add("ticker1", tickermarq);
                    //code added for Ticker -- End

                    obj.BindVideoProtocols(new DropDownList());

                    if (Request.QueryString["m"] != null) //FB Case 943
                        if (Request.QueryString["m"].Equals("1"))
                        {
                            errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                            errLabel.Visible = true;
                        }
                    if (Session["errMsg"] != null)
                        errLabel.Text = Session["errMsg"].ToString();

                    //FB 1830 - Translation
                    usrMsg = obj.GetTranslatedText("Your Email has been blocked since ");
                    orgMsg = obj.GetTranslatedText("Organization Email has been blocked since ");
                    emailMsg = obj.GetTranslatedText(". Please contact administrator for further assistance.");

                    Thread t1, t2, t3, t4, t5, t6, t7,t8;//FB 1860
                    t1 = new Thread(new ThreadStart(LoadTemplates));
                    t4 = new Thread(new ThreadStart(GetAlerts));
                    t2 = new Thread(new ThreadStart(LoadHistory));
                    t3 = new Thread(new ThreadStart(LoadSearch));
                    t5 = new Thread(new ThreadStart(LoadAVWorkOrders));
                    t6 = new Thread(new ThreadStart(LoadCATWorkOrders));
                    t7 = new Thread(new ThreadStart(LoadHKWorkOrders));
                    //FB 1860
                    t8 = new Thread(new ThreadStart(GetMailBlockStatus));
                    t8.Start();


                    t4.Start();
                    //while (t4.IsAlive) ;
                    //Thread.Sleep(1);
                    //t4.Abort();
                    log.Trace("in SettingsSelect PageLoad");
                    imgTemplate.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trTemplates1.ClientID + "', false);return false;");
                    imgHistory.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trHistory1.ClientID + "', false);return false;");
                    imgAVWO.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trAVWO1.ClientID + "', false);return false;");
                    imgCATWO.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trCATWO1.ClientID + "', false);return false;");
                    imgHKWO.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trHKWO1.ClientID + "', false);return false;");
                    imgSearch.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trSearch1.ClientID + "', false);return false;");
                    string[] mary = Session["sMenuMask"].ToString().Split('-');
                    string[] mmary = mary[1].Split('+');
                    string[] ccary = mary[0].Split('*');
                    int topMenu = Convert.ToInt32(ccary[1]);
                    int adminMenu = Convert.ToInt32(mmary[0].Split('*')[1]);
                    int subMenu; // = Convert.ToInt32(mmary[4].Split('*')[1]);
                    int orgMenu = Convert.ToInt32(mmary[1].Split('*')[1]);

                    hasCalendar = Convert.ToBoolean(topMenu & 64);//FB 1779
                    hasConference = Convert.ToBoolean(topMenu & 32); //FB 1779
                    Boolean hasFeedback = Convert.ToBoolean(Int32.Parse(mary[2].Split('*')[1]) & 8); //Login Management
                    Session.Add("hasFeedback", hasFeedback);
                    Boolean hasHelp = Convert.ToBoolean(Int32.Parse(mary[2].Split('*')[1]) & 4);//Login Management
                    Session.Add("hasHelp", hasHelp);
                    hasHistory = hasConference;
                    hasSearch = Convert.ToBoolean(topMenu & 32);
                    hasTemplates = Convert.ToBoolean(topMenu & 16) & Convert.ToBoolean(adminMenu & 2); //FB 1779

                    //if (Convert.ToBoolean(topMenu & 4))
                    //{
                        if (Convert.ToBoolean(orgMenu & 4))
                        {
                            subMenu = Convert.ToInt32(mmary[5].Split('*')[1]);
                            hasAVManagement = Convert.ToBoolean(subMenu & 2);
                            hasAVWO = Convert.ToBoolean(subMenu & 1);
                        }
                        if (Convert.ToBoolean(orgMenu & 2))
                        {
                            subMenu = Convert.ToInt32(mmary[6].Split('*')[1]);
                            hasCATManagement = Convert.ToBoolean(subMenu & 2);
                            hasCATWO = Convert.ToBoolean(subMenu & 1);
                        }
                        if (Convert.ToBoolean(orgMenu & 1))
                        {
                            subMenu = Convert.ToInt32(mmary[7].Split('*')[1]);
                            hasHKManagement = Convert.ToBoolean(subMenu & 2);
                            hasHKWO = Convert.ToBoolean(subMenu & 1);
                        }
                    //}
                    if (Session["roomModule"].ToString().Equals("0"))
                    {
                        hasAVManagement = false;
                        hasAVWO = false;
                    }
                    if (Session["foodModule"].ToString().Equals("0"))
                    {
                        hasCATManagement = false;
                        hasCATWO = false;
                    }
                    if (Session["hkModule"].ToString().Equals("0"))
                    {
                        hasHKManagement = false;
                        hasHKWO = false;
                    }

                    //Added for FB 1428 Start
                    if (Application["Client"].ToString().ToUpper() == "MOJ")
                    {
                        dgHistory.Columns[1].HeaderText = "Hearing Name";
                    }
                    /*else//FB 1830-translation
                    {
                        dgHistory.Columns[1].HeaderText = "Conference Name";
                    }*/
                    //Added for FB 1428 End


                    //if (hasHKWO == true)
                    //{
                    //    Response.Write("hk WK is : " + hasHKWO);
                    //    btnAVInvManagement.Visible = false;
                    //    btnCATMenuManagement.Visible = false;
                    //    btnNewAVWorkorder.Visible = false;
                    //    btnNewCATWorkorder.Visible = false;
                    //}

                    //Response.Write("Admin value is : " + (adminMenu & 0));
                    //isGeneral = (adminMenu == 7680) ? true : false;


                    TableRow tr;
                    TableCell tc;
                    //for Actions Table on upper right corner
                    Session.Add("hasCalendar", hasCalendar);
                    if (hasCalendar)
                    {
                        tc = new TableCell();
                        tr = new TableRow();
                        tr.CssClass = "tableBody";
                        tr.Height = System.Web.UI.WebControls.Unit.Point(20);
                        tc.Controls.Add(btnCalendar);// tc.BorderColor = System.Drawing.Color.Blue; tc.BorderStyle = BorderStyle.Solid; tc.BorderWidth = System.Web.UI.WebControls.Unit.Pixel(1);
                        tr.Cells.Add(tc);// tr.BorderColor = System.Drawing.Color.Blue; tr.BorderStyle = BorderStyle.Solid;
                        //if ((actionTable.Rows.Count % 2) == 0)
                        //    tr.BackColor = System.Drawing.Color.LightGray;
                        //else
                        //    tr.BackColor = System.Drawing.Color.White;
                        actionTable.Rows.Add(tr);
                    }
                    else
                        btnCalendar.Visible = false;
                    if (hasConference)
                    {
                        tc = new TableCell();
                        tr = new TableRow();
                        tr.Height = System.Web.UI.WebControls.Unit.Point(20);
                        tr.CssClass = "tableBody";
                        tc.Controls.Add(btnConference);// tc.BorderColor = System.Drawing.Color.Blue; tc.BorderStyle = BorderStyle.Solid; tc.BorderWidth = System.Web.UI.WebControls.Unit.Pixel(1);
                        tr.Cells.Add(tc);// tr.BorderColor = System.Drawing.Color.Blue; tr.BorderStyle = BorderStyle.Solid;
                        //if ((actionTable.Rows.Count % 2) == 0)
                        //    tr.BackColor = System.Drawing.Color.LightGray;
                        //else
                        //    tr.BackColor = System.Drawing.Color.White;

                        actionTable.Rows.Add(tr);

                        tc = new TableCell(); //Dash Board M
                        tr = new TableRow();
                        tr.Height = System.Web.UI.WebControls.Unit.Point(20);
                        tr.CssClass = "tableBody";
                        tc.Controls.Add(btnConferenceConsole);// tc.BorderColor = System.Drawing.Color.Blue; tc.BorderStyle = BorderStyle.Solid; tc.BorderWidth = System.Web.UI.WebControls.Unit.Pixel(1);
                        tr.Cells.Add(tc);// tr.BorderColor = System.Drawing.Color.Blue; tr.BorderStyle = BorderStyle.Solid;

                        actionTable.Rows.Add(tr);
                    }
                    else
                    {
                        //FB 1522 - Start
                        //trTemplates.Visible = false;
                        dgTemplates.Columns.Clear();
                        dgTemplates.Visible = false;
                        tblTemplates.Rows[0].Visible = false;
                        tblTemplates.Rows[1].Visible = false;
                        tblTemplates.Rows[2].Visible = false;
                        tblTemplates.Rows[0].Width = System.Web.UI.WebControls.Unit.Pixel(0);
                        tblTemplates.Rows[1].Width = System.Web.UI.WebControls.Unit.Pixel(0);
                        tblTemplates.Rows[2].Width = System.Web.UI.WebControls.Unit.Pixel(0);
                        //FB 1522 - End
                        btnConferenceConsole.Visible = false;
                        btnConference.Visible = false;
                    }

                    //Code added by Vivek on 29th Apr 2008 for issue number 256 - AV Specialist seeing CAT and HK
                    //An additional and condition is added to make link invisible for other roles
                    if (hasAVManagement)
                    {
                        tc = new TableCell();
                        tr = new TableRow();
                        tr.CssClass = "tableBody";
                        tr.Height = System.Web.UI.WebControls.Unit.Point(20);
                        tc.Controls.Add(btnAVInvManagement);// tc.BorderColor = System.Drawing.Color.Blue; tc.BorderStyle = BorderStyle.Solid; tc.BorderWidth = System.Web.UI.WebControls.Unit.Pixel(1);
                        tr.Cells.Add(tc); //tr.BorderColor = System.Drawing.Color.Blue; tr.BorderStyle = BorderStyle.Solid;
                        //if ((actionTable.Rows.Count % 2) == 0)
                        //    tr.BackColor = System.Drawing.Color.LightGray;
                        //else
                        //    tr.BackColor = System.Drawing.Color.White;

                        actionTable.Rows.Add(tr);
                    }
                    else
                        btnAVInvManagement.Visible = false;

                    //Code added by Vivek on 29th Apr 2008 for issue number 257 - Caterer able to see AV and HK
                    //An additional and condition is added to make link invisible for other roles
                    if (hasCATManagement)
                    {
                        tc = new TableCell();
                        tr = new TableRow();
                        tr.Height = System.Web.UI.WebControls.Unit.Point(20);
                        tr.CssClass = "tableBody";
                        tc.Controls.Add(btnCATMenuManagement);// tc.BorderColor = System.Drawing.Color.Blue; tc.BorderStyle = BorderStyle.Solid; tc.BorderWidth = System.Web.UI.WebControls.Unit.Pixel(1);
                        tr.Cells.Add(tc);//// tr.BorderColor = System.Drawing.Color.Blue; tr.BorderStyle = BorderStyle.Solid;
                        //if ((actionTable.Rows.Count % 2) == 0)
                        //    tr.BackColor = System.Drawing.Color.LightGray;
                        //else
                        //    tr.BackColor = System.Drawing.Color.White;

                        actionTable.Rows.Add(tr);
                    }
                    else
                        btnCATMenuManagement.Visible = false;

                    //Code added by Vivek on 29th Apr 2008 for issue number 258 - HK specialist able to see AV and CAT
                    //An additional and condition is added to make link invisible for other roles
                    if (hasHKManagement)
                    {
                        tc = new TableCell();
                        tr = new TableRow();
                        tr.Height = System.Web.UI.WebControls.Unit.Point(20);
                        tc.Controls.Add(btnHKGroupManagement); //tc.BorderColor = System.Drawing.Color.Blue; tc.BorderStyle = BorderStyle.Solid; tc.BorderWidth = System.Web.UI.WebControls.Unit.Pixel(1);
                        tr.CssClass = "tableBody";
                        tr.Cells.Add(tc); //tr.BorderColor = System.Drawing.Color.Blue; tr.BorderStyle = BorderStyle.Solid;
                        //if ((actionTable.Rows.Count % 2) == 0)
                        //    tr.BackColor = System.Drawing.Color.LightGray;
                        //else
                        //    tr.BackColor = System.Drawing.Color.White;

                        actionTable.Rows.Add(tr);
                    }
                    else
                        btnHKGroupManagement.Visible = false;

                    //Code added by Vivek on 29th Apr 2008 for issue number 256 - AV Specialist seeing CAT and HK
                    //An additional and condition is added to make link invisible for other roles
                    if (Application["Client"].ToString().ToUpper().Equals("WASHU") && Session["roomModule"].ToString().Equals("1") && ((hasAVWO == true) || (isGeneral == true)))
                    {
                        tc = new TableCell();
                        tr = new TableRow();
                        tr.Height = System.Web.UI.WebControls.Unit.Point(20);
                        tr.CssClass = "tableBody";
                        tc.Controls.Add(btnNewAVWorkorder); //tc.BorderColor = System.Drawing.Color.Blue; tc.BorderStyle = BorderStyle.Solid; tc.BorderWidth = System.Web.UI.WebControls.Unit.Pixel(1);
                        tr.Cells.Add(tc); //tr.BorderColor = System.Drawing.Color.Blue; tr.BorderStyle = BorderStyle.Solid;
                        //if ((actionTable.Rows.Count % 2) == 0)
                        //    tr.BackColor = System.Drawing.Color.LightGray;
                        //else
                        //    tr.BackColor = System.Drawing.Color.White;

                        actionTable.Rows.Add(tr);
                    }
                    else
                        btnNewAVWorkorder.Visible = false;

                    //Code added by Vivek on 29th Apr 2008 for issue number 257 - Caterer able to see AV and HK
                    //An additional and condition is added to make link invisible for other roles
                    if (Application["Client"].ToString().ToUpper().Equals("WASHU") && Session["foodModule"].ToString().Equals("1") && ((hasCATWO == true) || (isGeneral == true)))
                    {
                        tc = new TableCell();
                        tr = new TableRow();
                        tr.Height = System.Web.UI.WebControls.Unit.Point(20);
                        tr.CssClass = "tableBody";
                        tc.Controls.Add(btnNewCATWorkorder); //tc.BorderColor = System.Drawing.Color.Blue; tc.BorderStyle = BorderStyle.Solid; tc.BorderWidth = System.Web.UI.WebControls.Unit.Pixel(1);
                        tr.Cells.Add(tc); //tr.BorderColor = System.Drawing.Color.Blue; tr.BorderStyle = BorderStyle.Solid;
                        //if ((actionTable.Rows.Count % 2) == 0)
                        //    tr.BackColor = System.Drawing.Color.LightGray;
                        //else
                        //    tr.BackColor = System.Drawing.Color.White;

                        actionTable.Rows.Add(tr);
                    }
                    else
                        btnNewCATWorkorder.Visible = false;

                    //Code added by Vivek on 29th Apr 2008 for issue number 258 - HK specialist able to see AV and CAT
                    //An additional and condition is added to make link invisible for other roles
                    if (Application["Client"].ToString().ToUpper().Equals("WASHU") && Session["hkModule"].ToString().Equals("1") && ((hasHKWO == true) || (isGeneral == true)))
                    {
                        tc = new TableCell();
                        tr = new TableRow();
                        tr.Height = System.Web.UI.WebControls.Unit.Point(20);
                        tc.Controls.Add(btnNewHKWorkorder); //tc.BorderColor = System.Drawing.Color.Blue; tc.BorderStyle = BorderStyle.Solid; tc.BorderWidth = System.Web.UI.WebControls.Unit.Pixel(1);
                        tr.CssClass = "tableBody";
                        tr.Cells.Add(tc); //tr.BorderColor = System.Drawing.Color.Blue; tr.BorderStyle = BorderStyle.Solid;
                        //if ((actionTable.Rows.Count % 2) == 0)
                        //    tr.BackColor = System.Drawing.Color.LightGray;
                        //else
                        //    tr.BackColor = System.Drawing.Color.White;

                        actionTable.Rows.Add(tr);
                    }
                    else
                        btnNewHKWorkorder.Visible = false;
                    int countRow = 0;
                    tr = new TableRow();
                    //For Widgets on lobby page 
                    if (hasTemplates)
                    {
                        t1.Start();
                        //while (t1.IsAlive) ;
                        //Thread.Sleep(1);
                        //t1.Abort();
                        //LoadTemplates();
                    }
                    else
                    {
                        dgTemplates.Columns.Clear();
                        dgTemplates.Visible = false;
                        tblTemplates.Rows[0].Visible = false;
                        tblTemplates.Rows[1].Visible = false;
                        tblTemplates.Rows[2].Visible = false;
                        tblTemplates.Rows[0].Width = System.Web.UI.WebControls.Unit.Pixel(0);
                        tblTemplates.Rows[1].Width = System.Web.UI.WebControls.Unit.Pixel(0);
                        tblTemplates.Rows[2].Width = System.Web.UI.WebControls.Unit.Pixel(0);
                    }
                    if (hasHistory)
                    {
                        tc = new TableCell();
                        t2.Start();
                        //while (t2.IsAlive) ;
                        //Thread.Sleep(1);
                        //t2.Abort();
                        //LoadHistory();
                        countRow++;
                    }
                    else
                    {
                        dgHistory.Columns.Clear();
                        dgHistory.Visible = false;
                        tblTemplates.Rows[3].Visible = false;
                        tblTemplates.Rows[4].Visible = false;
                        tblTemplates.Rows[3].Width = System.Web.UI.WebControls.Unit.Pixel(0);
                        tblTemplates.Rows[4].Width = System.Web.UI.WebControls.Unit.Pixel(0);
                    }
                    //Response.Write(hasSearch);
                    //FB 1522 - Start
                    //if (hasSearch)
                    //{
                        hasSearch = true;
                        tc = new TableCell();
                        dgSearch.Visible = true;
                        lblNoSearch.Visible = false;
                        t3.Start();
                        //while (t2.IsAlive) ;
                        //Thread.Sleep(1);
                        //t2.Abort();
                        //LoadSearch();
                        countRow++;
                    //}
                    //else
                    //{
                    //    dgSearch.Visible = false;
                    //    lblNoSearch.Visible = true;
                    //}
                    //FB 1522 - End

                    if (hasAVWO)
                    {
                        dgAVWO.Visible = true;
                        t5.Start();
                        //while (t2.IsAlive) ;
                        //Thread.Sleep(1);
                        //t2.Abort();
                        //t1.Join();
                        //t2.Join();
                        //t3.Join();
                        //t5.Join();
                        //LoadAVWorkOrders();
                        countRow++;
                    }
                    else
                    {
                        dgAVWO.Visible = false;
                        tblWorOrders.Rows[0].Visible = false;
                        tblWorOrders.Rows[1].Visible = false;
                        tblWorOrders.Rows[2].Visible = false;
                        tblWorOrders.Rows[0].Width = System.Web.UI.WebControls.Unit.Pixel(0);
                        tblWorOrders.Rows[1].Width = System.Web.UI.WebControls.Unit.Pixel(0);
                        tblWorOrders.Rows[2].Width = System.Web.UI.WebControls.Unit.Pixel(0);
                    }

                    if (hasCATWO)
                    {
                        t6.Start();
                        //while (t2.IsAlive) ;
                        //Thread.Sleep(1);
                        //t2.Abort();
                        //LoadCATWorkOrders();
                    }
                    else
                    {
                        tblWorOrders.Rows[3].Visible = false;
                        tblWorOrders.Rows[4].Visible = false;
                        tblWorOrders.Rows[5].Visible = false;
                        tblWorOrders.Rows[3].Width = System.Web.UI.WebControls.Unit.Pixel(0);
                        tblWorOrders.Rows[4].Width = System.Web.UI.WebControls.Unit.Pixel(0);
                        tblWorOrders.Rows[5].Width = System.Web.UI.WebControls.Unit.Pixel(0);
                        dgCATWO.Visible = false;
                    }
                    if (hasHKWO)
                    {
                        t7.Start();
                        //while (t2.IsAlive) ;
                        //Thread.Sleep(1);
                        //t2.Abort();
                        //LoadHKWorkOrders();
                    }
                    else
                    {
                        tblWorOrders.Rows[6].Visible = false;
                        tblWorOrders.Rows[7].Visible = false;
                        tblWorOrders.Rows[6].Width = System.Web.UI.WebControls.Unit.Pixel(0);
                        tblWorOrders.Rows[7].Width = System.Web.UI.WebControls.Unit.Pixel(0);
                        dgHKWO.Visible = false;
                    }
                    //DateTime endTime = DateTime.Now;
                    //Response.Write(("<br>End time:" + endTime.ToString()));
                    if (!(hasAVWO && hasCATWO && hasHKWO))
                    {
                        tblWorOrders.Width = System.Web.UI.WebControls.Unit.Pixel(0);
                    }
                    if (!(hasTemplates && hasConference))
                    {
                        tblTemplates.Width = System.Web.UI.WebControls.Unit.Pixel(0);
                    }
                    //Thread.Sleep(10);
                    if (hasTemplates)
                    {
                        t1.Join();
                        t1.Abort();
                    }
                    if (hasHistory)
                    {
                        t2.Join();
                        t2.Abort();
                    }
                    if (hasSearch)
                    {
                        t3.Join();
                        t3.Abort();
                    }
                    if (hasAVWO)
                    {
                        t5.Join();
                        t5.Abort();
                    }
                    if (hasCATWO)
                    {
                        t6.Join();
                        t6.Abort();
                    }
                    if (hasHKWO)
                    {
                        t7.Join();
                        t7.Abort();
                    }
                    t4.Join();
                    t4.Abort();

                    t8.Join();
                    t8.Abort();

                    t1 = null;
                    t2 = null;
                    t3 = null;
                    t4 = null;
                    t5 = null;
                    t6 = null;
                    t7 = null;
                }
                catch (Exception ex)
                {
                    errLabel.Visible = true;
                    log.Trace(ex.StackTrace);
                }
            }

            
            // Put user code to initialize the page here
        }

        #region User code to populate the Grids and links table
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //


            InitializeComponent();
            // base.OnInit(e);
        }


        public void GoToCalendar(Object sender, EventArgs e)
        {
            //string url = "dispatcher/admindispatcher.asp?cmd=ManageConfRoom&f=v&hf=&pub=";//Login Management
            string url = "personalCalendar.aspx?v=1&r=1&hf=&m=&pub=&d=&comp=&f=v"; //Login Management
            Response.Redirect(url);
        }

        public void CreateNewConference(Object sender, EventArgs e)
        {
            //FB 1765 start
            String reDirectPage = "ConferenceSetup.aspx?t=n";
            if (Session["defaultConfTemp"] != null)
            {
                if (Session["defaultConfTemp"].ToString() != "0")
                    reDirectPage = "ConferenceSetup.aspx?t=t";
            }
            Response.Redirect(reDirectPage);
            //FB 1765 end
        }

        public void ConferenceConsole(Object sender, EventArgs e)  
        {
            Response.Redirect("Dashboard.aspx");
        }
        public void InventoryManagement(Object sender, EventArgs e)
        {
            Response.Redirect("InventoryManagement.aspx?t=1");
        }
        public void MenuManagement(Object sender, EventArgs e)
        {
            Response.Redirect("InventoryManagement.aspx?t=2");
        }
        public void GroupManagement(Object sender, EventArgs e)
        {
            Response.Redirect("InventoryManagement.aspx?t=3");
        }

        public void LoadTemplates()
        {
            try
            {
                //Response.Write("<br>Templates Start: " + DateTime.Now.ToString("mm:ss"));
                string inxml = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><sortBy></sortBy></login>";//Organization Module Fixes
                //string outxml = obj.CallCOM("GetTemplateList", inxml, Application["COM_ConfigPath"].ToString());
                //web_com_v18_Net.Com com = new web_com_v18_Net.ComClass();
                string outxml = obj.CallMyVRMServer("GetTemplateList", inxml, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write("<br>Templates after command: " + DateTime.Now.ToString("mm:ss"));
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outxml);
                XmlNodeList nodes = xmldoc.SelectNodes("//templates/template");
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataTable dt;
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        //dr["name"] = "<a href='dispatcher/userdispatcher.asp?cmd=GetOldTemplate&tid=" + dr["ID"].ToString() + "' >" + dr["name"].ToString() + "</a>";
                        dr["name"] = "<a href='ConferenceSetup.aspx?t=t&confid=" + dr["ID"] + "'>" + dr["name"].ToString() + "</a>";
                        //dr["name"] = "<a href='dispatcher/userdispatcher.asp?cmd=GetOldTemplate&tid=" + dr["ID"].ToString() + "' >" + dr["name"].ToString() + "</a>";
                    }
                    int tempcount = dt.Rows.Count;
                    //Response.Write(tempcount);
                    if (tempcount >= 5)
                    {
                        for (int i = tempcount - 1; i >= 5; i--)
                        {
                            //Response.Write(i + " : ");
                            dt.Rows[i].Delete();
                        }
                        dgTemplates.ShowFooter = true;
                    }
                    else
                        dgTemplates.ShowFooter = false;
                    dgTemplates.DataSource = dt;
                    dgTemplates.DataBind();
                }
                else
                {
                    lblNoTemplate.Visible = true;
                }
                //Response.Write("<br>Templates End: " + DateTime.Now.ToString("mm:ss"));
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }
        public void LoadHistory()
        {
            StringBuilder strBuilder = new StringBuilder();
            try
            {
                //Response.Write("<br>History Start: " + DateTime.Now.ToString("mm:ss"));
                string inxml = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><sortBy></sortBy></login>";//Organization Module Fixes
                //Response.Write("<br>History After command: " + DateTime.Now.ToString("mm:ss"));
                //web_com_v18_Net.Com com = new web_com_v18_Net.ComClass();
                //string outxml = com.comCentral("GetSettingsSelect", inxml, Application["COM_ConfigPath"].ToString());
                string outxml = obj.CallMyVRMServer("GetSettingsSelect", inxml, Application["MyVRMServer_ConfigPath"].ToString());
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outxml);
                XmlNodeList nodes = xmldoc.SelectNodes("//user/roomMostRecentConf/conf");
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv;
                DataTable dt;
                //Response.Write(ds.Tables.Count);
                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;
                    dv = new DataView(dt);
                    dgHistory.Visible = true;
                    dgHistory.DataSource = dv;
                    dgHistory.DataBind();
                }
                else
                {
                    lblNoHistory.Visible = true;
                }
                //Response.Write("<br>History End: " + DateTime.Now.ToString("mm:ss"));
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
        }
        protected void LoadSearch()
        {
            try
            {
                //Response.Write("<br>Search Start: " + DateTime.Now.ToString("mm:ss"));
                if (!Session["SearchID"].ToString().Equals("-1") && !Session["SearchID"].ToString().Equals("") && !Session["SearchID"].ToString().Equals("0"))
                {
                    String inXML = "<GetSearchTemplate>" + obj.OrgXMLElement() + "<UserID>" + Session["userID"].ToString() + "</UserID><TemplateID>" + Session["SearchID"].ToString() + "</TemplateID></GetSearchTemplate>";//Organization Module Fixes
                    //Response.Write("<br>Search after command: " + DateTime.Now.ToString("mm:ss"));
                    String outXML = myvrmCom.Operations(Application["MyVRMServer_ConfigPath"].ToString(), "GetSearchTemplate", inXML); //obj.CallMyVRMServer("GetSearchTemplate", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    if (outXML.IndexOf("<error>") < 0)
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outXML);
                        //FB 2272
                        lblSearchTemplate.Text = obj.GetTranslatedText("My Default Search") + " (" + xmldoc.SelectSingleNode("//SearchTemplate/TemplateName").InnerText + ")";
                        String ConferenceStatus = "0";
                        if (xmldoc.SelectNodes("//getSearchTemplate/confSearch").Count > 0)
                        {
                            if (xmldoc.SelectSingleNode("//getSearchTemplate/confSearch/confInfo/pending").InnerText.Equals("1"))
                                ConferenceStatus = "1";
                            String ConferenceName = xmldoc.SelectSingleNode("//getSearchTemplate/confSearch/confInfo/confName").InnerText;
                            String ConferenceUniqueID = xmldoc.SelectSingleNode("//getSearchTemplate/confSearch/confInfo/confUniqueID").InnerText;
                            String CTSNumericID = xmldoc.SelectSingleNode("//getSearchTemplate/confSearch/confInfo/CTSNumericID").InnerText;//Fb 2870
                            String ConferenceSearchType = xmldoc.SelectSingleNode("//getSearchTemplate/confSearch/confInfo/confDate/type").InnerText;
                            String DateFrom = "";
                            String DateTo = "";
                            String ConferenceHost = xmldoc.SelectSingleNode("//getSearchTemplate/confSearch/confInfo/confHost").InnerText;
                            String ConferenceParticipant = xmldoc.SelectSingleNode("//getSearchTemplate/confSearch/confResource/confParticipant").InnerText;
                            String Public = "";
                            if (!xmldoc.SelectSingleNode("//getSearchTemplate/confSearch/confInfo/public").InnerText.Equals("0"))
                                Public = xmldoc.SelectSingleNode("//getSearchTemplate/confSearch/confInfo/public").InnerText;
                            String SelectionType = xmldoc.SelectSingleNode("//getSearchTemplate/confSearch/confResource/confRooms/type").InnerText;
                            String SelectedRooms = "";
                            String PageNo = "1";
                            String SortBy = "3";
                            inXML = objInXML.SearchConference(Session["userID"].ToString(), ConferenceStatus, ConferenceName, "", ConferenceUniqueID, CTSNumericID, ConferenceSearchType, DateFrom, DateTo, ConferenceHost, ConferenceParticipant, Public, SelectionType, SelectedRooms, PageNo, SortBy, "0", "", "", "", "0", "", false, ns_MyVRMNet.vrmSearchFilterType.custom,"");//Custom Attribute Fix //FB 2632//FB 2694//FB 2822 //FB 2870 //FB 3006 //FB 2639 - Search
                        }
                        else
                            inXML = xmldoc.SelectSingleNode("//SearchTemplate/SearchConference").OuterXml;
                        //Response.Write("<br>Search1: " + DateTime.Now.ToString("mm:ss"));
                        outXML = myvrmCom.Operations(Application["MyVRMServer_ConfigPath"].ToString(), "SearchConference", inXML); // obj.CallMyVRMServer("SearchConference", inXML, Application["MyVRMServer_ConfigPath"].ToString()); ;
                        //Response.Write("<br>Search2: " + DateTime.Now.ToString("mm:ss"));
                        Session.Add("inXML", inXML);
                        if (outXML.IndexOf("<error>") >= 0)
                        {
                            errLabel.Text += "Search: " + obj.ShowErrorMessage(outXML);
                            errLabel.Visible = true;
                            lblNoSearch.Visible = true;
                        }
                        else
                        {
                            lblNoSearch.Visible = false;
                            xmldoc = new XmlDocument();
                            xmldoc.LoadXml(outXML);
                            XmlNodeList nodes = xmldoc.SelectNodes("//SearchConference/Conferences/Conference");
                            XmlTextReader xtr;
                            DataSet ds = new DataSet();

                            foreach (XmlNode node in nodes)
                            {
                                xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                                ds.ReadXml(xtr, XmlReadMode.Auto);
                            }
                            if (ds.Tables.Count > 0)
                            {
                                DataTable dt = ds.Tables[0];
                                //Response.Write(dt.Rows.Count - 1);
                                if (dt.Rows.Count >= 5)
                                {
                                    dgSearch.ShowFooter = true;
                                    ////Response.Write("<br>in if");
                                    for (int i = dt.Rows.Count - 1; i >= 5; i--)
                                        dt.Rows.RemoveAt(i);
                                }
                                //Response.Write(Session["systemDate"].ToString() + " : " + Session["systemTime"].ToString());
                                DateTime UPDT = DateTime.Parse(Session["systemDate"].ToString() + " " + Session["systemTime"].ToString());
                               
                                if (!dt.Columns.Contains("StartWithin")) dt.Columns.Add("StartWithin");
                                foreach (DataRow dr in dt.Rows)
                                {
                                    DateTime dtRec = DateTime.Parse(dr["ConferenceDateTime"].ToString());
                                    TimeSpan ts = UPDT.Subtract(dtRec);
                                    String startTime = myVRMNet.NETFunctions.GetFormattedTime(Convert.ToDateTime(dr["ConferenceDateTime"].ToString()).ToShortTimeString(), Session["timeFormat"].ToString());
                                    //Code added by Offshore for FB Issue 1073 -- Start
                                    dr["ConferenceDateTime"] = myVRMNet.NETFunctions.GetFormattedDate(dr["ConferenceDateTime"].ToString(), Session["FormatDateType"].ToString(), Session["EmailDateFormat"].ToString());
                                    //Code added by Offshore for FB Issue 1073 -- End
                                    //Code added for FB 1426
                                    dr["StartWithin"] = "";
                                    if (Math.Abs(ts.TotalMinutes) > 59)
                                        dr["StartWithin"] = dr["ConferenceDateTime"] + " " + startTime;
                                    else
                                    {
                                        //if (Math.Abs(ts.Days) > 0)
                                        //    dr["StartWithin"] += Math.Abs(ts.Days) + " day ";
                                        //if (Math.Abs(ts.Hours) > 0)
                                        //    dr["StartWithin"] += Math.Abs(ts.Hours) + " hr ";
                                        dr["StartWithin"] += Math.Abs(ts.Minutes) + " mins";//ZD 100528
                                        if (dtRec > UPDT)
                                            dr["StartWithin"] += " remaining";
                                        else
                                            dr["StartWithin"] += " ago";
                                    }
                                    //lblStartingIn
                                }
                                dgSearch.DataSource = dt;
                                dgSearch.DataBind();
                                foreach (DataGridItem dgi in dgSearch.Items)
                                {
                                    Label lblTemp = (Label)dgi.FindControl("lblStartingIn");
                                    if (lblTemp.Text.IndexOf(" ago") > 0)
                                        lblTemp.ForeColor = System.Drawing.Color.Red;
                                    if (lblTemp.Text.IndexOf(" remaining") > 0)
                                        lblTemp.ForeColor = System.Drawing.Color.Green;
                                }
                            }
                            else
                                lblNoSearch.Visible = true;
                        }
                    }
                    else
                    {
                        errLabel.Text += "GetsearchTemplate: " + obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                        lblNoSearch.Visible = true;
                    }
                }
                else
                    lblNoSearch.Visible = true;
                //Response.Write("<br>Search End: " + DateTime.Now.ToString("mm:ss"));
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
        }

        public void LoadAVWorkOrders()
        {
            try
            {
                //Response.Write("<br>AV WO Start: " + DateTime.Now.ToString("mm:ss"));
                String inXML = SearchConferenceWorkOrders(Session["userID"].ToString(), "", "", "", "", "", "", "", "", "1", "5", "1", "1");
                if (inXML.IndexOf("<error>") < 0)
                {
                    //string outxml = obj.CallMyVRMServer("SearchConferenceWorkOrders", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    string outxml = myvrmCom.Operations(Application["MyVRMServer_ConfigPath"].ToString(), "SearchConferenceWorkOrders", inXML);
                    //Response.Write(outxml.Replace("<", "&lt;").Replace(">", "&gt;"));
                    //Response.Write("<br>AV WO after command: " + DateTime.Now.ToString("mm:ss"));
                    if (outxml.IndexOf("<error>") >= 0)
                    {
                        errLabel.Visible = true;
                        //errLabel.Text += "LoadAVWorkOrders: " + obj.ShowErrorMessage(outxml);
                        errLabel.Text += "";
                    }
                    else
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outxml);

                        XmlNodeList nodes = xmldoc.SelectNodes("//WorkOrderList/WorkOrder");
                        XmlTextReader xtr;
                        DataSet ds = new DataSet();
                        //Response.Write(nodes.Count);
                        foreach (XmlNode node in nodes)
                        {
                            xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                            //if (node.SelectSingleNode("//WorkOrderList/WorkOrder/Type").InnerText.Equals("1"))
                            ds.ReadXml(xtr, XmlReadMode.InferSchema);
                        }
                        //Response.Write(ds.Tables[0].Rows.Count);
                        DataView dv;

                        if (ds.Tables.Count > 0)
                        {
                            dv = ds.Tables[0].DefaultView;
                            foreach (DataRow dr in dv.Table.Rows)
                            {
                                dr["Name"] = "<a href='EditConferenceOrder.aspx?id=" + dr["ConfID"].ToString() + "&t=1&woid=" + dr["ID"].ToString() + "'>" + dr["Name"].ToString() + "</a>";
                                //Code added by Offshore for FB Issue 1073 -- Start
                                dr["StartByDate"] = myVRMNet.NETFunctions.GetFormattedDate(dr["StartByDate"].ToString(), Session["FormatDateType"].ToString(), Session["EmailDateFormat"].ToString());//ZD 100995
                                //Code added by Offshore for FB Issue 1073 -- End
                                dr["StartByTime"] = myVRMNet.NETFunctions.GetFormattedTime(dr["StartByTime"].ToString(), Session["timeFormat"].ToString());
                            }
                            dv.RowFilter = "Type='1'";
                            //Response.Write(dv.Table.Rows.Count);
                            for (int i = dv.Table.Rows.Count - 1; i > 4; i--)
                                dv.Table.Rows.RemoveAt(i);
                            dgAVWO.Visible = true;
                            dgAVWO.DataSource = dv;
                            dgAVWO.DataBind();
                        }
                        else
                            lblNoAVWO.Visible = true;
                        if (dgAVWO.Items.Count >= 5)
                            dgAVWO.ShowFooter = true;
                        else
                            dgAVWO.ShowFooter = false;

                        if (dgAVWO.Items.Count.Equals(0))
                        {
                            lblNoAVWO.Visible = true;
                            dgAVWO.Visible = false;
                        }
                    }
                }
                //Response.Write("<br>AV WO End: " + DateTime.Now.ToString("mm:ss"));
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
        }
        public void LoadCATWorkOrders()
        {
            try
            {
                //Response.Write("<br>CAT WO Start: " + DateTime.Now.ToString("mm:ss"));
                String inXML = SearchConferenceWorkOrders(Session["userID"].ToString(), "", "", "", "", "", "", "", "", "2", "5", "1", "1");
                if (inXML.IndexOf("<error>") < 0)
                {
                    String outxml = myvrmCom.Operations(Application["MyVRMServer_ConfigPath"].ToString(), "SearchConferenceWorkOrders", inXML);
                    //Response.Write(obj.Transfer(outxml));
                    //Response.Write("<br>CAT WO After command: " + DateTime.Now.ToString("mm:ss"));
                    if (outxml.IndexOf("<error>") >= 0)
                    {
                        errLabel.Visible = true;
                        //errLabel.Text += "LoadCATWorkOrders: " + obj.ShowErrorMessage(outxml);
                        errLabel.Text += "";
                    }
                    else
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outxml);

                        XmlNodeList nodes = xmldoc.SelectNodes("/WorkOrderList/WorkOrder");
                        XmlTextReader xtr;
                        DataSet ds = new DataSet();

                        foreach (XmlNode node in nodes)
                        {
                            xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                            ds.ReadXml(xtr, XmlReadMode.InferSchema);
                        }
                        DataView dv;
                        DataTable dt;
                        //Response.Write(nodes.Count);
                        if (ds.Tables.Count > 0)
                        {
                            dv = new DataView(ds.Tables[0]);
                            dt = dv.Table;
                            foreach (DataRow dr in dt.Rows)
                            {
                                dr["Name"] = "<a href='EditConferenceOrder.aspx?id=" + dr["ConfID"].ToString() + "&t=2&woid=" + dr["ID"].ToString() + "'>" + dr["Name"].ToString() + "</a>";
                                //Code added by Offshore for FB Issue 1073 -- Start
                                dr["StartByDate"] = myVRMNet.NETFunctions.GetFormattedDate(dr["StartByDate"].ToString(), Session["FormatDateType"].ToString(), Session["EmailDateFormat"].ToString()); //ZD 100995
                                //Code added by Offshore for FB Issue 1073 -- End
                                dr["StartByTime"] = myVRMNet.NETFunctions.GetFormattedTime(dr["StartByTime"].ToString(), Session["timeFormat"].ToString());
                            }
                            dv = new DataView(dt);
                            dgCATWO.Visible = true;
                            dgCATWO.DataSource = dv;
                            dgCATWO.DataBind();
                        }
                        else
                        {
                            lblNoCATWO.Visible = true;
                        }

                        if (dgCATWO.Items.Count >= 5)
                            dgCATWO.ShowFooter = true;
                        else
                            dgCATWO.ShowFooter = false;
                    }
                }
                //Response.Write("<br>CAT WO End: " + DateTime.Now.ToString("mm:ss"));
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
        }
        public void LoadHKWorkOrders()
        {
            try
            {
                //Response.Write("<br>HKWO Start: " + DateTime.Now.ToString("mm:ss"));
                String inXML = SearchConferenceWorkOrders(Session["userID"].ToString(), "", "", "", "", "", "", "", "", "3", "5", "1", "1");
                if (inXML.IndexOf("<error>") < 0)
                {
                    string outxml = myvrmCom.Operations(Application["MyVRMServer_ConfigPath"].ToString(), "SearchConferenceWorkOrders", inXML);
                    //Response.Write("<br>HKWO After command: " + DateTime.Now.ToString("mm:ss"));
                    if (outxml.IndexOf("<error>") >= 0)
                    {
                        errLabel.Visible = true;
                        //errLabel.Text += "LoadHKWorkOrders: " + obj.ShowErrorMessage(outxml);
                        errLabel.Text += "";
                    }
                    else
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outxml);

                        XmlNodeList nodes = xmldoc.SelectNodes("/WorkOrderList/WorkOrder");
                        XmlTextReader xtr;
                        DataSet ds = new DataSet();

                        foreach (XmlNode node in nodes)
                        {
                            xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                            ds.ReadXml(xtr, XmlReadMode.InferSchema);
                        }
                        DataView dv;
                        DataTable dt;
                        //Response.Write(nodes.Count);
                        if (ds.Tables.Count > 0)
                        {
                            dv = new DataView(ds.Tables[0]);
                            dt = dv.Table;
                            foreach (DataRow dr in dt.Rows)
                            {
                                dr["Name"] = "<a href='EditConferenceOrder.aspx?id=" + dr["ConfID"].ToString() + "&t=3&woid=" + dr["ID"].ToString() + "'>" + dr["Name"].ToString() + "</a>";
                                //Code added by Offshore for FB Issue 1073 -- Start
                                dr["StartByDate"] = myVRMNet.NETFunctions.GetFormattedDate(dr["StartByDate"].ToString(), Session["FormatDateType"].ToString(), Session["EmailDateFormat"].ToString());//ZD 100995
                                //Code added by Offshore for FB Issue 1073 -- End
                                dr["StartByTime"] = myVRMNet.NETFunctions.GetFormattedTime(dr["StartByTime"].ToString(), Session["timeFormat"].ToString());
                                dv = new DataView(dt);
                                dgHKWO.Visible = true;
                                dgHKWO.DataSource = dv;
                                dgHKWO.DataBind();
                                if (dgHKWO.Items.Count >= 5)
                                    dgHKWO.ShowFooter = true;
                                else
                                    dgHKWO.ShowFooter = false;
                            }
                        }
                        else
                            lblNoHKWO.Visible = true;
                    }
                }
                //Response.Write("<br>HKWO End: " + DateTime.Now.ToString("mm:ss"));

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
        }
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //this.Load += new System.EventHandler(this.Page_Load);
        }

        protected void ViewMyPendingAVWorkOrders(Object sender, EventArgs e)
        {
            Response.Redirect("ConferenceOrders.aspx?t=1&view=3");
        }

        protected void ViewMyPendingCATWorkOrders(Object sender, EventArgs e)
        {
            Response.Redirect("ConferenceOrders.aspx?t=2&view=3");
        }
        protected void ViewMyPendingHKWorkOrders(Object sender, EventArgs e)
        {
            Response.Redirect("ConferenceOrders.aspx?t=3&view=3");
        }
        protected void ViewMyTemplates(Object sender, EventArgs e)
        {
            //Response.Redirect("dispatcher/conferencedispatcher.asp?cmd=GetTemplateList&frm=manage&sb=1");
            Response.Redirect("ManageTemplate.aspx");
        }
        protected void GetSearchConference(Object sender, EventArgs e)
        {
            Session.Add("SearchType", "1"); //1 means Search
            Response.Redirect("ConferenceList.aspx?t=1");
        }
        protected void ManageConference(Object sender, DataGridCommandEventArgs e)
        {
            Session.Add("confID", e.Item.Cells[0].Text);
            Response.Redirect("ManageConference.aspx?t=");
        }
        protected void GetAlerts()
        {
            try
            {
                //Response.Write("<br>Alerts Start: " + DateTime.Now.ToString("mm:ss"));
                Session.Add("AlertsA", "0");
                Session.Add("AlertsP", "0");
                int countAlert = 0;
                String inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID></login>";//Organization Module Fixes
                web_com_v18_Net.Com com = new web_com_v18_Net.ComClass();
                //String outXML = com.comCentral("GetApproveConference", inXML, Application["COM_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("GetApproveConference", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                //String outXML = com.CallCOM("GetApproveConference", inXML, Application["COM_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(outXML));
                //Response.Write("<br>Alerts After command: " + DateTime.Now.ToString("mm:ss"));
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                if (xmldoc.SelectNodes("//approveConference/conferences/conference").Count > 0)
                {
                    countAlert++;
                    Session["AlertsA"] = xmldoc.SelectNodes("//approveConference/conferences/conference").Count;
                }
                inXML = objInXML.SearchConference(Session["userID"].ToString(), "1", "", "", "", "", "", "", "", "", "", "", "1", "", "1", "3", "1", "", "", "", "0", "", false, ns_MyVRMNet.vrmSearchFilterType.ApprovalPending,"");//Custom Attribute Fix //FB 2632//FB 2694 //FB 2822 //FB 2870 //FB 3006 //FB 2639 - Search
                //Response.Write("<br>Alerts SearchConference1: " + DateTime.Now.ToString("mm:ss"));
                //outXML = obj.CallMyVRMServer("SearchConference", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                outXML = myvrmCom.Operations(Application["MyVRMServer_ConfigPath"].ToString(), "SearchConference", inXML);
                //Response.Write(obj.Transfer(inXML));
                //Response.Write("<br>Alerts SearchConference2: " + DateTime.Now.ToString("mm:ss"));
                xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                if (xmldoc.SelectNodes("//SearchConference/Conferences/Conference").Count > 0)
                {
                    countAlert++;
                    Session["AlertsP"] = xmldoc.SelectSingleNode("//SearchConference/TotalRecords").InnerText.ToString();
                }
                //Response.Write(Session["AlertsA"].ToString() + " : " + Session["AlertsP"].ToString());
                txtLAlerts.Value = countAlert.ToString();
                Session.Add("Alerts", txtLAlerts.Value);
                //Response.Write("<br>Alerts End: " + DateTime.Now.ToString("mm:ss"));
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
        }
        #endregion
        public String SearchConferenceWorkOrders(String UserID, String ConfID, String WOName, String Rooms, String DateFrom, String DateTo, String TimeFrom, String TimeTo, String Status, String Tpe, String MaxRecords, String PageEnable, String PageNo)
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <user>";
                inXML += "      <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  </user>";
                inXML += "  <userID>" + UserID + "</userID>";
                inXML += "  <ConfID>" + ConfID + "</ConfID>";
                inXML += "  <Name>" + WOName + "</Name>";
                inXML += "  <Rooms>";
                if (Rooms.IndexOf(",") >= 0)
                    for (int i = 0; i < Rooms.Split(',').Length; i++)
                        if (!Rooms.Split(',')[i].Trim().Equals(""))
                            inXML += "      <RoomID>" + Rooms.Split(',')[i] + "</RoomID>";
                inXML += "  </Rooms>";
                inXML += "  <DateFrom>" + DateFrom + "</DateFrom>";
                inXML += "  <DateTo>" + DateTo + "</DateTo>";
                inXML += "  <TimeFrom>" + TimeFrom + "</TimeFrom>";
                inXML += "  <TimeTo>" + TimeTo + "</TimeTo>";
                inXML += "  <Status>" + Status + "</Status>";
                inXML += "  <Type>" + Tpe + "</Type>";
                inXML += "  <MaxRecords>" + MaxRecords + "</MaxRecords>";
                inXML += "  <pageEnable>" + PageEnable + "</pageEnable>";
                inXML += "  <pageNo>" + PageNo + "</pageNo>";
                inXML += "  <SortBy>3</SortBy>";
                inXML += "</login>";
                return inXML;
            }
            catch (Exception ex)
            {
                log.Trace("error in SearchConferenceWorkOrder: " + ex.Message);
                return obj.ShowSystemMessage();//FB 1881
                //return "<error>error in SearchConferenceWorkOrder</error>";
            }
        }

        protected void CreateNewWorkorder(Object sender, CommandEventArgs e)
        {
            try
            {
                switch (e.CommandArgument.ToString())
                {
                    case "1":
                        Response.Redirect("EditConferenceOrder.aspx?id=ph&t=1");
                        break;
                    case "2":
                        Response.Redirect("EditConferenceOrder.aspx?id=ph&t=2");
                        break;
                    case "3":
                        Response.Redirect("EditConferenceOrder.aspx?id=ph&t=3");
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
        }

        //FB 1860
        #region Get Mail Block Status
        /// <summary>
        /// //Organization Module Fixes
        /// </summary>
        public void GetMailBlockStatus()
        {
            String inXML = "";
            String emailBlock = "";
            String blockdt = "";
            String blockLvl = "";
            DateTime blckdDate = DateTime.Now;
            try
            {
                inXML = "<login>" + obj.OrgXMLElement() + "<user><userID>" + Session["userID"].ToString() + "</userID></user></login>";
                emailBlock = obj.CallCommand("GetEmailsBlockStatus", inXML);
                if (emailBlock.IndexOf("<error>") < 0)
                {
                    XmlDocument blck = new XmlDocument();
                    blck.LoadXml(emailBlock);

                    XmlNode node = blck.SelectSingleNode("//login/emailBlockStatus");

                    if (node != null)
                    {
                        if (node.InnerText == "1")
                        {
                            XmlNode nodeDetail = blck.SelectSingleNode("//login/emailBlockDate");
                            if (nodeDetail != null)
                                blockdt = nodeDetail.InnerText;

                            DateTime.TryParse(blockdt, out blckdDate);
                            
                            nodeDetail = blck.SelectSingleNode("//login/Level");
                            if (nodeDetail != null)
                                blockLvl = nodeDetail.InnerText;

                            //FB 1830 - Translation - Starts
                            //ZD 100995 start
                            String[] Date;
                            String Expirydate = myVRMNet.NETFunctions.GetFormattedDate(blckdDate.ToShortDateString(), Session["FormatDateType"].ToString(), Session["EmailDateFormat"].ToString());
                            if (Session["EmailDateFormat"].ToString() == "1")
                            {
                               string  Expiry = myVRMNet.NETFunctions.GetFormattedDate(blckdDate.ToShortDateString(), Session["FormatDateType"].ToString(), Session["EmailDateFormat"].ToString());
                                Date = (Expirydate.ToString()).Split(' ');
                                Expirydate = Date[0] + " " + obj.GetTranslatedText(Date[1]) + " " + Date[2];
                            }
                            if (blockLvl == "Your Email")
                                errLabel.Text = usrMsg + " " + Expirydate + ". " + emailMsg; //ZD 100995
                            else if (blockLvl == "Organization Email")
                                errLabel.Text = orgMsg + " " + Expirydate + ". " + emailMsg; //ZD 100995
                            //FB 2272 - End
                            //ZD 100995 End
                            
                            errLabel.Visible = true;

                            //errLabel.Text = blockLvl + " has been blocked since " + myVRMNet.NETFunctions.GetFormattedDate(blckdDate.ToShortDateString(), Session["FormatDateType"].ToString()) + Error3;
                            //errLabel.Visible = true;
                            //FB 1830 - Translation - End
                        }
                    }


                }


            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("GetMailBlockStatus:" + ex.Message);//ZD 100263
                errLabel.Visible = true;

            }

        }
        #endregion
        //FB 1860

        

    }
}

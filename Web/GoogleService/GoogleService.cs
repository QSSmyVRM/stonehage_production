﻿//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace GoogleService
{
    public partial class GoogleService : ServiceBase
    {
        System.Timers.Timer _GoogleChannelTimer = new System.Timers.Timer();
        public GoogleService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            double _GoogleChannelInterval = 30000;
            try
            {

                GoogleWatchChannel GoogleWatchChannel = new GoogleWatchChannel();
                GoogleWatchChannel.GoogleChannelupdate();
                _GoogleChannelTimer.Elapsed += new System.Timers.ElapsedEventHandler(GoogleChannelInterval_Elapsed);
                _GoogleChannelTimer.Interval = _GoogleChannelInterval;
                _GoogleChannelTimer.AutoReset = true;
                _GoogleChannelTimer.Start();

            }
            catch (Exception)
            {

                throw;
            }
        }

        protected override void OnStop()
        {
            _GoogleChannelTimer.Enabled = false;
            _GoogleChannelTimer.AutoReset = false;
            _GoogleChannelTimer.Stop();
        }
        #region GoogleChannelInterval_Elapsed
        /// <summary>
        /// GoogleChannelInterval_Elapsed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GoogleChannelInterval_Elapsed(object sender, EventArgs e)
        {
            double GoogleChannelInterval = 1 * 60 * 1000;
            try
            {
                _GoogleChannelTimer.Stop();
                GoogleWatchChannel GoogleWatchChannel = new GoogleWatchChannel();
                GoogleWatchChannel.GoogleChannelupdate();

                _GoogleChannelTimer.Interval = GoogleChannelInterval;
                _GoogleChannelTimer.AutoReset = true;
                _GoogleChannelTimer.Start();

            }
            catch (Exception)
            {
            }


        }
        #endregion
    }
}
